var dir_5c65f22401cea4e8a6a62e72268bef53 =
[
    [ "GameNetworkManager.cs", "_game_network_manager_8cs.html", "_game_network_manager_8cs" ],
    [ "GameRoomTemp.cs", "_game_room_temp_8cs.html", [
      [ "GameRoomTemp", "class_game_room_temp.html", null ]
    ] ],
    [ "Instruction.cs", "_instruction_8cs.html", [
      [ "Instruction", "class_instruction.html", "class_instruction" ]
    ] ],
    [ "LobbyPhotonConnector.cs", "_lobby_photon_connector_8cs.html", [
      [ "LobbyPhotonConnector", "class_lobby_photon_connector.html", "class_lobby_photon_connector" ]
    ] ],
    [ "NetworkStream.cs", "_network_stream_8cs.html", [
      [ "NetworkStream", "class_network_stream.html", "class_network_stream" ]
    ] ],
    [ "NewPhotonLobby.cs", "_new_photon_lobby_8cs.html", [
      [ "NewPhotonLobby", "class_new_photon_lobby.html", null ]
    ] ],
    [ "PhotonNetworkManager.cs", "_photon_network_manager_8cs.html", [
      [ "PhotonNetworkManager", "class_photon_network_manager.html", "class_photon_network_manager" ]
    ] ]
];