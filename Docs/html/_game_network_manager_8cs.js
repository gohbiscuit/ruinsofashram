var _game_network_manager_8cs =
[
    [ "GameNetworkManager", "class_game_network_manager.html", "class_game_network_manager" ],
    [ "GameMode", "_game_network_manager_8cs.html#aaf5ef5a17b53e9997c837b07015589de", [
      [ "DEATHMATCH", "_game_network_manager_8cs.html#aaf5ef5a17b53e9997c837b07015589deaad84dfbe48a514e48c1c82a8d89ee234", null ],
      [ "TEAM_DEATHMATCH", "_game_network_manager_8cs.html#aaf5ef5a17b53e9997c837b07015589dea82254a08e40c5caa795410d20b0809af", null ]
    ] ],
    [ "InstructionType", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0d", [
      [ "STOP", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da615a46af313786fc4e349f34118be111", null ],
      [ "MOVE_TO_POSITION", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0dac530888009f719914bf1370e0b0cc33d", null ],
      [ "MOVE_TO_CREATURE", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0dad240193b61fcfd3e89240261197ea56b", null ],
      [ "USE_SKILL_AT_CREATURE", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da40e1bc4bd371f6f7ea1cd225e297c7ad", null ],
      [ "USE_SKILL_AT_POSITION", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0daf3c8136e21e00f11d9ca31aea55d9ca6", null ],
      [ "TAKE_DAMAGE", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0dac9f04745654a39fd9448c6ed30cab3fe", null ],
      [ "DIE", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da83ebd7c89f4cc1a85396fba7d6f7e900", null ],
      [ "GAIN_HEALTH", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da573316c54ad32be3de02d2b6c5bb2efe", null ],
      [ "POSSESS", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da4557b7a738f480c0c7fc152fc2478a86", null ],
      [ "RELEASE", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da7d649ef069df9885e382417c79f3d5cd", null ],
      [ "CREATE_PLAYER_WISP", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0dae54e3506c56481f6ffff054be4dd48dd", null ],
      [ "CREATE_AI", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da69b9e6ccf9297d190188869e7caaff94", null ],
      [ "RECOMPUTE_STATS", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da76bac2398978c737fc67901102e87ef2", null ],
      [ "AFFLICT", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da8a6530947044fdc10da3ac3be56bc1d1", null ],
      [ "AFFLICT_RECOVER", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0dac8d4c5ed29863de05251e1ce19045d5e", null ],
      [ "CORRUPTED", "_game_network_manager_8cs.html#a89ee1c4e5e5a33c60d0e6dc126ed2c0da079c6dcd1b1ee137ecf0244d4f8bad01", null ]
    ] ],
    [ "Team", "_game_network_manager_8cs.html#a9c13bb5b1d69698f9b47900990eaa598", [
      [ "NEUTRAL", "_game_network_manager_8cs.html#a9c13bb5b1d69698f9b47900990eaa598a31ba17aa58cdb681423f07ca21a6efc7", null ],
      [ "ONE", "_game_network_manager_8cs.html#a9c13bb5b1d69698f9b47900990eaa598abc21e6484530fc9d0313cb816b733396", null ],
      [ "TWO", "_game_network_manager_8cs.html#a9c13bb5b1d69698f9b47900990eaa598a0f82d86afa0f5dc965c5c15aca58dcfb", null ],
      [ "ON_YOUR_OWN", "_game_network_manager_8cs.html#a9c13bb5b1d69698f9b47900990eaa598a649d6a55c073fb70fd421f94c0ada0a3", null ]
    ] ]
];