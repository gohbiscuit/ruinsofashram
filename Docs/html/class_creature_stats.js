var class_creature_stats =
[
    [ "Curr_HP", "class_creature_stats.html#a858908b7b9813ece8593555140dba3b9", null ],
    [ "Curr_MP", "class_creature_stats.html#a465c585486e4cc9ceae225a5f684baaf", null ],
    [ "EVA", "class_creature_stats.html#a670508f8ed9d8bded3a39d90d58206e5", null ],
    [ "EXP_GIVEN", "class_creature_stats.html#a7545d814d508915a28fb640b2e18577c", null ],
    [ "HP", "class_creature_stats.html#afdd32ef3317d6218dbfe7bcd406235a4", null ],
    [ "HP_REGEN", "class_creature_stats.html#afd2931a1e8ced7c840cca585d7518f0c", null ],
    [ "MAG_ATK", "class_creature_stats.html#abc60dd6da2fe30c810b15eb658e75e3c", null ],
    [ "MAG_DEF", "class_creature_stats.html#a83f36012649547095df09eed7fd9a595", null ],
    [ "MOV", "class_creature_stats.html#a0962d80ed3eb37282835edbce4dee12e", null ],
    [ "MP", "class_creature_stats.html#a4e8d5cae646039ee4ee27d4d5d846c2b", null ],
    [ "MP_REGEN", "class_creature_stats.html#ab6a4b4613f428603a1d0aba3156dc2ca", null ],
    [ "PHY_ATK", "class_creature_stats.html#a2d035b8c7680d3e9719a57781be82684", null ],
    [ "PHY_DEF", "class_creature_stats.html#aa1503ea746a33f9878aed2a0da9904b4", null ],
    [ "SKI", "class_creature_stats.html#a36906106247b00bcab04f95395c585e2", null ]
];