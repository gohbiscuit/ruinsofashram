var class_skill_button =
[
    [ "IsOnCooldown", "class_skill_button.html#a88fb5c0c15a19a604f6d258cbc4c63ee", null ],
    [ "SetCooldown", "class_skill_button.html#ac70045e5ef4248b96e7ece3eee0ae568", null ],
    [ "SetCurrentCooldownTime", "class_skill_button.html#ab67be46763596f97fc41141d50b89054", null ],
    [ "SetSkillText", "class_skill_button.html#aa6ca01252368d98557f601612b985bdd", null ],
    [ "UpdateCoolDown", "class_skill_button.html#a94daba661ca12b413eb1084be5a96e31", null ],
    [ "UpdateManaDisplay", "class_skill_button.html#a24bb46c53d84249707579be5f44450c2", null ],
    [ "coolTimeMax", "class_skill_button.html#ade5d74f99b1f31026a5f96b987887157", null ]
];