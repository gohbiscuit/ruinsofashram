var interface_creature_interface =
[
    [ "canUseSkill", "interface_creature_interface.html#ad301d5c0230604354a27906e5abecfae", null ],
    [ "canUseSkill", "interface_creature_interface.html#ac56afc5f9607f067b921ef362dab5113", null ],
    [ "getSkillList", "interface_creature_interface.html#aa98f8c7338a121be7801f601bd8311dd", null ],
    [ "hasStatusEffectLater", "interface_creature_interface.html#ac417232423ee6df9a6df8e2de781f217", null ],
    [ "isStillUsingSkillLater", "interface_creature_interface.html#a2608137670d9cad5ac560b3c0c24368a", null ],
    [ "MoveLater", "interface_creature_interface.html#a9c7c9d34f3554c294fe74c1c407a2786", null ],
    [ "StopLater", "interface_creature_interface.html#a004131b86b580283855caec73c1a85c6", null ],
    [ "UseSkillLater", "interface_creature_interface.html#a1f55e2cb10845c7fba13883cfb6957c5", null ],
    [ "CClass", "interface_creature_interface.html#aa2dba6229d01b4cbf2badebc2dd8ce06", null ],
    [ "CRace", "interface_creature_interface.html#a81c90aa3dbddce8c551505f9bf5feb18", null ],
    [ "GameObjectProperty", "interface_creature_interface.html#a7049f19320617bef249006d605718278", null ],
    [ "HP", "interface_creature_interface.html#a616a58ae144c0224ddc5da8316f08b99", null ],
    [ "LastCreatureToHitMe", "interface_creature_interface.html#a479304806b959126966971d4b9877fb2", null ],
    [ "MaxHP", "interface_creature_interface.html#aa0e8bc051ae172c2cc719df5b589b7f2", null ],
    [ "MaxMP", "interface_creature_interface.html#a69b1af27b6813d2a84973f2cc128c441", null ],
    [ "MP", "interface_creature_interface.html#a020c7dd4151dbc7cc5898954b04b435f", null ],
    [ "SpawnPoint", "interface_creature_interface.html#ae6154f0818c929bb19e525aab9199d36", null ],
    [ "TransformProperty", "interface_creature_interface.html#a96dbcd48fa7921c5af4ccaf5e1c5bafa", null ]
];