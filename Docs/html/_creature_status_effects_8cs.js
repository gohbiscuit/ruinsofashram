var _creature_status_effects_8cs =
[
    [ "CreatureStatusEffects", "class_creature_status_effects.html", "class_creature_status_effects" ],
    [ "StatusEffect", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93", [
      [ "STUN", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93a3a361d79bfa3d4ad1ee1d3d453d087d1", null ],
      [ "SLOW", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93a0e3066cbbd284dce8b76e7c4620d6d75", null ],
      [ "BATTLE_ROAR", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93a103f450c42adb30dbbb0d24fce29327a", null ],
      [ "MANA_SHIELD", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93ae9acec71a3729b89a64cc7562959eb26", null ],
      [ "ORGE_AURA", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93aba4de30c6cc91f03b18dd74526629ee9", null ],
      [ "SHIELD_BARRIER", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93abd68fe5f923e9e4bbbd7660173cbe569", null ],
      [ "FREEZING_SHIELD", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93a5f52dbc79881da19108176d4c3ea4a31", null ],
      [ "EARTH_SHIELD", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93af9bf5f3cbe96001c0eb889f6eca63acb", null ],
      [ "RATKIN_EYE", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93ab2d3278e30f852d2b57791aa04d8d69c", null ],
      [ "ASSASSIN_LOCK", "_creature_status_effects_8cs.html#ab8367a12576406bda6900cf54bafaa93a412921257ae8ca2afed03805039049ea", null ]
    ] ]
];