var class_skill_test =
[
    [ "HighCharge", "class_skill_test.html#adb3c0a389e149ddb6e18b06d1c3898cd", null ],
    [ "HighCooldown", "class_skill_test.html#a7f73718d1e2b55f9136a0f7df94fafdb", null ],
    [ "Init", "class_skill_test.html#a7e00fbcf4c73d17306e03fcf4707b970", null ],
    [ "InvalidCharge", "class_skill_test.html#a6b068e8d54efba1339646387e944de7d", null ],
    [ "InvalidCooldown", "class_skill_test.html#a64655e8382d5af637f953a1699f432e6", null ],
    [ "NormalCharge", "class_skill_test.html#aca874123d6e3ac3226a16c5db3692e7e", null ],
    [ "NormalCooldown", "class_skill_test.html#a8a8e0bbbadf40efe00b6074c43d0618e", null ],
    [ "PositiveChargeTime", "class_skill_test.html#ad25b34a9bfd34eb59788f1f863f67791", null ],
    [ "PositiveChargeTimeReduction", "class_skill_test.html#a8eea0539eec2f59ec72143a8cb834bc7", null ],
    [ "PositiveCooldown", "class_skill_test.html#aa0fd51446d746b297f48b0add9618905", null ],
    [ "PositiveCooldownReduction", "class_skill_test.html#afd7f3c18e2f49d6c3b69021fc3893cc0", null ],
    [ "PositiveMaxChargeTime", "class_skill_test.html#ad9bc5cda7ba90cd38ad36c6e6c1a9188", null ],
    [ "PositiveMaxCooldownTime", "class_skill_test.html#af179460e28f4d48c7b68b1d8bcd4495e", null ],
    [ "PositiveMPCost", "class_skill_test.html#af62d3ba808e4156eae2dd53538020e20", null ],
    [ "PositiveRange", "class_skill_test.html#a8eb5fa5784630cdaf08795cb45a23778", null ],
    [ "PositiveRangeIncrease", "class_skill_test.html#abd704bd1653ce86bded4bb7d57312119", null ],
    [ "StartCharge", "class_skill_test.html#a00915982d7840ab765093efc5e869108", null ]
];