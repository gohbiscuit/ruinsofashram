var searchData=
[
  ['cclass',['CClass',['../class_creature.html#a014ad4f3800b36bec994d0b40bd3eb11',1,'Creature.CClass()'],['../interface_creature_interface.html#aa2dba6229d01b4cbf2badebc2dd8ce06',1,'CreatureInterface.CClass()']]],
  ['continuebasicattack',['ContinueBasicAttack',['../class_creature.html#a544118e2a50786094977338ba84a42ea',1,'Creature']]],
  ['cphotonview',['CPhotonView',['../class_creature.html#af1f3c5a9233c85c49de79327a0b5a4f1',1,'Creature']]],
  ['crace',['CRace',['../class_creature.html#af460ff8556ec954a4872e7962ca25943',1,'Creature.CRace()'],['../interface_creature_interface.html#a81c90aa3dbddce8c551505f9bf5feb18',1,'CreatureInterface.CRace()']]],
  ['curr_5fhp',['Curr_HP',['../class_creature_stats.html#a858908b7b9813ece8593555140dba3b9',1,'CreatureStats']]],
  ['curr_5fmp',['Curr_MP',['../class_creature_stats.html#a465c585486e4cc9ceae225a5f684baaf',1,'CreatureStats']]],
  ['current_5fexp',['CURRENT_EXP',['../class_player_attributes.html#a1089a44876d4ea976f467a1423faa31e',1,'PlayerAttributes']]],
  ['current_5flevel',['CURRENT_LEVEL',['../class_player_attributes.html#a4f2337338232edfff04a2e9a3f7a4bf7',1,'PlayerAttributes']]],
  ['currentround',['CurrentRound',['../class_game_network_manager.html#aa8904d3e6018c975bf15587a866fedcd',1,'GameNetworkManager']]],
  ['currentscene',['CurrentScene',['../class_game_network_manager.html#a4afb9af2450d588d13f9211520601a79',1,'GameNetworkManager']]]
];
