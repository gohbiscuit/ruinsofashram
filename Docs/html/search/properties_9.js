var searchData=
[
  ['mag_5fatk',['MAG_ATK',['../class_creature_stats.html#abc60dd6da2fe30c810b15eb658e75e3c',1,'CreatureStats']]],
  ['mag_5fdef',['MAG_DEF',['../class_creature_stats.html#a83f36012649547095df09eed7fd9a595',1,'CreatureStats']]],
  ['max_5fexp',['MAX_EXP',['../class_player_attributes.html#a6539175afe4b355a76bdf7c931c0b58c',1,'PlayerAttributes']]],
  ['maxhp',['MaxHP',['../class_creature.html#a2e3e2fa2db4c5f7d7d29d7313bf33fa7',1,'Creature.MaxHP()'],['../interface_creature_interface.html#aa0e8bc051ae172c2cc719df5b589b7f2',1,'CreatureInterface.MaxHP()']]],
  ['maxmp',['MaxMP',['../class_creature.html#abc57f7080cbb1d5970beb47eb488ec23',1,'Creature.MaxMP()'],['../interface_creature_interface.html#a69b1af27b6813d2a84973f2cc128c441',1,'CreatureInterface.MaxMP()']]],
  ['mov',['MOV',['../class_creature_stats.html#a0962d80ed3eb37282835edbce4dee12e',1,'CreatureStats']]],
  ['mp',['MP',['../class_creature.html#a50594e3eb8fddb075ab30beac380e8d1',1,'Creature.MP()'],['../interface_creature_interface.html#a020c7dd4151dbc7cc5898954b04b435f',1,'CreatureInterface.MP()'],['../class_creature_stats.html#a4e8d5cae646039ee4ee27d4d5d846c2b',1,'CreatureStats.MP()']]],
  ['mp_5fregen',['MP_REGEN',['../class_creature_stats.html#ab6a4b4613f428603a1d0aba3156dc2ca',1,'CreatureStats']]]
];
