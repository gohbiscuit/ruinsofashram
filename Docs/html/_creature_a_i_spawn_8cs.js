var _creature_a_i_spawn_8cs =
[
    [ "CreatureAISpawn", "class_creature_a_i_spawn.html", "class_creature_a_i_spawn" ],
    [ "SpawnIconType", "_creature_a_i_spawn_8cs.html#ad1f5067b6e85c92d1209ca3620b58399", [
      [ "NONE", "_creature_a_i_spawn_8cs.html#ad1f5067b6e85c92d1209ca3620b58399ab50339a10e1de285ac99d4c3990b8693", null ],
      [ "AGI", "_creature_a_i_spawn_8cs.html#ad1f5067b6e85c92d1209ca3620b58399afc6fdf11b83b8999f99991fbaef45757", null ],
      [ "INT", "_creature_a_i_spawn_8cs.html#ad1f5067b6e85c92d1209ca3620b58399a53f93baa3057821107c750323892fa92", null ],
      [ "STR", "_creature_a_i_spawn_8cs.html#ad1f5067b6e85c92d1209ca3620b58399a3fe0dfff438296bb525e0e8642586c2d", null ]
    ] ]
];