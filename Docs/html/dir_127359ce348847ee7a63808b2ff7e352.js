var dir_127359ce348847ee7a63808b2ff7e352 =
[
    [ "AudioManagerTest.cs", "_audio_manager_test_8cs.html", [
      [ "AudioManagerTest", "class_audio_manager_test.html", "class_audio_manager_test" ]
    ] ],
    [ "CreatureStatsCalculatorTest.cs", "_creature_stats_calculator_test_8cs.html", [
      [ "CreatureStatsCalculatorTest", "class_creature_stats_calculator_test.html", "class_creature_stats_calculator_test" ]
    ] ],
    [ "CreatureStatusEffectsTest.cs", "_creature_status_effects_test_8cs.html", [
      [ "CreatureStatusEffectsTest", "class_creature_status_effects_test.html", "class_creature_status_effects_test" ]
    ] ],
    [ "InstructionTest.cs", "_instruction_test_8cs.html", [
      [ "InstructionTest", "class_instruction_test.html", "class_instruction_test" ]
    ] ],
    [ "SkillFactoryTest.cs", "_skill_factory_test_8cs.html", [
      [ "SkillFactoryTest", "class_skill_factory_test.html", "class_skill_factory_test" ]
    ] ],
    [ "SkillTest.cs", "_skill_test_8cs.html", [
      [ "SkillTest", "class_skill_test.html", "class_skill_test" ]
    ] ]
];