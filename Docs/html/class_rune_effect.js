var class_rune_effect =
[
    [ "RuneEffect", "class_rune_effect.html#a4032c9cb9bca5994444cf8afd1950f57", null ],
    [ "AfflictRuneEffect", "class_rune_effect.html#a3bb138003729dde87068f2885cc94008", null ],
    [ "DamageEffect", "class_rune_effect.html#a33fe57c8361418e338ceb7c1b742a263", null ],
    [ "DamageRecover", "class_rune_effect.html#ac76bc50e4516bf57f8d03b1ff5d7c517", null ],
    [ "HasDamageEffect", "class_rune_effect.html#a976684ca8386fd48a6247292685732a8", null ],
    [ "HasHasteEffect", "class_rune_effect.html#a4a10a0e93d2767db0e1035a63ebe4090", null ],
    [ "HasInvisEffect", "class_rune_effect.html#a203d2cbb3fcb86ce1322780ebcd95638", null ],
    [ "HasRegenEffect", "class_rune_effect.html#aaf3542b44b1f8e4f5cabc5d7bad7f820", null ],
    [ "HasteEffect", "class_rune_effect.html#a12e4c60fc6a581456aee40b63f29905c", null ],
    [ "HasteRecover", "class_rune_effect.html#a34e6a953e80cd365eda0758ab3c27035", null ],
    [ "InvisEffect", "class_rune_effect.html#a7bed38919486afeaecd9b7d42d8e33b8", null ],
    [ "InvisRecover", "class_rune_effect.html#a74067eb3806d6787204d95861d0eb883", null ],
    [ "RegenEffect", "class_rune_effect.html#abddbf5d755c67b940d9f8e657e0a60c9", null ],
    [ "RegenRecover", "class_rune_effect.html#a3190c8f7f7c4ca4e11f3b9cad1966ea8", null ],
    [ "RemoveRuneEffect", "class_rune_effect.html#abeeb5cf8c5065dff059af22d26e459e9", null ],
    [ "UpdateRuneEffect", "class_rune_effect.html#a170c1b8e0652d005444ec0fe41d625c3", null ]
];