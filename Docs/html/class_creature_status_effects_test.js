var class_creature_status_effects_test =
[
    [ "AssassinLockAfflictionAndRecovery", "class_creature_status_effects_test.html#aa4b09bb94c9f7ade78316ad5c5d19f6e", null ],
    [ "BattleRoarAfflictionAndRecovery", "class_creature_status_effects_test.html#a281d6f4d3467f2d733c245dd69624ea9", null ],
    [ "EarthShieldAfflictionAndRecovery", "class_creature_status_effects_test.html#aa37ea9e018d6a3cf0e040c919821989f", null ],
    [ "FreezingShieldAfflictionAndRecovery", "class_creature_status_effects_test.html#a19055cac08c24d74443575c009965b52", null ],
    [ "Init", "class_creature_status_effects_test.html#a297848dbe2d53b4fd8d1187dac7d5eb5", null ],
    [ "ManaShieldAfflictionAndRecovery", "class_creature_status_effects_test.html#a41bcc3c3c77298c9c0410e0215cda4e1", null ],
    [ "RatkinEyeAfflictionAndRecovery", "class_creature_status_effects_test.html#ac5d300c3d9a73bddc33c79e47d150f12", null ],
    [ "ShieldBarrierAfflictionAndRecovery", "class_creature_status_effects_test.html#af8aa5df080ddc24de248bf3afddfa9b4", null ],
    [ "SlowAfflictionAndRecovery", "class_creature_status_effects_test.html#a5710f20b2470600fc01a296267f8639b", null ]
];