var _custom_cursor_8cs =
[
    [ "CustomCursor", "class_custom_cursor.html", "class_custom_cursor" ],
    [ "CURSOR_STATE", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0", [
      [ "TARGET_ENEMY", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a425d733d74262c7bbf81cefaf3e5d2fa", null ],
      [ "DEFAULT", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a5b39c8b553c821e7cddc6da64b5bd2ee", null ],
      [ "USE_SKILL_POSSESSRELEASE", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a2fc2fdbe6fc2e23f6d95230adad51065", null ],
      [ "USE_SKILL_1", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a7dd1ba664c85f72ad14cf0c0f99cd1a3", null ],
      [ "USE_SKILL_2", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0aadf0d4ed22e128266e44fc96d787c6aa", null ],
      [ "USE_SKILL_3", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a3241377470edff482d19df11c756b454", null ],
      [ "TARGET_ALLY", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a22c44fdfef5b6b3c229b6a42eed63da2", null ],
      [ "CANNOT_ATTACK", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a3c9d56897a6179ae9136478d7c16660f", null ],
      [ "HOVER_ENEMY", "_custom_cursor_8cs.html#ae8ec8ee0712d85f1f83386d7e6d44ca0a2aa7dd161180a06f6dc55966bfd6011d", null ]
    ] ]
];