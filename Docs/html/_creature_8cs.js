var _creature_8cs =
[
    [ "Creature", "class_creature.html", "class_creature" ],
    [ "CreatureClass", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09", [
      [ "WISP", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09a2e556889cc7f80bf832aea97595a2919", null ],
      [ "WARRIOR", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09abb26dae038073a33dd85044c31e6be5d", null ],
      [ "ARCHER", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09aed766ae19299df52b8b375e370ba0ec2", null ],
      [ "ROGUE", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09a416f104f4dfca42fccfd1d6e7534d3ad", null ],
      [ "MAGE", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09ae9af6985dd404f6c53989203890211f3", null ],
      [ "WARLOCK", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09aad5ba70fffecdb7061db11d17442d535", null ],
      [ "PRIEST", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09af97b352f735dd1cb723e4606f0782532", null ],
      [ "PALADIN", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09aeb99aa003dc077a0c4d09de6ac7e694d", null ],
      [ "SHAMAN", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09a8355e09d373a99830d71d4ef75bc2edb", null ],
      [ "GUNNER", "_creature_8cs.html#a85fa6c85db2609cd5a49429fc2f61b09a6c5b84407641bef00e3e61f56e8119c2", null ]
    ] ],
    [ "CreatureCommands", "_creature_8cs.html#ac096a1f8c63eb9ad5b11732c19d3e562", [
      [ "NONE", "_creature_8cs.html#ac096a1f8c63eb9ad5b11732c19d3e562ab50339a10e1de285ac99d4c3990b8693", null ],
      [ "STOP", "_creature_8cs.html#ac096a1f8c63eb9ad5b11732c19d3e562a615a46af313786fc4e349f34118be111", null ],
      [ "MOVE", "_creature_8cs.html#ac096a1f8c63eb9ad5b11732c19d3e562af7f93635f8e193a924ae4a691bb66b8f", null ],
      [ "USESKILL", "_creature_8cs.html#ac096a1f8c63eb9ad5b11732c19d3e562af3dbba5d06fb80c0456c084bf31dc8db", null ]
    ] ],
    [ "CreatureRace", "_creature_8cs.html#aaef7214dfc0c9b4cbee043b753fcfd20", [
      [ "WISP", "_creature_8cs.html#aaef7214dfc0c9b4cbee043b753fcfd20a2e556889cc7f80bf832aea97595a2919", null ],
      [ "RATKIN", "_creature_8cs.html#aaef7214dfc0c9b4cbee043b753fcfd20a7abfd8d0c989cf53a1f37bb988e6b067", null ],
      [ "OGRE", "_creature_8cs.html#aaef7214dfc0c9b4cbee043b753fcfd20a6b010b3dcd8e6e1fe1f31cc23357ffa8", null ],
      [ "SKELETON", "_creature_8cs.html#aaef7214dfc0c9b4cbee043b753fcfd20a057e1bb1892582c5b4b02ed27fddf4a7", null ],
      [ "DEMON", "_creature_8cs.html#aaef7214dfc0c9b4cbee043b753fcfd20a150afaa990a81e1a160575bf2b9b0711", null ],
      [ "WEREWOLF", "_creature_8cs.html#aaef7214dfc0c9b4cbee043b753fcfd20aa114c5584a4c77512ebadf503749baaf", null ]
    ] ],
    [ "SkillUsingMessage", "_creature_8cs.html#ae7511f343b3d8f8e4a396c67e3e34fd4", [
      [ "SUCCESS", "_creature_8cs.html#ae7511f343b3d8f8e4a396c67e3e34fd4ad0749aaba8b833466dfcbb0428e4f89c", null ],
      [ "NOT_ENOUGH_MP", "_creature_8cs.html#ae7511f343b3d8f8e4a396c67e3e34fd4a748604e58c60b123e0dbe4c7242ec4b1", null ],
      [ "IS_STUNNED", "_creature_8cs.html#ae7511f343b3d8f8e4a396c67e3e34fd4a9eb2a1ac07835d365bee26fd8f0f3077", null ],
      [ "NOT_COOLED", "_creature_8cs.html#ae7511f343b3d8f8e4a396c67e3e34fd4a2f5c275f9a5264c0156ec0ac701210d0", null ],
      [ "IS_PASSIVE", "_creature_8cs.html#ae7511f343b3d8f8e4a396c67e3e34fd4ab03c3b19e2eafdb5e4a5df3b68f08542", null ],
      [ "INVALID_TARGET", "_creature_8cs.html#ae7511f343b3d8f8e4a396c67e3e34fd4ac95472125b90ae311ae87e4686168c14", null ]
    ] ]
];