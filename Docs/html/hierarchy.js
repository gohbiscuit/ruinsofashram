var hierarchy =
[
    [ "ColorUtil", "class_color_util.html", null ],
    [ "CreatureAIState", "class_creature_a_i_state.html", [
      [ "CreatureAIEngageState", "class_creature_a_i_engage_state.html", null ],
      [ "CreatureAIIdleState", "class_creature_a_i_idle_state.html", null ],
      [ "CreatureAIPatrolState", "class_creature_a_i_patrol_state.html", null ],
      [ "CreatureAIReturnState", "class_creature_a_i_return_state.html", null ]
    ] ],
    [ "CreatureInterface", "interface_creature_interface.html", [
      [ "Creature", "class_creature.html", null ]
    ] ],
    [ "CreatureMoveAgent", "class_creature_move_agent.html", null ],
    [ "CreatureState", "class_creature_state.html", [
      [ "CreatureCastState", "class_creature_cast_state.html", null ],
      [ "CreatureChargeState", "class_creature_charge_state.html", null ],
      [ "CreatureChaseState", "class_creature_chase_state.html", null ],
      [ "CreatureCooldownState", "class_creature_cooldown_state.html", null ],
      [ "CreatureStopState", "class_creature_stop_state.html", null ],
      [ "CreatureWalkState", "class_creature_walk_state.html", null ]
    ] ],
    [ "CreatureStats", "class_creature_stats.html", null ],
    [ "CreatureStatsCalculatorTest", "class_creature_stats_calculator_test.html", null ],
    [ "CreatureStatusEffects", "class_creature_status_effects.html", null ],
    [ "CreatureStatusEffectsTest", "class_creature_status_effects_test.html", null ],
    [ "Helper", "class_helper.html", null ],
    [ "Instruction", "class_instruction.html", null ],
    [ "InstructionTest", "class_instruction_test.html", null ],
    [ "IPhotonPeerListener", null, [
      [ "LobbyPhotonConnector", "class_lobby_photon_connector.html", null ]
    ] ],
    [ "MiniJSON.Json", "class_mini_j_s_o_n_1_1_json.html", null ],
    [ "MathParserTK.MathParser", "class_math_parser_t_k_1_1_math_parser.html", null ],
    [ "MouseCameraControl.Modifiers", "class_mouse_camera_control_1_1_modifiers.html", null ],
    [ "MonoBehaviour", null, [
      [ "AttributesTooltipHandler", "class_attributes_tooltip_handler.html", null ],
      [ "AudioKiller", "class_audio_killer.html", null ],
      [ "CorpseDecay", "class_corpse_decay.html", null ],
      [ "HUDHandler", "class_h_u_d_handler.html", null ],
      [ "LobbySoundLoader", "class_lobby_sound_loader.html", null ],
      [ "NameLabel", "class_name_label.html", null ],
      [ "PhotonNetworkManager", "class_photon_network_manager.html", null ],
      [ "RegisterHandler", "class_register_handler.html", null ],
      [ "RuneFactory", "class_rune_factory.html", null ],
      [ "StatsTooltipHandler", "class_stats_tooltip_handler.html", null ],
      [ "TargetMarker", "class_target_marker.html", null ]
    ] ],
    [ "MonoBehaviour", null, [
      [ "AIBaseBehaviour", "class_a_i_base_behaviour.html", [
        [ "AIOgreMageBehaviour", "class_a_i_ogre_mage_behaviour.html", null ],
        [ "AIOgrePriestBehaviour", "class_a_i_ogre_priest_behaviour.html", null ],
        [ "AIOgreRogueBehaviour", "class_a_i_ogre_rogue_behaviour.html", null ],
        [ "AIOgreWarriorBehaviour", "class_a_i_ogre_warrior_behaviour.html", null ],
        [ "AIRatkinGunnerBehaviour", "class_a_i_ratkin_gunner_behaviour.html", null ],
        [ "AIRatkinMageBehaviour", "class_a_i_ratkin_mage_behaviour.html", null ],
        [ "AIRatkinShamanBehaviour", "class_a_i_ratkin_shaman_behaviour.html", null ],
        [ "AIRatkinWarriorBehaviour", "class_a_i_ratkin_warrior_behaviour.html", null ]
      ] ],
      [ "AIStateManager", "class_a_i_state_manager.html", null ],
      [ "AOEMarker", "class_a_o_e_marker.html", null ],
      [ "AshramLobbyChat", "class_ashram_lobby_chat.html", null ],
      [ "AshramLobbyRefreshHandler", "class_ashram_lobby_refresh_handler.html", null ],
      [ "AshramRoomChat", "class_ashram_room_chat.html", null ],
      [ "AttackButtonHandler", "class_attack_button_handler.html", null ],
      [ "AttributesButtonHandler", "class_attributes_button_handler.html", null ],
      [ "AudioManager", "class_audio_manager.html", null ],
      [ "AudioManagerTest", "class_audio_manager_test.html", null ],
      [ "AvatarBar", "class_avatar_bar.html", null ],
      [ "BackToLobbyHandler", "class_back_to_lobby_handler.html", null ],
      [ "BillBoard", "class_bill_board.html", null ],
      [ "CameraSelection", "class_camera_selection.html", null ],
      [ "CancelQuitHandler", "class_cancel_quit_handler.html", null ],
      [ "CancelRoomHandler", "class_cancel_room_handler.html", null ],
      [ "CinematicsHandler", "class_cinematics_handler.html", null ],
      [ "CloseDialogHandler", "class_close_dialog_handler.html", null ],
      [ "CreateButtonHandler", "class_create_button_handler.html", null ],
      [ "CreateRoomHandler", "class_create_room_handler.html", null ],
      [ "Creator", "class_creator.html", null ],
      [ "Creature", "class_creature.html", null ],
      [ "CreatureSpawn", "class_creature_spawn.html", [
        [ "CreatureAISpawn", "class_creature_a_i_spawn.html", null ],
        [ "CreatureWispSpawn", "class_creature_wisp_spawn.html", null ]
      ] ],
      [ "CreatureStatsCalculator", "class_creature_stats_calculator.html", null ],
      [ "CreatureUI", "class_creature_u_i.html", null ],
      [ "CustomCursor", "class_custom_cursor.html", null ],
      [ "DBConnectionHandler", "class_d_b_connection_handler.html", null ],
      [ "DestroyAfterTime", "class_destroy_after_time.html", null ],
      [ "Exithandler", "class_exithandler.html", null ],
      [ "ExpBar", "class_exp_bar.html", null ],
      [ "ForgotPasswordHandler", "class_forgot_password_handler.html", null ],
      [ "GameListLoader", "class_game_list_loader.html", null ],
      [ "GameNetworkManager", "class_game_network_manager.html", null ],
      [ "GameRoomSoundLoader", "class_game_room_sound_loader.html", null ],
      [ "GameRoomTemp", "class_game_room_temp.html", null ],
      [ "HealthBar", "class_health_bar.html", null ],
      [ "HighlightingFXController", "class_highlighting_f_x_controller.html", null ],
      [ "HPBar", "class_h_p_bar.html", null ],
      [ "HUDDisplayText", "class_h_u_d_display_text.html", null ],
      [ "HUDErrorMessage", "class_h_u_d_error_message.html", null ],
      [ "HUDExploredText", "class_h_u_d_explored_text.html", null ],
      [ "HUDRespawnMessage", "class_h_u_d_respawn_message.html", null ],
      [ "JoinRoomHandler", "class_join_room_handler.html", null ],
      [ "JoinSlotHandler", "class_join_slot_handler.html", null ],
      [ "LeaveRoomHandler", "class_leave_room_handler.html", null ],
      [ "Loading", "class_loading.html", null ],
      [ "LobbyPhotonConnector", "class_lobby_photon_connector.html", null ],
      [ "LoginHandler", "class_login_handler.html", null ],
      [ "ManaBar", "class_mana_bar.html", null ],
      [ "MouseCameraControl", "class_mouse_camera_control.html", null ],
      [ "MouseClick", "class_mouse_click.html", null ],
      [ "MoveParticleFixPosition", "class_move_particle_fix_position.html", null ],
      [ "NetworkStream", "class_network_stream.html", null ],
      [ "NewPhotonLobby", "class_new_photon_lobby.html", null ],
      [ "OverlayListButton", "class_overlay_list_button.html", null ],
      [ "PageLeftHandler", "class_page_left_handler.html", null ],
      [ "PageRightHandler", "class_page_right_handler.html", null ],
      [ "particleScript", "classparticle_script.html", null ],
      [ "PlayerCommand", "class_player_command.html", null ],
      [ "PlayerListLoader", "class_player_list_loader.html", null ],
      [ "PopupDialogHandler", "class_popup_dialog_handler.html", null ],
      [ "QuitGameHandler", "class_quit_game_handler.html", null ],
      [ "QuitPopupHandler", "class_quit_popup_handler.html", null ],
      [ "RadioButtonsSelection", "class_radio_buttons_selection.html", null ],
      [ "RoomPlayerListLoader", "class_room_player_list_loader.html", null ],
      [ "RTSCamera", "class_r_t_s_camera.html", null ],
      [ "Rune", "class_rune.html", null ],
      [ "SceneManager", "class_scene_manager.html", null ],
      [ "SendButtonHandler", "class_send_button_handler.html", null ],
      [ "ShowPopupHandler", "class_show_popup_handler.html", null ],
      [ "SkillButton", "class_skill_button.html", null ],
      [ "SkillFactory", "class_skill_factory.html", null ],
      [ "SkillObject", "class_skill_object.html", [
        [ "SkillObject_GenericInstantDamageWithEffects", "class_skill_object___generic_instant_damage_with_effects.html", null ],
        [ "SkillObject_GenericMelee", "class_skill_object___generic_melee.html", null ],
        [ "SkillObject_OgreMageFireball", "class_skill_object___ogre_mage_fireball.html", null ],
        [ "SkillObject_OgreMageInfernalScorch", "class_skill_object___ogre_mage_infernal_scorch.html", null ],
        [ "SkillObject_OgrePriestHolyLight", "class_skill_object___ogre_priest_holy_light.html", null ],
        [ "SkillObject_OgrePriestManaShield", "class_skill_object___ogre_priest_mana_shield.html", null ],
        [ "SkillObject_OgrePriestOgreAura", "class_skill_object___ogre_priest_ogre_aura.html", null ],
        [ "SkillObject_OgreRogueAssassinLock", "class_skill_object___ogre_rogue_assassin_lock.html", null ],
        [ "SkillObject_OgreRogueAssassinStrike", "class_skill_object___ogre_rogue_assassin_strike.html", null ],
        [ "SkillObject_OgreRogueBasicAttack", "class_skill_object___ogre_rogue_basic_attack.html", null ],
        [ "SkillObject_OgreWarriorBattleRoar", "class_skill_object___ogre_warrior_battle_roar.html", null ],
        [ "SkillObject_OgreWarriorEarthQuake", "class_skill_object___ogre_warrior_earth_quake.html", null ],
        [ "SkillObject_OgreWarriorSkullCrusher", "class_skill_object___ogre_warrior_skull_crusher.html", null ],
        [ "SkillObject_RatkinGunnerBasicAttack", "class_skill_object___ratkin_gunner_basic_attack.html", null ],
        [ "SkillObject_RatkinGunnerGunnerAssassination", "class_skill_object___ratkin_gunner_gunner_assassination.html", null ],
        [ "SkillObject_RatkinGunnerRapidFire", "class_skill_object___ratkin_gunner_rapid_fire.html", null ],
        [ "SkillObject_RatkinGunnerRatkinEye", "class_skill_object___ratkin_gunner_ratkin_eye.html", null ],
        [ "SkillObject_RatkinMageArcaneBlast", "class_skill_object___ratkin_mage_arcane_blast.html", null ],
        [ "SkillObject_RatkinMageFreezingShield", "class_skill_object___ratkin_mage_freezing_shield.html", null ],
        [ "SkillObject_RatkinMageImpactWave", "class_skill_object___ratkin_mage_impact_wave.html", null ],
        [ "SkillObject_RatkinShamanChainLightning", "class_skill_object___ratkin_shaman_chain_lightning.html", null ],
        [ "SkillObject_RatkinShamanDivineHeal", "class_skill_object___ratkin_shaman_divine_heal.html", null ],
        [ "SkillObject_RatkinShamanEarthShield", "class_skill_object___ratkin_shaman_earth_shield.html", null ],
        [ "SkillObject_RatkinWarriorDeadlyBlow", "class_skill_object___ratkin_warrior_deadly_blow.html", null ],
        [ "SkillObject_RatkinWarriorShieldBarrier", "class_skill_object___ratkin_warrior_shield_barrier.html", null ],
        [ "SkillObject_RatkinWarriorThunderClap", "class_skill_object___ratkin_warrior_thunder_clap.html", null ]
      ] ],
      [ "StartGameHandler", "class_start_game_handler.html", null ],
      [ "StatusEffectButton", "class_status_effect_button.html", null ],
      [ "StopButtonHandler", "class_stop_button_handler.html", null ],
      [ "TabHandler", "class_tab_handler.html", null ],
      [ "TestCase", "class_test_case.html", [
        [ "AIReturnTest", "class_a_i_return_test.html", null ],
        [ "DieTest", "class_die_test.html", null ],
        [ "LevelUpTest", "class_level_up_test.html", null ],
        [ "PlayerDieTest", "class_player_die_test.html", null ],
        [ "SkillOgreMageBasicAttackTest", "class_skill_ogre_mage_basic_attack_test.html", null ],
        [ "SkillOgreMageBlinkTest", "class_skill_ogre_mage_blink_test.html", null ],
        [ "SkillOgreMageFireballTest", "class_skill_ogre_mage_fireball_test.html", null ],
        [ "SkillOgreMageInfernalScorchTest", "class_skill_ogre_mage_infernal_scorch_test.html", null ],
        [ "SkillOgrePriestBasicAttackTest", "class_skill_ogre_priest_basic_attack_test.html", null ],
        [ "SkillOgrePriestHolyLightTest", "class_skill_ogre_priest_holy_light_test.html", null ],
        [ "SkillOgrePriestManaShieldTest", "class_skill_ogre_priest_mana_shield_test.html", null ],
        [ "SkillOgrePriestOgreAuraTest", "class_skill_ogre_priest_ogre_aura_test.html", null ],
        [ "SkillOgreRogueAssassinLockTest", "class_skill_ogre_rogue_assassin_lock_test.html", null ],
        [ "SkillOgreRogueAssassinStrikeTest", "class_skill_ogre_rogue_assassin_strike_test.html", null ],
        [ "SkillOgreRogueBasicAttackTest", "class_skill_ogre_rogue_basic_attack_test.html", null ],
        [ "SkillOgreWarriorBasicAttackTest", "class_skill_ogre_warrior_basic_attack_test.html", null ],
        [ "SkillOgreWarriorBattleRoarTest", "class_skill_ogre_warrior_battle_roar_test.html", null ],
        [ "SkillOgreWarriorEarthQuakeTest", "class_skill_ogre_warrior_earth_quake_test.html", null ],
        [ "SkillOgreWarriorSkullCrusherTest", "class_skill_ogre_warrior_skull_crusher_test.html", null ],
        [ "SkillRatkinGunnerBasicAttackTest", "class_skill_ratkin_gunner_basic_attack_test.html", null ],
        [ "SkillRatkinGunnerGunnerAssassinationTest", "class_skill_ratkin_gunner_gunner_assassination_test.html", null ],
        [ "SkillRatkinGunnerRapidFireTest", "class_skill_ratkin_gunner_rapid_fire_test.html", null ],
        [ "SkillRatkinGunnerRatkinEyeTest", "class_skill_ratkin_gunner_ratkin_eye_test.html", null ],
        [ "SkillRatkinMageArcaneBlastTest", "class_skill_ratkin_mage_arcane_blast_test.html", null ],
        [ "SkillRatkinMageBasicAttackTest", "class_skill_ratkin_mage_basic_attack_test.html", null ],
        [ "SkillRatkinMageFreezingShieldTest", "class_skill_ratkin_mage_freezing_shield_test.html", null ],
        [ "SkillRatkinMageImpactWaveTest", "class_skill_ratkin_mage_impact_wave_test.html", null ],
        [ "SkillRatkinShamanBasicAttackTest", "class_skill_ratkin_shaman_basic_attack_test.html", null ],
        [ "SkillRatkinShamanChainLightningTest", "class_skill_ratkin_shaman_chain_lightning_test.html", null ],
        [ "SkillRatkinShamanDivineHealTest", "class_skill_ratkin_shaman_divine_heal_test.html", null ],
        [ "SkillRatkinShamanEarthShieldTest", "class_skill_ratkin_shaman_earth_shield_test.html", null ],
        [ "SkillRatkinWarriorBasicAttackTest", "class_skill_ratkin_warrior_basic_attack_test.html", null ],
        [ "SkillRatkinWarriorDeadlyBlowTest", "class_skill_ratkin_warrior_deadly_blow_test.html", null ],
        [ "SkillRatkinWarriorShieldBarrierTest", "class_skill_ratkin_warrior_shield_barrier_test.html", null ],
        [ "SkillRatkinWarriorThunderClapTest", "class_skill_ratkin_warrior_thunder_clap_test.html", null ],
        [ "SkillWispTeleportTest", "class_skill_wisp_teleport_test.html", null ],
        [ "TestCaseExample", "class_test_case_example.html", null ],
        [ "WispCreationTest", "class_wisp_creation_test.html", null ],
        [ "WispMassPossessTest", "class_wisp_mass_possess_test.html", null ],
        [ "WispMoveStopTest", "class_wisp_move_stop_test.html", null ],
        [ "WispMoveTest", "class_wisp_move_test.html", null ],
        [ "WispPossessTest", "class_wisp_possess_test.html", null ],
        [ "WispPossessUseSkillTest", "class_wisp_possess_use_skill_test.html", null ],
        [ "WispUseSkillStopTest", "class_wisp_use_skill_stop_test.html", null ]
      ] ],
      [ "UserProfileLoader", "class_user_profile_loader.html", null ],
      [ "Utility", "class_utility.html", null ],
      [ "WebsiteHandler", "class_website_handler.html", null ]
    ] ],
    [ "MouseCameraControl.MouseControlConfiguration", "class_mouse_camera_control_1_1_mouse_control_configuration.html", null ],
    [ "MouseCameraControl.MouseScrollConfiguration", "class_mouse_camera_control_1_1_mouse_scroll_configuration.html", null ],
    [ "Player", "class_player.html", null ],
    [ "PlayerAttributes", "class_player_attributes.html", null ],
    [ "RuneEffect", "class_rune_effect.html", null ],
    [ "Skill", "class_skill.html", [
      [ "Skill_OgreMageBasicAttack", "class_skill___ogre_mage_basic_attack.html", null ],
      [ "Skill_OgreMageBlink", "class_skill___ogre_mage_blink.html", null ],
      [ "Skill_OgreMageFireball", "class_skill___ogre_mage_fireball.html", null ],
      [ "Skill_OgreMageInfernalScorch", "class_skill___ogre_mage_infernal_scorch.html", null ],
      [ "Skill_OgrePriestBasicAttack", "class_skill___ogre_priest_basic_attack.html", null ],
      [ "Skill_OgrePriestHolyLight", "class_skill___ogre_priest_holy_light.html", null ],
      [ "Skill_OgrePriestManaShield", "class_skill___ogre_priest_mana_shield.html", null ],
      [ "Skill_OgrePriestOgreAura", "class_skill___ogre_priest_ogre_aura.html", null ],
      [ "Skill_OgreRogueAssassinLock", "class_skill___ogre_rogue_assassin_lock.html", null ],
      [ "Skill_OgreRogueAssassinStrike", "class_skill___ogre_rogue_assassin_strike.html", null ],
      [ "Skill_OgreRogueBackstab", "class_skill___ogre_rogue_backstab.html", null ],
      [ "Skill_OgreRogueBasicAttack", "class_skill___ogre_rogue_basic_attack.html", null ],
      [ "Skill_OgreWarriorBasicAttack", "class_skill___ogre_warrior_basic_attack.html", null ],
      [ "Skill_OgreWarriorBattleRoar", "class_skill___ogre_warrior_battle_roar.html", null ],
      [ "Skill_OgreWarriorEarthQuake", "class_skill___ogre_warrior_earth_quake.html", null ],
      [ "Skill_OgreWarriorSkullCrusher", "class_skill___ogre_warrior_skull_crusher.html", null ],
      [ "Skill_RatkinGunnerBasicAttack", "class_skill___ratkin_gunner_basic_attack.html", null ],
      [ "Skill_RatkinGunnerGunnerAssassination", "class_skill___ratkin_gunner_gunner_assassination.html", null ],
      [ "Skill_RatkinGunnerRapidFire", "class_skill___ratkin_gunner_rapid_fire.html", null ],
      [ "Skill_RatkinGunnerRatkinEye", "class_skill___ratkin_gunner_ratkin_eye.html", null ],
      [ "Skill_RatkinMageArcaneBlast", "class_skill___ratkin_mage_arcane_blast.html", null ],
      [ "Skill_RatkinMageBasicAttack", "class_skill___ratkin_mage_basic_attack.html", null ],
      [ "Skill_RatkinMageFreezingShield", "class_skill___ratkin_mage_freezing_shield.html", null ],
      [ "Skill_RatkinMageImpactWave", "class_skill___ratkin_mage_impact_wave.html", null ],
      [ "Skill_RatkinShamanBasicAttack", "class_skill___ratkin_shaman_basic_attack.html", null ],
      [ "Skill_RatkinShamanChainLightning", "class_skill___ratkin_shaman_chain_lightning.html", null ],
      [ "Skill_RatkinShamanDivineHeal", "class_skill___ratkin_shaman_divine_heal.html", null ],
      [ "Skill_RatkinShamanEarthShield", "class_skill___ratkin_shaman_earth_shield.html", null ],
      [ "Skill_RatkinWarriorBasicAttack", "class_skill___ratkin_warrior_basic_attack.html", null ],
      [ "Skill_RatkinWarriorDeadlyBlow", "class_skill___ratkin_warrior_deadly_blow.html", null ],
      [ "Skill_RatkinWarriorShieldBarrier", "class_skill___ratkin_warrior_shield_barrier.html", null ],
      [ "Skill_RatkinWarriorThunderClap", "class_skill___ratkin_warrior_thunder_clap.html", null ],
      [ "Skill_Release", "class_skill___release.html", null ],
      [ "Skill_WispPossess", "class_skill___wisp_possess.html", null ],
      [ "Skill_WispTeleport", "class_skill___wisp_teleport.html", null ]
    ] ],
    [ "SkillFactoryTest", "class_skill_factory_test.html", null ],
    [ "SkillTest", "class_skill_test.html", null ],
    [ "TextUtil", "class_text_util.html", null ]
];