var class_audio_manager_test =
[
    [ "Init", "class_audio_manager_test.html#aceb30ff2ee600a6e237b8a83d80f8cb6", null ],
    [ "TestIsPlaying", "class_audio_manager_test.html#a9222242b3a817afe3611bd18a982ab17", null ],
    [ "TestLoop", "class_audio_manager_test.html#a92610c6b1075c571deaef4c04a3fbe41", null ],
    [ "TestPlay", "class_audio_manager_test.html#ad96c22921fddc37a33bfd4b20c028446", null ],
    [ "TestPlayOnce", "class_audio_manager_test.html#ab1197ae78f712b18501f91f6a4fd5517", null ],
    [ "TestStop", "class_audio_manager_test.html#aafaa64bfd4a0a795b39852ec3e980c55", null ],
    [ "TestStop2", "class_audio_manager_test.html#aec948abe424ee08bdb41eef66fbe99d4", null ]
];