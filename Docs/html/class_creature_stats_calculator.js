var class_creature_stats_calculator =
[
    [ "Awake", "class_creature_stats_calculator.html#a45874e2eb0359d7aad7204e70835088e", null ],
    [ "computeNewPossessedStats", "class_creature_stats_calculator.html#acdb36689e38fa247cd80940a24ac540e", null ],
    [ "computeOldPossessedStats", "class_creature_stats_calculator.html#a1c8e85e2ee286bbb8eaade084df52b7b", null ],
    [ "computeUnpossessedStats", "class_creature_stats_calculator.html#a71549f88193312258bcf70581abf0974", null ],
    [ "getCreatureStats", "class_creature_stats_calculator.html#afe7ee53e70bd67aab00cf0c943d7dd53", null ],
    [ "getSerizalizedCreatureStats", "class_creature_stats_calculator.html#a61504f10ccac904e404750919e31a45d", null ],
    [ "LoadStats", "class_creature_stats_calculator.html#ad0b0ebbd2836d7115c39636e2674adc0", null ],
    [ "Instance", "class_creature_stats_calculator.html#a8176a04fe12699125f645cfd542b5c11", null ]
];