var _creator_8cs =
[
    [ "Creator", "class_creator.html", "class_creator" ],
    [ "CreatureIconType", "_creator_8cs.html#afa2a1ade1c13292585833b8c4bbbf870", [
      [ "PLAYER", "_creator_8cs.html#afa2a1ade1c13292585833b8c4bbbf870a07c80e2a355d91402a00d82b1fa13855", null ],
      [ "ALLY", "_creator_8cs.html#afa2a1ade1c13292585833b8c4bbbf870a8a909f5ee9185002151cd3779f9fbb0b", null ],
      [ "ENEMY", "_creator_8cs.html#afa2a1ade1c13292585833b8c4bbbf870a92b09d1635332c90ae8508618a174244", null ],
      [ "NEUTRAL", "_creator_8cs.html#afa2a1ade1c13292585833b8c4bbbf870a31ba17aa58cdb681423f07ca21a6efc7", null ]
    ] ],
    [ "CreatureType", "_creator_8cs.html#a5bc40f1116d89138cda8ebd1a04694df", [
      [ "PLAYER", "_creator_8cs.html#a5bc40f1116d89138cda8ebd1a04694dfa07c80e2a355d91402a00d82b1fa13855", null ],
      [ "AI", "_creator_8cs.html#a5bc40f1116d89138cda8ebd1a04694dfa0a40e3c91a3a55c9a37428c6d194d0e5", null ]
    ] ]
];