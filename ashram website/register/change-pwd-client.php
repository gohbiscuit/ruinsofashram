<?PHP
require_once("./include/membersite_config.php");

if(isset($_POST['submitted']))
{
   if($fgmembersite->ChangePassword())
   {
        $fgmembersite->RedirectToURL("changed-pwd.html");
   }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
    <title>Change password</title>
    <link rel="STYLESHEET" type="text/css" href="style/fg_membersite.css" />
	<link rel="STYLESHEET" type="text/css" href="../css/style.css" >
    <script type='text/javascript' src='scripts/gen_validatorv31.js'></script>
    <link rel="STYLESHEET" type="text/css" href="style/pwdwidget.css" />
    <script src="scripts/pwdwidget.js" type="text/javascript"></script>   
	<link rel="shortcut icon" href="../images/favicon.ico">  
</head>
<body>
<div id="background">
	<div id="header">
		<div>
			<div>
				<a href="../index.html" class="logo"><img src="../images/logo.png" alt="" /></a>
				<ul>
					<li>
						<a href="../index.html" id="menu1">home</a>
					</li>
					<li>
						<a href="index.php" id="menu2">register</a>
					</li>
					<li>
						<a href="../games.html" id="menu3">download</a>
					</li>
					<li>
						<a href="../about.html" id="menu4">about</a>
					</li>
					<li>
						<a href="../blog.html" id="menu5">blog</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div id="body">
		<div>
			<div>
				<div class="media">
					<div>
						<div>
							<h3>Change Password</h3>
							<!-- Form Code Start -->
							<div id='fg_membersite'>
								<form id='changepwd' action='<?php echo $fgmembersite->GetSelfScript(); ?>' method='post' accept-charset='UTF-8'>
									<fieldset >

										<input type='hidden' name='submitted' id='submitted' value='1'/>

										<div class='short_explanation'>* required fields</div>

										<div><span class='error'><?php echo $fgmembersite->GetErrorMessage(); ?></span></div>
										<div class='container'>
											<label for='email' >Email:</label><br/>
											<input type='text' name='email' id='email' value='<?php echo $_POST['email']; ?>' maxlength="50" readonly /><br/>
										</div>

										<div class='container'>
											<label for='oldpwd' >Old Password*:</label><br/>
											<div class='pwdwidgetdiv' id='oldpwddiv' ></div><br/>
											<noscript>
											<input type='password' name='oldpwd' id='oldpwd' maxlength="50" />
											</noscript>    
											<span id='changepwd_oldpwd_errorloc' class='error'></span>
										</div>

										<div class='container'>
											<label for='newpwd' >New Password*:</label><br/>
											<div class='pwdwidgetdiv' id='newpwddiv' ></div>
											<noscript>
											<input type='password' name='newpwd' id='newpwd' maxlength="50" /><br/>
											</noscript>
											<span id='changepwd_newpwd_errorloc' class='error'></span>
										</div>

										<br/><br/><br/>
										<div class='container'>
											<input type='submit' name='Submit' value='Submit' />
										</div>
									</fieldset>
								</form>
							</div>
						</div>
						<div class="aside">
							<img src="../images/hud2.png" alt="" width="449" height="473" class="figure">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="footer">
		<div>
			<ul>
				<li id="facebook">
						<a href="http://www.facebook.com/Ashram3284/">facebook</a>
					</li>
					<li id="twitter">
						<a href="">twitter</a>
					</li>
					<li id="googleplus">
						<a href="">googleplus</a>
					</li>
			</ul>
			<p>
				@ copyright 2014. all rights reserved.
			</p>
		</div>
	</div>
</div>
<!-- client-side Form Validations:
Uses the excellent form validation script from JavaScript-coder.com-->

<script type='text/javascript'>
// <![CDATA[
    var pwdwidget = new PasswordWidget('oldpwddiv','oldpwd');
    pwdwidget.enableGenerate = false;
    pwdwidget.enableShowStrength=false;
    pwdwidget.enableShowStrengthStr =false;
    pwdwidget.MakePWDWidget();
    
    var pwdwidget = new PasswordWidget('newpwddiv','newpwd');
    pwdwidget.MakePWDWidget();
    
    
    var frmvalidator  = new Validator("changepwd");
    frmvalidator.EnableOnPageErrorDisplay();
    frmvalidator.EnableMsgsTogether();

    frmvalidator.addValidation("oldpwd","req","Please provide your old password");
    
    frmvalidator.addValidation("newpwd","req","Please provide your new password");

// ]]>
</script>

</div>
<!--
Form Code End (see html-form-guide.com for more info.)
-->

</body>
</html>