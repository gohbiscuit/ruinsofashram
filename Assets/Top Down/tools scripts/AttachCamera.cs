using UnityEngine;
using System.Collections;


public class AttachCamera : MonoBehaviour
{
	Transform myTransform;
	public Transform target;
	public Vector3 offset = new Vector3(0, 5, -5);

 	bool isFollowPlayerCamera = true;		// true - follow player RPG camera, false - means RTS camera
	
	// --- Zoom In Camera Functions -- //
	float distance = 60f;
	float sensitivityDistance = 50f;
	float damping = 5f;
	float minFOV = 40f;
	float maxFOV = 60f;
	
	// -- Pan Functions (By Mouse) -- //
	float CamSpeed = 1.00f;
 	float GUIsize = 25f;
	
	// -- Move Functions -- //
	// Clipping defines the boundary of the camera movement
	float left_clip_x = -86.0f;
	float bottom_clip_z = -165.0f;
	float top_clip_z = 33.0f;
	float right_clip_x = 90.0f;
	float cameraSpeed = 40.0f; 
	
	
	void Start()
	{
		myTransform = this.transform;
		
		// -- Default Look At Camera -- //
		//myTransform.position = target.position + offset;
		//myTransform.LookAt(target.position, Vector3.up);

		//if(target != null)
		//z	setTarget(target);

		distance = camera.fieldOfView;
	}

	void setTarget(Transform transform){
		target = transform;
		myTransform.position = target.position + offset;
		myTransform.LookAt(target.position, Vector3.up);
	}
	
	// -- Move Camera by Directional Arrow Keys -- //
	void MoveCamera()
	{
		if (Input.GetKey(KeyCode.UpArrow) ) { MoveUp(); return; }
	  	if (Input.GetKey(KeyCode.RightArrow)  ) { MoveRight(); return; }
	  	if (Input.GetKey(KeyCode.DownArrow) ) { MoveDown(); return; }
	  	if (Input.GetKey(KeyCode.LeftArrow) ) { MoveLeft(); return; }
	}
	
	public void MoveUp()
	{
		//if(transform.position.z > top_clip_z)
		//	return;
		
		//transform.Translate( 0, cameraSpeed * Time.deltaTime, 0);
		transform.Translate(0, 0, CamSpeed, Space.World);
	}
	
	public void MoveRight()
	{
		//if(transform.position.x > right_clip_x)
		//	return;
		
		//transform.Translate( cameraSpeed * Time.deltaTime, 0, 0);
		transform.Translate(CamSpeed, 0, 0, Space.World);
	}
	
	public void MoveDown()
	{
		//if(transform.position.z < bottom_clip_z)
		//	return;
		
		//transform.Translate( 0, -cameraSpeed * Time.deltaTime, 0);
		transform.Translate(0, 0, -CamSpeed, Space.World);
	}
	
	public void MoveLeft()
	{
		//if(transform.position.x < left_clip_x)
		//	return;
		
		//transform.Translate( -cameraSpeed * Time.deltaTime, 0, 0);
		transform.Translate(-CamSpeed, 0, 0, Space.World);
	}
	
	void PanCameraByMouse()
	{
		var rect_down = new Rect (0, 0, Screen.width, GUIsize);
		var rect_up = new Rect (0, Screen.height-GUIsize, Screen.width, GUIsize);
		var rect_left = new Rect (0, 0, GUIsize, Screen.height);
		var rect_right = new Rect (Screen.width-GUIsize, 0, GUIsize, Screen.height);

		if (rect_down.Contains(Input.mousePosition))
    		transform.Translate(0, 0, -CamSpeed, Space.World);

		if (rect_up.Contains(Input.mousePosition))
    		transform.Translate(0, 0, CamSpeed, Space.World);

		if (rect_left.Contains(Input.mousePosition))
   			transform.Translate(-CamSpeed, 0, 0, Space.World);

		if (rect_right.Contains(Input.mousePosition))
    		transform.Translate(CamSpeed, 0, 0, Space.World);
	}
	
	// -- Zoom in by Scroll-- //
	void ZoomInCamera()
	{
		distance -= Input.GetAxis("Mouse ScrollWheel") * sensitivityDistance;
		distance = Mathf.Clamp(distance, minFOV, maxFOV);
		camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, distance, Time.deltaTime * damping);
	}

	void UpdateCamera()
	{
		if (target != null)
		{
			// RPG Camera
			if(isFollowPlayerCamera)
			{
				// -- Default Look At Camera -- //
				myTransform.position = target.position + offset;
				myTransform.LookAt(target.position, Vector3.up);
				ZoomInCamera();
			}
			// RTS Camera
			else
			{
				ZoomInCamera();
				PanCameraByMouse();
				MoveCamera();
			}
		} 
		else 
		{
			Creature controlledCreature = PlayerCommand.Instance.getControlledCreature();
			if(controlledCreature != null){
				//Debug.Log("TARGET SET");
				setTarget(controlledCreature.transform);
			}
		}
	}
	
	void Update()
	{
		// Player Changes Camera
		//if (   (Input.GetKeyDown(KeyCode.LeftAlt) || Input.GetKeyDown (KeyCode.RightAlt)) && Input.GetKeyDown(KeyCode.C) )  ALT-C is some special command in Unity therefore not working
		if (  Input.GetKeyDown(KeyCode.C) )
		{
			isFollowPlayerCamera = !isFollowPlayerCamera;
		}

		UpdateCamera();
	}
}
