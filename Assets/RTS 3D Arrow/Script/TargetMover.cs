﻿using UnityEngine;
using System.Collections;

public class TargetMover : MonoBehaviour {

	public GameObject arrow;
	public float animationSpeed = 4;
	 
	void Start () {
		
		arrow.animation["Play"].speed = animationSpeed;
	}
	
	public LayerMask groundLayer;

	void Update () {
		
		/*if( Input.GetMouseButtonDown(1)){
			
			RaycastHit hit;
			if (Physics.Raycast	(Camera.main.ScreenPointToRay (Input.mousePosition),out hit, Mathf.Infinity, groundLayer)) 
			{
			}
		}*/
	}

	public void PlayMoveParticle(Vector3 position)
	{
		transform.position = position;
		arrow.animation.Rewind("Play");
		arrow.animation.Play("Play", PlayMode.StopAll);
	}
}
