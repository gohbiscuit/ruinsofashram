using UnityEngine;
using System.Collections;

public class RotationController : MonoBehaviour
{
	private Transform tr;
	
	void Awake()
	{
		tr = GetComponent<Transform>();
	}
	
	void Update()
	{
		tr.Rotate(50f * Time.deltaTime, 100f * Time.deltaTime, 200f * Time.deltaTime);
	}
}
