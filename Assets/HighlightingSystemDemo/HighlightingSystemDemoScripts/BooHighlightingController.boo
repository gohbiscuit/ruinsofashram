import UnityEngine

class BooHighlightingController (MonoBehaviour): 
	
	protected ho as HighlightableObject
	private constantlyHighlighted as bool = false

	def Awake():
		//ho = (gameObject.AddComponent('HighlightableObject') as HighlightableObject)
		ho = (gameObject.AddComponent[of HighlightableObject]() as HighlightableObject);
	
	def Update():
		// Fade in/out constant highlighting by 'Tab' button
		if Input.GetKeyDown(KeyCode.Tab):
			constantlyHighlighted = not constantlyHighlighted
			if constantlyHighlighted:
				ho.ConstantOn(Color.yellow)
			else:
				ho.ConstantOff()
		// Turn on/off constant highlighting by 'Q' button
		elif Input.GetKeyDown(KeyCode.Q):
			constantlyHighlighted = not constantlyHighlighted
			if constantlyHighlighted:
				ho.ConstantOnImmediate(Color.yellow)
			else:
				ho.ConstantOffImmediate()
		
		// Turn off all highlighting modes by 'Z' button
		if Input.GetKeyDown(KeyCode.Z):
			constantlyHighlighted = false
			ho.Off()
