using UnityEngine;
using System.Collections;

public class Singleton : MonoBehaviour
{
	void OnEnable()
	{
		// Initialize highlighting system
		Highlighting.Init();
	}
}
