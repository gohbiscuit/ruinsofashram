#pragma strict

protected var ho : HighlightableObject;
private var constantlyHighlighted : boolean = false;

function Awake ()
{
	ho = gameObject.AddComponent(HighlightableObject);
}

function Update ()
{
	// Fade in/out constant highlighting by 'Tab' button
	if (Input.GetKeyDown(KeyCode.Tab)) 
	{
		constantlyHighlighted = !constantlyHighlighted;
		if (constantlyHighlighted)
			ho.ConstantOn(Color.yellow);
		else
			ho.ConstantOff();
	}
	// Turn on/off constant highlighting by 'Q' button
	else if (Input.GetKeyDown(KeyCode.Q))
	{
		constantlyHighlighted = !constantlyHighlighted;
		if (constantlyHighlighted)
			ho.ConstantOnImmediate(Color.yellow);
		else
			ho.ConstantOffImmediate();
	}
	
	// Turn off all highlighting modes by 'Z' button
	if (Input.GetKeyDown(KeyCode.Z)) 
	{
		constantlyHighlighted = false;
		ho.Off();
	}
}