using UnityEngine;
using System.Collections;

public class WallController : MonoBehaviour
{
	public float offset;
	
	private Transform tr;
	private float counter;
	
	void Awake()
	{
		tr = GetComponent<Transform>();
		counter = 0f;
	}
	
	void Update()
	{
		counter += Time.deltaTime * 0.5f;
		tr.position = new Vector3(tr.position.x, 1.5f * Mathf.Cos(counter + offset), tr.position.z);
	}
}
