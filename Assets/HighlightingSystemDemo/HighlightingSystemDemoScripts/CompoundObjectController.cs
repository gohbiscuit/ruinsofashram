using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CompoundObjectController : HighlightingController
{
	// Cached transform component
	private Transform tr;
	
	// Cached list of child objects
	private List<GameObject> objects;
	
	private int currentShaderID = 2;
	private string[] shaderNames = new string[] {"Bumped Diffuse", "Bumped Specular", "Diffuse", "Diffuse Detail", "Parallax Diffuse", "Parallax Specular" , "Specular", "VertexLit"};
	
	void Start()
	{
		tr = GetComponent<Transform>();
		objects = new List<GameObject>();
	}
	
	void OnGUI()
	{
		GUI.Label(new Rect(10, 10, 500, 100), "Compound object controls:");
		
		if (GUI.Button(new Rect(10, 30, 200, 30), "Add Random Primitive"))
			AddObject();
		
		if (GUI.Button(new Rect(10, 70, 200, 30), "Change Material"))
			ChangeMaterial();
		
		if (GUI.Button(new Rect(10, 110, 200, 30), "Change Shader"))
			ChangeShader();
		
		if (GUI.Button(new Rect(10, 150, 200, 30), "Remove Object"))
			RemoveObject();
	}
	
	void AddObject()
	{
		int primitiveType = Random.Range(0, 4);
		GameObject newObject = GameObject.CreatePrimitive((PrimitiveType)primitiveType);
		Transform newObjectTransform = newObject.GetComponent<Transform>();
		newObjectTransform.parent = tr;
		newObjectTransform.localPosition = Random.insideUnitSphere * 2f;
		objects.Add(newObject);
		
		// Reinitialize highlighting materials, because we changed child objects
		ho.ReinitMaterials();
	}
	
	void ChangeMaterial()
	{
		if (objects.Count < 1)
			AddObject();
		
		currentShaderID = (currentShaderID + 1 >= shaderNames.Length) ? 0 : currentShaderID + 1;
		
		foreach (GameObject obj in objects)
		{
			Renderer renderer = obj.GetComponent<Renderer>();
			Shader newShader = Shader.Find(shaderNames[currentShaderID]);
			renderer.material = new Material(newShader);
		}
		
		// Reinitialize highlightable materials, because we changed material(s)
		ho.ReinitMaterials();
	}
	
	void ChangeShader()
	{
		if (objects.Count < 1)
			AddObject();
		
		currentShaderID = (currentShaderID + 1 >= shaderNames.Length) ? 0 : currentShaderID + 1;
		
		// Restore original material shaders, but don't reset current highlighting state
		// Call this before you change any highlighted material shader
		ho.RestoreMaterials();
		
		foreach (GameObject obj in objects)
		{
			Renderer renderer = obj.GetComponent<Renderer>();
			Shader newShader = Shader.Find(shaderNames[currentShaderID]);
			renderer.material.shader = newShader;
		}
	}
	
	void RemoveObject()
	{
		if (objects.Count < 1)
			return;
		
		GameObject toRemove = objects[objects.Count-1];
		objects.Remove(toRemove);
		Destroy(toRemove);
		
		// Reinitialize highlighting materials, because we changed child objects
		ho.ReinitMaterials();
	}
}
