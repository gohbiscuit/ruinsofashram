using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Highlighting {
	// Shader replacement map ("source shader name", "replacement shader name")
	static private string[,] map = new string[,] {
		{"Bumped Diffuse", "Hidden/Highlighted/Bumped Diffuse"}, 
		{"Bumped Specular", "Hidden/Highlighted/Bumped Specular"}, 
		{"Diffuse", "Hidden/Highlighted/Diffuse"}, 
		{"Diffuse Detail", "Hidden/Highlighted/Diffuse Detail"}, 
		{"Parallax Diffuse", "Hidden/Highlighted/Parallax Diffuse"}, 
		{"Parallax Specular", "Hidden/Highlighted/Parallax Specular"}, 
		{"Specular", "Hidden/Highlighted/Specular"}, 
		{"VertexLit", "Hidden/Highlighted/VertexLit"}, 
		{"Unlit/Transparent Colored", "Hidden/Highlighted/Unlit/Transparent Colored"}
	};
	
	static private Dictionary<Shader, Shader> shaderMap;
	
	private static Shader _defaultHighlightingShader = null;
	protected static Shader defaultReplacementShader {
		get {
			return _defaultHighlightingShader;
		}
	}
	
	// Replacement shaders initialization
	static public void Init()
	{
		_defaultHighlightingShader = Resources.Load("HighlightingDefaultShader", typeof(Shader)) as Shader;
		shaderMap = new Dictionary<Shader, Shader>();
		
		for (int i = 0; i < map.GetLength(0); i++)
		{
			Shader ss = Shader.Find(map[i, 0]);
			if (ss == null)
			{
				#if UNITY_EDITOR
				Debug.LogWarning("Highlighting : Init() : Source shader with name '" + map[i, 0] + "' not found!");
				#endif
				continue;
			}
			else
			{
				if (shaderMap.ContainsKey(ss))
				{
					#if UNITY_EDITOR
					Debug.LogWarning("Highlighting : Init() : Value for duplicated source shader name '" + map[i, 0] + "' will not be used!");
					#endif
					continue;
				}
				
				Shader rs = Shader.Find(map[i, 1]);
				
				if (rs == null)
				{
					#if UNITY_EDITOR
					Debug.LogWarning("Highlighting : Init() : Replacement shader with name '" + map[i, 1] + "' for source shader with name '" + map[i, 0] + "' not found!");
					#endif
					continue;
				}
				else shaderMap.Add(ss, rs);
			}
		}
		
		// Preload all shaders
		Shader.WarmupAllShaders();
	}
	
	static public Shader GetReplacementShader(Shader sourceShader)
	{
		if (shaderMap == null)
			Init();
		
		Shader replacementShader;
		if (shaderMap.TryGetValue(sourceShader, out replacementShader))
			return replacementShader;
		else
			return defaultReplacementShader;
	}
}