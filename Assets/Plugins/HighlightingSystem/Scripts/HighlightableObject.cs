using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HighlightableObject : MonoBehaviour
{
	// Constant highlighting turning on speed
	static public float constantOnSpeed = 4.5f;
	
	// Constant highlighting turning off speed
	static public float constantOffSpeed = 4f;
	
	// Class for caching material shaders
	private class HighlightingMaterialCache
	{
		public Material material;
		public Shader sourceShader;
		public Shader replacementShader;
		
		public HighlightingMaterialCache(Material mat, Shader source, Shader replacement)
		{
			this.material = mat;
			this.sourceShader = source;
			this.replacementShader = replacement;
		}
	}
	
	// 2 * PI constant required for flashing
	private const float doublePI = 2f * Mathf.PI;
	
	// This object cached transform component
	private Transform tr;
	
	// Cached materials
	private List<HighlightingMaterialCache> highlightableMaterials;
	
	// When set to true - highlighting component is dying
	private bool isDying = false;
	
	// Need to reinit materials flag
	private bool materialsIsDirty = false;
	
	// Current state of highlighting
	private bool currentState = false;
	
	// Current materials highlighting color
	private Color currentColor;
	
	// Transition is active flag
	private bool transitionActive = false;
	
	// Current transition value
	private float transitionValue = 0f;
	
	// Flashing frequency
	private float flashingFreq = 2f;
	
	// One-frame highlighting flag
	private bool once = false;
	
	// One-frame highlighting color
	private Color onceColor = Color.clear;
	
	// Flashing state flag
	private bool flashing = false;
	
	// Flashing from color
	private Color flashingColorMin = new Color(0f, 0.3412f, 0.6941f, 0f);
	
	// Flashing to color
	private Color flashingColorMax = new Color(0f, 0.6196f, 0.9961f, 1f);
	
	// Constant highlighting state flag
	private bool constantly = false;
	
	// Constant highlighting color
	protected Color constantColor = Color.clear;
	
	#region Common
	protected virtual void OnEnable()
	{
		tr = GetComponent<Transform>();
		InitMaterials();
	}
	
	protected virtual void LateUpdate()
	{
		HighlightingUpdate();
	}
	#endregion
	
	#region Public Methods
	/// <summary>
	/// Materials reinitialization. 
	/// Call this method right after highlighted object has changed his materials or child objects.
	/// Can be called multiple times per update - meshes reinitialization will occur only once.
	/// </summary>
	public void ReinitMaterials()
	{
		materialsIsDirty = true;
	}
	
	/// <summary>
	/// Immediately restore original materials.
	/// You need to call this method if you want to change your material shader and don't want to reset current state of highlighting.
	/// </summary>
	public void RestoreMaterials()
	{
		MaterialsOff();
		highlightableMaterials = null;
		materialsIsDirty = true;
	}
	
	/// <summary>
	/// Turn on one-frame highlighting.
	/// </summary>
	public void On()
	{
		// Highlight object only in this frame
		once = true;
	}
	
	/// <summary>
	/// Turn on one-frame highlighting with given color.
	/// Can be called multiple times per update, color only from latest call will be used.
	/// </summary>
	/// <param name='color'>
	/// Highlighting color.
	/// </param>
	public void On(Color color)
	{
		// Set new color for highlighting
		onceColor = color;
		On();
	}
	
	/// <summary>
	/// Turn on flashing.
	/// </summary>
	public void FlashingOn()
	{
		// Enable flashing
		flashing = true;
	}
	
	/// <summary>
	/// Turn on flashing from color1 to color2.
	/// </summary>
	/// <param name='color1'>
	/// Starting color.
	/// </param>
	/// <param name='color2'>
	/// Ending color.
	/// </param>
	public void FlashingOn(Color color1, Color color2)
	{
		flashingColorMin = color1;
		flashingColorMax = color2;
		FlashingOn();
	}
	
	/// <summary>
	/// Turn on flashing from color1 to color2, and set flashing frequency to a given value.
	/// </summary>
	/// <param name='color1'>
	/// Starting color.
	/// </param>
	/// <param name='color2'>
	/// Ending color.
	/// </param>
	/// <param name='freq'>
	/// Flashing frequency.
	/// </param>
	public void FlashingOn(Color color1, Color color2, float freq)
	{
		flashingFreq = freq;
		FlashingOn(color1, color2);
	}
	
	/// <summary>
	/// Turn on flashing with given frequency.
	/// </summary>
	/// <param name='f'>
	/// Flashing frequency.
	/// </param>
	public void FlashingOn(float freq)
	{
		flashingFreq = freq;
		FlashingOn();
	}
	
	/// <summary>
	/// Turn off flashing.
	/// </summary>
	public void FlashingOff()
	{
		flashing = false;
	}
	
	/// <summary>
	/// Fade in constant highlighting.
	/// </summary>
	public void ConstantOn()
	{
		// Enable constant highlighting
		constantly = true;
		// Start transition
		transitionActive = true;
	}
	
	/// <summary>
	/// Fade in constant highlighting with given color.
	/// </summary>
	/// <param name='color'>
	/// Constant highlighting color.
	/// </param>
	public void ConstantOn(Color color)
	{
		// Set constant highlighting color
		constantColor = color;
		ConstantOn();
	}
	
	/// <summary>
	/// Fade out constant highlighting.
	/// </summary>
	public void ConstantOff()
	{
		// Disable constant highlighting
		constantly = false;
		// Start transition
		transitionActive = true;
	}
	
	/// <summary>
	/// Turn on constant highlighting immediately (without fading in).
	/// </summary>
	public void ConstantOnImmediate()
	{
		constantly = true;
		// Set transition value to 1
		transitionValue = 1f;
		// Stop transition
		transitionActive = false;
	}
	
	/// <summary>
	/// Turn on constant highlighting with given color immediately (without fading in).
	/// </summary>
	/// <param name='color'>
	/// Constant highlighting color.
	/// </param>
	public void ConstantOnImmediate(Color color)
	{
		// Set constant highlighting color
		constantColor = color;
		ConstantOnImmediate();
	}
	
	/// <summary>
	/// Turn off constant highlighting immediately (without fading out).
	/// </summary>
	public void ConstantOffImmediate()
	{
		constantly = false;
		// Set transition value to 0
		transitionValue = 0f;
		// Stop transition
		transitionActive = false;
	}
	
	/// <summary>
	/// Turn off all types of highlighting. 
	/// </summary>
	public void Off()
	{
		// Turn off all types of highlighting
		once = false;
		flashing = false;
		constantly = false;
		// Set transition value to 0
		transitionValue = 0f;
		// Stop transition
		transitionActive = false;
	}
	
	/// <summary>
	/// Turn off all types of highlighting in this frame, and kill this HighlightableObject component.
	/// </summary>
	public void Die()
	{
		// Set dying flag
		isDying = true;
	}
	#endregion
	
	
	#region Private Methods
	/// <summary>
	/// Materials initialisation.
	/// </summary>
	private void InitMaterials()
	{
		if (currentState)
		{
			MaterialsOff();
		}
		
		highlightableMaterials = new List<HighlightingMaterialCache>();
		
		MeshRenderer[] mr = tr.GetComponentsInChildren<MeshRenderer>();
		AddMaterials(mr);
		
		SkinnedMeshRenderer[] smr = tr.GetComponentsInChildren<SkinnedMeshRenderer>();
		AddMaterials(smr);
		
		ClothRenderer[] cr = tr.GetComponentsInChildren<ClothRenderer>();
		AddMaterials(cr);
		
		currentState = false;
		materialsIsDirty = false;
		currentColor = Color.clear;
	}
	
	private void AddMaterials(Renderer[] renderers)
	{
		foreach (Renderer renderer in renderers)
		{
			Material[] materials = renderer.materials;
			
			if (materials != null && materials.Length > 0)
			{
				foreach (Material material in materials)
				{
					Shader source = material.shader;
					Shader replacement = Highlighting.GetReplacementShader(source);
					if (source != null)
					{
						if (replacement != null)
							highlightableMaterials.Add(new HighlightingMaterialCache(material, source, replacement));
						#if UNITY_EDITOR
						else 
							Debug.LogWarning("HighlightableObject : InitMaterials() : Can't find replacement shader for source shader with name " + source.name + ". You must adapt this shader in order to make materials using this shader highlightable (see documentation).");
						#endif
					}
				}
			}
		}
	}
	
	/// <summary>
	/// Switch shaders for highlighting materials and set highlighting color.
	/// </summary>
	private void MaterialsOn()
	{
		currentState = true;
		
		if (highlightableMaterials == null)
			return;
		
		foreach (HighlightingMaterialCache hmc in highlightableMaterials)
		{
			hmc.material.shader = hmc.replacementShader;
		}
	}
	
	/// <summary>
	/// Restore shaders from cache.
	/// </summary>
	private void MaterialsOff()
	{
		currentState = false;
		
		if (highlightableMaterials == null)
			return;
		
		foreach (HighlightingMaterialCache hmc in highlightableMaterials)
		{
			hmc.material.shader = hmc.sourceShader;
		}
	}
	
	/// <summary>
	/// Update highlighting color to a given value.
	/// </summary>
	/// <param name='c'>
	/// New highlighting color.
	/// </param>
	private void SetColor(Color c)
	{
		if (currentColor == c)
			return;
		
		foreach (HighlightingMaterialCache hmc in highlightableMaterials)
		{
			hmc.material.SetColor("_Outline", c);
		}
		
		currentColor = c;
	}
	
	private void HighlightingUpdate()
	{
		// If dying - return old materials if needed, then die
		if (isDying)
		{
			if (currentState)
				MaterialsOff();
			Destroy(this);
			return;
		}
		
		// Initialize new materials if needed
		if (materialsIsDirty)
			InitMaterials();
		
		bool targetState = (once || flashing || constantly || transitionActive);
		
		// Enable highlighting if needed
		if (currentState == false)
		{
			if (targetState == true)
				MaterialsOn();
		}
		// Disable highlighting if needed
		else if (targetState == false)
			MaterialsOff();
		
		UpdateColors();
		
		PerformTransition();
		
		once = false;
	}
	
	/// <summary>
	/// Set new color if needed.
	/// </summary>
	private void UpdateColors()
	{
		// Don't update colors if highlighting is disabled
		if (currentState == false)
			return;
		
		if (once)
		{
			SetColor(onceColor);
			return;
		}
		
		if (flashing)
		{
			Color c = Color.Lerp(flashingColorMin, flashingColorMax, 0.5f * Mathf.Sin(Time.realtimeSinceStartup * flashingFreq * doublePI) + 0.5f);
			SetColor(c);
			return;
		}
		
		if (transitionActive)
		{
			Color c = new Color(constantColor.r, constantColor.g, constantColor.b, constantColor.a * transitionValue);
			SetColor(c);
			return;
		}
		else if (constantly)
		{
			Color c = constantColor;
			SetColor(c);
			return;
		}
	}
	
	/// <summary>
	/// Calculate new transition value if needed.
	/// </summary>
	private void PerformTransition()
	{
		if (transitionActive == false)
			return;
		
		float targetValue = constantly ? 1f : 0f;
		
		// Is transition finished?
		if (transitionValue == targetValue)
		{
			transitionActive = false;
			return;
		}
		
		if (Time.timeScale != 0f)
		{
			// Calculating delta time untouched by Time.timeScale
			float unscaledDeltaTime = Time.deltaTime / Time.timeScale;
			
			// Calculating new transition value
			transitionValue += (constantly ? constantOnSpeed : -constantOffSpeed) * unscaledDeltaTime;
			transitionValue = Mathf.Clamp01(transitionValue);
		}
		else
			return;
	}
	#endregion
}
