using UnityEngine;

[RequireComponent(typeof(Camera))]
public class HighlightingEffect : MonoBehaviour
{
	#region Inspector Fields
	// Blur iterations
	public int iterations = 2;
	
	// Blur minimal spread
	public float blurMinSpread = 0.25f;
	
	// Blur spread per iteration
	public float blurSpread = 0.125f;
	#endregion
	
	
	#region Private Fields
	// This GameObject reference
	private GameObject go = null;
	
	// Camera for rendering stencil buffer GameObject
	private GameObject shaderCameraGO = null;
	
	// Camera for rendering stencil buffer
	private Camera shaderCamera = null;
	
	// RenderTexture with stencil buffer
	private RenderTexture rt = null;
	
	// Camera reference
	private Camera refCam = null;
	
	// Stencil shader
	private static string stencilMatString = 
@"Shader ""Stencil"" {
	Properties { _Outline (""Outline"", Color) = (1,1,1,1) _InnerTint (""Inner Tint Value"", Range (0, 1)) = 1 }
	SubShader {
		Tags { ""RenderEffect""=""Highlighted"" }
		ZWrite Off Lighting Off ColorMask RGBA Fog { Mode Off }
		Pass {
			SetTexture [_Dummy] { constantColor [_Outline] combine constant lerp(constant) previous }
			SetTexture [_Dummy] { constantColor (0,0,0,[_InnerTint]) combine previous, constant alpha }
		}
	}
	Fallback Off
}";
	
	// Blur Shader
	private static Shader _blurShader;
	private static Shader blurShader
	{
		get
		{
			if (_blurShader == null)
			{
				_blurShader = Shader.Find("Hidden/Highlighted/Blur");
			}
			return _blurShader;
		}
	}
	
	// Compositing Shader
	private static Shader _compShader;
	private static Shader compShader 
	{
		get
		{
			if (_compShader == null)
			{
				_compShader = Shader.Find("Hidden/Highlighted/Composite");
			}
			return _compShader;
		}
	}
	
	// Stencil Material
	private static Material _stencilMaterial = null;
	private static Material stencilMaterial
	{
		get
		{
			if (_stencilMaterial == null)
			{
				_stencilMaterial = new Material(stencilMatString);
				_stencilMaterial.hideFlags = HideFlags.HideAndDontSave;
				_stencilMaterial.shader.hideFlags = HideFlags.HideAndDontSave;
			}
			return _stencilMaterial;
		}
	}
	
	// Blur Material
	private static Material _blurMaterial = null;
	private static Material blurMaterial
	{
		get
		{
			if (_blurMaterial == null)
			{
				_blurMaterial = new Material(blurShader);
				_blurMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			return _blurMaterial;
		}
	}
	
	// Compositing Material
	private static Material _compMaterial = null;
	private static Material compMaterial
	{
		get
		{
			if (_compMaterial == null)
			{
				_compMaterial = new Material(compShader);
				_compMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			return _compMaterial;
		}
	}
	
	// Stencil Shader
	private static Shader stencilShader
	{
		get
		{
			return stencilMaterial.shader;
		}
	}
	#endregion
	
	
	void Awake()
	{
		go = gameObject;
		refCam = GetComponent<Camera>();
	}
	
	void OnDisable()
	{
		if (shaderCameraGO != null)
		{
			DestroyImmediate(shaderCameraGO);
		}
		
		if (_blurShader)
		{
			_blurShader = null;
		}
		
		if (_compShader)
		{
			_compShader = null;
		}
		
		if (_stencilMaterial)
		{
			DestroyImmediate(_stencilMaterial.shader);
			DestroyImmediate(_stencilMaterial);
		}
		
		if (_blurMaterial)
		{
			DestroyImmediate(_blurMaterial);
		}
		
		if (_compMaterial)
		{
			DestroyImmediate(_compMaterial);
		}
		
		if (rt != null)
		{
			RenderTexture.ReleaseTemporary(rt);
			rt = null;
		}
	}
	
	void Start()
	{
		// Disable if we don't support image effects
		if (!SystemInfo.supportsImageEffects)
		{
			Debug.LogWarning("HighlightingSystem : Image effects is not supported on this platform! Disabling.");
			this.enabled = false;
			return;
		}

		// Disable if the shader can't run on the users graphics card
		if (stencilMaterial.shader.isSupported == false)
		{
			Debug.LogWarning("HighlightingSystem : Stencil shader is not supported on this platform! Disabling.");
			this.enabled = false;
			return;
		}
		
		if (blurShader.isSupported == false)
		{
			Debug.LogWarning("HighlightingSystem : Blur shader is not supported on this platform! Disabling.");
			this.enabled = false;
			return;
		}
		
		if (compShader.isSupported == false)
		{
			Debug.LogWarning("HighlightingSystem : Compositing shader is not supported on this platform! Disabling.");
			this.enabled = false;
			return;
		}
	}
	
	// Performs one blur iteration
	public void FourTapCone(RenderTexture source, RenderTexture dest, int iteration)
	{
		float off = blurMinSpread + iteration * blurSpread;
		blurMaterial.SetFloat("_OffsetScale", off);
		Graphics.Blit(source, dest, blurMaterial);
	}
	
	// Downsamples source texture
	private void DownSample4x(RenderTexture source, RenderTexture dest)
	{
		float off = 1.0f;
		blurMaterial.SetFloat("_OffsetScale", off);
		Graphics.Blit(source, dest, blurMaterial);
	}
	
	// Render objects to the stencil buffer
	void OnPreRender()
	{
		if (this.enabled == false || go.active == false)	// TODO Upgrade this to go.activeInHierarchy for Unity 4+
			return;

		if (rt != null)
		{
			RenderTexture.ReleaseTemporary(rt);
			rt = null;
		}

		rt = RenderTexture.GetTemporary ((int)camera.pixelWidth, (int)camera.pixelHeight, 0);

		if (!shaderCameraGO)
		{
			shaderCameraGO = new GameObject("ShaderCamera", typeof(Camera));
			shaderCameraGO.camera.enabled = false;
			shaderCameraGO.hideFlags = HideFlags.HideAndDontSave;
		}
		
		if (!shaderCamera)
		{
			shaderCamera = shaderCameraGO.GetComponent<Camera>();
		}
		
		shaderCamera.CopyFrom(refCam);
		shaderCamera.rect = new Rect(0, 0, 1, 1);
		shaderCamera.renderingPath = RenderingPath.VertexLit;
		shaderCamera.hdr = false;
		shaderCamera.useOcclusionCulling = false;
		shaderCamera.depthTextureMode = DepthTextureMode.None;
		shaderCamera.backgroundColor = new Color(0, 0, 0, 0);
		shaderCamera.clearFlags = CameraClearFlags.SolidColor;
		shaderCamera.targetTexture = rt;
		shaderCamera.RenderWithShader(stencilShader, "RenderEffect");
	}
	
	// Compose final frame with highlighting
	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		// Create two buffers for blurring image
		RenderTexture buffer = RenderTexture.GetTemporary(source.width / 4, source.height / 4, 0);
		RenderTexture buffer2 = RenderTexture.GetTemporary(source.width / 4, source.height / 4, 0);
		
		// Copy source to the 4x4 smaller texture
		DownSample4x(rt, buffer);
		
		// Blur the small texture
		bool oddEven = true;
		for (int i = 0; i < iterations; i++)
		{
			if(oddEven)
				FourTapCone(buffer, buffer2, i);
			else
				FourTapCone(buffer2, buffer, i);
			
			oddEven = !oddEven;
		}
		
		// Compose
		compMaterial.SetTexture("_StencilTex", rt);
		compMaterial.SetTexture("_BlurTex", oddEven ? buffer : buffer2);
		Graphics.Blit(source, destination, compMaterial);
		
		// Cleanup
		RenderTexture.ReleaseTemporary(buffer);
		RenderTexture.ReleaseTemporary(buffer2);
		if (rt != null)
		{
			RenderTexture.ReleaseTemporary(rt);
			rt = null;
		}
	}
}