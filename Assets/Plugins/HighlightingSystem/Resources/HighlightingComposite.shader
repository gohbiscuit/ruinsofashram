Shader "Hidden/Highlighted/Composite"
{
	Properties
	{
		_MainTex ("", 2D) = "" {}
		_BlurTex ("", 2D) = "" {}
		_StencilTex ("", 2D) = "" {}
	}
	
	SubShader
	{
		Pass
		{
			ZTest Always
			Cull Off
			ZWrite Off
			Lighting Off
			Fog { Mode off }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "UnityCG.cginc"
			
			struct v2f
			{
				float4 pos : POSITION;
				float2 uv[2] : TEXCOORD0;
			};
			
			sampler2D _MainTex;
			sampler2D _BlurTex;
			sampler2D _StencilTex;
			
			float4 _MainTex_TexelSize;
			
			v2f vert (appdata_img v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				
				o.uv[0] = v.texcoord.xy;
				o.uv[1] = v.texcoord.xy;
				
				if (_MainTex_TexelSize.y < 0)
					o.uv[1].y = 1 - o.uv[1].y;
				
				return o;
			}
			
			half4 frag(v2f i) : COLOR
			{
				half4 color = tex2D(_MainTex, i.uv[0]);
				half stencil = 1 - tex2D(_StencilTex, i.uv[1]).a;
				half4 blurred  = tex2D(_BlurTex, i.uv[1]);
				color.rgb += blurred.rgb * 2 * stencil;
				
				return color;
			}
			
			ENDCG
		}
	}
	
	SubShader
	{
		Pass
		{
			SetTexture [_MainTex] {}
		}
	}
	
	Fallback Off
}