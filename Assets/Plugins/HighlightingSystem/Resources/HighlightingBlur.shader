Shader "Hidden/Highlighted/Blur"
{
	Properties
	{
		_MainTex ("", 2D) = "" {}
	}
	
	SubShader
	{
		Pass
		{
			ZTest Always
			Cull Off
			ZWrite Off
			Lighting Off
			Fog { Mode Off }
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "UnityCG.cginc"
			
			uniform sampler2D _MainTex;
			uniform float4 _MainTex_TexelSize;
			uniform float _OffsetScale;
			
			struct v2f
			{
				float4 pos : POSITION;
				float2 uv[4] : TEXCOORD0;
			};
			
			v2f vert (appdata_img v)
			{
				v2f o;
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				
				float2 uv = v.texcoord.xy;
				
				float2 up = float2(0.0, _MainTex_TexelSize.y) * _OffsetScale;
				float2 right = float2(_MainTex_TexelSize.x, 0.0) * _OffsetScale;
				
				o.uv[0] = uv - up - right;
				o.uv[1] = uv + up - right;
				o.uv[2] = uv + up + right;
				o.uv[3] = uv - up + right;
				
				return o;
			}
			
			half4 frag(v2f i) : COLOR
			{
				half4 color = tex2D(_MainTex, i.uv[0]) * 0.25;
				color += tex2D(_MainTex, i.uv[1]) * 0.25;
				color += tex2D(_MainTex, i.uv[2]) * 0.25;
				color += tex2D(_MainTex, i.uv[3]) * 0.25;
				
				return color;
			}
			
			ENDCG
		}
	}
	
	Fallback off
}