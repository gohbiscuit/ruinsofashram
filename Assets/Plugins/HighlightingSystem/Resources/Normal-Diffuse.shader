Shader "Hidden/Highlighted/Diffuse" {
Properties {
	_Color ("Main Color", Color) = (1,1,1,1)
	_MainTex ("Base (RGB)", 2D) = "white" {}
	
	_Outline ("Outline", Color) = (1,1,1,1)
	_InnerTint ("Inner Tint Value", Range (0, 1)) = 1
}
SubShader {
	Tags { "RenderType"="Opaque" "RenderEffect"="Highlighted" }
	LOD 200

CGPROGRAM
#pragma surface surf Lambert

sampler2D _MainTex;
fixed4 _Color;

struct Input {
	float2 uv_MainTex;
};

void surf (Input IN, inout SurfaceOutput o) {
	fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
	o.Albedo = c.rgb;
	o.Alpha = c.a;
}
ENDCG
}

Fallback "VertexLit"
}
