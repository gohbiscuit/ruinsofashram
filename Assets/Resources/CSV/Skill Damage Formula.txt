Syntax Explanation (edit in Skill.csv)

LHS of the following are the only things you can type in the formula
HP = HP stat
MP = MP stat
HPR = HP_REGEN stat
MPR = MP_REGEN stat
PA = PHY_ATK stat
MA = MAGIC_ATK stat
PD = PHY_DEF stat
MD = MAGIC_DEF stat
EVA = EVASION stat
MOV = MOVEMENT_SPEED stat
SKI = SKILL_MASTERY stat
"RANGE(x, y) = random number between x and y (can be -ve)"
"+, -, +, /"
0-9
# = TARGET's stats (see below for explanation)

Adding # to the stat makes it the TARGET's stats
eg : 10 + 10*PA - 5*#PD
means dmg = 10 + 10 * PHY_ATK of attacker - 5 * PHY_DEF of target

"you can use parenthesis, whitespaces and even sine or cosine
BUT you can't use expression inside RANGE(x,y). x & y must be numbers.

The minimum value is 0. The same goes for healing skills.

"for some special skills like chain lightning where damage decreases the longer the chain,"
the change in value will be hardcoded and cant be tweaked via the formula




Unpossessed form (Every Level Increases Wisp base Health + 50, Mana + 50)						
Wisp base stats: 100 health, 100 mana						
Level 1 base stats starts from 5						
Each point add in stats e.g. Strength will + 5 in that attribute						
Armor: 10 (2% damage reduction)						
Magic Defense: 10 (5% damage reduction)						
Attack Speed: N/A						
Movement Speed: 20						
Level Up: + HP 50, MP + 50						
						
Stats Distribution						
+1 strength: each strength will increase 1 warrior base damage by 1, max HP + 10, 1 physical armor, 0.01 HP regen 						
+1 agility: each agility will increase archer/rogue base damage by 0.8, 1 attack speed, 1 movement speed						
+1 intelligence: each intelligence will increase mage/warlock/paladin/healer base damage by 0.6, max MP +10, magic defense + 1, + 0.01 MP regen						
+1 spell ability: increase all levels of the spell						
						
APPENDIX:						
Magic Defense: 1 magic defense = +0.5% magic damage reduction						
Armor: 1 point in armor = +0.2% damage reduction						
Attack Speed: 1 agility = 1 attack speed, 0 to 20 (Slow), 21 to 40 (moderate), 41 to 60 (fast), 61 to infinity (very fast)						
Movespeed: 1 agility = 1 movement speed, 0 to 20 (Slow), 21 to 40 (moderate), 41 to 60 (fast), 61 to infinity (very fast)						
