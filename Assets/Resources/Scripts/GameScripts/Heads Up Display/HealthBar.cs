﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Health bar for player HUD interface 
/// </summary>
public class HealthBar : MonoBehaviour {

	UISlider slider = null;
	[SerializeField] UILabel m_healthText;
	[SerializeField] UILabel m_regenText;

	private float maxHP;
	private float currentHP;
	private double hpRegen;

	// Use this for initialization
	void Awake () {
		slider = GetComponent<UISlider>();
	}

	// Update is called once per frame
	void Update () 
	{
		float fillPercent = currentHP/maxHP;
		slider.sliderValue = fillPercent;
		
		m_healthText.text = Mathf.CeilToInt(currentHP).ToString() + " / " + Mathf.CeilToInt(maxHP).ToString();
		m_regenText.text = "+ " + hpRegen.ToString ();
	}

	/// <summary>
	/// Sets the max health that is invoked by HUDHandler.cs
	/// </summary>
	/// <param name="health">Health.</param>
	public void SetMaxHealth(float health)
	{
		maxHP = health;
	}

	/// <summary>
	/// Sets the current health of the player
	/// </summary>
	/// <param name="currentHealth">Current health.</param>
	public void SetCurrentHealth(float currentHealth)
	{
		currentHP = currentHealth;
	}

	/// <summary>
	/// Sets the HP regen amount of the player
	/// </summary>
	/// <param name="hpRegen">Hp regen.</param>
	public void SetHPRegen(double hpRegen)
	{
		this.hpRegen = hpRegen;
	}
}
