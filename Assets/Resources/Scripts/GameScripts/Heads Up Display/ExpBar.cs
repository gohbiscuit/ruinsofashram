﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Responsible for displaying experience bar for every creature
/// </summary>
public class ExpBar : MonoBehaviour {

	UISlider slider = null;

	float maxExp;
	float currentExp;
	private string tooltipText = "NULL";

	// Use this for initialization
	void Awake () {
		slider = GetComponent<UISlider>();
	}


	public void SetMaxEXP(int expToNextLevel)
	{
		maxExp = (float) expToNextLevel;
	}

	public void SetCurrentExp(int currentExp)
	{
		this.currentExp = (float) currentExp;
	}

	public void UpdateExpDisplay()
	{
		float fillPercent = currentExp/maxExp;
		slider.sliderValue = fillPercent;
	}


	/// <summary>
	/// Shows the status tool tip.
	/// </summary>
	void ShowStatusToolTip()
	{
		tooltipText = "Experience (" + currentExp + " / " + maxExp +")";
		UITooltip.ShowText (tooltipText);
	}
	
	/// <summary>
	/// Hides the status tool tip.
	/// </summary>
	void HideStatusToolTip()
	{
		UITooltip.ShowText (null);
	}
	
	/// <summary>
	/// Raises the hover event.
	/// </summary>
	/// <param name="isOver">If set to <c>true</c> is over.</param>
	void OnHover( bool isOver )
	{
		// Roll Out
		if( isOver )
		{
			ShowStatusToolTip();
		}
		else
		{
			HideStatusToolTip();
		}
	}
}
