﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Stop button handler class that in-charge of functionality when user press "H" or clicked on the icon
/// </summary>
public class StopButtonHandler : MonoBehaviour {

	[SerializeField] CustomCursor m_cursor;

	// Use this for initialization
	void Start () {
	
	}

	void OnClick()
	{
		DoStopPlayer();
	}

	void DoStopPlayer()
	{
		PlayerCommand.Instance.StopPlayer();
	}
	
	// Update is called once per frame
	void Update () 
	{
		// Player Presses Stop
		if (Input.GetKeyDown(KeyCode.H))
		{
			DoStopPlayer();
		}
	}
}
