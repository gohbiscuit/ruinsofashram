﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Attack button, responsible for setting the cursor of the mouse when user press "A" or click on attack icon
/// </summary>
public class AttackButtonHandler : MonoBehaviour {

	[SerializeField] CustomCursor m_cursor;

	// Use this for initialization
	void Start () {
	
	}

	void OnClick()
	{
		m_cursor.SetToAttack();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			m_cursor.SetToAttack();
		}
	}
}
