﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Loading class that handles the loading progress bar
/// </summary>
public class Loading : MonoBehaviour {

	// just put your loading picture in front of all
	// your HUD in the UI Root

	[SerializeField] GameObject m_loadingPanel;
	[SerializeField] UISlider m_loadingBar;
	[SerializeField] GameObject m_minimap;

	[SerializeField] Creator m_creatorScript;

	bool isSoundPlayed = false;

	/// <summary>
	/// Init the loading panel components
	/// </summary>
	void Start()
	{
		if(!PhotonNetwork.offlineMode)
		{
			m_loadingPanel.SetActive(true);
		}
	}

	/// <summary>
	/// Update the progress bar when loading
	/// </summary>
	void Update () {

		// so if your loading bar is x units wide,
		// then the loading bar should be length (currRound/60)*x
		// currRound will increment gradually

		// when currRound/60 == 1 (meaning round 60),
		// then loading is considered complete

		// yes 60 is a magic number, but that's normally how long
		// it 'jerks' before it smoothens out.

		float percent = GameNetworkManager.Instance.CurrentRound/60.0f;
		m_loadingBar.sliderValue = percent;

		//Debug.Log("Loading : " + percent);

		if(isSoundPlayed == false && !PhotonNetwork.offlineMode)
		{
			AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/PVPWARNING (Loading Screen)", 0.8f);
			isSoundPlayed = true;
		}

		if(GameNetworkManager.Instance.CurrentRound >= 60) 
		{
			// Plays the background music
			m_creatorScript.PlayMusic();

			if(GameNetworkManager.Instance.CurrentScene == GameNetworkManager.Instance.GameScene)
			{
				PlayerCommand.Instance.DisplayRuinsOfAshram();
			}
			else
			{
				PlayerCommand.Instance.DisplayCryptsOfAshram();
			}

			// fade out super weird, so just destroy
			Destroy(m_loadingPanel);
			Destroy(gameObject);
			m_minimap.SetActive(true);
		}
	}
}
