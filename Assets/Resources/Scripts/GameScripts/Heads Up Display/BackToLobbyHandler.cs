﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class that is in-charge of event handling when the Back to lobby button is being pressed
/// </summary>
public class BackToLobbyHandler : MonoBehaviour 
{
	void OnClick()
	{
		gameObject.GetComponent<UIButton>().isEnabled = false;

		// means we are in a room
		if(PhotonNetwork.room != null)
		{
			CleanUpNetworkObjects();
			PhotonNetwork.LeaveRoom();
		}
		else
		{
			PhotonNetwork.LoadLevel("Lobby");
		}
	}

	void CleanUpNetworkObjects()
	{
		GameObject localNetworkManager = GameObject.Find ("NetworkManager");
		GameObject networkStream = GameObject.Find ("NetworkOutputStream");

		if(localNetworkManager != null)
			Destroy(localNetworkManager);
	}

	void OnLeftRoom()
	{
		// go back to lobby
		PhotonNetwork.LoadLevel("Lobby");
	}

}
