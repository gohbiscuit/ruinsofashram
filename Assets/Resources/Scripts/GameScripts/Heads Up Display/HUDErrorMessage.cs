﻿using UnityEngine;
using System.Collections;

/// <summary>
/// HUD error message is responsible for displaying error message for Spell-related classes
/// </summary>
public class HUDErrorMessage : MonoBehaviour {

	public GameObject errorTextPrefab;
	HUDText m_errorText = null;

	// Use this for initialization
	void Start () 
	{
		// We need the HUD object to know where in the hierarchy to put the element
		if (HUDRoot.go == null)
		{
			GameObject.Destroy(this);
			return;
		}
		
		GameObject errorTextChild = NGUITools.AddChild(HUDRoot.go, errorTextPrefab);
		m_errorText = errorTextChild.GetComponentInChildren<HUDText>();

		errorTextChild.transform.localPosition = new Vector3(100f, -250f, 0f);
	}


	/// <summary>
	/// Popup the error message e.g. "Skill is still on cooldown" etc.
	/// </summary>
	/// <param name="message">Message.</param>
	public void AddErrorMessage(string message)
	{
		// Add an empty string to make sure it is not numeric, so the text will not keep appending
		m_errorText.Add(message, Color.red, 1f);
	}
}
