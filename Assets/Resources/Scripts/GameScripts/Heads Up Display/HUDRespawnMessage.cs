﻿using UnityEngine;
using System.Collections;

public class HUDRespawnMessage : MonoBehaviour 
{
	public GameObject respawnTextPrefab;
	HUDText m_respawnText = null;
	
	// Use this for initialization
	void Start () 
	{
		// We need the HUD object to know where in the hierarchy to put the element
		if (HUDRoot.go == null)
		{
			GameObject.Destroy(this);
			return;
		}
		
		GameObject respawnTextChild = NGUITools.AddChild(HUDRoot.go, respawnTextPrefab);
		m_respawnText = respawnTextChild.GetComponentInChildren<HUDText>();
		
		respawnTextChild.transform.localPosition = new Vector3(-650f, -270f, 0f);
	}
	

	/// <summary>
	/// Adds the respawn text.
	/// </summary>
	/// <param name="message">Message.</param>
	public void AddRespawnText(string message)
	{
		m_respawnText.Add(message, getRandomColor(), 0f);
	}

	/// <summary>
	/// Adds the kill text, when a player kill another player
	/// </summary>
	/// <param name="message">Message.</param>
	public void AddKillText(string message)
	{
		m_respawnText.Add(message, getRandomColor(), 2f);
	}

	/// <summary>
	/// Gets the random color.
	/// </summary>
	/// <returns>The random color.</returns>
	public static Color getRandomColor () 
	{
		return new Color (Random.Range (0f, 255f)/255f, Random.Range (0f, 255f)/255f, Random.Range (0f, 255f)/255f );  
	}
}
