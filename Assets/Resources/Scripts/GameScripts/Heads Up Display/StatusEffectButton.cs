﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Status effect button that is in-charge of displaying status effect status on the HUD e.g. Slow, Stun, Mana-Shield etc
/// </summary>
public class StatusEffectButton : MonoBehaviour 
{
	public float maxCooldown;
	public float currentCooldown;
	UISlider silder = null;
	UILabel label = null;

	private string tooltipText = "NULL";

	void Awake () 
	{
		silder = GetComponent<UISlider>();
		label = GetComponentInChildren<UILabel>();
	}

	/// <summary>
	/// Sets the sprite.
	/// </summary>
	/// <param name="spriteName">Sprite name.</param>
	public void setSprite(string spriteName)
	{
		UISprite foregroundSprite = Utility.FindChild(gameObject, "Foreground").GetComponent<UISprite>();
		UISprite backgroundSprite = Utility.FindChild(gameObject, "Background").GetComponent<UISprite>();

		foregroundSprite.spriteName = spriteName;
		backgroundSprite.spriteName = spriteName;
	}

	/// <summary>
	/// Sets the current cooldown.
	/// </summary>
	/// <param name="cooldown">Cooldown.</param>
	public void setCurrentCooldown(float cooldown)
	{
		this.currentCooldown = cooldown;
		if(maxCooldown > 0f)
		{
			//float percent = 1f - currentCooldown/maxCooldown;
			//silder.sliderValue = percent;
			if(label != null)
			{
				label.text = Mathf.CeilToInt(currentCooldown).ToString() + "s";
			}
		}
		else
		{
			if(label != null)
				label.text = "";
		}
	}

	/// <summary>
	/// Sets the name.
	/// </summary>
	/// <param name="statusName">Status name.</param>
	public void SetName(string statusName)
	{
		tooltipText = TextUtil.STATS_TOOLTIP_DESC_COLOR_ + statusName;
	}


	/// <summary>
	/// Shows the status tool tip.
	/// </summary>
	void ShowStatusToolTip()
	{
		UITooltip.ShowText (tooltipText);
	}

	/// <summary>
	/// Hides the status tool tip.
	/// </summary>
	void HideStatusToolTip()
	{
		UITooltip.ShowText (null);
	}

	/// <summary>
	/// Raises the hover event.
	/// </summary>
	/// <param name="isOver">If set to <c>true</c> is over.</param>
	void OnHover( bool isOver )
	{
		// Roll Out
		if( isOver )
		{
			ShowStatusToolTip();
		}
		else
		{
			HideStatusToolTip();
		}
	}
}
