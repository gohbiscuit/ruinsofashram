﻿using UnityEngine;
using System.Collections;

public class MoveParticleFixPosition : MonoBehaviour {

	public Vector3 mouseClickedPoint;

	/// <summary>
	/// Lates the update. A hack to fix the position of the game object even when camera moves, it does not render that point
	/// </summary>
	void LateUpdate () 
	{
		Vector3 pos = Camera.main.WorldToViewportPoint(mouseClickedPoint);
		pos = UICamera.currentCamera.ViewportToWorldPoint(pos);
		pos.z = 0;
		
		if(gameObject != null)
		{
			gameObject.transform.position = pos;
		}
	}
}
