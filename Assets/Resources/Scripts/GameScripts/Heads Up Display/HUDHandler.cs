using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// HUD handler is the core class that handles the displaying of player information such as avatar picture, health-bars, skills, stats, attributes etc.
/// </summary>
public class HUDHandler : MonoBehaviour {

	// HUD Skin
	[SerializeField] UISprite m_hudSkin;

	// Avatar Panel
	[SerializeField] UISprite m_avatarPanel;
	[SerializeField] UILabel m_raceLabel;
	[SerializeField] UILabel m_classLabel;
	[SerializeField] UILabel m_levelLabel;
	
	// Stats Panel
	[SerializeField] UILabel m_attackLabel;
	[SerializeField] UILabel m_defLabel;
	[SerializeField] UILabel m_magicDefLabel;
	[SerializeField] UILabel m_moveSpeedLabel;

	// Attributes Panel
	[SerializeField] GameObject m_statsStrength;
	[SerializeField] GameObject m_statsAgility;
	[SerializeField] GameObject m_statsIntelligence;
	[SerializeField] GameObject m_statsMagic;

	// Skills Bar Panel
	[SerializeField] GameObject m_skillButtonGO1;
	[SerializeField] GameObject m_skillButtonGO2;
	[SerializeField] GameObject m_skillButtonGO3;
	[SerializeField] GameObject m_skillButtonGO4;

	// Health Bars
	[SerializeField] HealthBar m_healthBar;
	[SerializeField] ManaBar m_manaBar;

	// Exp Bars
	[SerializeField] ExpBar m_expBar;

	// Time Left
	[SerializeField] UILabel m_timeLabel;

	[SerializeField] CustomCursor m_cursor;
	[SerializeField] RtsCamera m_camera;
	[SerializeField] KGFMapSystem m_minimap;

	//Scoreboard
	[SerializeField] GameObject m_scoreBoardGO;
	[SerializeField] GameObject m_scoreTemplate_1;
	[SerializeField] GameObject m_scoreTemplate_2;
	[SerializeField] GameObject m_scoreTemplate_3;
	[SerializeField] GameObject m_scoreTemplate_4;
	[SerializeField] GameObject m_scoreTemplate_5;
	[SerializeField] GameObject m_scoreTemplate_6;
	[SerializeField] GameObject m_scoreTemplate_7;
	[SerializeField] GameObject m_scoreTemplate_8;
	[SerializeField] UISprite m_gameModeTitle;
	[SerializeField] GameObject m_backToLobbyButton;

	private List<GameObject> m_scoreTemplateList = new List<GameObject>();

	// Skills
	private SkillButton m_skillButton1;
	private SkillButton m_skillButton2;
	private SkillButton m_skillButton3;
	private SkillButton m_skillButton4;

	private UISprite m_skillSprite1_Foreground;
	private UISprite m_skillSprite1_Background;

	private UISprite m_skillSprite2_Foreground;
	private UISprite m_skillSprite2_Background;

	private UISprite m_skillSprite3_Foreground;
	private UISprite m_skillSprite3_Background;

	private UISprite m_skillSprite4_Foreground;
	private UISprite m_skillSprite4_Background;
	
	// Attributes
	private List<UISprite> m_strength_list = new List<UISprite>();
	private UISprite m_strength1;
	private UISprite m_strength2;
	private UISprite m_strength3;
	private UISprite m_strength4;
	private UISprite m_strength5;

	private List<UISprite> m_agility_list = new List<UISprite>();
	private UISprite m_agility1;
	private UISprite m_agility2;
	private UISprite m_agility3;
	private UISprite m_agility4;
	private UISprite m_agility5;

	private List<UISprite> m_intelligence_list = new List<UISprite>();
	private UISprite m_intelligence1;
	private UISprite m_intelligence2;
	private UISprite m_intelligence3;
	private UISprite m_intelligence4;
	private UISprite m_intelligence5;

	private List<UISprite> m_magic_list = new List<UISprite>();
	private UISprite m_magic1;
	private UISprite m_magic2;
	private UISprite m_magic3;
	private UISprite m_magic4;
	private UISprite m_magic5;

	const string ATTRIBUTES_STRENGTH_SPRITENAME = "Strength_AttributeBox";
	const string ATTRIBUTES_AGILITY_SPRITENAME = "Agility_AttributeBox";
	const string ATTRIBUTES_INTELLIGENCE_SPRITENAME = "Int_AttributeBox";
	const string ATTRIBUTES_MAGIC_SPRITENAME = "Spell_AttributeBox";
	const string ATTRIBUTES_DEFAULT_SPRITENAME = "Default_AttributeBox";

	// Potrait Pictures
	const string BLANK_PROFILE = "Blank"; 
	const string WISP = "Wisp"; 
	const string OGRE_WARRIOR = "Ogre_Warrior"; 
	const string OGRE_ROGUE = "Ogre_Rogue";
	const string OGRE_PRIEST = "Ogre_Priest"; 
	const string OGRE_MAGE = "Ogre_Mage"; 
	
	const string RATKIN_WARRIOR = "Ratkin_Warrior"; 
	const string RATKIN_MAGE = "Ratkin_Mage";
	const string RATKIN_GUNNER = "Ratkin_Gunner"; 
	const string RATKIN_SHAMAN = "Ratkin_Shaman"; 

	private CreatureRace currentRace;
	private CreatureClass currentClass;

	private bool isRTSCamera = false;				// if false, camera is RPG camera e.g. follow

	private Color blueTeamColor = new Color(114f/255f, 168f/255f, 255f/255f);
	private Color redTeamColor = new Color(255f/255f, 84f/255f, 95f/255f);


	// Attributs for SkillButton.cs class to access
	private Skill selectedSkill;
	private Skill skillPossessRelease;
	private Skill skill_1;
	private Skill skill_2;
	private Skill skill_3;
	private int skillLevel;
	private Creature playerCreature = null;

	// Use this for initialization
	void Start () 
	{
		InitHUDSkinCollider();	// add collider on runtime to fit the sprite
		InitAttributes();
		InitPlayerGUI();
		InitSkills();
		InitScoreTemplate();
	}

	void InitScoreTemplate()
	{
		m_scoreTemplateList.Add(m_scoreTemplate_1);
		m_scoreTemplateList.Add(m_scoreTemplate_2);
		m_scoreTemplateList.Add(m_scoreTemplate_3);
		m_scoreTemplateList.Add(m_scoreTemplate_4);
		m_scoreTemplateList.Add(m_scoreTemplate_5);
		m_scoreTemplateList.Add(m_scoreTemplate_6);
		m_scoreTemplateList.Add(m_scoreTemplate_7);
		m_scoreTemplateList.Add(m_scoreTemplate_8);
	}

	/// <summary>
	/// Inits the HUD skin collider.
	/// </summary>
	void InitHUDSkinCollider()
	{
		NGUITools.AddWidgetCollider(m_hudSkin.gameObject);
	}

	/// <summary>
	/// Inits the player GUI
	/// </summary>
	void InitPlayerGUI()
	{
		playerCreature = PlayerCommand.Instance.getControlledCreature();
		currentClass = CreatureClass.MAGE;		// set to class race so HUD will get updated
		currentRace = CreatureRace.DEMON;		// set to random race so HUD will get updated
	}

	/// <summary>
	/// Inits the skills panel
	/// </summary>
	void InitSkills()
	{
		m_skillSprite1_Background = Utility.FindChild(m_skillButtonGO1, "Skill Slider/Background").GetComponent<UISprite>();
		m_skillSprite1_Foreground = Utility.FindChild(m_skillButtonGO1, "Skill Slider/Foreground").GetComponent<UISprite>();

		m_skillSprite2_Background = Utility.FindChild(m_skillButtonGO2, "Skill Slider/Background").GetComponent<UISprite>();
		m_skillSprite2_Foreground = Utility.FindChild(m_skillButtonGO2, "Skill Slider/Foreground").GetComponent<UISprite>();

		m_skillSprite3_Background = Utility.FindChild(m_skillButtonGO3, "Skill Slider/Background").GetComponent<UISprite>();
		m_skillSprite3_Foreground = Utility.FindChild(m_skillButtonGO3, "Skill Slider/Foreground").GetComponent<UISprite>();

		m_skillSprite4_Background = Utility.FindChild(m_skillButtonGO4, "Skill Slider/Background").GetComponent<UISprite>();
		m_skillSprite4_Foreground = Utility.FindChild(m_skillButtonGO4, "Skill Slider/Foreground").GetComponent<UISprite>();

		m_skillButton1 = m_skillButtonGO1.GetComponent<SkillButton>();
		m_skillButton2 = m_skillButtonGO2.GetComponent<SkillButton>();
		m_skillButton3 = m_skillButtonGO3.GetComponent<SkillButton>();
		m_skillButton4 = m_skillButtonGO4.GetComponent<SkillButton>();
	}

	/// <summary>
	/// Inits the attributes panel
	/// </summary>
	void InitAttributes()
	{
		m_strength1 = Utility.FindChild(m_statsStrength, "Strength_1").GetComponent<UISprite>();
		m_strength2 = Utility.FindChild(m_statsStrength, "Strength_2").GetComponent<UISprite>();
		m_strength3 = Utility.FindChild(m_statsStrength, "Strength_3").GetComponent<UISprite>();
		m_strength4 = Utility.FindChild(m_statsStrength, "Strength_4").GetComponent<UISprite>();
		m_strength5 = Utility.FindChild(m_statsStrength, "Strength_5").GetComponent<UISprite>();
		m_strength_list.Add(m_strength1);
		m_strength_list.Add(m_strength2);
		m_strength_list.Add(m_strength3);
		m_strength_list.Add(m_strength4);
		m_strength_list.Add(m_strength5);

		m_agility1 = Utility.FindChild(m_statsAgility, "Agility_1").GetComponent<UISprite>();
		m_agility2 = Utility.FindChild(m_statsAgility, "Agility_2").GetComponent<UISprite>();
		m_agility3 = Utility.FindChild(m_statsAgility, "Agility_3").GetComponent<UISprite>();
		m_agility4 = Utility.FindChild(m_statsAgility, "Agility_4").GetComponent<UISprite>();
		m_agility5 = Utility.FindChild(m_statsAgility, "Agility_5").GetComponent<UISprite>();
		m_agility_list.Add (m_agility1);
		m_agility_list.Add (m_agility2);
		m_agility_list.Add (m_agility3);
		m_agility_list.Add (m_agility4);
		m_agility_list.Add (m_agility5);

		m_intelligence1 = Utility.FindChild(m_statsIntelligence, "Intelligence_1").GetComponent<UISprite>();
		m_intelligence2 = Utility.FindChild(m_statsIntelligence, "Intelligence_2").GetComponent<UISprite>();
		m_intelligence3 = Utility.FindChild(m_statsIntelligence, "Intelligence_3").GetComponent<UISprite>();
		m_intelligence4 = Utility.FindChild(m_statsIntelligence, "Intelligence_4").GetComponent<UISprite>();
		m_intelligence5 = Utility.FindChild(m_statsIntelligence, "Intelligence_5").GetComponent<UISprite>();
		m_intelligence_list.Add (m_intelligence1);
		m_intelligence_list.Add (m_intelligence2);
		m_intelligence_list.Add (m_intelligence3);
		m_intelligence_list.Add (m_intelligence4);
		m_intelligence_list.Add (m_intelligence5);

		m_magic1 = Utility.FindChild(m_statsMagic, "Magic_1").GetComponent<UISprite>();
		m_magic2 = Utility.FindChild(m_statsMagic, "Magic_2").GetComponent<UISprite>();
		m_magic3 = Utility.FindChild(m_statsMagic, "Magic_3").GetComponent<UISprite>();
		m_magic4 = Utility.FindChild(m_statsMagic, "Magic_4").GetComponent<UISprite>();
		m_magic5 = Utility.FindChild(m_statsMagic, "Magic_5").GetComponent<UISprite>();
		m_magic_list.Add (m_magic1);
		m_magic_list.Add (m_magic2);
		m_magic_list.Add (m_magic3);
		m_magic_list.Add (m_magic4);
		m_magic_list.Add (m_magic5);
	}

	/// <summary>
	/// Raises the GUI event, and checks the creature HUD every frame
	/// </summary>
	void OnGUI()
	{
		UpdateRoundTime();
	
		playerCreature = PlayerCommand.Instance.getControlledCreature();

		if(playerCreature == null)
		{
			SetBlankPotrait();
			return;
		}

		UpdateHealthPanel(playerCreature);
		UpdateSkillCooldownGUI(playerCreature);

		bool shouldUpdateHUD = CheckHUDUpdated(playerCreature);		// If a player possess a new creature, shouldUpdateHUD will return true
		if(shouldUpdateHUD == false)
		{
			return;
		}

		UpdateCameraFollowTarget(playerCreature);
		UpdateSelectionMarker(playerCreature);		
		UpdateFogOfWar(playerCreature);
		UpdatePotrait(playerCreature.CRace, playerCreature.CClass);
		UpdateAttributes(playerCreature);
		UpdateStatsPanel(playerCreature);
		UpdateSkillsBar(playerCreature);
	}

	/// <summary>
	/// Handles the switching of camera, checking it every frame
	/// </summary>
	Creature prevTarget;
	void Update()
	{
		Creature currControlledCreature = PlayerCommand.Instance.getControlledCreature();
		if(currControlledCreature != prevTarget){

			// force it to fly to the new target when player dies/possess/release
			isRTSCamera = false;
			prevTarget = currControlledCreature;
			UpdateCameraFollowTarget(currControlledCreature);
		}

		// Check if player press F2
		if(Input.GetKeyDown(KeyCode.F1))
		{
			isRTSCamera = !isRTSCamera;
			UpdateCameraFollowTarget(playerCreature);
		}
		// Reset Camera Orientation
		else if(Input.GetKeyDown(KeyCode.F2))
		{
			ResetCameraOrientation();
		}
	}

	void ResetCameraOrientation()
	{
		if(m_camera != null)
			m_camera.ResetToInitialValues(false);
	}


	/// <summary>
	/// Updates the round time and shows the scoreboard if round ends
	/// </summary>
	void UpdateRoundTime()
	{
		int timeLeftInSeconds = GameNetworkManager.Instance.getRoundTime();	

		if(timeLeftInSeconds >= 0)
		{
			m_timeLabel.text = string.Format("{0:00}:{1:00}", timeLeftInSeconds/60, timeLeftInSeconds % 60);		// format in minutes:seconds
		}

		if(timeLeftInSeconds <= 0)
		{
			// pause the game (not sure if it'll affect ngui buttons)
			Time.timeScale = 0;

			// show scoreboard (some error abt control 0's will appear
			//					but that's an OnGUI problem, if you use NGUI, it shouldn't have a problem)
			PlayerCommand.Instance.showEndScoreBoard();

			// display win or lose
			return;

			// there should be some button to return or something (or could be time-based, in which you can use coroutines)
			// just load back your lobby/gameroom when onclick
			// no need to reset player kills/deaths and stuff, they'll automatically be resetted at new match
			// Application.LoadLevel("SOMETHING");
		}
	}

	public void HideScoreBoard()
	{
		m_scoreBoardGO.SetActive(false);
	}

	public void DisplayScoreBoard(bool isGameEnded)
	{
		m_scoreBoardGO.SetActive(true);

		// What if all of them is not connected..?
		int counter = 0;

		foreach(PhotonPlayer player in PhotonNetwork.playerList)
		{
			ExitGames.Client.Photon.Hashtable playerData = player.customProperties;

			if(counter < m_scoreTemplateList.Count)
			{
				GameObject scoreTemplate = m_scoreTemplateList[counter];
				UILabel level = Utility.FindChild(scoreTemplate, "Level").GetComponent<UILabel>();
				UILabel name = Utility.FindChild(scoreTemplate, "Name").GetComponent<UILabel>();
				UILabel status = Utility.FindChild(scoreTemplate, "Status").GetComponent<UILabel>();
				UILabel kills = Utility.FindChild(scoreTemplate, "Kills").GetComponent<UILabel>();
				UILabel deaths = Utility.FindChild(scoreTemplate, "Deaths").GetComponent<UILabel>();
				UILabel ping = Utility.FindChild(scoreTemplate, "Ping").GetComponent<UILabel>();
				GameObject highlightBackground = Utility.FindChild(scoreTemplate, "Highlight");

				int respawnTime = (int) playerData["match_respawn_timer"];

				level.text = playerData["match_level"].ToString();
				name.text = player.name;
				status.text = ((bool) playerData["match_isAlive"] == true) ? "Alive" : "Respawn in (" + respawnTime + ")";
				kills.text = playerData["match_kills"].ToString();
				deaths.text = playerData["match_deaths"].ToString();
				ping.text = playerData["ping"].ToString();


				// Team Death-Match
				if(playerData["Team"] != null && (Team) playerData["Team"] == Team.ONE)
				{
					level.color = blueTeamColor;
					name.color = blueTeamColor;
					status.color = blueTeamColor;
					kills.color = blueTeamColor;
					deaths.color = blueTeamColor;
					ping.color = blueTeamColor;
				}
				else if(playerData["Team"] != null && (Team) playerData["Team"] == Team.TWO)
				{
					level.color = redTeamColor;
					name.color = redTeamColor;
					status.color = redTeamColor;
					kills.color = redTeamColor;
					deaths.color = redTeamColor;
					ping.color = redTeamColor;
				}

				if(player.name == PhotonNetwork.playerName)
				{
					highlightBackground.SetActive(true);

					// Death-Match
					if(playerData["Team"] != null && (Team) playerData["Team"] == Team.ON_YOUR_OWN)
					{
						if(m_gameModeTitle.spriteName != "Death-Match_Title")
							m_gameModeTitle.spriteName = "Death-Match_Title";
					
					}
					else if(playerData["Team"] != null)
					{
						if(m_gameModeTitle.spriteName != "Team_DeathMatch_Title")
							m_gameModeTitle.spriteName = "Team_DeathMatch_Title";
					}
				}
				else
				{
					highlightBackground.SetActive(false);
				}

				scoreTemplate.SetActive(true);
			}

			counter++;
		}

		if(isGameEnded)
		{
			m_backToLobbyButton.SetActive(true);
		}

	}
	
	
	/// <summary>
	/// Adds the ally health bar.
	/// </summary>
	/// <param name="enemyCreature">Enemy creature.</param>
	/*public void AddAllyHealthBar(Creature enemyCreature)
	{
		if(enemyCreature != null)
		{
			HUDDisplayText creatureHUD = enemyCreature.gameObject.GetComponent<HUDDisplayText>();
			PlayerAttributes attributes = PlayerCommand.Instance.getPlayerAttributes();
			if(creatureHUD != null && attributes != null)
			{
				creatureHUD.SetAllyHUD(enemyCreature.getOwnerName(), attributes.CURRENT_LEVEL);
			}
		}
	}*/

	/// <summary>
	/// Adds the enemy health bar.
	/// </summary>
	/// <param name="enemyCreature">Enemy creature.</param>
	/*public void AddEnemyHealthBar(Creature enemyCreature)
	{
		if(enemyCreature != null)
		{
			HUDDisplayText creatureHUD = enemyCreature.gameObject.GetComponent<HUDDisplayText>();
			PlayerAttributes attributes = PlayerCommand.Instance.getPlayerAttributes();
			if(creatureHUD != null && attributes != null)
			{
				creatureHUD.SetEnemyHUD(enemyCreature.getOwnerName(), attributes.CURRENT_LEVEL);
			}
		}
	}*/
	

	/// <summary>
	/// Updates the HUD (player selection marker) e.g. the green round circle below the creature
	/// </summary>
	/// <param name="possessedCreature">Possessed creature.</param>
	void UpdateSelectionMarker(Creature possessedCreature)
	{
		if(possessedCreature != null)
		{
			HUDDisplayText creatureHUD = possessedCreature.gameObject.GetComponent<HUDDisplayText>();
			PlayerAttributes attributes = PlayerCommand.Instance.getPlayerAttributes();
			if(creatureHUD != null && attributes != null)
			{
				creatureHUD.SetAllyHUD(possessedCreature.getOwnerName(), attributes.CURRENT_LEVEL);
			}
		}
	}


	/// <summary>
	/// Updates the camera follow target.
	/// </summary>
	/// <param name="possessedCreature">Possessed creature.</param>
	void UpdateCameraFollowTarget(Creature possessedCreature)
	{
		if(possessedCreature != null)
		{
			if( !isRTSCamera ){
				m_camera.Follow(possessedCreature.gameObject, true);
			}else
				m_camera.EndFollow();
		}
	}


	/// <summary>
	/// Updates the fog of war. Set the fog of war of new possessed creature to active
	/// </summary>
	/// <param name="possessedCreature">Possessed creature.</param>
	void UpdateFogOfWar(Creature possessedCreature)
	{
		if(possessedCreature.gameObject.GetComponent<FOWRevealer>().isActive == false)
		{
			possessedCreature.gameObject.GetComponent<FOWRevealer>().isActive = true;
		}
	}

	/// <summary>
	/// Updates the skill cooldown GUI on HUD
	/// </summary>
	void UpdateSkillCooldownGUI(Creature possessedCreature)
	{
		CreatureStats stats = possessedCreature.getStats();

		if(skillPossessRelease != null)
		{
			m_skillButton4.UpdateCoolDown(skillPossessRelease.getCooldown());
			m_skillButton4.UpdateManaDisplay(stats.Curr_MP, skillPossessRelease.getMPCost());
		}

		if(skill_1 != null)
		{
			m_skillButton1.UpdateCoolDown(skill_1.getCooldown());
			m_skillButton1.UpdateManaDisplay(stats.Curr_MP, skill_1.getMPCost());
		}

		if(skill_2 != null)
		{
			m_skillButton2.UpdateCoolDown(skill_2.getCooldown());
			m_skillButton2.UpdateManaDisplay(stats.Curr_MP, skill_2.getMPCost());
		}

		if(skill_3 != null)
		{
			m_skillButton3.UpdateCoolDown(skill_3.getCooldown());
			m_skillButton3.UpdateManaDisplay(stats.Curr_MP, skill_3.getMPCost());
		}
	}

	/// <summary>
	/// Checks whether the HUD updated. If player possesses a new creature, return true. Otherwise e.g. same creature, return false
	/// </summary>
	/// <returns><c>true</c>, if HUD updated was checked, <c>false</c> otherwise.</returns>
	/// <param name="playerCreature">Player creature.</param>
	bool CheckHUDUpdated(Creature playerCreature)
	{
		bool shouldUpdateHUD = true;
		if(playerCreature != null)
		{
			// same creature, no update
			if(playerCreature.CRace.Equals(currentRace) && playerCreature.CClass.Equals(currentClass))
				shouldUpdateHUD = false;
		}
		else
		{
			//playerCreature is null and profile is already blank
			if(m_avatarPanel.spriteName.Equals (BLANK_PROFILE))
				shouldUpdateHUD = false;
		}

		return shouldUpdateHUD;
	}

	/// <summary>
	/// Sets the blank potrait.
	/// </summary>
	void SetBlankPotrait()
	{
		m_avatarPanel.spriteName = BLANK_PROFILE;
	}

	/// <summary>
	/// Updates the potrait. It also updates the FOW of the creature
	/// </summary>
	/// <param name="raceType">Race type.</param>
	/// <param name="classType">Class type.</param>
	void UpdatePotrait(CreatureRace raceType, CreatureClass classType)
	{
		currentRace = raceType;
		currentClass = classType;

		if(raceType == CreatureRace.OGRE)
		{
			if(classType == CreatureClass.WARRIOR)
			{
				m_avatarPanel.spriteName = OGRE_WARRIOR;
			}
			else if(classType == CreatureClass.MAGE)
			{
				m_avatarPanel.spriteName = OGRE_MAGE;
			}
			else if(classType == CreatureClass.ROGUE)
			{
				m_avatarPanel.spriteName = OGRE_ROGUE;
			}
			else if(classType == CreatureClass.PRIEST)
			{
				m_avatarPanel.spriteName = OGRE_PRIEST;
			}
		}
		else if(raceType == CreatureRace.RATKIN)
		{
			if(classType == CreatureClass.WARRIOR)
			{
				m_avatarPanel.spriteName = RATKIN_WARRIOR;
			}
			else if(classType == CreatureClass.GUNNER)
			{
				m_avatarPanel.spriteName = RATKIN_GUNNER;
			}
			else if(classType == CreatureClass.MAGE)
			{
				m_avatarPanel.spriteName = RATKIN_MAGE;
			}
			else if(classType == CreatureClass.SHAMAN)
			{
				m_avatarPanel.spriteName = RATKIN_SHAMAN;
			}
		}
		else if(raceType == CreatureRace.WISP)
		{
			m_avatarPanel.spriteName = WISP;
		}

		m_raceLabel.text = TextUtil.UppercaseFirst(raceType.ToString());
		m_classLabel.text = TextUtil.UppercaseFirst(classType.ToString());
	}

	/// <summary>
	/// Updates the attributes of the player
	/// </summary>
	/// <param name="playerCreature">Player creature.</param>
	public void UpdateAttributes(Creature playerCreature)
	{
		PlayerAttributes attributes = PlayerCommand.Instance.getPlayerAttributes();
		int maxAttributeLevel = 5;

		// Set the skill level
		skillLevel = attributes.SKILL_MASTERY;

		for(int i = 1; i <= maxAttributeLevel; i++)
		{
			UISprite strength_attribute_box = m_strength_list[i - 1];
			UISprite agility_attribute_box = m_agility_list[i - 1];
			UISprite intelligence_attribute_box = m_intelligence_list[i - 1];
			UISprite magic_attribute_box = m_magic_list[i - 1];

			if(i <= attributes.STRENGTH)
				strength_attribute_box.spriteName = ATTRIBUTES_STRENGTH_SPRITENAME;
			else
				strength_attribute_box.spriteName = ATTRIBUTES_DEFAULT_SPRITENAME;

			if(i <= attributes.AGILITY)
				agility_attribute_box.spriteName = ATTRIBUTES_AGILITY_SPRITENAME;
			else
				agility_attribute_box.spriteName = ATTRIBUTES_DEFAULT_SPRITENAME;

			if(i <= attributes.INTELLIGENCE)
				intelligence_attribute_box.spriteName = ATTRIBUTES_INTELLIGENCE_SPRITENAME;
			else
				intelligence_attribute_box.spriteName = ATTRIBUTES_DEFAULT_SPRITENAME;

			if(i <= attributes.SKILL_MASTERY)
				magic_attribute_box.spriteName = ATTRIBUTES_MAGIC_SPRITENAME;
			else
				magic_attribute_box.spriteName = ATTRIBUTES_DEFAULT_SPRITENAME;
		}

		m_levelLabel.text = attributes.CURRENT_LEVEL.ToString();
		//Debug.Log ("CURRENT LEVEL IS: "+attributes.CURRENT_LEVEL.ToString());
		//Debug.Log ("STRENGTH LEVEL IS: "+attributes.STRENGTH.ToString());
	}

	public void UpdatePlayersLevel(string playerLevel)
	{
		m_levelLabel.text = playerLevel;
	}

	/// <summary>
	/// Updates the stats panel from the player
	/// </summary>
	/// <param name="playerCreature">Player creature.</param>
	public void UpdateStatsPanel(Creature playerCreature)
	{
		CreatureStats stats = playerCreature.getStats();

		m_attackLabel.text = stats.PHY_ATK.ToString();
		m_defLabel.text = stats.PHY_DEF.ToString();
		m_magicDefLabel.text = stats.MAG_DEF.ToString();
		m_moveSpeedLabel.text = stats.MOV.ToString();
	}

	/// <summary>
	/// Updates the health panel. that includes HP/MP bar
	/// </summary>
	/// <param name="playerCreature">Player creature.</param>
	void UpdateHealthPanel(Creature playerCreature)
	{
		CreatureStats stats = playerCreature.getStats();
	
		m_healthBar.SetMaxHealth(stats.HP);
		m_healthBar.SetCurrentHealth(stats.Curr_HP);
		m_healthBar.SetHPRegen(stats.HP_REGEN);

		m_manaBar.SetMaxMana(stats.MP);
		m_manaBar.SetCurrentMana(stats.Curr_MP);
		m_manaBar.SetMPRegen(stats.MP_REGEN);
	}

	public void UpdateExpBar(int currentExp, int maxExp)
	{
		m_expBar.SetCurrentExp(currentExp);
		m_expBar.SetMaxEXP(maxExp);
		m_expBar.UpdateExpDisplay();
	}

	/// <summary>
	/// Gets the skill ability text for display when user roll over the skills
	/// </summary>
	/// <returns>The skill ability text.</returns>
	/// <param name="skill">Skill.</param>
	string GetSkillAbilityText(Skill skill)
	{
		if(skill == null)
		{
			Debug.LogError("Skill is Null when setting text");
			return "Error";
		}

		string skillTarget = "";

		if(skill.isPassive ())
			skillTarget = "Passive";
		else if(skill.isExecutedOnRelease ())
			skillTarget = "Active (Self Cast)";
		else if(skill.isPointSelectable())
			skillTarget = "Active (Target Point)";
		else if(skill.isEnemySelectable())
			skillTarget = "Active (Target Enemy)";
		else if(skill.isAllySelectable())
			skillTarget = "Active (Target Self or Ally)";
	
		return skillTarget;
	}

	/// <summary>
	/// Gets the skill description text for display when user roll over the skills
	/// </summary>
	/// <returns>The skill description text.</returns>
	/// <param name="skill">Skill.</param>
	string GetSkillDescriptionText(Skill skill)
	{
		if(skill == null)
		{
			Debug.LogError("Skill is Null when skill damage text");
			return "Error";
		}

		int dmgMin; int dmgMax;
		skill.computeBaseDamage(out dmgMin, out dmgMax);
		string damageText = "(" + dmgMin + " ~ " + dmgMax + ")";

		return skill.getDescription().Replace("###", damageText);
	}

	/// <summary>
	/// Updates the skills bar, when user possesses a new creature
	/// </summary>
	/// <param name="playerCreature">Player creature.</param>
	public void UpdateSkillsBar(Creature playerCreature)
	{
		List<Skill> skillLists = playerCreature.getSkillList();

		if(playerCreature.CRace == CreatureRace.WISP && playerCreature.CClass == CreatureClass.WISP)
		{
			//0: possess, 1: teleport
			skillPossessRelease = skillLists[0];		//teleport
			skill_1 = skillLists[1];					//possess

			// Must be skill 1 and 4 cos of the skill type
			m_skillSprite1_Foreground.spriteName = (m_skillSprite1_Foreground != null) ? "Skill_Teleport" : "Skill_Possess";
			m_skillSprite1_Background.spriteName = (m_skillSprite1_Background != null) ? "Skill_Teleport" : "Skill_Possess";
			m_skillSprite4_Foreground.spriteName = (m_skillSprite4_Foreground != null) ? "Skill_Possess" : "Skill_Possess";
			m_skillSprite4_Background.spriteName = (m_skillSprite4_Background != null) ? "Skill_Possess" : "Skill_Possess";

			// Teleport and Possess
			m_skillButtonGO1.SetActive(true);
			m_skillButtonGO4.SetActive(true);

			m_skillButtonGO2.SetActive(false);
			m_skillButtonGO3.SetActive(false);

			m_skillButton1.SetCooldown(skill_1.getMaxCooldown());
			m_skillButton4.SetCooldown(skillPossessRelease.getMaxCooldown());	

			m_skillButton1.SetCurrentCooldownTime(skill_1.getCooldown());
			m_skillButton4.SetCurrentCooldownTime(skillPossessRelease.getCooldown());

			//skill_1.

			// Set Skill Text
			m_skillButton1.SetSkillText(skill_1.SkillName, skillLevel, GetSkillDescriptionText(skill_1), GetSkillAbilityText(skill_1), skill_1.getDescription2(), skill_1.getMPCost(), skill_1.getMaxCooldown(), skill_1.getCooldownReduction(), skill_1.getRange(), skill_1.getMaxChargeTime());
			m_skillButton4.SetSkillText(skillPossessRelease.SkillName, skillLevel, GetSkillDescriptionText(skillPossessRelease), GetSkillAbilityText(skillPossessRelease), skillPossessRelease.getDescription2(), skillPossessRelease.getMPCost(), skillPossessRelease.getMaxCooldown(), skillPossessRelease.getCooldownReduction(), skillPossessRelease.getRange(), skillPossessRelease.getMaxChargeTime());

		}
		else
		{
			skillPossessRelease = skillLists[0];
			Skill skill_basicAttack = skillLists[1];
			skill_1 = skillLists[2];
			skill_2 = skillLists[3];
			skill_3 = skillLists[4];

			string skillSpriteName = "";

			// Alter Skill Icons Name
			if(currentRace == CreatureRace.OGRE)
			{
				if(currentClass == CreatureClass.WARRIOR)
				{
					skillSpriteName = OGRE_WARRIOR + "_Skill_";
				}
				else if(currentClass == CreatureClass.MAGE)
				{
					skillSpriteName = OGRE_MAGE + "_Skill_";
				}
				else if(currentClass == CreatureClass.ROGUE)
				{
					skillSpriteName = OGRE_ROGUE + "_Skill_";
				}
				else if(currentClass == CreatureClass.PRIEST)
				{
					skillSpriteName = OGRE_PRIEST + "_Skill_";
				}
			}
			else if(currentRace == CreatureRace.RATKIN)
			{
				if(currentClass == CreatureClass.WARRIOR)
				{
					skillSpriteName = RATKIN_WARRIOR + "_Skill_";
				}
				else if(currentClass == CreatureClass.GUNNER)
				{
					skillSpriteName = RATKIN_GUNNER + "_Skill_";
				}
				else if(currentClass == CreatureClass.MAGE)
				{
					skillSpriteName = RATKIN_MAGE + "_Skill_";
				}
				else if(currentClass == CreatureClass.SHAMAN)
				{
					skillSpriteName = RATKIN_SHAMAN + "_Skill_";
				}
			}

			m_skillSprite1_Foreground.spriteName = (m_skillSprite1_Foreground != null) ? skillSpriteName + "1" : "Skill_Possess";
			m_skillSprite1_Background.spriteName = (m_skillSprite1_Background != null) ? skillSpriteName + "1" : "Skill_Possess";
			m_skillSprite2_Foreground.spriteName = (m_skillSprite2_Foreground != null) ? skillSpriteName + "2" : "Skill_Possess";
			m_skillSprite2_Background.spriteName = (m_skillSprite2_Background != null) ? skillSpriteName + "2" : "Skill_Possess";
			m_skillSprite3_Foreground.spriteName = (m_skillSprite3_Foreground != null) ? skillSpriteName + "3" : "Skill_Possess";
			m_skillSprite3_Background.spriteName = (m_skillSprite3_Background != null) ? skillSpriteName + "3" : "Skill_Possess";
			m_skillSprite4_Foreground.spriteName = (m_skillSprite4_Foreground != null) ? "Skill_Release" : "Skill_Possess";
			m_skillSprite4_Background.spriteName = (m_skillSprite4_Background != null) ? "Skill_Release" : "Skill_Possess";
			
			m_skillButtonGO1.SetActive(true);
			m_skillButtonGO2.SetActive(true);
			m_skillButtonGO3.SetActive(true);
			m_skillButtonGO4.SetActive(true);

			// Set text
			m_skillButton1.SetSkillText(skill_1.SkillName, skillLevel, GetSkillDescriptionText(skill_1), GetSkillAbilityText(skill_1), skill_1.getDescription2(), skill_1.getMPCost(), skill_1.getMaxCooldown(),  skill_1.getCooldownReduction(), skill_1.getRange(), skill_1.getMaxChargeTime());
			m_skillButton2.SetSkillText(skill_2.SkillName, skillLevel, GetSkillDescriptionText(skill_2), GetSkillAbilityText(skill_2), skill_2.getDescription2(), skill_2.getMPCost(), skill_2.getMaxCooldown(), skill_2.getCooldownReduction(), skill_2.getRange(), skill_2.getMaxChargeTime());
			m_skillButton3.SetSkillText(skill_3.SkillName, skillLevel, GetSkillDescriptionText(skill_3), GetSkillAbilityText(skill_3), skill_3.getDescription2(), skill_3.getMPCost(), skill_3.getMaxCooldown(), skill_3.getCooldownReduction(), skill_3.getRange(), skill_3.getMaxChargeTime());
			m_skillButton4.SetSkillText(skillPossessRelease.SkillName, skillLevel, GetSkillDescriptionText(skillPossessRelease), GetSkillAbilityText(skillPossessRelease), skillPossessRelease.getDescription2(), skillPossessRelease.getMPCost(), skillPossessRelease.getMaxCooldown(),  skillPossessRelease.getCooldownReduction(), skillPossessRelease.getRange(), skillPossessRelease.getMaxChargeTime());
			
			m_skillButton1.SetCooldown(skill_1.getMaxCooldown());
			m_skillButton2.SetCooldown(skill_2.getMaxCooldown());
			m_skillButton3.SetCooldown(skill_3.getMaxCooldown());
			m_skillButton4.SetCooldown(skillPossessRelease.getMaxCooldown());

			m_skillButton1.SetCurrentCooldownTime(skill_1.getCooldown());
			m_skillButton2.SetCurrentCooldownTime(skill_2.getCooldown());
			m_skillButton3.SetCurrentCooldownTime(skill_3.getCooldown());
			m_skillButton4.SetCurrentCooldownTime(skillPossessRelease.getCooldown());
		}
	}

	/// <summary>
	/// Enables the AOE marker.
	/// </summary>
	void EnableAOEMarker(){
		GameObject AOEMarkerGO = GameObject.FindGameObjectWithTag("AOEMarker");
		AOEMarkerGO.GetComponent<AOEMarker>().Enable();
	}

	/// <summary>
	/// Selects the target and displays the AOE marker, if it is an AOE skill
	/// </summary>
	/// <param name="skillType">Skill type.</param>
	public void SelectTarget(SKILL_TYPE skillType)
	{
		if(skillType == SKILL_TYPE.SPECIAL_SKILL_ONE){
			m_cursor.SetToUseSkill_1();
			if(skill_1.isAOE())
				EnableAOEMarker();
		}else if(skillType == SKILL_TYPE.SPECIAL_SKILL_TWO){
			m_cursor.SetToUseSkill_2();
			if(skill_2.isAOE())
				EnableAOEMarker();
		}else if(skillType == SKILL_TYPE.SPECIAL_SKILL_THREE){
			m_cursor.SetToUseSkill_3();
			if(skill_3.isAOE())
				EnableAOEMarker();
		}else if(skillType == SKILL_TYPE.POSSESS_OR_RELEASE)
			m_cursor.SetToUseSkill_PossessRelease();
	}

	/// <summary>
	/// Dos the use skill, execute skill for the target
	/// </summary>
	/// <param name="skillType">Skill type.</param>
	public void DoUseSkill(SKILL_TYPE skillType)
	{
		switch(skillType)
		{
			case SKILL_TYPE.POSSESS_OR_RELEASE:
			if(skillPossessRelease.isExecutedOnRelease())	
			{
				PlayerCommand.Instance.UsePossessReleaseSkill();
			}
			else
			{
				SelectTarget(skillType);
			}

			break;

			case SKILL_TYPE.BASIC_ATTACK:
			break;

			case SKILL_TYPE.SPECIAL_SKILL_ONE:

			if(skill_1.isExecutedOnRelease())
			{
				PlayerCommand.Instance.UseSpecialSkill_1();
			}
			else if( !skill_1.isPassive() )
			{
				SelectTarget(skillType);
			}
			break;

			case SKILL_TYPE.SPECIAL_SKILL_TWO:
			if(skill_2.isExecutedOnRelease())
			{
				PlayerCommand.Instance.UseSpecialSkill_2();
			}
			else if( !skill_2.isPassive() )
			{
				SelectTarget(skillType);
			}

			break;

			case SKILL_TYPE.SPECIAL_SKILL_THREE:
			if(skill_3.isExecutedOnRelease())
			{
				PlayerCommand.Instance.UseSpecialSkill_3();
			}
			else if( !skill_3.isPassive() )
			{
				SelectTarget(skillType);
			}

			break;
		}
	}
}
