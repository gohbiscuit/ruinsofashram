﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Target marker that is responsible for generating the target marker when player clicks on a ground, creature etc
/// </summary>
public class TargetMarker : MonoBehaviour {

	Vector3 m_orgScale;
	object m_target;
	bool m_initialShrink;

	Vector3 m_scaleSpeed;

	public void setTarget(Creature target){
		m_target = target;
	}

	public void setTarget(Vector3 target){
		m_target = target;
	}

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start(){
		m_orgScale = transform.localScale;
		transform.localScale += new Vector3(1.0f,0,1.0f);
		m_initialShrink = true;
		m_scaleSpeed = new Vector3(-0.2f, 0, -0.2f);

		StartCoroutine(FadeAway());
	}
	
	/// <summary>
	/// Update this instance. Set the position of the marker to creature or ground
	/// </summary>
	void Update () {

		if(m_initialShrink){
			transform.localScale -= new Vector3(0.2f, 0, 0.2f);
			if(transform.localScale == m_orgScale)
				m_initialShrink = false;
		} else {
			if(transform.localScale.x < m_orgScale.x)
				m_scaleSpeed += new Vector3(0.1f, 0, 0.1f);
			else
				m_scaleSpeed -= new Vector3(0.1f, 0, 0.1f);
			transform.localScale += m_scaleSpeed;
		}

		// stick it to the creature/position
		if(m_target is Creature){
			Creature creatureTarget = ((Creature)m_target);
			if(creatureTarget == null)
				return;
			transform.position = creatureTarget.transform.position;
		}else if(m_target is Vector3)
			transform.position = (Vector3)m_target;
	}

	IEnumerator FadeAway(){
		yield return new WaitForSeconds(0.75f);
		while(renderer.material.color.a > 0){
			renderer.material.color -= new Color(0,0,0,0.1f);
			yield return new WaitForSeconds(0.005f);
		}
		Destroy(this.gameObject);
	}
}
