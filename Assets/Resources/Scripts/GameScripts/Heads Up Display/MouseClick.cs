﻿using UnityEngine;
using System.Collections;

public class MouseClick : MonoBehaviour {

	[SerializeField] GameObject arrowAnimPrefab;
	[SerializeField] TargetMover m_arrowAnimation;
	[SerializeField] GameObject parentObject;
	[SerializeField] CustomCursor m_cursor;

	private GameObject animInstance = null;
	private Vector3 clickedPoint;

	void Awake()
	{
		// Initialize highlighting system
		Highlighting.Init();
	}

	void DoRaycastClick()
	{
		//if (Input.GetButtonDown("Fire2"))
		//if ( Input.GetMouseButtonDown(0) )

		// Right Click on the Ground
		if( Input.GetMouseButtonDown(1) )
		{
			RaycastHit hit;
			Ray guiRay = UICamera.currentCamera.ScreenPointToRay(Input.mousePosition);

			// If ray hit on GUI area, (Do nothing)
			if(Physics.Raycast(guiRay.origin, guiRay.direction, out hit, 500,  1<<11))
			{
				return;
			}

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Physics.Raycast(ray.origin, ray.direction, out hit, 500,  1<<10 | 1<<9);

			// Move to the ground
			if(hit.collider.gameObject.tag != "Creature")
			{
				//Debug.Log ("HIT: "+hit.collider.gameObject.tag);
				//Debug.Log ("HIT POINT IS: "+hit.point);
				PlayerCommand.Instance.MoveCreatureToPosition(hit.point);
				m_arrowAnimation.PlayMoveParticle(hit.point);
				//CreateMoveParticle(hit.point);
			}
			else
			{
				Creature targetedCreature = hit.transform.GetComponent<Creature>();

				if(m_cursor.CurrentCursor == CURSOR_STATE.TARGET_ENEMY || m_cursor.CurrentCursor == CURSOR_STATE.HOVER_ENEMY)
					PlayerCommand.Instance.BasicAttack(hit.transform);
				else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_1)
					PlayerCommand.Instance.UseSpecialSkill_1(targetedCreature, true);
				else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_2)
					PlayerCommand.Instance.UseSpecialSkill_2(targetedCreature, true);
				else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_3)
					PlayerCommand.Instance.UseSpecialSkill_3(targetedCreature, true);
				else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_POSSESSRELEASE)
					PlayerCommand.Instance.UsePossessReleaseSkill(targetedCreature, true);
			}

			m_cursor.SetToDefault();
			GameObject AOEMarkerGO = GameObject.FindGameObjectWithTag("AOEMarker");
			AOEMarkerGO.GetComponent<AOEMarker>().Disable();
		}

		// Left Click while targetting
		if( Input.GetMouseButtonDown(0) )
		{
			RaycastHit hit;

			Ray guiRay = UICamera.currentCamera.ScreenPointToRay(Input.mousePosition);
			// If ray hit on GUI area, (Do nothing)
			if(Physics.Raycast(guiRay.origin, guiRay.direction, out hit, 500,  1<<11))
			{
				return;
			}

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Physics.Raycast(ray.origin, ray.direction, out hit, 500, 1<<10 | 1<<9);

			if(m_cursor.CurrentCursor == CURSOR_STATE.DEFAULT)
			{
				if(hit.collider.gameObject.tag == "Creature")
				{
					// Check i left click player or ENEMY!!!
					/*Creature targetedCreature = hit.transform.GetComponent<Creature>();
					HUDDisplayText creatureHUD = hit.transform.GetComponent<HUDDisplayText>();

					if(targetedCreature == PlayerCommand.Instance.getControlledCreature())
					{
						creatureHUD.TogglePlayerMarker();
					}
					else
					{
						creatureHUD.ToggleEnemyMarker();
					}*/
				}
			}
			else
			{
				// Click on the terrain
				if(hit.collider.gameObject.tag != "Creature")
				{
					if(m_cursor.CurrentCursor != CURSOR_STATE.USE_SKILL_1 && m_cursor.CurrentCursor != CURSOR_STATE.USE_SKILL_2 && m_cursor.CurrentCursor != CURSOR_STATE.USE_SKILL_3)
					{
						PlayerCommand.Instance.MoveCreatureToPosition(hit.point);
						m_arrowAnimation.PlayMoveParticle(hit.point);
						//CreateMoveParticle(hit.point);
					}
					else
					{
						// select a point to cast skill
						if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_1)
							PlayerCommand.Instance.UseSpecialSkill_1_AtPoint(hit.point);
						else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_2)
							PlayerCommand.Instance.UseSpecialSkill_2_AtPoint(hit.point);
						else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_3)
							PlayerCommand.Instance.UseSpecialSkill_3_AtPoint(hit.point);
					}
				}
				else
				{
					Creature playerCreature = PlayerCommand.Instance.getControlledCreature();

					// Player uses mage blink on a selected creature
					if(playerCreature.CRace == CreatureRace.OGRE && playerCreature.CClass == CreatureClass.MAGE && m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_3)
					{
						SkillUsingMessage message = PlayerCommand.Instance.DisplayFeedbackMessage(SkillUsingMessage.INVALID_TARGET);
						m_cursor.SetToDefault();
						return;
					}
					// Player uses teleport on a selected creature
					else if(playerCreature.CRace == CreatureRace.WISP && playerCreature.CClass == CreatureClass.WISP && m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_1)
					{
						SkillUsingMessage message = PlayerCommand.Instance.DisplayFeedbackMessage(SkillUsingMessage.INVALID_TARGET);
						m_cursor.SetToDefault();
						return;
					}

					Creature targetedCreature = hit.transform.GetComponent<Creature>();
					if(m_cursor.CurrentCursor == CURSOR_STATE.TARGET_ENEMY)
						PlayerCommand.Instance.BasicAttack(hit.transform);
					else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_1)
						PlayerCommand.Instance.UseSpecialSkill_1(targetedCreature, true);
					else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_2)
						PlayerCommand.Instance.UseSpecialSkill_2(targetedCreature, true);
					else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_3)
						PlayerCommand.Instance.UseSpecialSkill_3(targetedCreature, true);
					else if(m_cursor.CurrentCursor == CURSOR_STATE.USE_SKILL_POSSESSRELEASE)
						PlayerCommand.Instance.UsePossessReleaseSkill(targetedCreature, true);
				}
				
				m_cursor.SetToDefault();
				GameObject AOEMarkerGO = GameObject.FindGameObjectWithTag("AOEMarker");
				AOEMarkerGO.GetComponent<AOEMarker>().Disable();
			}
		}
	}

	/*void CreateMoveParticle(Vector3 targetPoint)
	{
		Vector3 worldCoordiantePos = Camera.main.WorldToViewportPoint(targetPoint);
		Vector3 guiCoordinatePos = UICamera.currentCamera.ViewportToWorldPoint(worldCoordiantePos);
		guiCoordinatePos.z = 0;

		animInstance = Instantiate(arrowAnimPrefab, guiCoordinatePos, Quaternion.identity) as GameObject;
		animInstance.GetComponent<MoveParticleFixPosition>().mouseClickedPoint = targetPoint;
		
		// Set the parent of the terrain object to this
		if(parentObject != null)
		{
			animInstance.transform.parent = parentObject.transform;
			animInstance.transform.localScale = Vector3.one;
		}
	}*/
	

	void Update () 
	{
		DoRaycastClick();
	}
}
