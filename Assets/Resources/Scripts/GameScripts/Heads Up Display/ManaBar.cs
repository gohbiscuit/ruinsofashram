﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Mana bar class that shows up in the HUD
/// </summary>
public class ManaBar : MonoBehaviour {

	UISlider slider = null;
	[SerializeField] UILabel m_manaText;
	[SerializeField] UILabel m_regenText;

	private float maxMP;
	private float currentMP;
	private double mpRegen;

	// Use this for initialization
	void Start () {
		slider = GetComponent<UISlider>();
	}
	
	// Update is called once per frame
	void Update () 
	{
		float fillPercent = currentMP/maxMP;
		slider.sliderValue = fillPercent;

		m_manaText.text = Mathf.CeilToInt(currentMP).ToString() + " / " + Mathf.CeilToInt(maxMP).ToString();
		m_regenText.text = "+ " + mpRegen.ToString ();
	}

	/// <summary>
	/// Sets the max mana.
	/// </summary>
	/// <param name="mana">Mana.</param>
	public void SetMaxMana(float mana)
	{
		maxMP = mana;
	}

	/// <summary>
	/// Sets the current mana.
	/// </summary>
	/// <param name="currentMana">Current mana.</param>
	public void SetCurrentMana(float currentMana)
	{
		currentMP = currentMana;
	}

	/// <summary>
	/// Sets the MP regen.
	/// </summary>
	/// <param name="mpRegen">Mp regen.</param>
	public void SetMPRegen(double mpRegen)
	{
		this.mpRegen = mpRegen;
	}
}
