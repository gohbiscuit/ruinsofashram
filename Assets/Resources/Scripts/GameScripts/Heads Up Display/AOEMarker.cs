﻿using UnityEngine;
using System.Collections;

/// <summary>
/// AOE marker class responsible for generating a HUD area of effect texture
/// </summary>
public class AOEMarker : MonoBehaviour {

	bool m_active;

	// Use this for initialization
	void Start () {
		Disable();
	}
	
	// Update is called once per frame
	void Update () {
		if(m_active){

			RaycastHit hit;
			Ray guiRay = UICamera.currentCamera.ScreenPointToRay(Input.mousePosition);

			// If ray hit on GUI area, (Do nothing)
			if(Physics.Raycast(guiRay.origin, guiRay.direction, out hit, 500,  1<<11)){
				transform.position = new Vector3(-100, 0, -100);
				return;
			}

			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			if(Physics.Raycast(ray.origin, ray.direction, out hit, 500,  1<<10)){
				transform.position = hit.point + new Vector3(0, 0.1f, 0);
			}
		}
	}

	/// <summary>
	/// Disable this instance.
	/// </summary>
	public void Disable(){
		m_active = false;
		transform.position = new Vector3(-100, 0, -100);
	}

	/// <summary>
	/// Enable this instance.
	/// </summary>
	public void Enable(){
		m_active = true;
	}
}
