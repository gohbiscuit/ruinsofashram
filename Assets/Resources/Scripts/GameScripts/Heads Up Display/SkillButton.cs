﻿using UnityEngine;
using System.Collections;

public enum SKILL_TYPE {POSSESS_OR_RELEASE, SPECIAL_SKILL_ONE, SPECIAL_SKILL_TWO, SPECIAL_SKILL_THREE, BASIC_ATTACK};

/// <summary>
/// Skill button class that handles the individual functionality for skills
/// </summary>
public class SkillButton : MonoBehaviour 
{
	public float coolTimeMax = 0.5f;
	float coolTime;
	UISlider silder = null;

	[SerializeField] UILabel cooldownText;
	[SerializeField] HUDHandler ashramHUD;
	[SerializeField] SKILL_TYPE m_skillType;
	[SerializeField] CustomCursor m_cursor;


	[SerializeField] GameObject m_skillPanel;
	
	[SerializeField] UILabel m_skillAbility;	// target e.g. self-cast, passive, active etc
	[SerializeField] UILabel m_skillTitle;
	[SerializeField] UILabel m_skillDescription;
	[SerializeField] UILabel m_skillMPCost;
	[SerializeField] UILabel m_skillCooldown;
	[SerializeField] UILabel m_skillLevel;
	[SerializeField] UILabel m_skillRange;		// type: direct-damage, heal etc
	[SerializeField] UILabel m_skillCastTime;		// type: direct-damage, heal etc

	private Color grayColor = new Color(125f/255f, 125f/255f, 125f/255f); 
	private Color lightBlueColor = new Color(55f/255f, 55f/255f, 255f/255f);
	private Color darkerBlueColor = new Color(35f/255f, 35f/255f, 255f/255f);

	// Use this for initialization
	void Start () 
	{
		silder = GetComponentInChildren<UISlider>();
		//coolTime = coolTimeMax;
		coolTime = -1.0f;
		cooldownText.text = "";
	}

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick ()
	{
		DoUseSkill();
	}

	/// <summary>
	/// Raises the hover event.
	/// </summary>
	/// <param name="isOver">If set to <c>true</c> is over.</param>
	void OnHover( bool isOver )
	{
		// Roll Out
		if( isOver )
		{
			ShowSkillPanel();
		}
		else
		{
			HideSkillPanel();
		}
	}

	/// <summary>
	/// Hides the skill panel.
	/// </summary>
	void HideSkillPanel()
	{
		if(m_skillPanel.activeSelf)
		{
			m_skillPanel.SetActive(false);
		}
	}

	/// <summary>
	/// Shows the skill panel.
	/// </summary>
	void ShowSkillPanel()
	{
		if(m_skillPanel.activeSelf == false)
		{
			m_skillPanel.SetActive(true);
		}
	}

	/// <summary>
	/// Sets the skill description text. When user mouse over it, it will be shown as an HUD overlay
	/// </summary>
	/// <param name="title">Title.</param>
	/// <param name="level">Level.</param>
	/// <param name="description">Description.</param>
	/// <param name="ability">Ability.</param>
	/// <param name="attackType">Attack type.</param>
	/// <param name="mp_cost">Mp_cost.</param>
	/// <param name="maxCooldown">Max cooldown.</param>
	/// <param name="cooldownReduction">Cooldown reduction.</param>
	/// <param name="range">Range.</param>
	/// <param name="castTime">Cast time.</param>
	public void SetSkillText(string title, int level, string description, string ability, string attackType, int mp_cost, float maxCooldown, float cooldownReduction, float range, float castTime)
	{
		m_skillTitle.text = title;
		m_skillLevel.text = "Level " + level.ToString ();
		m_skillDescription.text = description + "\n\n" + attackType;		
		//m_skillAbility.text = "Ability: " + ability;
		m_skillAbility.text = ability;
		//m_skillAttackType.text = attackType;
		m_skillMPCost.text = (mp_cost == 0) ? "Mana cost: N/A" : "Mana cost: "+mp_cost;

		m_skillRange.text = (range == 0f) ? "Range: N/A" : "Range: " + (Mathf.RoundToInt(range) * 100).ToString();
		m_skillCastTime.text = (castTime == 0f) ? "Cast: Instant" : "Cast: " + castTime.ToString() + " seconds";

		string nextLevelCooldown = (cooldownReduction == 0f) ? "" : " (" + (maxCooldown-cooldownReduction) +")"; 
		m_skillCooldown.text = (maxCooldown == 0f) ? "Cooldown: N/A" : "Cooldown: "+maxCooldown + nextLevelCooldown;
	}


	/// <summary>
	/// Dos the use skill and start the cooldown
	/// </summary>
	void DoUseSkill()
	{
		// Skill cooldown has finished, can be use
		ashramHUD.DoUseSkill(m_skillType);
	}

	/// <summary>
	/// Updates the mana display, if not enough mana should have a blue overlay
	/// </summary>
	public void UpdateManaDisplay(float currentMP, int spellManaCost)
	{
		UISprite backgroundSprite = Utility.FindChild(gameObject, "Skill Slider/Background").GetComponent<UISprite>();
		UISprite foregroundSprite = Utility.FindChild(gameObject, "Skill Slider/Foreground").GetComponent<UISprite>();

		// mp cost to use this skill
		if(currentMP < spellManaCost)
		{
			foregroundSprite.color = lightBlueColor;
			backgroundSprite.color = darkerBlueColor;
		}
		else
		{
			foregroundSprite.color = Color.white;
			backgroundSprite.color = Color.gray;
		}
	}

	/// <summary>
	/// Updates the cool down.
	/// </summary>
	/// <param name="serverCooldownTime">Server cooldown time.</param>
	public void UpdateCoolDown(float serverCooldownTime)
	{
		coolTime = serverCooldownTime;

		if(coolTime > 0f)
		{
			cooldownText.text = Mathf.CeilToInt(coolTime).ToString();

			if(coolTimeMax > 0)
			{
				float percent = 1f - coolTime/coolTimeMax;
				silder.sliderValue = percent;
			}
		}
		else if(cooldownText.text != "")
		{
			cooldownText.text = "";
			silder.sliderValue = 1f;
		}
	}

	/// <summary>
	/// Sets the cooldown.
	/// </summary>
	/// <param name="newCooldownTime">New cooldown time.</param>
	public void SetCooldown(float newCooldownTime)
	{
		// Note if newcooldowntime == 0, then the skill has no cooldown
		coolTimeMax = newCooldownTime;
	}

	/// <summary>
	/// Sets the current cooldown time.
	/// </summary>
	/// <param name="newTime">New time.</param>
	public void SetCurrentCooldownTime(float newTime)
	{
		coolTime = newTime;
	}

	/// <summary>
	/// Determines whether this instance is on cooldown.
	/// </summary>
	/// <returns><c>true</c> if this instance is on cooldown; otherwise, <c>false</c>.</returns>
	public bool IsOnCooldown()
	{
		return coolTime > 0;
	}

	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.R) && m_skillType == SKILL_TYPE.POSSESS_OR_RELEASE)
		{
			DoUseSkill();
		}
		else if (Input.GetKeyDown(KeyCode.Q) && m_skillType == SKILL_TYPE.SPECIAL_SKILL_ONE)
		{
			DoUseSkill();
		}
		else if (Input.GetKeyDown(KeyCode.W) && m_skillType == SKILL_TYPE.SPECIAL_SKILL_TWO)
		{
			DoUseSkill();
		}
		else if (Input.GetKeyDown(KeyCode.E) && m_skillType == SKILL_TYPE.SPECIAL_SKILL_THREE)
		{
			DoUseSkill();
		}
	}
}