﻿using UnityEngine;
using System.Collections;

public enum ATTRIBUTES_TYPE {STRENGTH, AGILITY, INTELLIGENCE, SPELL_MASTERY};

public class AttributesButtonHandler : MonoBehaviour {

	[SerializeField] ATTRIBUTES_TYPE m_attributesType;		// attributes type of this button
	[SerializeField] HUDHandler m_hudHandler;		// attributes type of this button

	void OnClick()
	{

		AudioManager.Instance.PlayOnce("Creature/playbutton", 1.0f);

		PlayerAttributes attributes = PlayerCommand.Instance.getPlayerAttributes();

		if(m_attributesType == ATTRIBUTES_TYPE.STRENGTH)
		{
			if(attributes.STRENGTH < 5)
			{
				PlayerCommand.Instance.incrementStrength();
			}
		}
		else if(m_attributesType == ATTRIBUTES_TYPE.AGILITY)
		{
			if(attributes.AGILITY < 5)
			{
				PlayerCommand.Instance.incrementAgility();
			}
		}
		else if(m_attributesType == ATTRIBUTES_TYPE.INTELLIGENCE)
		{
			if(attributes.INTELLIGENCE < 5)
			{
				PlayerCommand.Instance.incrementIntelligence();
			}
		}
		else if(m_attributesType == ATTRIBUTES_TYPE.SPELL_MASTERY)
		{
			if(attributes.SKILL_MASTERY < 5)
			{
				PlayerCommand.Instance.incrementSkillMastery();
			}
		}


		Creature playerCreature = PlayerCommand.Instance.getControlledCreature();

		// Hide attributes button upon clicking it
		PlayerCommand.Instance.DisplayAttributesButton(false);

		if(playerCreature != null)
		{
			m_hudHandler.UpdateAttributes(playerCreature);
			m_hudHandler.UpdateStatsPanel(playerCreature);
			m_hudHandler.UpdateSkillsBar(playerCreature);
		}
	}
}
