﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Responsible for handling GUI mouse cursor selection when player rolls over a target
/// </summary>
[RequireComponent(typeof(Camera))]
public class CameraSelection : MonoBehaviour 
{

	// Which layers targeting ray must hit (-1 = everything)
	public LayerMask targetingLayerMask = -1;
	
	// Targeting ray length
	private float targetingRayLength = Mathf.Infinity;

	[SerializeField] CustomCursor m_cursor;


	// Update is called once per frame
	void Update () 
	{
		TargetingRaycast();
	}

	/// <summary>
	/// Handles the roll-over event when a player mouse over the cursor to a target
	/// </summary>
	void TargetingRaycast()
	{
		// Current mouse position on screen
		Vector3 mousePos = Input.mousePosition;
		
		// Current target object transform component
		Transform targetTransform = null;
		
		// If camera component is available
		if (Camera.main != null && UICamera.currentCamera != null)
		{
			RaycastHit hitInfo;
			Ray guiRay = UICamera.currentCamera.ScreenPointToRay(mousePos);
			
			// If ray hit on GUI area, (Do nothing)
			if(Physics.Raycast(guiRay.origin, guiRay.direction, out hitInfo, 500,  1<<11))
			{
				return;
			}

			// Create a ray from mouse coords
			Ray ray = Camera.main.ScreenPointToRay(new Vector3(mousePos.x, mousePos.y, 0f));
			// Targeting raycast 1<<10 | 1<<9
			//if (Physics.Raycast(ray.origin, ray.direction, out hitInfo, targetingRayLength, targetingLayerMask.value))
			if( Physics.Raycast(ray.origin, ray.direction, out hitInfo, 500, 1<<10 | 1<<9))
			{
				// Cache what we've hit
				targetTransform = hitInfo.collider.transform;
			}
		}
		
		// If we got an object that was hit
		if (targetTransform != null)
		{
			// And this object has HighlightableObject component
			HighlightableObject ho = targetTransform.root.GetComponentInChildren<HighlightableObject>();
			if (ho != null)
			{
				// If left mouse button down
				/*if (Input.GetButtonDown("Fire1"))
					// Start flashing with frequency = 2
					ho.FlashingOn(2f);
				
				// If right mouse button is up
				if (Input.GetButtonUp("Fire2"))
					// Stop flashing
					ho.FlashingOff();*/
				
				// One-frame highlighting (to highlight object that currently under mouse cursor)
				ho.On(Color.red);

				if(m_cursor != null && m_cursor.CurrentCursor == CURSOR_STATE.DEFAULT)
				{
					// Set mouse cursor to red
					m_cursor.SetToHoverEnemy();
				}
			}
		}
	}
}
