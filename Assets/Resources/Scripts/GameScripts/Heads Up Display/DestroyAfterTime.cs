﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Utility scripts that allows game object to be destroyed after time.
/// </summary>
public class DestroyAfterTime : MonoBehaviour {

	[SerializeField] float destroyTime = 1.0f;

	// Use this for initialization
	void Start () {
	
	}

	void Awake()
	{
		Destroy(gameObject, destroyTime);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
