﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Highlighting FX controller that controls the mouse over shader effect of the creature
/// </summary>
public class HighlightingFXController : MonoBehaviour {

	protected HighlightableObject ho;
	private bool constantlyHighlighted = false;
	
	void Awake()
	{
		ho = gameObject.AddComponent<HighlightableObject>();
	}
	
	void Update()
	{
		if (Input.GetKey(KeyCode.Space))
		{
			ho.On(Color.yellow);
		}

		/*if (Input.GetKeyDown(KeyCode.Space)) 
		{
			constantlyHighlighted = !constantlyHighlighted;
			if (constantlyHighlighted)
				ho.ConstantOn(Color.yellow);
			else
				ho.ConstantOff();
		}*/



		//ho.ConstantOn(Color.yellow);
		// Fade in/out constant highlighting by 'Tab' button
		/*if (Input.GetKeyDown(KeyCode.Tab)) 
		{
			constantlyHighlighted = !constantlyHighlighted;
			if (constantlyHighlighted)
				ho.ConstantOn(Color.yellow);
			else
				ho.ConstantOff();
		}*/
		// Turn on/off constant highlighting by 'Q' button
		/*else if (Input.GetKeyDown(KeyCode.Q))
		{
			constantlyHighlighted = !constantlyHighlighted;
			if (constantlyHighlighted)
				ho.ConstantOnImmediate(Color.yellow);
			else
				ho.ConstantOffImmediate();
		}
		
		// Turn off all highlighting modes by 'Z' button
		if (Input.GetKeyDown(KeyCode.Z)) 
		{
			constantlyHighlighted = false;
			ho.Off();
		}*/
	}
}
