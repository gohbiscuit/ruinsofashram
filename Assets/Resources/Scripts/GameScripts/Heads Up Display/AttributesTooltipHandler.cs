﻿using UnityEngine;
using System.Collections;

public class AttributesTooltipHandler : MonoBehaviour {

	private string tooltipText = "NULL";
	private string title = "";
	private string text = "";
	[SerializeField] ATTRIBUTES_TYPE m_attributesType;		// attributes type of this button

	int maxAttributeLevel = 5;

	/// <summary>
	/// Shows the attributes panel tool tip.
	/// </summary>
	void ShowAttributesTooltip()
	{
		//tooltipText = "Experience (" + currentExp + " / " + maxExp +")";
		PlayerAttributes attributes = PlayerCommand.Instance.getPlayerAttributes();

		if(m_attributesType == ATTRIBUTES_TYPE.STRENGTH)
		{
			title = "Strength (" + attributes.STRENGTH + " / " + maxAttributeLevel +")";
			text = "Increases the attack power, armor, max health, and health regeneration for Warrior-type creatures";

			//+1 strength: each strength will increase 1 warrior base damage by 1, max HP + 10, 1 physical armor, 0.01 HP regen 
			tooltipText = TextUtil.STATS_TOOLTIP_TITLE_COLOR_ + title + "\n\n" + TextUtil.STATS_TOOLTIP_DESC_COLOR_ + text;
		}
		else if(m_attributesType == ATTRIBUTES_TYPE.AGILITY)
		{
			title = "Agility (" + attributes.AGILITY + " / " + maxAttributeLevel +")";
			text = "Increases the attack damage and movement speed for Agility-types creatures ";

			//+1 agility: each agility will increase archer/rogue base damage by 0.8, 1 attack speed, 1 movement speed
			tooltipText = TextUtil.STATS_TOOLTIP_TITLE_COLOR_ + title + "\n\n" + TextUtil.STATS_TOOLTIP_DESC_COLOR_ + text;
		}
		else if(m_attributesType == ATTRIBUTES_TYPE.INTELLIGENCE)
		{
			title = "Intelligence (" + attributes.INTELLIGENCE + " / " + maxAttributeLevel +")";
			text = "Increases attack damage, max mana, mana regeneration and magic defense for Mage-types creatures";

			//+1 intelligence: each intelligence will increase mage/warlock/paladin/healer base damage by 0.6, max MP +10, magic defense + 1, + 0.01 MP regen
			tooltipText = TextUtil.STATS_TOOLTIP_TITLE_COLOR_ + title + "\n\n" + TextUtil.STATS_TOOLTIP_DESC_COLOR_ + text;
		}
		else if(m_attributesType == ATTRIBUTES_TYPE.SPELL_MASTERY)
		{
			title = "Spell Mastery (" + attributes.SKILL_MASTERY + " / " + maxAttributeLevel +")";
			text = "Increases the power of all the spells";

			//+1 spell ability: increase all levels of the spell
			tooltipText = TextUtil.STATS_TOOLTIP_TITLE_COLOR_ + title + "\n\n" + TextUtil.STATS_TOOLTIP_DESC_COLOR_ + text;
		}


		UITooltip.ShowText (tooltipText);
	}
	
	/// <summary>
	/// Hides the attributes panel tool tip.
	/// </summary>
	void HideAttributesTooltip()
	{
		UITooltip.ShowText (null);
	}
	
	/// <summary>
	/// Raises the hover event.
	/// </summary>
	/// <param name="isOver">If set to <c>true</c> is over.</param>
	void OnHover( bool isOver )
	{
		// Roll Out
		if( isOver )
		{
			ShowAttributesTooltip();
		}
		else
		{
			HideAttributesTooltip();
		}
	}
}
