﻿using UnityEngine;
using System.Collections;

public enum AVATARTYPE {CREATURE, PLAYER, ENEMY};

/// <summary>
/// Avatar bar class that is responsbble for showing HP/MP bar on the creature
/// </summary>
public class AvatarBar : MonoBehaviour {

	public AVATARTYPE m_type;

	[SerializeField] UISlider m_healthSlider;
	[SerializeField] UISlider m_manaSlider;
	[SerializeField] UILabel m_name;
	[SerializeField] UILabel m_playerLevel;
	[SerializeField] GameObject m_background;

	/// <summary>
	/// Displays the creature avatar bar
	/// </summary>
	/// <param name="creatureName">Creature name.</param>
	public void SetCreatureUI(string creatureName)
	{
		m_name.text = creatureName;
	}

	/// <summary>
	/// Displays the player avatar bar
	/// </summary>
	/// <param name="playerName">Player name.</param>
	/// <param name="playerLevel">Player level.</param>
	public void SetPlayerUI(string playerName, int playerLevel)
	{
		m_name.text = playerName;
		m_playerLevel.text = playerLevel.ToString();
	}

	float prevHPFillPercentage = 1;
	float prevMPFillPercentage = 1;

	/// <summary>
	/// Updates the health slider, of the avatar bar
	/// </summary>
	/// <param name="fillPercent">Fill percent.</param>
	public void UpdateHealthSlider(float fillPercent)
	{
		prevHPFillPercentage -= (prevHPFillPercentage - fillPercent)/1.5f;
		m_healthSlider.sliderValue = prevHPFillPercentage;
	}

	/// <summary>
	/// Updates the mana slider of the avatar bar
	/// </summary>
	/// <param name="fillPercent">Fill percent.</param>
	public void UpdateManaSlider(float fillPercent)
	{
		prevMPFillPercentage -= (prevMPFillPercentage - fillPercent)/3.0f;
		m_manaSlider.sliderValue = prevMPFillPercentage;
	}
}
