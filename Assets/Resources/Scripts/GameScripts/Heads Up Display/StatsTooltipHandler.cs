﻿using UnityEngine;
using System.Collections;

enum ATTRIBUTE_TYPE {ARMOR, ATTACK, MAGIC_DEF, MOVEMENT_SPEED};

/// <summary>
/// Stats tooltip handler that handling the displays when user mouse over the stats
/// </summary>
public class StatsTooltipHandler : MonoBehaviour {

	[SerializeField] ATTRIBUTE_TYPE m_type;
	private string m_attributeTitle;
	private string m_attributeDescription;

	private Creature playerCreature = null;
	private string tooltipText = "NULL";

	void OnTooltip ()
	{
		//
	}

	/// <summary>
	/// Gets the creature information
	/// </summary>
	void GetCreatureInfo()
	{
		playerCreature = PlayerCommand.Instance.getControlledCreature();
		if(playerCreature != null)
		{
			CreatureStats stats = playerCreature.getStats();

			if(m_type == ATTRIBUTE_TYPE.ARMOR)
			{
				string physicalDefRatio = (stats.PHY_DEF * 0.2) + "%";
				
				m_attributeTitle = "Armor ("+physicalDefRatio+") reduction from physical attack";
				m_attributeDescription = "Each point in strength will add 5 points to defense" + "\n" +
				"Each point in agility will add 5 points to defense";
			}
			else if(m_type == ATTRIBUTE_TYPE.MAGIC_DEF)
			{	
				string magicDefRatio = (stats.MAG_DEF * 0.5) + "%";
				
				m_attributeTitle = "Magic Defense ("+magicDefRatio+") reduction from spells";
				m_attributeDescription = "Each point in intelligence will add 5 points to magic defense";
			}
			else if(m_type == ATTRIBUTE_TYPE.ATTACK)
			{
				// 1 magic defense = +0.5% magic damage reduction
				m_attributeTitle = "Physical Attack ("+stats.PHY_ATK+")";
				m_attributeDescription = "Each point in strength will add 10 points to warrior physical attack" + "\n" +
				"Each point in agility will add 10 points to archer/rogue physical attack" + "\n" +
				"Each point in intelligence will add 10 points to mage/warlock/paladin/healer physical attack";
			}
			else if(m_type == ATTRIBUTE_TYPE.MOVEMENT_SPEED)
			{
				string movementType = GetSpeedBasedType(stats.MOV);

				m_attributeTitle = "Movement Speed ("+movementType+")";
				m_attributeDescription = "Each point in agility will add 1 movement speed" + "\n"+
				"Movement speed: 0 to 20 (Slow), 21 to 40 (moderate), 41 to 60 (fast), 61 onwards (very fast)";
			}

			tooltipText = TextUtil.STATS_TOOLTIP_TITLE_COLOR_ + m_attributeTitle + "\n\n" + TextUtil.STATS_TOOLTIP_DESC_COLOR_ + m_attributeDescription;
		}
	}

	/// <summary>
	/// Gets the type of the speed based.
	/// </summary>
	/// <returns>The speed based type.</returns>
	/// <param name="movementSpeed">Movement speed.</param>
	string GetSpeedBasedType(int movementSpeed)
	{
		if(movementSpeed >= 0 && movementSpeed <= 20)
			return "Slow";
		else if(movementSpeed >= 21 && movementSpeed <= 40)
			return "Moderate";
		else if(movementSpeed >= 41 && movementSpeed <= 60)
			return "Fast";
		else if(movementSpeed >= 60 )
			return "Very Fast";

		return "NULL";
	}

	/// <summary>
	/// Shows the tool tip for stats
	/// </summary>
	void ShowStatsToolTip()
	{
		GetCreatureInfo();
		UITooltip.ShowText (tooltipText);
	}

	/// <summary>
	/// Hides the tool tip for stats
	/// </summary>
	void HideStatsToolTip()
	{
		UITooltip.ShowText (null);
	}

	/// <summary>
	/// Raises the hover event. When mouse over show tooltip text
	/// </summary>
	/// <param name="isOver">If set to <c>true</c> is over.</param>
	void OnHover( bool isOver )
	{
		// Roll Out
		if( isOver )
		{
			ShowStatsToolTip();
		}
		else
		{
			HideStatsToolTip();
		}

		/*if( isOver )
			controller.tooltipAnchor.SetActive(true);
		else
			controller.tooltipAnchor.SetActive(false);*/
	}
}
