﻿using UnityEngine;
using System.Collections;

/// <summary>
/// HUD display text that is responsible for showing up the "Damage Text", "Experience Text", "Avatar Bar" for player
/// </summary>
public class HUDDisplayText : MonoBehaviour {

	// The UI prefab that is going to be instantiated above the player
	public GameObject prefab;		// hud text
	public GameObject expPrefab;	// exp HUD text

	public Transform target;

	public GameObject playerAvatarBar;
	public GameObject enemyAvatarBar;
	public GameObject creatureAvatarBar;

	public Transform avatarBarTarget;

	public Transform expTextPivot;

	HUDText m_damageText = null;
	HUDText m_expText = null;

	private GameObject playerMarker;
	private Creature creatureScript;

	private GameObject playerAvatarChild;
	private GameObject enemyAvatarChild;
	private GameObject creatureAvatarChild;

	private GameObject avatarChild;		// cache the current avatar object

	/// <summary>
	/// Awake this instance. Initializing all the components to the respective position
	/// </summary>
	void Awake () 
	{
		// We need the HUD object to know where in the hierarchy to put the element
		if (HUDRoot.go == null)
		{
			GameObject.Destroy(this);
			return;
		}

		GameObject damageTextChild = NGUITools.AddChild(HUDRoot.go, prefab);
		GameObject expTextChild = NGUITools.AddChild(HUDRoot.go, expPrefab);
		playerAvatarChild = NGUITools.AddChild(HUDRoot.go, playerAvatarBar);
		enemyAvatarChild = NGUITools.AddChild(HUDRoot.go, enemyAvatarBar);
		creatureAvatarChild = NGUITools.AddChild(HUDRoot.go, creatureAvatarBar);

		creatureScript = gameObject.GetComponent<Creature>();

		m_damageText = damageTextChild.GetComponentInChildren<HUDText>();
		m_expText = expTextChild.GetComponentInChildren<HUDText>();

		// Make the UI follow the target
		damageTextChild.AddComponent<UIFollowTarget>().target = target;
		playerAvatarChild.AddComponent<UIFollowTarget>().target = avatarBarTarget;
		enemyAvatarChild.AddComponent<UIFollowTarget>().target = avatarBarTarget;
		creatureAvatarChild.AddComponent<UIFollowTarget>().target = avatarBarTarget;
		expTextChild.AddComponent<UIFollowTarget>().target = expTextPivot;
	
		playerMarker = Utility.FindChild(gameObject, "marker");

		SetCreatureHUD();
	}

	/// <summary>
	/// Popup the damage text
	/// </summary>
	/// <param name="damage">Damage.</param>
	public void AddDamageText(int damage)
	{
		// Add an empty string to make sure it is not numeric, so the text will not keep appending
		m_damageText.Add("" + damage, Color.white, 0f);
	}

	/// <summary>
	/// Popsup the healing text
	/// </summary>
	/// <param name="healAmt">Heal amt.</param>
	public void AddHealText(int healAmt)
	{
		m_damageText.Add("+" + healAmt, Color.green, 0f);
	}

	/// <summary>
	/// Pop up the evasion text.
	/// </summary>
	public void AddEvasionText()
	{
		m_damageText.Add("Miss", Color.white, 0f);
	}

	/// <summary>
	/// Pop up the absorb text.
	/// </summary>
	public void AddAbsorbText()
	{
		m_damageText.Add("Absorb", Color.white, 0f);
	}

	/// <summary>
	/// Pop up the status effect text.
	/// </summary>
	public void AddStatusText(string statusEffect)
	{
		m_damageText.Add(statusEffect, Color.yellow, 1f);
	}

	/// <summary>
	/// Popup experience gained text
	/// </summary>
	/// <param name="expGained">Exp gained.</param>
	public void AddExpGainedText(int expGained)
	{
		// purple color
		//m_expText.Add("+" + expGained + " EXP", new Color(140f/255f, 47f/255f, 255f/255f, 1f), 0f);
		m_expText.Add("+" + expGained + " EXP", Color.cyan, 0f);
	}


	/// <summary>
	/// Adds the killed text, display you killed message on top
	/// </summary>
	/// <param name="message">Message.</param>
	public void AddKilledText(string message)
	{
		m_expText.Add(message, Color.red, 1f);
	}

	/// <summary>
	/// Sets the allies HUD, such as AvatarBar as well as Marker selection
	/// </summary>
	public void SetAllyHUD(string playerName, int level)
	{
		AvatarBar avatarBarScript = playerAvatarChild.GetComponent<AvatarBar>();
		avatarBarScript.SetPlayerUI(playerName, level);
		SetHUD(true, true, false, false);

		avatarChild = playerAvatarChild;
	}

	/// <summary>
	/// Sets the HUD for creeps, AvatarBar
	/// </summary>
	public void SetCreatureHUD()
	{
		// Default avatarbar is creature
		AvatarBar avatarBarScript = creatureAvatarChild.GetComponent<AvatarBar>();
		//avatarBarScript.SetCreatureUI(this.GetCreatureName());
		avatarBarScript.SetCreatureUI("");	// players should identify by appearance
		SetHUD(false, false, false, true);

		avatarChild = creatureAvatarChild;
	}

	/// <summary>
	/// Sets the HUD for enemy, avatarbar
	/// </summary>
	public void SetEnemyHUD(string enemyName, int enemyLevel)
	{
		AvatarBar avatarBarScript = enemyAvatarChild.GetComponent<AvatarBar>();
		avatarBarScript.SetPlayerUI(enemyName, enemyLevel);
		SetHUD(false, false, true, false);

		avatarChild = enemyAvatarChild;
	}

	private void SetHUD(bool isMarkerVisible, bool isPlayerVisible, bool isEnemyVisible, bool isCreatureVisible)
	{
		if(playerMarker)
		{
			playerMarker.SetActive(isMarkerVisible);
		}
		
		if(playerAvatarChild)
		{
			playerAvatarChild.SetActive(isPlayerVisible);
		}
		
		if(enemyAvatarChild)
		{
			enemyAvatarChild.SetActive(isEnemyVisible);
		}
		
		if(creatureAvatarChild)
		{
			creatureAvatarChild.SetActive(isCreatureVisible);
		}
	}

	public string GetCreatureName()
	{
		return TextUtil.UppercaseFirst(creatureScript.CRace.ToString()) + " " + TextUtil.UppercaseFirst(creatureScript.CClass.ToString());
	}

	void Update()
	{
		// setting creature health
		if( avatarChild != null && creatureScript != null)
		{
			// health and mana
			creatureScript = gameObject.GetComponent<Creature>();
			CreatureStats stats = creatureScript.getStats();
			avatarChild.GetComponent<AvatarBar>().UpdateHealthSlider(stats.Curr_HP / stats.HP);
			avatarChild.GetComponent<AvatarBar>().UpdateManaSlider(stats.Curr_MP / stats.MP);

			// level and bar type
			int ownerID = creatureScript.getOwnerID();
			if(ownerID != -1)
			{
				string ownerName = creatureScript.getOwnerName();
				PhotonPlayer[] players = PhotonNetwork.playerList;
				int level = 0;
				foreach(PhotonPlayer player in players)
				{
					if(player.ID == ownerID)
					{
						level = (int)(player.customProperties["match_level"]);
						break;
					}
				}
			
				Creature playerCreature = PlayerCommand.Instance.getControlledCreature();
				if(playerCreature != null)
				{
					if(creatureScript.areAllies(playerCreature))
					{
						SetAllyHUD(ownerName, level);
					}
					else
					{
						SetEnemyHUD(ownerName, level);
					}
				}
			} 
			else
			{
				SetCreatureHUD();
			}

			if(creatureScript.GetComponent<FOWRenderers>().isVisible)
			{
				if(avatarChild.activeSelf == false)
					avatarChild.SetActive(true);
			}
			else
			{
				if(avatarChild.activeSelf)
					avatarChild.SetActive(false);
			}
		}
	}
}
