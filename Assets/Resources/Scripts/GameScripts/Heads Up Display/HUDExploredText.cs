﻿using UnityEngine;
using System.Collections;

/// <summary>
/// HUD explored text, responsible for displaying text when user explores a certain area
/// </summary>
public class HUDExploredText : MonoBehaviour {

	public GameObject exploredTextPrefab;
	HUDText m_exploredText = null;
	
	// Use this for initialization
	void Awake () 
	{
		// We need the HUD object to know where in the hierarchy to put the element
		if (HUDRoot.go == null)
		{
			GameObject.Destroy(this);
			return;
		}
		
		GameObject exploredTextChild = NGUITools.AddChild(HUDRoot.go, exploredTextPrefab);
		m_exploredText = exploredTextChild.GetComponentInChildren<HUDText>();

		exploredTextChild.transform.localPosition = new Vector3(0f, 180f, 0f);
	}
	
	/// <summary>
	/// Popup the explored text. e.g. "Ruins of Ashram"
	/// </summary>
	/// <param name="message">Message.</param>
	public void AddExploredText(string message)
	{
		if(m_exploredText != null)
			m_exploredText.Add(message, Color.white, 3f);
	}
}
