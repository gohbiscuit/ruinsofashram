﻿using UnityEngine;
using System;
using System.Collections.Generic;

/// <summary>
/// Calculator for creature's stats
/// </summary>
public class CreatureStatsCalculator : MonoBehaviour {

	const float HP_INCREMENT_PER_LVL = 40;
	const float MP_INCREMENT_PER_LVL = 20;
	const float HP_REGEN_INCREMENT_PER_LVL = 1.5f;
	const float MP_REGEN_INCREMENT_PER_LVL = 1;
	const float MAG_ATK_INCREMENT_PER_LVL = 5;
	const float PHY_DEF_INCREMENT_PER_LVL = 2;
	const float MAG_DEF_INCREMENT_PER_LVL = 2;
	const float EVA_INCREMENT_PER_LVL = 1;
	const float MOV_INCREMENT_PER_LVL = 1;
	const float PHY_ATK_INCREMENT_PER_LVL_AGI_BASED = 5;
	const float PHY_ATK_INCREMENT_PER_LVL_STR_BASED = 5;
	const float PHY_ATK_INCREMENT_PER_LVL_INT_BASED = 5;

	private static CreatureStatsCalculator instance;
	Dictionary<string, object[]> statsList;
	CreatureStats wispBaseStats;

	public static CreatureStatsCalculator Instance
	{
		get {
			return instance;
		}
	}
	
	public void Awake () {
		instance = this;

		// load stats from csv
		LoadStats();
	}
	
	/// <summary>
	/// Return the stats of a possessed creature based on the player's own stats
	/// if the player comes from another creature
	/// </summary>
	/// <returns>The old possessed stats.</returns>
	/// <param name="CRace">Creature Race.</param>
	/// <param name="creatureClass">Creature Class.</param>
	/// <param name="prevStats">stats of previous creature</param>
	public CreatureStats computeOldPossessedStats(CreatureRace CRace, CreatureClass creatureClass, CreatureStats prevStats) {

		if(prevStats == null)
			throw new ArgumentException("Invalid stats");

		CreatureStats newStats = computeNewPossessedStats(CRace, creatureClass);

		// set the current HP/MP to be the same proportion of the old stats
		double HPLeftPercentage = prevStats.Curr_HP/prevStats.HP;
		double MPLeftPercentage = prevStats.Curr_MP/prevStats.MP;
		newStats.Curr_HP = (float)(HPLeftPercentage * newStats.HP);
		newStats.Curr_MP = (float)(MPLeftPercentage * newStats.MP);

		return newStats;
	}
	
	/// <summary>
	/// Return the stats of a brand new possessed creature based on the player's own stats
	/// if the player spawns anew
	/// </summary>
	/// <returns>The new possessed stats.</returns>
	/// <param name="CRace">Creature Race</param>
	/// <param name="CClass">Creature Class</param>
	public CreatureStats computeNewPossessedStats(CreatureRace CRace, CreatureClass CClass) {
		CreatureStats stats = new CreatureStats();

		PlayerAttributes attrs = PlayerCommand.Instance.getPlayerAttributes();
		if(attrs == null)
			throw new ArgumentException("invalid player attributes");

		string raceStr = CRace.ToString();
		string classStr = CClass.ToString();
		string keyStr = raceStr + "_" + classStr;

		// See whether Dictionary contains this string.
		if (statsList.ContainsKey(keyStr))
		{
			// FINAL STAT = 
			//		(WISP_BASE_STAT + CREATURE_BASE_STAT)*(PLAYER_ATTRIBUTES BASED ON CREATURE_MULTIPLIER)

			// retrieve (WISP_BASE_STAT + CREATURE_BASE_STAT)
			object[] objArray = (object[]) statsList[keyStr];
			stats = computeBaseStats(stats, objArray);
			if(CRace != CreatureRace.WISP)	// special case: wisp has no additional base improvement
				stats = addWispBase(stats);

			// retrieve CREATURE_MULTIPLIER
			double str_multiplier = (double) objArray[11];
			double agi_multiplier = (double) objArray[12];
			double int_multiplier = (double) objArray[13];

			// add PLAYER_ATTRIBUTES to stats to get FINAL STAT
			stats = addPlayerAttributes(stats, CClass, attrs, 
			                    str_multiplier, int_multiplier, agi_multiplier);

			return stats;
		}
		else
			throw new ArgumentException("invalid creature type");

		return null;
	}

	/// <summary>
	/// Return the stats of a neutral creature (unpossessed form)
	/// </summary>
	/// <returns>The stats of an unpossessed (neutral) creature</returns>
	/// <param name="CRace">Creature Race</param>
	/// <param name="CClass">Creature Class</param>
	public CreatureStats computeUnpossessedStats(CreatureRace CRace, CreatureClass CClass) {

		CreatureStats stats = new CreatureStats();
		string raceStr = CRace.ToString();
		string classStr = CClass.ToString();
		string keyStr = raceStr + "_" + classStr;

		// See whether Dictionary contains this string.
		if (statsList.ContainsKey(keyStr)){
			object[] objArray = (object[]) statsList[keyStr];
			stats = computeBaseStats(stats, objArray);
		} else
			throw new ArgumentException("invalid creature type");

		return stats;
	}

	/// <summary>
	/// Computes the base stats.
	/// </summary>
	/// <returns>The base stats.</returns>
	/// <param name="stats">Stats object to assign the base stats</param>
	/// <param name="para">Parameters</param>
	CreatureStats computeBaseStats(CreatureStats stats, object[] para){

		if(para.Length != 14)
			throw new ArgumentException("invalid parameters" + para.Length);

		stats.EXP_GIVEN = (int) para[0];
		stats.HP = (int) para[1];
		stats.MP = (int) para[2];
		stats.HP_REGEN = (double) para[3];
		stats.MP_REGEN = (double) para[4];
		stats.PHY_ATK = (int) para[5];
		stats.MAG_ATK = (int) para[6];
		stats.PHY_DEF = (int) para[7];
		stats.MAG_DEF = (int) para[8];
		stats.EVA = (int) para[9];
		stats.MOV = (int) para[10];
		stats.SKI = 0;

		MakeFullHPMP(stats);
		return stats;
	}

	/// <summary>
	/// Adds the wisp base statistics to the given stats
	/// </summary>
	/// <returns>The wisp base statistics</returns>
	/// <param name="stats">Stats</param>
	CreatureStats addWispBase(CreatureStats stats){

		if(stats == null)
			throw new ArgumentException("null stats");

		stats.EXP_GIVEN += wispBaseStats.EXP_GIVEN;
		stats.HP += wispBaseStats.HP;
		stats.MP += wispBaseStats.MP;
		stats.HP_REGEN += wispBaseStats.HP_REGEN;
		stats.MP_REGEN += wispBaseStats.MP_REGEN;
		stats.PHY_ATK += wispBaseStats.PHY_ATK;
		stats.MAG_ATK += wispBaseStats.MAG_ATK;
		stats.PHY_DEF += wispBaseStats.PHY_DEF;
		stats.MAG_DEF += wispBaseStats.MAG_DEF;
		stats.EVA += wispBaseStats.EVA;
		stats.MOV += wispBaseStats.MOV;
		stats.SKI += wispBaseStats.SKI;

		MakeFullHPMP(stats);
		return stats;
	}

	/// <summary>
	/// Adds the player attributes to the given stats.
	/// </summary>
	/// <returns>The affected stats</returns>
	/// <param name="stats">Original stats</param>
	/// <param name="creatureClass">Creature Class</param>
	/// <param name="attrs">Player attributes</param>
	/// <param name="str_multiplier">Strength multiplier</param>
	/// <param name="int_multiplier">Intelligence multiplier.</param>
	/// <param name="agi_multiplier">Agility multiplier.</param>
	CreatureStats addPlayerAttributes(CreatureStats stats, CreatureClass creatureClass, PlayerAttributes attrs, 
	                                  double str_multiplier, double int_multiplier, double agi_multiplier){

		if(stats == null)
			throw new ArgumentException("null stats");
		else if(attrs == null)
			throw new ArgumentException("null player attributes");

		double lvl = attrs.CURRENT_LEVEL;
		double str_ratio = str_multiplier * attrs.STRENGTH;
		double int_ratio = int_multiplier * attrs.INTELLIGENCE;
		double agi_ratio = agi_multiplier * attrs.AGILITY;

		stats.HP += (int)(str_ratio * HP_INCREMENT_PER_LVL * lvl);
		stats.MP += (int)(int_ratio * MP_INCREMENT_PER_LVL * lvl);
		stats.HP_REGEN += str_ratio * HP_REGEN_INCREMENT_PER_LVL * lvl;
		stats.MP_REGEN += int_ratio * MP_REGEN_INCREMENT_PER_LVL * lvl;
		stats.MAG_ATK += (int)(int_ratio * MAG_ATK_INCREMENT_PER_LVL * lvl);
		stats.PHY_DEF += (int)(str_ratio * PHY_DEF_INCREMENT_PER_LVL * lvl);
		stats.MAG_DEF += (int)(int_ratio * MAG_DEF_INCREMENT_PER_LVL * lvl);
		stats.EVA += (int)(agi_ratio * EVA_INCREMENT_PER_LVL * lvl);
		stats.MOV += (int)(agi_ratio * MOV_INCREMENT_PER_LVL * lvl);
		stats.SKI += (int)(attrs.SKILL_MASTERY);

		// physical attack for agility classes scale by agility, * 10 means every level the PHY ATTK increase by 10, since ratio is 1
		// physical attack for strength classes scale by strength, * 10 means every level the PHY ATTK increase by 10, since ratio is 1
		// physical attack for intelligence classes scale by intelligence, * 10 means every level the PHY ATTK increase by 10, since ratio is 1
		if(creatureClass == CreatureClass.ROGUE || creatureClass == CreatureClass.ARCHER || creatureClass == CreatureClass.GUNNER)
			stats.PHY_ATK += (int)(agi_ratio * PHY_ATK_INCREMENT_PER_LVL_AGI_BASED * lvl);
		else if(creatureClass == CreatureClass.WARRIOR)
			stats.PHY_ATK += (int)(str_ratio * PHY_ATK_INCREMENT_PER_LVL_STR_BASED * lvl);
		else if(creatureClass == CreatureClass.MAGE || creatureClass == CreatureClass.WARLOCK || 
		        creatureClass == CreatureClass.PRIEST || creatureClass == CreatureClass.SHAMAN || 
		        creatureClass == CreatureClass.PALADIN)
			stats.PHY_ATK += (int)(int_ratio * PHY_ATK_INCREMENT_PER_LVL_INT_BASED * lvl);

		MakeFullHPMP(stats);
		return stats;
	}

	/// <summary>
	/// Increase the stats to Full HP and MP
	/// </summary>
	/// <param name="stats">Stats to increase</param>
	void MakeFullHPMP(CreatureStats stats){
		if(stats == null)
			throw new ArgumentException("null stats object");
		stats.Curr_HP = stats.HP;
		stats.Curr_MP = stats.MP;
	}

	/// <summary>
	/// Reads in the stats
	/// </summary>
	public void LoadStats()
	{
		statsList = Utility.ReadStatCSV();
		if(statsList == null)
			throw new ArgumentException("file cannot be loaded");
		else {
			// cache wisp base stats, which plays a role in final computation of stats for all creatures
			wispBaseStats = computeUnpossessedStats(CreatureRace.WISP, CreatureClass.WISP);
		}
	}

	/// <summary>
	/// Gets the creature stats using the serialised stats string array
	/// </summary>
	/// <returns>The creature stats.</returns>
	/// <param name="serializedCreatureStats">Serialized creature stats.</param>
	public CreatureStats getCreatureStats(string[] serializedCreatureStats){

		if(serializedCreatureStats.Length != 14)
			throw new ArgumentException("invalid length");

		CreatureStats stats = new CreatureStats();
		double tempDouble;
		float tempFloat;
		int tempInt;

		if(int.TryParse(serializedCreatureStats[0], out tempInt))
			stats.EXP_GIVEN = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(float.TryParse(serializedCreatureStats[1], out tempFloat))
			stats.Curr_HP = tempFloat;
		else
			throw new ArgumentException("wrong string array format");

		if(float.TryParse(serializedCreatureStats[2], out tempFloat))
			stats.Curr_MP = tempFloat;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[3], out tempInt))
			stats.HP = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[4], out tempInt))
			stats.MP = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(double.TryParse(serializedCreatureStats[5], out tempDouble))
			stats.HP_REGEN = tempDouble;
		else
			throw new ArgumentException("wrong string array format");

		if(double.TryParse(serializedCreatureStats[6], out tempDouble))
			stats.MP_REGEN = tempDouble;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[7], out tempInt))
			stats.PHY_ATK = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[8], out tempInt))
			stats.MAG_ATK = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[9], out tempInt))
			stats.PHY_DEF = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[10], out tempInt))
			stats.MAG_DEF = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[11], out tempInt))
			stats.EVA = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[12], out tempInt))
			stats.MOV = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		if(int.TryParse(serializedCreatureStats[13], out tempInt))
			stats.SKI = tempInt;
		else
			throw new ArgumentException("wrong string array format");

		return stats;
	}

	/// <summary>
	/// Gets the serizalized creature stats using the stats object.
	/// </summary>
	/// <returns>The serizalized creature stats.</returns>
	/// <param name="stats">Stats.</param>
	public string[] getSerizalizedCreatureStats(CreatureStats stats){
		if(stats == null)
			throw new ArgumentException("null stats object");
		return new string[]{
			stats.EXP_GIVEN.ToString(),
			stats.Curr_HP.ToString(), stats.Curr_MP.ToString(), 
			stats.HP.ToString(), stats.MP.ToString(), 
			stats.HP_REGEN.ToString(), stats.MP_REGEN.ToString(),
			stats.PHY_ATK.ToString(), stats.MAG_ATK.ToString(), 
			stats.PHY_DEF.ToString(), stats.MAG_DEF.ToString(),
			stats.EVA.ToString(), stats.MOV.ToString(), stats.SKI.ToString()
		};
	}
}
