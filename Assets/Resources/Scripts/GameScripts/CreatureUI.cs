﻿using UnityEngine;
using System.Collections;

public class CreatureUI : MonoBehaviour {

	Camera worldCamera;
	Camera uiCamera;
	UISprite hpBar;
	UILabel nameLabel;
	Creature creature;
	
	// Use this for initialization
	void Awake () {
		
		creature = GetComponent<Creature>();
		
		//worldCamera = (Camera)(GameObject.Find("Main Camera").GetComponent("Camera"));
		//uiCamera = (Camera)(GameObject.Find ("UI Root (2D)").transform.FindChild("Camera").GetComponent("Camera"));

		// Instantiation of HP Bar
		/*GameObject hpBarPrefab = (GameObject)(Resources.Load("Prefab/HPBar"));
		GameObject hpBarGameObject = (GameObject)(Instantiate(hpBarPrefab, Vector3.zero, Quaternion.identity));
		hpBar = hpBarGameObject.GetComponent<UISprite>();
		hpBar.transform.parent = uiCamera.transform.FindChild("Top-Left").FindChild("Panel").transform;
		hpBar.transform.localScale = new Vector3(1, 4, 1);
		hpBar.GetComponent<HPBar>().setCreature(creature);

		// Instantiation of Name Label
		GameObject namePrefab = (GameObject)(Resources.Load("Prefab/NameLabel"));
		GameObject nameLabelGameObject = (GameObject)(Instantiate(namePrefab, Vector3.zero, Quaternion.identity));
		nameLabel = nameLabelGameObject.GetComponent<UILabel>();
		nameLabel.transform.parent = uiCamera.transform.FindChild("Top-Left").FindChild("Panel").transform;
		nameLabel.transform.localScale = new Vector3(10, 10, 10);
		nameLabel.GetComponent<NameLabel>().setCreature(creature);*/
	}
	
	// Update is called once per frame
	void LateUpdate () {
		/*Vector3 pos = worldCamera.WorldToViewportPoint(transform.position + new Vector3(-1.5f,0,2));
		pos = uiCamera.ViewportToWorldPoint(pos);
		pos.z = 0;

		// update HP
		hpBar.transform.position = pos;
		if(creature.getStats() != null)
			updateHP(creature.getStats().Curr_HP);

		// update name
		nameLabel.transform.position = pos + new Vector3(0.05f,0.02f,0);
		nameLabel.text = creature.getOwnerName();*/
	}
	
	public void updateHP(float HP){
		/*float HPlength = HP/6;
		if(HPlength<=0)
			HPlength = 0.001f;
		hpBar.transform.localScale = new Vector3(HPlength,
												hpBar.transform.localScale.y,
												hpBar.transform.localScale.z);*/
	}
}
