﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Interface/Adapter between Creature Class & AI Class.
/// </summary>
public interface CreatureInterface {

	/// <summary>
	/// Returns the transform of the creature which provides the position and rotation of the creature's gameobject.
	/// </summary>
	Transform TransformProperty {
		get;
	}

	/// <summary>
	/// Returns the current HP of the creature after induced lag.
	/// </summary>
	float HP {
		get;
	}

	/// <summary>
	/// Returns the maximum HP of the creature after induced lag.
	/// </summary>
	float MaxHP {
		get;
	}

	/// <summary>
	/// Returns the current MP of the creature after induced lag.
	/// </summary>
	float MP {
		get;
	}

	/// <summary>
	/// Returns the maximum MP of the creature after induced lag.
	/// </summary>
	float MaxMP {
		get;
	}


	/// <summary>
	/// Returns the game object of the creature.
	/// </summary>
	GameObject GameObjectProperty {
		get;
	}

	/// <summary>
	/// Returns the spawn point of the creature.
	/// </summary>
	CreatureSpawn SpawnPoint {
		get;
	}

	/// <summary>
	/// Returns the class of the creature, such as ARCHER or WARRIOR.
	/// </summary>
	CreatureClass CClass {
		get;
	}

	/// <summary>
	/// Returns the race of the creature, such as RATKIN or OGRE.
	/// </summary>
	CreatureRace CRace {
		get;
	}

	/// <summary>
	/// Returns the last creature to hit me. Returns null if
	/// no creature has hit it, or the creature has already died.
	/// </summary>
	Creature LastCreatureToHitMe {
		get;
	}

	/// <summary>
	/// Stop moving or using skills after induced lag.
	/// </summary>
	void StopLater();
	
	/// <summary>
	/// Starts moving towards the target after induced lag.
	/// </summary>
	void MoveLater(object target);

	/// <summary>
	/// Uses the given skill of the creature on a target after an induced lag, which can either be a Vector3 position or Creature creatureTarget.
	/// Whenever this is called, the creature will chase after it. When it is within range, it will start charging and casts the skill.
	/// Persistent (basic attack) skills will not stop until overwritten by another command.
	///
	/// Returns SUCCESS if successful, otherwise returns a message with the associated error.
	/// </summary>
	SkillUsingMessage UseSkillLater(Skill skill, object target);

	/// <summary>
	/// Use canUseSkill(Skill skill) if a target does not exist instead of this.
	/// Check if the skill is usable (has a valid target, may not have cooled, not enough MP, is a passive skill or creature is stunned)
	/// </summary>
	SkillUsingMessage canUseSkill(Skill skill, object target);

	/// <summary>
	/// Use canUseSkill(Skill skill, object target) if a target exists instead of this.
	/// Check if the skill is usable (may not have cooled, not enough MP, is a passive skill or creature is stunned)
	/// </summary>
	SkillUsingMessage canUseSkill(Skill skill);

	/// <summary>
	/// Checks if the skill is currently being in use before it is being executed after the induced lag.
	/// Returns true right after UseSkillLater is used, and returns false if the skill has finished execution (but SkillObject may still be around)
	/// Note that persistent (basic attack), after used, will always return true until another command is given.
	/// </summary>
	bool isStillUsingSkillLater(Skill skill, object target);

	/// <summary>
	/// Checks if the creature has the given status effect type before the induced lag in the skill done by this creature.
	/// </summary>
	bool hasStatusEffectLater(StatusEffect effectType);

	/// <summary>
	/// Returns the skill list of this creature. The 1st skill is Release/Possess, 2nd skill is a basic attack and the rest are special skills/spells.
	/// </summary>
	List<Skill> getSkillList();
}
