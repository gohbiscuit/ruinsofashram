using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;

public enum CreatureClass { WISP, WARRIOR, ARCHER, ROGUE, MAGE, WARLOCK, PRIEST, PALADIN, SHAMAN, GUNNER };
public enum CreatureRace { WISP, RATKIN, OGRE, SKELETON, DEMON, WEREWOLF };
public enum CreatureCommands { NONE, STOP, MOVE, USESKILL };
public enum SkillUsingMessage { SUCCESS, NOT_ENOUGH_MP, IS_STUNNED, NOT_COOLED, IS_PASSIVE, INVALID_TARGET };

/// <summary>
/// Governs the creature (which can also be a wisp)
/// </summary>
public class Creature : MonoBehaviour, CreatureInterface {

	const float BASE_MOVEMENT_SPEED = 2.8f;
	const float MOV_STATS_DAMPENING = 12.0f;

	const int SQR_DISTANCE_SHOW_ICON_NONE = 600;
	const int SQR_DISTANCE_SHOW_ICON_RATKIN_EYE = 3000;

	// properties
	[SerializeField] CreatureClass m_creatureClass;
	[SerializeField] CreatureRace m_creatureRace;
	Team m_team;
	CreatureStats m_stats;
	CreatureStatusEffects m_statusEffects;
	RuneEffect m_runeEffect;
	List<Skill> m_equippedSkills;
	int m_ownerID = -1;	// -1 if not player controlled

	// variables
	Skill m_currSkill; Skill m_skillForLater;
	object m_target; object m_skillTargetForLater;
	Vector3 m_destination;
	Creature m_lastCreatureToHitMe;
	float m_timeSinceLastHit;
	bool m_continueBasicAtk;
	CreatureIconType m_minimapIconType;

	// reference
	CreatureSpawn m_spawnPoint;
	PhotonView m_photonView;
	CreatureMoveAgent m_agent;
	KGFMapIcon m_minimapIcon;
	[SerializeField] PlayMakerFSM m_fsm;
	[SerializeField] GameObject m_corpsePrefab;
	[SerializeField] string[] m_dyingSounds;

	// animation
	public Animation animation;
	[SerializeField] AnimationClip m_stopAnimation;
	[SerializeField] AnimationClip m_walkAnimation;
	[SerializeField] AnimationClip m_readyAnimation;
	[SerializeField] AnimationClip m_chargeAnimation;

	// behaviourial states
	CreatureState m_currState;
	CreatureStopState m_creatureStopState;
	CreatureWalkState m_creatureWalkState;
	CreatureChaseState m_creatureChaseState;
	CreatureCooldownState m_creatureCooldownState;
	CreatureChargeState m_creatureChargeState;
	CreatureCastState m_creatureCastState;

	// commands
	CreatureCommands m_prevCommand;
	object[] m_prevCommandParameters = new object[2];
	CreatureCommands m_pendingCommand;
	object[] m_pendingCommandParameters = new object[2];
	bool m_unexecutedCommand = false;

	// network round timing
	int m_lastSeenRound = 0;
	float m_roundsPerSec;
	float m_onePerRound;

	// means that this creature is the authority
	// meaning that the creature sends message if isMine==true
	// and remotely, the same creature passively receives message
	// not to be confused with player control!!
	// in our architecture, MasterClient sets all AI + own Player Creature to true
	//						all other clients set only own Player Creature to true
	bool m_isMine;

	// ------------------------------------------------------------------

	/// <summary>
	/// performed once the moment the GameObject 'wakes up'
	/// </summary>
	void Awake () {
		// initialise concrete states
		m_creatureStopState = new CreatureStopState(this);
		m_creatureWalkState = new CreatureWalkState(this);
		m_creatureChaseState = new CreatureChaseState(this);
		m_creatureCooldownState = new CreatureCooldownState(this);
		m_creatureChargeState = new CreatureChargeState(this);
		m_creatureCastState = new CreatureCastState(this);
		
		// start state
		m_currState = m_creatureStopState;
		
		// initialise skills
		m_equippedSkills = SkillFactory.Instance.getCreatureSkills(m_creatureRace, m_creatureClass);
		foreach(Skill skill in m_equippedSkills){
			skill.setUser(this);
			if(skill.isPassive())		// use passive skills immediately
				skill.Activate(this);
		}
		
		// initialise references
		animation = GetComponentInChildren<Animation>();
		m_photonView = GetComponent<PhotonView>();
		m_roundsPerSec = 1/GameNetworkManager.Instance.RoundLength;
		m_onePerRound = (float)(1/m_roundsPerSec);
		m_statusEffects = new CreatureStatusEffects(this);
		m_runeEffect = new RuneEffect(this);
		m_agent = new CreatureMoveAgent(GetComponent<RichAI>());
		m_minimapIcon = GetComponent<KGFMapIcon>();

		// reset round seen
		m_lastSeenRound = GameNetworkManager.Instance.ProcessedRounds;
	}
	
	/// <summary>
	/// performs every frame
	/// </summary>
	void Update () {

		// determine whether to hide or show map icon (distance-based)
		// ratkin's eye has much further range
		if(m_minimapIconType == CreatureIconType.NEUTRAL || 
		   m_minimapIconType == CreatureIconType.ENEMY){
			Creature playerCreature = PlayerCommand.Instance.getControlledCreature();

			if(playerCreature != null){
				int viewRange = SQR_DISTANCE_SHOW_ICON_NONE;
				if(playerCreature.hasStatusEffectNow(StatusEffect.RATKIN_EYE))
				   viewRange = SQR_DISTANCE_SHOW_ICON_RATKIN_EYE;

				if((playerCreature.transform.position - transform.position).sqrMagnitude < viewRange)
					m_minimapIcon.SetVisibility(true);
				else
					m_minimapIcon.SetVisibility(false);
			} else
				m_minimapIcon.SetVisibility(false);

		} else {
			// Ally or Player
			m_minimapIcon.SetVisibility(true);
		}

		
		m_timeSinceLastHit += Time.deltaTime;
		
		// execute any pending command
		if(m_pendingCommand != CreatureCommands.NONE){
			switch(m_pendingCommand){
			case CreatureCommands.STOP:
				StopLater();
				break;
			case CreatureCommands.MOVE:
				MoveLater(m_pendingCommandParameters[0]);
				break;
			case CreatureCommands.USESKILL:
				UseSkillLater((Skill)(m_pendingCommandParameters[0]), m_pendingCommandParameters[1]);
				break;
			}
		}

		// execute current state every frame
		m_currState.Do();
		
		// get time that has passed
		int currRound = GameNetworkManager.Instance.ProcessedRounds;
		int missedRounds = currRound - m_lastSeenRound;
		m_lastSeenRound = currRound;

		// update regeneration of MP/HP
		float hpRegenPerRound = (float)(m_stats.HP_REGEN/m_roundsPerSec);
		float mpRegenPerRound = (float)(m_stats.MP_REGEN/m_roundsPerSec);
		m_stats.Curr_HP = Mathf.Min(m_stats.HP, (float)(m_stats.Curr_HP + hpRegenPerRound*missedRounds));
		m_stats.Curr_MP = Mathf.Min(m_stats.MP, (float)(m_stats.Curr_MP + mpRegenPerRound*missedRounds));
		
		// update skill cooldown/charge
		foreach(Skill skill in m_equippedSkills){
			skill.Cooldown(m_onePerRound*missedRounds);
			skill.Charge(m_onePerRound*missedRounds);
		}

		// update status and rune effects
		m_statusEffects.UpdateStatusEffects(m_onePerRound*missedRounds);
		m_runeEffect.UpdateRuneEffect(m_onePerRound*missedRounds);
	}
	
	/// <summary>
	/// calls 'event' to transit to another state
	/// </summary>
	public void fireEvent(string eventFSM){
		CreatureState oldState = m_currState;
		
		// send event to transit in PlayMaker
		m_fsm.SendEvent(eventFSM);
		
		// set current state to the new active state in PlayMaker
		switch(m_fsm.ActiveStateName){
			
		case "Stop":
			m_currState = m_creatureStopState;
			break;
		case "Walk":
			m_currState = m_creatureWalkState;
			break;
		case "Chase":
			m_currState = m_creatureChaseState;
			break;
		case "Cooldown":
			m_currState = m_creatureCooldownState;
			break;
		case "Charge":
			m_currState = m_creatureChargeState;
			break;
		case "Cast":
			m_currState = m_creatureCastState;
			break;
			
		}
		
		oldState.Exit();		// execute exit activity
		m_currState.Entry();	// execute entry activity
	}

	// ------------------------------------------------------------------

	#region Networked Functions
	// - all calls to these functions will not be executed immediately,
	//   a 'lag' time must pass before the 'actual' functions are executed
	//   the same command is sent to the rest and executed optimally at the same time

	/// <summary>
	/// Stop moving or using skills after induced lag.
	/// </summary>
	public void StopLater(){

		// if command already given to stop, ignore
		if(m_prevCommand == CreatureCommands.STOP)
			return;

		// postpone command to later if character is casting skill
		if(m_currState == m_creatureCastState){
			m_pendingCommand = CreatureCommands.STOP;
			return;
		}

		m_prevCommand = CreatureCommands.STOP;

		m_pendingCommand = CreatureCommands.NONE;
		removeSkillLaterCommand();

		// store stop command to execute later
		GameNetworkManager.Instance.StoreStop(m_photonView);

		m_unexecutedCommand = true;
	}
	
	/// <summary>
	/// Starts moving towards the target after induced lag.
	/// </summary>
	/// <param name="target">target (Creature or Vector3) to move to</param>
	public void MoveLater(object target){

		if(target == null)
			StopLater();

		if(!(target is Vector3 || target is Creature))
			throw new ArgumentException("Invalid target type " + target.GetType());

		// cannot perform actions when stunned or assassin locked
		if(m_statusEffects.isStunned() || m_statusEffects.isAssassinLocked())
			return;

		// if move to the same place/creature last time, ignore
		if(m_prevCommand == CreatureCommands.MOVE ||
		   m_prevCommand == CreatureCommands.STOP){
			if(target is Vector3 && m_prevCommandParameters[0] is Vector3){
				Vector3 newDest = (Vector3)(target);
				Vector3 oldDest = (Vector3)(m_prevCommandParameters[0]);
				if(oldDest.AlmostEquals(newDest, 0.01f))
					return;
			} else if(target is Creature && m_prevCommandParameters[0] is Creature){
				if((Creature)(target) == (Creature)(m_prevCommandParameters[0]))
					return;
			}
		}

		// postpone command to later if character is casting skill
		if(m_currState == m_creatureCastState){
			m_pendingCommand = CreatureCommands.MOVE;
			m_pendingCommandParameters[0] = target;
			return;
		}

		m_prevCommand = CreatureCommands.MOVE;
		m_prevCommandParameters[0] = target;

		m_pendingCommand = CreatureCommands.NONE;
		removeSkillLaterCommand();

		// store move command to execute later
		if(target is Creature)
			GameNetworkManager.Instance.StoreMoveToCreature(m_photonView, ((Creature)(target)).m_photonView);
		else if(target is Vector3)
			GameNetworkManager.Instance.StoreMoveToPosition(m_photonView, (Vector3)(target));

		m_unexecutedCommand = true;
	}
	
	/// <summary>
	/// Uses the given skill of the creature on a target after an induced lag, which can either be a Vector3 position or Creature creatureTarget.
	/// Whenever this is called, the creature will chase after it. When it is within range, it will start charging and casts the skill.
	/// Persistent (basic attack) skills will not stop until overwritten by another command.
	///
	/// Returns SUCCESS if successful, otherwise returns a message with the associated error.
	/// </summary>
	/// <returns>error/success message</returns>
	/// <param name="skill">skill to use</param>
	/// <param name="target">target to use skill on</param>
	public SkillUsingMessage UseSkillLater(Skill skill, object target){

		if(!(target is Vector3 || target is Creature))
			throw new ArgumentException("Invalid target type : " + skill.SkillName);

		// check if skill can be used
		SkillUsingMessage msg = canUseSkill(skill, target);
		if(msg != SkillUsingMessage.SUCCESS)
			return msg;

		// if use the same skill on the same target, ignore
		if(m_prevCommand == CreatureCommands.USESKILL){
			Skill lastSkill = (Skill)(m_prevCommandParameters[0]);
			object lastTarget = (object)(m_prevCommandParameters[1]);
			if(lastSkill == skill && lastTarget == target)
				return SkillUsingMessage.SUCCESS;
		}

		// postpone command to later if character is casting skill
		if(m_currState == m_creatureCastState){
			m_pendingCommand = CreatureCommands.USESKILL;
			m_pendingCommandParameters[0] = skill;
			m_pendingCommandParameters[1] = target;
			return SkillUsingMessage.SUCCESS;
		}

		m_prevCommand = CreatureCommands.USESKILL;
		m_prevCommandParameters[0] = skill;
		m_prevCommandParameters[1] = target;

		// store to check if a skill is executing later on
		m_pendingCommand = CreatureCommands.NONE;
		m_skillForLater = skill;
		m_skillTargetForLater = target;

		// store useSkill command to execute later
		if(target is Creature)
			GameNetworkManager.Instance.StoreUseSkill(m_photonView, skill.getSkillTypeID(), ((Creature)(target)).m_photonView);
		else
			GameNetworkManager.Instance.StoreUseSkill(m_photonView, skill.getSkillTypeID(), (Vector3)(target));

		m_unexecutedCommand = true;

		return SkillUsingMessage.SUCCESS;
	}
	
	/// <summary>
	/// Takes the damage later.
	/// </summary>
	/// <param name="skillID">ID of the skill that deals the damage</param>
	/// <param name="attacker">The creature that deals the damage</param>
	public void TakeDamageLater(int skillID, Creature attacker){
		TakeDamageLater(skillID, attacker, 1.0f);
	}
	
	/// <summary>
	/// Takes the damage later.
	/// </summary>
	/// <param name="skillID">ID of the skill that deals the damage</param>
	/// <param name="attacker">The creature that deals the damage</param>
	/// <param name="multiplier">damage multiplier</param>
	public void TakeDamageLater(int skillID, Creature attacker, float multiplier){

		// compute the damage to deal
		int dmg = SkillFactory.Instance.getDamage(skillID, attacker, this);

		if(dmg < -1)
			throw new ArgumentException("Invalid damage output");

		// if not evaded, and there's not enough HP (after shield's effect) to take the damage, then send die message
		if(dmg != -1 && (m_stats.Curr_HP - m_statusEffects.getDamageLeftFromManaShield(
			m_statusEffects.getDamageLeftFromEarthShield((int)(multiplier*dmg)))) <= 0)
			DieLater(skillID, attacker, (int)(multiplier*dmg));
		else
			// otherwise, simply send message for the creature to take damage
			GameNetworkManager.Instance.StoreTakeDamage(m_photonView, attacker.m_photonView, (int)(dmg*multiplier));
	}
	
	/// <summary>
	/// Kill this creature after induced lag.
	/// </summary>
	/// <param name="skillID">ID of the skill that deals the damage</param>
	/// <param name="attacker">The creature that deals the damage</param>
	/// <param name="dmg">actual damage dealt (after applying multiplier and status)</param>
	void DieLater(int skillID, Creature attacker, int dmg){
		GameNetworkManager.Instance.StoreDie(m_photonView, attacker.m_photonView, dmg);
	}
	
	/// <summary>
	/// Make this creature gain health after an induced lag
	/// </summary>
	/// <param name="skillID">ID of the skill that heals</param>
	/// <param name="healer">The healer</param>
	public void GainHealthLater(int skillID, Creature healer){

		// compute the health to increase
		int gain = SkillFactory.Instance.getHealedAmt(skillID, healer, this);

		if(gain < -1)
			throw new ArgumentException("Invalid recovery output");

		GameNetworkManager.Instance.StoreGainHealth(m_photonView, gain);
	}

	/// <summary>
	/// due to change in player's attribute (lvl up, +str), do a recomputation of m_stats
	/// not to be used for initialising of m_stats for possessed creatures due to latency
	/// </summary>
	public void RecomputePossessedStatsLater(){

		// if this creature belongs to the player
		if(PlayerCommand.Instance.isControlling(this)){

			// refresh its stats
			CreatureStats newStats = CreatureStatsCalculator.Instance.computeOldPossessedStats(m_creatureRace, m_creatureClass, m_stats);

			// send it over all other clients
			GameNetworkManager.Instance.StoreRecomputePossessedStats(m_photonView, newStats);
		} else
			throw new ArgumentException("Not to be used with unpossessed creatures");
	}

	/// <summary>
	/// remove the status effect of the type
	/// </summary>
	/// <param name="type">Type of status to remove</param>
	/// <param name="intensityToRemove">Type of status intensity to remove</param>
	public void RemoveStatusEffectLater(StatusEffect type, float intensityToRemove){
		m_statusEffects.RemoveStatusEffectLater(type, intensityToRemove);
	}
	
	/// <summary>
	/// afflict a status effect of the type, with the duration and intensity (if any)
	/// </summary>
	/// <param name="type">Type of status</param>
	/// <param name="duration">Duration of status</param>
	/// <param name="intensity">Intensity of status (if any)</param>
	public void AfflictStatusEffectLater(StatusEffect type, float duration, float intensity){
		m_statusEffects.AfflictStatusEffectLater(type, duration, intensity);
	}

	#endregion

	// -------------------------------------------------------------------------------------------

	#region 'Actual' Networked Functions
	// - all calls are executed immediately
	//   Networked Functions -> GameNetworkManager ('creates' lag) and broadcast to all -> 'Actual' Networked Functions

	/// <summary>
	/// Stop moving or using skills now.
	/// </summary>
	public void StopLocallyNow(){
		m_currState.Player_Stop();
		m_unexecutedCommand = false;
	}

	/// <summary>
	/// Starts moving towards the target now.
	/// </summary>
	/// <param name="target">target (Creature or Vector3) to move to</param>
	public void MoveLocallyNow(object target){
		if(!(target is Vector3 || target is Creature))
			throw new ArgumentException("Invalid target type");

		m_currState.Player_Move(target);
		m_unexecutedCommand = false;
	}

	/// <summary>
	/// Uses the given skill of the creature on a target now, which can either be a Vector3 position or Creature creatureTarget.
	/// Whenever this is called, the creature will chase after it. When it is within range, it will start charging and casts the skill.
	/// Persistent (basic attack) skills will not stop until overwritten by another command.
	/// </summary>
	/// <param name="skill">skill to use</param>
	/// <param name="target">target to use skill on</param>
	public void UseSkillLocallyNow(Skill skill, System.Object target){
		if(!(target is Vector3 || target is Creature))
			throw new ArgumentException("Invalid target type");

		m_currState.Player_UseSkill(skill, target);
		m_unexecutedCommand = false;
	}

	/// <summary>
	/// Takes the damage now.
	/// </summary>
	/// <param name="attacker">The creature that deals the damage</param>
	/// <param name="damage">damage dealt</param>
	public void TakeDamageLocallyNow(Creature attacker, int damage){
		if(damage >= 0){

			// recover assassin's lock when damaged
			if(m_statusEffects.isAssassinLocked())
				m_statusEffects.RemoveStatusEffectsLocallyNow(StatusEffect.ASSASSIN_LOCK, 0);

			// damage after shielding
			damage = m_statusEffects.absorbMPFromManaShield(
				m_statusEffects.getDamageLeftFromEarthShield(damage));

			// update last attacker/time
			m_lastCreatureToHitMe = attacker;
			m_timeSinceLastHit = 0;

			// inform command that the creature took damage for display in HUD
			PlayerCommand.Instance.creatureTookDamage(this, damage);

			// deal damage to HP
			m_stats.Curr_HP -= damage;

			// if due to some unexpected network lag, creature may not have died when command
			// is first sent, but yet do not have enough HP now, then keep it barely alive.
			if(m_stats.Curr_HP < 0)
				m_stats.Curr_HP = 1;
	
		} else if(damage == -1) {

			// evaded
			// inform command that the creature evaded the damage for display in HUD
			PlayerCommand.Instance.creatureTookDamage(this, damage);

		} else
			throw new ArgumentException("Invalid damage output");
	}

	/// <summary>
	/// Kill this creature now.
	/// </summary>
	/// <param name="attacker">The creature that deals the damage</param>
	/// <param name="dmg">actual damage dealt (after applying multiplier and status)</param>
	public void DieLocallyNow(Creature attacker, int damage){

		// HP Reduction from Shield
		damage = m_statusEffects.absorbMPFromManaShield(
			m_statusEffects.getDamageLeftFromEarthShield(damage));

		// Reduce the HP
		m_stats.Curr_HP -= damage;

		// inform spawn point that it has died so it can spawn a new one
		InformSpawnPointDeath();

		Die (attacker);

		// player gains experience/kill if he's the attacker
		if(PlayerCommand.Instance.isControlling(attacker)){

			int ownerID = getOwnerID();
			PhotonPlayer[] players = PhotonNetwork.playerList;
			int level = 1;
			bool victimIsPlayer = false;
			foreach(PhotonPlayer player in players){
				if(player.ID == ownerID){
					level = (int)(player.customProperties["match_level"]);
					victimIsPlayer = true;
					break;
				}
			}

			PlayerCommand.Instance.madeKill(m_stats.EXP_GIVEN + (level-1)*50, victimIsPlayer);
		}
	}

	/// <summary>
	/// Make this creature gain health now
	/// </summary>
	/// <param name="gain">healed amount</param>
	public void GainHealthLocallyNow(int gain){

		// Increase HP
		m_stats.Curr_HP = Mathf.Min(m_stats.HP, m_stats.Curr_HP + gain);

		// inform HUD to display the increase
		PlayerCommand.Instance.creatureGainHealth(this, gain);
	}

	/// <summary>
	/// Set the new stats of this creature
	/// </summary>
	public void RecomputePossessedStatsLocallyNow(CreatureStats newStats){

		// update stats
		m_stats = newStats;

		// set the movement speed based on the new stats
		BaseSpdOnMovStats();

		// add the currently ongoing status effects on this new stats
		m_statusEffects.newlyAffect();
	}

	#endregion

	// ------------------------------------------------------------------

	/// <summary>
	/// Checks if the skill is currently being in use before it is being executed after the induced lag.
	/// Returns true right after UseSkillLater is used, and returns false if the skill has finished execution (but SkillObject may still be around)
	/// Note that persistent (basic attack), after used, will always return true until another command is given.
	/// </summary>
	/// <returns><c>true</c>, if still executing, <c>false</c> otherwise.</returns>
	/// <param name="skill">Skill to check</param>
	/// <param name="target">Target skill is being used on</param>
	public bool isStillUsingSkillLater(Skill skill, object target){

		if(!(target is Vector3 || target is Creature))
		   throw new ArgumentException("Invalid target type");

		if(m_skillForLater == null)
			return false;
		else if(skill == m_skillForLater && m_skillTargetForLater == target)
			return true;
		else
			return false;
	}
	
	/// <summary>
	/// Checks if the creature has the given status effect type before the induced lag in the skill done by this creature.
	/// </summary>
	/// <returns><c>true</c>, if status effect will be in effect, <c>false</c> otherwise.</returns>
	/// <param name="effectType">Effect type to check</param>
	public bool hasStatusEffectLater(StatusEffect effectType){
		switch(effectType){
		case StatusEffect.STUN:
			return m_statusEffects.isStunnedLater();
		case StatusEffect.ASSASSIN_LOCK:
			return m_statusEffects.isAssassinLockedLater();
		case StatusEffect.SLOW:
			return m_statusEffects.isSlowedLater();
		case StatusEffect.ORGE_AURA:
			return m_statusEffects.isOgreAuraedLater();
		case StatusEffect.MANA_SHIELD:
			return m_statusEffects.isManaShieldedLater();
		case StatusEffect.FREEZING_SHIELD:
			return m_statusEffects.isFreezingShieldedLater();
		case StatusEffect.EARTH_SHIELD:
			return m_statusEffects.isEarthShieldedLater();
		case StatusEffect.BATTLE_ROAR:
			return m_statusEffects.isBattleRoaredLater();
		case StatusEffect.SHIELD_BARRIER:
			return m_statusEffects.isShieldBarrieredLater();
		case StatusEffect.RATKIN_EYE:
			return m_statusEffects.isRatkinEyedLater();
		}
		
		throw new ArgumentException("Invalid status effect type");
		return false;
	}
	
	/// <summary>
	/// Gets the duration of the status.
	/// </summary>
	/// <returns>status duration</returns>
	/// <param name="effectType">status type to check</param>
	public float getStatusDuration(StatusEffect effectType){
		return m_statusEffects.getStatusDuration(effectType);
	}

	/// <summary>
	/// Checks if the creature has the given status effect type currently.
	/// </summary>
	/// <returns><c>true</c>, if status effect will be in effect, <c>false</c> otherwise.</returns>
	/// <param name="effectType">Effect type to check</param>
	public bool hasStatusEffectNow(StatusEffect effectType){
		switch(effectType){
		case StatusEffect.STUN:
			return m_statusEffects.isStunned();
		case StatusEffect.ASSASSIN_LOCK:
			return m_statusEffects.isAssassinLocked();
		case StatusEffect.SLOW:
			return m_statusEffects.isSlowed();
		case StatusEffect.ORGE_AURA:
			return m_statusEffects.isOgreAuraed();
		case StatusEffect.MANA_SHIELD:
			return m_statusEffects.isManaShielded();
		case StatusEffect.FREEZING_SHIELD:
			return m_statusEffects.isFreezingShielded();
		case StatusEffect.EARTH_SHIELD:
			return m_statusEffects.isEarthShielded();
		case StatusEffect.BATTLE_ROAR:
			return m_statusEffects.isBattleRoared();
		case StatusEffect.SHIELD_BARRIER:
			return m_statusEffects.isShieldBarriered();
		case StatusEffect.RATKIN_EYE:
			return m_statusEffects.isRatkinEyed();
		}

		throw new ArgumentException("Invalid status effect type");
		return false;
	}

	public void AfflictRuneEffect(RuneType type) {
		m_runeEffect.AfflictRuneEffect(type);
	}
	
	public void RemoveRuneEffect(RuneType type) {
		m_runeEffect.RemoveRuneEffect(type);
	}
	
	public void AfflictStatusEffectsLocallyNow(StatusEffect type, float duration, float intensity){
		if(type == StatusEffect.ASSASSIN_LOCK || type == StatusEffect.STUN)
			StopLocallyNow();
		m_statusEffects.AfflictStatusEffectsLocallyNow(type, duration, intensity);
	}
	
	public void RemoveStatusEffectsLocallyNow(StatusEffect type, float intensity){
		m_statusEffects.RemoveStatusEffectsLocallyNow(type, intensity);
	}
	
	/// <summary>
	/// Use canUseSkill(Skill skill) if a target does not exist instead of this.
	/// Check if the skill is usable (has a valid target, may not have cooled, not enough MP, is a passive skill or creature is stunned)
	/// </summary>
	/// <returns>The error/success message if skill is used</returns>
	/// <param name="skill">Skill to check</param>
	/// <param name="target">Target to use the skill on</param>
	public SkillUsingMessage canUseSkill(Skill skill, object target){

		// check if skill can be used (without caring about the target)
		SkillUsingMessage msg = canUseSkill(skill);
		if(msg == SkillUsingMessage.SUCCESS){

			// test if the skill can be used on the target
			target = tryTarget(skill, target);

			// if not, then the target is invalid
			if(target == null)
				return SkillUsingMessage.INVALID_TARGET;
		} else
			return msg;

		return SkillUsingMessage.SUCCESS;
	}

	/// <summary>
	/// Use canUseSkill(Skill skill, object target) if a target exists instead of this.
	/// Check if the skill is usable (may not have cooled, not enough MP, is a passive skill or creature is stunned)
	/// </summary>
	/// <param name="skill">Skill to check</param>
	public SkillUsingMessage canUseSkill(Skill skill){

		// cannot use if haven't cooled (except for persistent skills)
		if(!skill.isCooled() && !skill.isPersistent())
			return SkillUsingMessage.NOT_COOLED;

		// cannot perform actions when stunned or assassin's locked
		if(m_statusEffects.isStunned() || m_statusEffects.isAssassinLocked())
			return SkillUsingMessage.IS_STUNNED;

		// passive skills cannot be activated manually (automatic)
		if(skill.isPassive())
			return SkillUsingMessage.IS_PASSIVE;

		// check for enough mp to use skill
		if(skill.getMPCost() > m_stats.Curr_MP)
			return SkillUsingMessage.NOT_ENOUGH_MP;

		return SkillUsingMessage.SUCCESS;
	}

	/// <summary>
	/// Kill off the creature
	/// </summary>
	public void Die(Creature attacker){

		if(attacker != null){
			// inform player that his own creature dies
			if(PlayerCommand.Instance.isControlling(this)){
				PlayerCommand.Instance.Dying();
				//PlayerCommand.Instance.DisplayCombatMessage("", "have been killed", "you");
				if(attacker.getOwnerID() != -1)
					PlayerCommand.Instance.DisplayCombatMessage(attacker.getOwnerName(), "died by the hands of", "You");
				else
					PlayerCommand.Instance.DisplayCombatMessage(attacker.CRace + " " + attacker.CClass, "died by the hands of", "You");
			} else if(getOwnerID() != -1) {	// guy killed is player-controlled
				if(attacker != null){
					if(attacker.getOwnerID() != -1)
					{
						PlayerCommand.Instance.DisplayCombatMessage(attacker.getOwnerName(), "killed", getOwnerName());
					}
					else
						PlayerCommand.Instance.DisplayCombatMessage(attacker.CRace + " " + attacker.CClass, "killed", getOwnerName());
				}
			}
		}

		// play dying sound
		if(m_dyingSounds.Length > 0){
			int randDyingIdx = UnityEngine.Random.Range(0, m_dyingSounds.Length);
			AudioManager.Instance.PlayOnce(m_dyingSounds[randDyingIdx], transform, 1.0f);
		}

		// remove from list of creatures and destroy the gameobject
		Creator.Instance.removeCreature(this);
		Destroy(gameObject);

		// instantiate a corpse on it
		if(m_corpsePrefab != null){
			Transform dead = ((GameObject)(Instantiate(m_corpsePrefab, transform.position, transform.rotation))).transform;
			CopyTransformsRecurse(transform, dead);
		}
	}

	/// <summary>
	/// Informs the spawn point that it is dying so that a new
	/// creature can replace it.
	/// </summary>
	public void InformSpawnPointDeath(){
		if(m_spawnPoint != null)
			m_spawnPoint.OnMyCreatureDeath(this);
	}
	
	/// <summary>
	/// Tries to see if target can be targeted by the skill.
	/// </summary>
	/// <returns>the target the skill is to be used on</returns>
	/// <param name="skill">skill to use</param>
	/// <param name="target">target to use on</param>
	object tryTarget(Skill skill, object target){
		if(target is Creature){
			Creature creatureTarget = (Creature)(target);

			// special case - cannot possess player controlled creature
			if(skill.getSkillTypeID() == 0 && creatureTarget.isPlayerControlled())
				return null;

			// simply return the creature if player can select them
			// if playing deathmatch (team 'on your own'), creatures are NEVER allies
			bool isAlly = areAllies(creatureTarget);
			if((isAlly && skill.isAllySelectable()) ||
			   (!isAlly && skill.isEnemySelectable()))
				return target;

			// return the creature's position if player cannot select creature
			// but can select a point
			if(skill.isPointSelectable())
				return creatureTarget.transform.position;

		} else if(target is Vector3){

			// if target is a position and the skill can be used on a point,
			// then return the point
			if(skill.isPointSelectable())
				return target;
		} else
			throw new ArgumentException("Invalid target type");

		return null;
	}

	/// <summary>
	/// Sets the spawn point of this creature.
	/// </summary>
	/// <param name="spawnPoint">designated spawn point</param>
	public void setSpawnPoint(CreatureSpawn spawnPoint){
		this.m_spawnPoint = spawnPoint;
	}

	/// <summary>
	/// Get the spawn point of this creature.
	/// </summary>
	public CreatureSpawn getSpawnPoint(){
		return m_spawnPoint;
	}

	/// <summary>
	/// Sets the team of this creature.
	/// </summary>
	/// <param name="team">designated team</param>
	public void setTeam(Team team){
		this.m_team = team;
	}

	/// <summary>
	/// Get the team of this creature.
	/// </summary>
	public Team getTeam(){
		return m_team;
	}

	/// <summary>
	/// replacement with a transform (for ragdoll)
	/// </summary>
	void CopyTransformsRecurse (Transform src,  Transform dst) {
		dst.position = src.position;
		dst.rotation = src.rotation;

		foreach (Transform child in dst) {
			// Match the transform with the same name
			Transform curSrc = src.Find(child.name);
			if (curSrc)
				CopyTransformsRecurse(curSrc, child);
		}
	}

	/// <summary>
	/// Returns the skill list of this creature. The 1st skill is Release/Possess, 2nd skill is a basic attack and the rest are special skills/spells.
	/// </summary>
	public List<Skill> getSkillList(){
		return m_equippedSkills;
	}
	
	/// <summary>
	/// Get equipped skill of creature given the skill ID
	/// </summary>
	/// <returns>skill with the given skill ID</returns>
	/// <param name="skillID">skill ID required for skill to find</param>
	public Skill getCreatureSkill(int skillID){
		foreach(Skill skill in m_equippedSkills)
			if(skill.getSkillTypeID() == skillID)
				return skill;

		throw new ArgumentException("retrieving unequipped skill!");
		return null;
	}

	/// <summary>
	/// Get the skill the creature is using
	/// </summary>
	public Skill getSkillUsing(){
		return m_currSkill;
	}
	
	/// <summary>
	/// Set the skill the creature is using
	/// </summary>
	/// <param name="skill">skill to use</param>
	public void setSkill(Skill skill){
		m_currSkill = skill;
	}
	
	/// <summary>
	/// Get the destination to move to
	/// </summary>
	/// <returns>destination point</returns>
	public Vector3 getDestination(){
		return m_destination;
	}
	
	/// <summary>
	/// Sets the destination to move to
	/// </summary>
	/// <param name="point">point to move to</param>
	public void setDestination(Vector3 point){
		m_destination = point;
	}
	
	/// <summary>
	/// Get the current target of the creature
	/// </summary>
	/// <returns>the current target</returns>
	public object getTarget(){
		return m_target;
	}
	
	/// <summary>
	/// Gets the world coordinates of the currently assigned target
	/// </summary>
	/// <returns>coordinates of target</returns>
	public Vector3 getTargetPoint(){
		if(m_target is Creature){				// creature target, has to keep updating path
			if(((Creature)m_target) == null)	// creature has died
				return Vector3.zero;
			return ((Creature)m_target).transform.position;
		} else if(m_target is Vector3)		// point target
			return (Vector3)(m_target);
		else
			throw new ArgumentException("Invalid target");
	}
	
	/// <summary>
	/// Set the current target of the creature
	/// </summary>
	/// <param name="target">target to set to</param>
	public void setTarget(System.Object target){
		if(target is Vector3)
			m_target = (Vector3)(target);
		else if(target is Creature)
			m_target = (Creature)(target);
		else
			throw new ArgumentException("Invalid target");
	}

	/// <summary>
	/// Gets the pathfinding agent
	/// </summary>
	public CreatureMoveAgent getAgent(){
		return m_agent;
	}
	
	/// <summary>
	/// Sets the charging animation
	/// </summary>
	/// <param name="animationName">name of animation to set</param>
	public void setSkillAnimation(string animationName){
		animation[animationName].speed = 1.0f;
		animation[animationName].wrapMode = WrapMode.ClampForever;
		animation.CrossFade(animationName);
		//animation.Play(animationName, AnimationPlayMode.Stop);
	}

	/// <summary>
	/// Sets the charging animation with the given speed
	/// </summary>
	/// <param name="animationName">name of animation to set</param>
	/// <param name="animSpeed">speed of animation</param>
	public void setSkillAnimation(string animationName, float animSpeed){
		animation[animationName].speed = animSpeed;
		animation[animationName].wrapMode = WrapMode.ClampForever;
		animation.CrossFade(animationName);
		//animation.Play(animationName, AnimationPlayMode.Stop);
	}
	
	/// <summary>
	/// Check if this creature is controlled by a player
	/// </summary>
	/// <returns><c>true</c>, if this creature is controlled by a player, <c>false</c> otherwise.</returns>
	public bool isPlayerControlled(){
		return m_ownerID != -1;
	}
	
	/// <summary>
	/// Get the name of this creature's owner
	/// </summary>
	/// <returns>name of owner, empty string if no owner</returns>
	public string getOwnerName(){
		if(isPlayerControlled()){
			foreach(PhotonPlayer player in PhotonNetwork.playerList)
				if(player.ID == m_ownerID)
					return player.name;
		}

		return "";
	}
	
	/// <summary>
	/// Get the ID of this creature's owner. Returns -1 if it has no owner.
	/// </summary>
	/// <returns>The owner ID</returns>
	public int getOwnerID(){
		return m_ownerID;
	}
	
	/// <summary>
	/// Set the owner of this creature
	/// </summary>
	/// <param name="ownerID">Owner ID to own this creature</param>
	public void setOwner(int ownerID){
		this.m_ownerID = ownerID;
	}
	
	/// <summary>
	/// Remove owner of this creature
	/// </summary>
	public void disown(){
		m_ownerID = -1;
	}

	/// <summary>
	/// Initialise the stats of this creature (not possessed)
	/// </summary>
	public void initUnpossessedStats(){

		// compute the stats of the creature
		m_stats = CreatureStatsCalculator.Instance.computeUnpossessedStats(m_creatureRace, m_creatureClass);

		// set the speed of moving with this stats
		BaseSpdOnMovStats();

		// affect the stats with status effects
		m_statusEffects.newlyAffect();
	}
	
	/// <summary>
	/// Initialise/Replace the stats of this creature
	/// </summary>
	/// <param name="stats">Stats to replace</param>
	public void initStats(CreatureStats stats){

		// set the stats to the given stats
		this.m_stats = stats;

		// set the speed of moving with this stats
		BaseSpdOnMovStats();

		// affect the stats with status effects
		m_statusEffects.newlyAffect();
	}

	/// <summary>
	/// Update the actual movement speed to the speed stats
	/// </summary>
	void BaseSpdOnMovStats(){

		// compute actual speed of the pathfinding agent
		float speed = BASE_MOVEMENT_SPEED + m_stats.MOV/MOV_STATS_DAMPENING;

		// update the agent's speed if it is not the same
		if(m_agent.getSpeed() != speed)
			m_agent.setSpeed(speed);
	}
	
	/// <summary>
	/// Returns whether this creature and the given creature are allies
	/// </summary>
	/// <returns><c>true</c>, if they are allies, <c>false</c> otherwise.</returns>
	/// <param name="otherCreature">Creature to check with</param>
	public bool areAllies(Creature otherCreature){
		if(otherCreature == this)		// you are your own ally
			return true;
		if(m_team == Team.ON_YOUR_OWN ||
		   otherCreature.getTeam() == Team.ON_YOUR_OWN)	// you don't have an ally (except yourself) if you are 'on your own'
			return false;
		return (otherCreature.getTeam() == m_team);
	}

	/// <summary>
	/// Remove previous "UseSkillLater" command
	/// </summary>
	public void removeSkillLaterCommand(){
		m_skillForLater = null;
		m_skillTargetForLater = null;
	}
	
	/// <summary>
	/// Returns the stats object.
	/// </summary>
	/// <returns>current stats</returns>
	public CreatureStats getStats(){
		return m_stats;
	}
	
	/// <summary>
	/// Returns the transform of the creature which provides the position and rotation of the creature's gameobject.
	/// </summary>
	/// <value>transform of this creature</value>
	public Transform TransformProperty {
		get {return transform;}
	}
	
	/// <summary>
	/// Returns the game object of the creature.
	/// </summary>
	/// <value>game object of this creature.</value>
	public GameObject GameObjectProperty {
		get {return gameObject;}
	}

	/// <summary>
	/// Returns the HP of the creature.
	/// </summary>
	/// <value>HP of this creature.</value>
	public float HP {
		get {return m_stats.Curr_HP;}
	}

	/// <summary>
	/// Returns the MP of the creature.
	/// </summary>
	/// <value>HP of this creature.</value>
	public float MP {
		get {return m_stats.Curr_MP;}
	}

	/// <summary>
	/// Returns the maximum HP of the creature.
	/// </summary>
	/// <value>maximum HP of this creature.</value>
	public float MaxHP {
		get {return m_stats.HP;}
	}

	/// <summary>
	/// Returns the maximum MP of the creature.
	/// </summary>
	/// <value>maximum MP of this creature.</value>
	public float MaxMP {
		get {return m_stats.MP;}
	}

	/// <summary>
	/// Returns the spawn point of the creature.
	/// </summary>
	/// <value>spawn point of this creature.</value>
	public CreatureSpawn SpawnPoint {
		get {return m_spawnPoint;}
	}
	
	/// <summary>
	/// Returns the class of the creature, such as ARCHER or WARRIOR.
	/// </summary>
	/// <value>Creature class of this creature</value>
	public CreatureClass CClass {
		get {return m_creatureClass;}
	}

	/// <summary>
	/// Returns the class of the creature, such as ARCHER or WARRIOR.
	/// </summary>
	/// <value>Creature race of this creature</value>
	public CreatureRace CRace {
		get {return m_creatureRace;}
	}
	
	/// <summary>
	/// Returns the last creature to hit me. Returns null if
	/// no creature has hit it, or the creature has already died.
	/// </summary>
	/// <value>The last creature to hit me</value>
	public Creature LastCreatureToHitMe {
		get {return m_lastCreatureToHitMe;}
	}

	/// <summary>
	/// Gets or sets a value indicating whether this instance is mine.
	/// </summary>
	/// <value><c>true</c> if the player is authoritative of this creature; otherwise, <c>false</c>.</value>
	public bool IsMine {
		get {return m_isMine;}
		set {m_isMine = value;}
	}

	/// <summary>
	/// Gets the previous command given to this creature.
	/// </summary>
	/// <returns>previous command</returns>
	/// <param name="parameter1">1st parameter of the previous command</param>
	/// <param name="parameter2">2nd parameter of the previous command</param>
	public CreatureCommands getPrevCommand(out object parameter1, out object parameter2) {
		parameter1 = m_prevCommandParameters[0];
		parameter2 = m_prevCommandParameters[1];
		return m_prevCommand;
	}

	/// <summary>
	/// Gets the previous command given to this creature.
	/// </summary>
	/// <returns>previous command</returns>
	public CreatureCommands getPrevCommand() {
		return m_prevCommand;
	}

	/// <summary>
	/// Sets the previous command.
	/// </summary>
	/// <param name="prevCommand">previous command</param>
	public void setPrevCommand(CreatureCommands prevCommand) {
		m_prevCommand = prevCommand;
	}

	/// <summary>
	/// Gets the pending command that is to be executed soon.
	/// </summary>
	/// <returns>pending command</returns>
	/// <param name="parameter1">1st parameter of the pending command</param>
	/// <param name="parameter2">1st parameter of the pending command</param>
	public CreatureCommands getPendingCommand(out object parameter1, out object parameter2) {
		parameter1 = m_pendingCommandParameters[0];
		parameter2 = m_pendingCommandParameters[1];
		return m_pendingCommand;
	}

	/// <summary>
	/// Gets the pending command.
	/// </summary>
	/// <returns>pending command</returns>
	public CreatureCommands getPendingCommand() {
		return m_pendingCommand;
	}

	/// <summary>
	/// Checks if there is an unexecuted command.
	/// </summary>
	/// <value><c>true</c> if there is an unexecuted command; otherwise, <c>false</c>.</value>
	public bool UnexecutedCommand {
		get {return m_unexecutedCommand;}
	}

	/// <summary>
	/// Checks if the creature is going to counter/continue with a basic attack.
	/// </summary>
	/// <value><c>true</c> if it is going to do so; otherwise, <c>false</c>.</value>
	public bool ContinueBasicAttack {
		get {return m_continueBasicAtk;}
		set {m_continueBasicAtk = value;}
	}
	
	/// <summary>
	/// Gets the time since last hit by another creature.
	/// </summary>
	/// <value>time since last hit</value>
	public float TimeSinceLastHit {
		get {return m_timeSinceLastHit;}
	}

	/// <summary>
	/// Gets ready animation
	/// </summary>
	/// <value>ready animation/value>
	public AnimationClip ReadyAnimation {
		get {return m_readyAnimation;}
	}

	/// <summary>
	/// Gets walk animation
	/// </summary>
	/// <value>walk animation/value>
	public AnimationClip WalkAnimation {
		get {return m_walkAnimation;}
	}

	/// <summary>
	/// Gets walk animation
	/// </summary>
	/// <value>walk animation/value>
	public AnimationClip StopAnimation {
		get {return m_stopAnimation;}
	}

	/// <summary>
	/// Gets the Creature's photon view.
	/// </summary>
	/// <value>photon view</value>
	public PhotonView CPhotonView {
		get {return m_photonView;}
	}

	/// <summary>
	/// Returns whether creature is currently charging a skill
	/// </summary>
	/// <value><c>true</c> if creature is charging skill; otherwise, <c>false</c>.</value>
	public bool isChargingSkill() {
		return m_currState == m_creatureChargeState;
	}

	/// <summary>
	/// Sets the map icon.
	/// </summary>
	/// <param name="type">Type.</param>
	public void setMapIcon(CreatureIconType type){
		m_minimapIconType = type;
		m_minimapIcon.SetTextureIcon(Creator.Instance.getMinimapIconTexture(type));
	}
}