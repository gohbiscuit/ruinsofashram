﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class CreatureSpawn : MonoBehaviour {
	public abstract void OnReadyToSpawn();
	public abstract void OnMyCreatureDeath(Creature dyingCreature);
}