﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum SpawnIconType { NONE, AGI, INT, STR };

public class CreatureAISpawn : CreatureSpawn {

	[SerializeField] SpawnIconType m_spawnType;
	const int SPAWN_POS_MIN = -3;
	const int SPAWN_POS_MAX = 3;
	const float SPAWN_ROT_MAX = 360;
	const int SPAWN_NUMBER = 2;			// 2 is less-cluttered, and balanced cos won't get gang

	void Start(){
		renderer.enabled = false;

		// turn off script if it is not in charge of the AIs
		// only the MasterClient can spawn and control AIs
		enabled = PhotonNetwork.isMasterClient;

		// set icon for minimap
		KGFMapIcon icon = transform.FindChild("MapIcon").GetComponent<KGFMapIcon>();
		icon.SetTextureIcon(Creator.Instance.getMinimapIconTexture(m_spawnType));
	}

	/// <summary>
	/// Called when the spawnpoint is ready to spawn.
	/// </summary>
	public override void OnReadyToSpawn(){

		if(!enabled)	// if this client not in charge of this creature, ignore
			return;

		// spawn 2 creatures
		for(int i=0; i<SPAWN_NUMBER; i++)
			spawnACreature();
	}

	/// <summary>
	/// Called whenever a creature spawned by this spawnpoint dies.
	/// </summary>
	/// <param name="dyingCreature">Creature that is going to die</param>
	public override void OnMyCreatureDeath(Creature dyingCreature){
		
		if(!enabled)	// if this client not in charge of this creature, ignore
			return;

		// spawn after 40s
		StartCoroutine(SpawnCreatureAfterSomeTime());
	}

	IEnumerator SpawnCreatureAfterSomeTime(){

		yield return new WaitForSeconds(40.0f);
		spawnACreature();
	}

	void spawnACreature(){

		Vector3 spawnPos = transform.position + new Vector3(Random.Range(SPAWN_POS_MIN, SPAWN_POS_MAX), 0, Random.Range(SPAWN_POS_MIN, SPAWN_POS_MAX));
		Quaternion spawnRot = Quaternion.Euler(new Vector3(0, Random.Range(0, SPAWN_ROT_MAX), 0));

		switch(m_spawnType){
		
		// AGILITY TYPE - OgreRogue, RatkinGunner
		case SpawnIconType.AGI:
			switch (Random.Range(0,2))
			{
			case 0:
				Creator.Instance.createAILater(this, CreatureRace.OGRE, CreatureClass.ROGUE, spawnPos, spawnRot);
				break;
			case 1:
				Creator.Instance.createAILater(this, CreatureRace.RATKIN, CreatureClass.GUNNER, spawnPos, spawnRot);
				break;
			}
			break;

		// INTELLIGENT TYPE - OgreMage, OgrePriest, RatkinMage, RatkinShaman
		case SpawnIconType.INT:
			switch (Random.Range(0,4))
			{
			case 0:
				Creator.Instance.createAILater(this, CreatureRace.OGRE, CreatureClass.MAGE, spawnPos, spawnRot);
				break;
			case 1:
				Creator.Instance.createAILater(this, CreatureRace.OGRE, CreatureClass.PRIEST, spawnPos, spawnRot);
				break;
			case 2:
				Creator.Instance.createAILater(this, CreatureRace.RATKIN, CreatureClass.MAGE, spawnPos, spawnRot);
				break;
			case 3:
				Creator.Instance.createAILater(this, CreatureRace.RATKIN, CreatureClass.SHAMAN, spawnPos, spawnRot);
				break;
			}
			break;

		// STRENGTH TYPE - OgreWarrior, RatkinWarrior
		case SpawnIconType.STR:
			switch (Random.Range(0,2))
			{
			case 0:
				Creator.Instance.createAILater(this, CreatureRace.OGRE, CreatureClass.WARRIOR, spawnPos, spawnRot);
				break;
			case 1:
				Creator.Instance.createAILater(this, CreatureRace.RATKIN, CreatureClass.WARRIOR, spawnPos, spawnRot);
				break;
			}
			break;
		}
	}
}