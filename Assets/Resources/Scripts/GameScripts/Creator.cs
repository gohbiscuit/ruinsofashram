﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum CreatureType { PLAYER, AI };
public enum CreatureIconType { PLAYER, ALLY, ENEMY, NEUTRAL };

/// <summary>
/// Responsible for the creation of all creatures.
/// </summary>
public class Creator : MonoBehaviour {

	static Creator instance;
	const float SEC_TILL_WISP_SPAWN = 1;
	const float SEC_TILL_AI_SPAWN = 0;
	const float SEC_BETWEEN_AI_SPAWN = 0.01f;

	const float SEC_BETWEEN_AMBIENT_CHANGE = 5.0f;
	const float MAX_AMBIENT_CHANGE = 0.4f;
	const float MIN_AMBIENT_COLOR = 0.15f;
	const float MAX_AMBIENT_COLOR = 0.55f;
	const float MAX_AMBIENT_CHANGE_SPEED = 100.0f;
	
	// prefabs for instantiation
	[SerializeField] GameObject m_wisp;
	[SerializeField] GameObject m_ratkin_warrior;
	[SerializeField] GameObject m_ratkin_gunner;
	[SerializeField] GameObject m_ratkin_mage;
	[SerializeField] GameObject m_ratkin_shaman;
	[SerializeField] GameObject m_ogre_warrior;
	[SerializeField] GameObject m_ogre_rogue;
	[SerializeField] GameObject m_ogre_mage;
	[SerializeField] GameObject m_ogre_priest;

	// textures for minimap
	[SerializeField] Texture2D m_playerCreatureIcon;
	[SerializeField] Texture2D m_allyCreatureIcon;
	[SerializeField] Texture2D m_enemyCreatureIcon;
	[SerializeField] Texture2D m_neutralCreatureIcon;
	[SerializeField] Texture2D m_spawnAGI;
	[SerializeField] Texture2D m_spawnINT;
	[SerializeField] Texture2D m_spawnATK;

	// list containing creatures GO that are players and AIs
    static List<GameObject> m_listPlayer;
    static List<GameObject> m_listAI;

	[SerializeField] HUDHandler m_gameHUD;	// reference to game HUD

	bool m_isTesting;		// is doing test

	// is a singleton
	public static Creator Instance {
		get {
			return instance;
		}
	}

	void Awake(){

		// singleton instantiation
		instance = this;

		// enabling offline mode if run directly from this room
		if(PhotonNetwork.room == null){
			GameObject networkManagerPrefab = (GameObject)(Resources.Load("Prefab/NetworkManager"));
			GameObject networkManagerGO = (GameObject)(Instantiate(networkManagerPrefab, Vector3.zero, Quaternion.identity));
			networkManagerGO.GetComponent<GameNetworkManager>().startDebugServer();
		}

		if(!Application.loadedLevelName.Equals("IntegrationTest_Scene")){

		} else
			m_isTesting = true;

		//StartCoroutine(changeAmbientColor());
	}

	IEnumerator changeAmbientColor(){
		nextAmbientRed = RenderSettings.ambientLight.r;
		nextAmbientGreen = RenderSettings.ambientLight.g;
		nextAmbientBlue = RenderSettings.ambientLight.b;

		while(true){
			yield return new WaitForSeconds(SEC_BETWEEN_AMBIENT_CHANGE);
			nextAmbientRed = RenderSettings.ambientLight.r + UnityEngine.Random.Range(-MAX_AMBIENT_CHANGE, MAX_AMBIENT_CHANGE);
			nextAmbientGreen = RenderSettings.ambientLight.g + UnityEngine.Random.Range(-MAX_AMBIENT_CHANGE, MAX_AMBIENT_CHANGE);
			nextAmbientBlue = RenderSettings.ambientLight.b + UnityEngine.Random.Range(-MAX_AMBIENT_CHANGE, MAX_AMBIENT_CHANGE);

			nextAmbientRed = Math.Max(MIN_AMBIENT_COLOR, Math.Min(nextAmbientRed, MAX_AMBIENT_COLOR));
			nextAmbientGreen = Math.Max(MIN_AMBIENT_COLOR, Math.Min(nextAmbientGreen, MAX_AMBIENT_COLOR));
			nextAmbientBlue = Math.Max(MIN_AMBIENT_COLOR, Math.Min(nextAmbientBlue, MAX_AMBIENT_COLOR));
		}
	}

	/// <summary>
	/// Plays the music after loading have been completed
	/// </summary>
	public void PlayMusic()
	{
		// play music if not doing testing
		// just wanna switch music cos' bored of the previous lol
		//AudioManager.Instance.Add("Music/howlingabyss_champselect");
		//AudioManager.Instance.Loop("Music/howlingabyss_champselect");
		AudioManager.Instance.Add("Ashram/User Interface/Background Music/Approaching the WestLands");
		AudioManager.Instance.Loop("Ashram/User Interface/Background Music/Approaching the WestLands", 0.2f);
	}

	void Start ()
	{
		m_listPlayer = new List<GameObject>();
        m_listAI = new List<GameObject>();

		// creating wisp at start (note that this only happens when played online)
		if(!PhotonNetwork.offlineMode)
			StartCoroutine(SpawnAll());

		// spawning is handled inside GameNetworkManager for offline mode
		// due to some script execution order problem
		// but will ultimately be removed in the final version, so shouldn't be a concern
	}

	float nextAmbientRed;
	float nextAmbientGreen;
	float nextAmbientBlue;

	void Update(){
		/*Color ambientLight = RenderSettings.ambientLight;
		RenderSettings.ambientLight = new Color(ambientLight.r + (nextAmbientRed - ambientLight.r)/MAX_AMBIENT_CHANGE_SPEED, 
		                                        ambientLight.g + (nextAmbientGreen - ambientLight.g)/MAX_AMBIENT_CHANGE_SPEED, 
		                                        ambientLight.b + (nextAmbientBlue - ambientLight.b)/MAX_AMBIENT_CHANGE_SPEED);*/
	}

	/// <summary>
	/// Staggered spawned of all creatures (including players' wisps) at the beginning of the game
	/// Done only when not doing testing
	/// </summary>
	public IEnumerator SpawnAll(){

		if(m_isTesting)	// let test case spawn own creatures in testing
			yield break;

		// temporarily disabled
		//RuneFactory.Instance.Run();

		yield return new WaitForSeconds(SEC_TILL_WISP_SPAWN);

		// spawn wisps
		GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");
		foreach(GameObject spawnPoint in spawnPoints){
			CreatureSpawn spawn = spawnPoint.GetComponent<CreatureSpawn>();
			if(spawn is CreatureWispSpawn){
				spawn.OnReadyToSpawn();
			}
		}

		// wait for 10s
		yield return new WaitForSeconds(SEC_TILL_AI_SPAWN);

		if(PhotonNetwork.isMasterClient){
			foreach(GameObject spawnPoint in spawnPoints){
				CreatureSpawn spawn = spawnPoint.GetComponent<CreatureSpawn>();
				if(spawn is CreatureAISpawn){
					spawn.OnReadyToSpawn();
					yield return new WaitForSeconds(SEC_BETWEEN_AI_SPAWN);
				}
			}
		}
	}

	// -------------------------------------------------------------------------------------------

	#region Networked Functions
	// - all calls to these functions will not be executed immediately,
	//   a 'lag' time must pass before the 'actual' functions are executed
	//   the same command is sent to the rest and executed optimally at the same time

	/// <summary>
	/// Possess the given creature with the local player's wisp after induced lag.
	/// </summary>
	/// <param name="playerControlledCreatureGO">GameObject of creature controlled by the player</param>
	/// <param name="targetCreatureGO">GameObject of creature to possess</param>
	public void PossessCreatureLater(GameObject playerControlledCreatureGO, GameObject targetCreatureGO){

		// ignore if player is not controlling the given creature
		Creature playerCreature = playerControlledCreatureGO.GetComponent<Creature>();
		if(playerCreature == null)
			throw new ArgumentException("Cannot possess with a null creature");

		if(!PlayerCommand.Instance.isControlling(playerCreature))
			return;

		Creature targetCreature = targetCreatureGO.GetComponent<Creature>();
		if(targetCreature == null)
			throw new ArgumentException("Cannot possess a null creature");

		// compute the stats of the newly possessed creature
		CreatureStats newCreatureStats = CreatureStatsCalculator.Instance.computeOldPossessedStats(targetCreature.CRace, targetCreature.CClass, playerCreature.getStats());

		// store the instruction to possess to send over network
		GameNetworkManager.Instance.StorePossess(playerControlledCreatureGO.GetPhotonView(), targetCreatureGO.GetPhotonView(), newCreatureStats);
	}

	/// <summary>
	/// Releases this creature from the local player's control after induced lag.
	/// </summary>
	/// <param name="playerControlledCreatureGO">GameObject of creature controlled by the player</param>
	public void releaseCreatureLater(GameObject playerControlledCreatureGO){
		Creature playerCreature = playerControlledCreatureGO.GetComponent<Creature>();
		if(playerCreature == null)
			throw new ArgumentException("Cannot release null creature");

		// if creature is being controlled by player
		if(PlayerCommand.Instance.isControlling(playerCreature)){

			// compute the stats of the wisp that is to be created
			CreatureStats wispStats = CreatureStatsCalculator.Instance.computeOldPossessedStats(CreatureRace.WISP, CreatureClass.WISP, playerCreature.getStats());

			// store the instruction to release to send over network
			GameNetworkManager.Instance.StoreRelease(playerControlledCreatureGO.GetPhotonView(), wispStats);
		}
	}

	/// <summary>
	/// Creates the local player's own wisp at the given position and rotation.
	/// Note that the creation will be propagated to all other players automatically
	/// </summary>
	/// <param name="position">Position to create the wisp</param>
	/// <param name="rotation">Rotation to create the wisp</param>
	public void createMyWispLater(CreatureSpawn spawnPoint, Vector3 position, Quaternion rotation){

		// compute the stats of the wisp
		CreatureStats wispStats = CreatureStatsCalculator.Instance.computeNewPossessedStats(CreatureRace.WISP, CreatureClass.WISP);

		// store the instruction to create the wisp to send over network
		GameNetworkManager.Instance.StoreCreateOwnPlayerWisp(spawnPoint, position, rotation, wispStats);
	}

	/// <summary>
	/// Creates an AI creature at the given position and rotation.
	/// Note that the creation will be propagated to all other players automatically.
	/// Only the MasterClient (aka host) is able to create and controls AIs.
	/// </summary>
	/// <param name="spawnPoint">Spawn Point in which the AI will spawn from</param>
	/// <param name="creatureRace">Creature race to create</param>
	/// <param name="creatureClass">Creature class to create</param>
	/// <param name="position">Position to create the AI</param>
	/// <param name="rotation">Rotation to create the AI</param>
	public void createAILater(CreatureSpawn spawnPoint, CreatureRace creatureRace, CreatureClass creatureClass,
	                          Vector3 position, Quaternion rotation){
		if(PhotonNetwork.isMasterClient){

			// get the standardised creature name from the race and class
			string creatureName = getNameFromRaceClass(creatureRace, creatureClass);

			// store the instruction to create the AI to send over network
			GameNetworkManager.Instance.StoreCreateAI(spawnPoint, creatureName, position, rotation);
		}
	}

	#endregion

	// -------------------------------------------------------------------------------------------

	#region 'Actual' Networked Functions
	// - all calls are executed immediately
	//   Networked Functions -> GameNetworkManager ('creates' lag) and broadcast to all -> 'Actual' Networked Functions

	/// <summary>
	/// Possesses creature now - kill off wisp and let player get control of new creature
	/// </summary>
	/// <param name="playerControlledCreatureGO">Player controlled creature GameObject</param>
	/// <param name="targetCreatureGO">Target creature GameObject</param>
	/// <param name="newCreatureStats">Target creature's stats</param>
	public void possessCreatureLocallyNow(GameObject playerControlledCreatureGO, GameObject targetCreatureGO,
	                                      CreatureStats newCreatureStats){

		Creature playerControlledCreature = playerControlledCreatureGO.GetComponent<Creature>();
		Creature targetCreature = targetCreatureGO.GetComponent<Creature>();

		// check if both creatures were removed from game
		if(playerControlledCreature == null || targetCreature == null)
			return;

		// transfer of owner
		targetCreature.setOwner(playerControlledCreature.getOwnerID());
		playerControlledCreature.disown();

		// inform spawnpoint that the possessed creature died
		targetCreature.InformSpawnPointDeath();

		// set new stats for the newly possessed creature & transfer its team/spawnpoint
		targetCreature.initStats(newCreatureStats);
		targetCreature.setTeam(playerControlledCreature.getTeam());
		targetCreature.setSpawnPoint(playerControlledCreature.getSpawnPoint());

		// remove AI script
		Destroy(targetCreature.GetComponent<AIBaseBehaviour>());

		bool playerControlled = false;

		// if that creature belongs to me, then change give player command to the new creature
		if(PlayerCommand.Instance.isControlling(playerControlledCreatureGO.GetComponent<Creature>())) {
			PlayerCommand.Instance.setControlledCreature(targetCreature);
			playerControlled = true;
			targetCreature.GetComponent<FOWRevealer>().isActive = true;

		} else
			targetCreature.IsMine = false;

		if(m_gameHUD != null){
			// TODO for edwin: add an enemy bar only if it is an enemy
			if(targetCreature.areAllies(PlayerCommand.Instance.getControlledCreature())){
				//m_gameHUD.AddAllyHealthBar(targetCreature);		// Add health bar for creature that just got possessed by ALLY/PLAYER
				if(playerControlled)
					targetCreature.setMapIcon(CreatureIconType.PLAYER);
				else
					targetCreature.setMapIcon(CreatureIconType.ALLY);
			} else {
				//m_gameHUD.AddEnemyHealthBar(targetCreature);	// Add health bar for creature that just got possessed by ENEMY
				targetCreature.setMapIcon(CreatureIconType.ENEMY);
			}
		}

		// FX instantiation when possessing
		GameObject.Instantiate(Resources.Load("Prefab/Possess"), targetCreature.transform.position, Quaternion.identity);

		// update list of creature gameobjects
		m_listPlayer.Remove(playerControlledCreatureGO);
		m_listAI.Remove(targetCreatureGO);
		m_listPlayer.Add(targetCreatureGO);

		// remove AI for player controlled gameobject
		targetCreature.StopLocallyNow();
		if(targetCreatureGO.GetComponent<AIStateManager>())
			Destroy(targetCreatureGO.GetComponent<AIStateManager>());

		// remove original creature
		Destroy(playerControlledCreatureGO);
	}

	/// <summary>
	/// Releases the creature now - create new wisp and kill off old creature.
	/// </summary>
	/// <param name="playerControlledCreatureGO">Player controlled creature GameObject</param>
	/// <param name="newViewID">new ID of the photonView</param>
	/// <param name="stats">Stats of the new wisp</param>
	public void releaseCreatureLocallyNow(GameObject playerControlledCreatureGO, int newViewID,
	                                      CreatureStats stats){
		Creature oldCreature = playerControlledCreatureGO.GetComponent<Creature>();

		// check if original creature is removed from game
		if(oldCreature == null)
			return;

		// FX instantiation when releasing
		GameObject.Instantiate(Resources.Load("Prefab/Possess"), oldCreature.transform.position, Quaternion.identity);

		// create a new wisp
		createPlayerWispLocallyNow(newViewID, oldCreature.getSpawnPoint(), oldCreature.transform.position,
		                           oldCreature.transform.rotation, oldCreature.getOwnerID(), stats);

		// update list of creature gameobjects
		m_listPlayer.Remove(playerControlledCreatureGO);

		// remove original creature
		oldCreature.Die(null);
	}

	/// <summary>
	/// Create a wisp without AI now and gives it player control (if belongs to this player)
	/// </summary>
	/// <param name="newViewID">new ID of the photonView</param>
	/// <param name="position">Position of the new wisp</param>
	/// <param name="rotation">Rotation of the new wisp</param>
	/// <param name="networkPlayerID">ID of the player</param>
	/// <param name="stats">stats of the wisp</param>
	public void createPlayerWispLocallyNow(int newViewID, CreatureSpawn spawnPoint, Vector3 position, Quaternion rotation,
	                                       int networkPlayerID, CreatureStats stats){

		// get the wisp prefab
		GameObject prefab = getPrefab(CreatureRace.WISP, CreatureClass.WISP);

		// create the wisp GameObject and assign its ID
		GameObject wispGO = (GameObject)Instantiate(prefab, position, rotation);
		wispGO.GetPhotonView().viewID = newViewID;

		// Get the wisp's Creature component
		Creature wisp = wispGO.GetComponent<Creature>();
		if(wisp == null)
			throw new ArgumentException("Cannot get wisp prefab.");

		// set stats, owner & spawnpoint for the new wisp
		wisp.initStats(stats);
		wisp.setOwner(networkPlayerID);
		wisp.setSpawnPoint(spawnPoint);

		if(networkPlayerID == PhotonNetwork.player.ID){

			// wisp belongs to you
			PlayerCommand.Instance.setControlledCreature(wisp);						// give player's control of this wisp
			wisp.setTeam((Team)(PhotonNetwork.player.customProperties["Team"]));	// set team of wisp
			wisp.setMapIcon(CreatureIconType.PLAYER);
			wisp.GetComponent<FOWRevealer>().isActive = true;

		} else {

			// wisp belongs to other players
			foreach(PhotonPlayer player in PhotonNetwork.playerList){
				if(player.ID == networkPlayerID){
					wisp.setTeam((Team)(player.customProperties["Team"]));	// set team of wisp

					// TODO for edwin: add an enemy bar only if it is an enemy
					if(wisp.areAllies(PlayerCommand.Instance.getControlledCreature())){
					   //m_gameHUD.AddAllyHealthBar(wisp);	// Add ally health bar display
					 //  m_gameHUD.AddEnemyHealthBar(wisp);	// Add enemy health bar display
					   wisp.setMapIcon(CreatureIconType.ALLY);
					} else {
						//m_gameHUD.AddEnemyHealthBar(wisp);	// Add enemy health bar display
						wisp.setMapIcon(CreatureIconType.ENEMY);
					}
					break;
				}
			}
		}

		m_listPlayer.Add(wispGO);
	}

	/// <summary>
	/// Create an AI. Called from all hosts at about same time and created AI has the same ID'
	/// GameObjects spawned remotely have no AI scripts.
	/// </summary>
	/// <param name="newViewID">ID of the new AI</param>
	/// <param name="creatureName">Standardised creature name of the AI</param>
	/// <param name="position">Position of the new AI</param>
	/// <param name="rotation">Rotation of the new AI</param>
	public void createAILocallyNow(int newViewID, CreatureSpawn spawnPoint, string creatureName, Vector3 position, Quaternion rotation){

		// Create the AI GameObject, and set its ID
		GameObject prefab = getPrefab(creatureName);
		GameObject aiCreatureGO = (GameObject)(Instantiate(prefab, position, rotation));
		aiCreatureGO.GetPhotonView().viewID = newViewID;
		Creature newAI = aiCreatureGO.GetComponent<Creature>();
		if(newAI == null)
			throw new ArgumentException("Unable to create AI");

		// set the AI's stats, team & spawnPoint
		newAI.initUnpossessedStats();
		newAI.setTeam(Team.NEUTRAL);
		newAI.setSpawnPoint(spawnPoint);
		newAI.setMapIcon(CreatureIconType.NEUTRAL);

		// only creatures created by MasterClient has AI Behaviour
		// the same creatures created on other clients are 'dummies' which do not have AI
		if(PhotonNetwork.isMasterClient){

			// set to this player's network authority/responsibility
			newAI.IsMine = true;

			// give AI behaviour
			aiCreatureGO.AddComponent<AIStateManager>();

			// for individual AI testing (will be removed eventually)
			/*if (newAI.m_creatureRace == CreatureRace.OGRE) {
				if (newAI.m_creatureClass == CreatureClass.WARRIOR) {
					aiCreatureGO.AddComponent<AIStateManager>();
				} else if (newAI.m_creatureClass == CreatureClass.ROGUE) {
					aiCreatureGO.AddComponent<AIStateManager>();
				} else if (newAI.m_creatureClass == CreatureClass.MAGE) {
					aiCreatureGO.AddComponent<AIStateManager>();
				} else if (newAI.m_creatureClass == CreatureClass.PRIEST) {
					aiCreatureGO.AddComponent<AIStateManager>();
				}
			} else if (newAI.m_creatureRace == CreatureRace.RATKIN) {
				if (newAI.m_creatureClass == CreatureClass.WARRIOR) {
					aiCreatureGO.AddComponent<AIStateManager>();
				} else if (newAI.m_creatureClass == CreatureClass.GUNNER) {
					aiCreatureGO.AddComponent<AIStateManager>();
				} else if (newAI.m_creatureClass == CreatureClass.MAGE) {
					aiCreatureGO.AddComponent<AIStateManager>();
				} else if (newAI.m_creatureClass == CreatureClass.SHAMAN) {
					aiCreatureGO.AddComponent<AIStateManager>();
				}
			}
	        if (newAI.m_creatureClass == CreatureClass.WARRIOR && newAI.m_creatureRace == CreatureRace.OGRE) {
				//aiCreatureGO.AddComponent<AIStateManager>();
			}
			else if (newAI.m_creatureClass == CreatureClass.ROGUE)
			{
				//.AddComponent<AIStateManager>();
			}*/
		}

		// update AI list
		m_listAI.Add(aiCreatureGO);
	}

	#endregion

	// -------------------------------------------------------------------------------------------

	/// <summary>
	/// Removes the creature.
	/// </summary>
	/// <param name="creatureToRemove">Creature to remove</param>
	public void removeCreature(Creature creatureToRemove){
		m_listPlayer.Remove(creatureToRemove.gameObject);
		m_listAI.Remove(creatureToRemove.gameObject);
	}

	/// <summary>
	/// Gets the nearest creature GameObject
	/// </summary>
	/// <returns>The nearest GameObject</returns>
	/// <param name="position">Source Position</param>
	/// <param name="creatureType">Player Creature or AI Creature</param>
	public static GameObject GetNearestGO(Vector3 position, CreatureType creatureType)
	{
		GameObject nearestGO = null;
		float dist = float.PositiveInfinity;
		List<GameObject> listCreatures = null;

		// decide which list (player or AI) to use
		if (creatureType == CreatureType.PLAYER)
			listCreatures = m_listPlayer;
		else if (creatureType == CreatureType.AI)
			listCreatures = m_listAI;

		for (int i = 0; i < listCreatures.Count; i++)
		{
			if (listCreatures[i].transform.position != position)
			{
				if ((listCreatures[i].transform.position - position).sqrMagnitude < dist)
				{
					dist = (listCreatures[i].transform.position - position).sqrMagnitude;
					nearestGO = listCreatures[i];
				}
			}
		}
		return nearestGO;
	}

	/// <summary>
	/// Gets the standardised name of the creature given its race and class.
	/// </summary>
	/// <returns>name of the creature</returns>
	/// <param name="m_creatureRace">Creature race</param>
	/// <param name="m_creatureClass">Creature class</param>
	public string getNameFromRaceClass(CreatureRace m_creatureRace, CreatureClass m_creatureClass){
		return m_creatureRace.ToString() + "_" + m_creatureClass.ToString();
	}

	/// <summary>
	/// Gets the creature prefab based on its race and class.
	/// </summary>
	/// <returns>The prefab of the creature</returns>
	/// <param name="m_creatureRace">Creature race</param>
	/// <param name="m_creatureClass">Creature class</param>
	GameObject getPrefab(CreatureRace m_creatureRace, CreatureClass m_creatureClass){
		return getPrefab(getNameFromRaceClass(m_creatureRace, m_creatureClass));
	}

	/// <summary>
	/// Gets the creature prefab based on its standardised name.
	/// </summary>
	/// <returns>The prefab of the creature.</returns>
	/// <param name="creatureName">Standardised creature name</param>
	GameObject getPrefab(string creatureName){
		GameObject prefab = null;

		switch(creatureName){
		case "WISP_WISP":
			prefab = m_wisp;
			break;
		case "RATKIN_WARRIOR":
			prefab = m_ratkin_warrior;
			break;
		case "RATKIN_GUNNER":
			prefab = m_ratkin_gunner;
			break;
		case "RATKIN_MAGE":
			prefab = m_ratkin_mage;
			break;
		case "RATKIN_SHAMAN":
			prefab = m_ratkin_shaman;
			break;
		case "OGRE_WARRIOR":
			prefab = m_ogre_warrior;
			break;
		case "OGRE_ROGUE":
			prefab = m_ogre_rogue;
			break;
		case "OGRE_MAGE":
			prefab = m_ogre_mage;
			break;
		case "OGRE_PRIEST":
			prefab = m_ogre_priest;
			break;
		}

		if(prefab == null)
			throw new ArgumentException("creature prefab not found");

		return prefab;
	}

	/// <summary>
	/// Get all AIs currently in the arena.
	/// </summary>
	/// <returns>AI List</returns>
	public List<GameObject> getAIs(){
		return m_listAI;
	}

	/// <summary>
	/// Gets the first AI in the list that belongs to the given race and class.
	/// </summary>
	/// <returns>The first AI in the list.</returns>
	/// <param name="cRace">Creature Race</param>
	/// <param name="cClass">Creature Class</param>
	public GameObject getFirstAI(CreatureRace cRace, CreatureClass cClass){
		return getAI(cRace, cClass, 0);
	}

	/// <summary>
	/// Gets the AI belonging to type cRace and cClass.
	/// If index == 0, returns the first AI of this type.
	/// If index == 1, returns the second AI of this type and so on.
	/// </summary>
	/// <returns>AI of the given type</returns>
	/// <param name="cRace">Creature Race</param>
	/// <param name="cClass">Creature Class</param>
	/// <param name="index">Position in the list</param>
	public GameObject getAI(CreatureRace cRace, CreatureClass cClass, int index){
		foreach(GameObject go in m_listAI){
			Creature creature = go.GetComponent<Creature>();
			if(creature.CRace == cRace &&
			   creature.CClass == cClass){
				index--;
				if(index == -1)
					return go;
			}
		}

		throw new ArgumentException("Cannot find AI");
		return null;
	}

	/// <summary>
	/// Get the list of all players in the arena.
	/// </summary>
	/// <returns>Player list</returns>
	public List<GameObject> getPlayers(){
		return m_listPlayer;
	}

	/// <summary>
	/// Gets the first wisp in the list of players
	/// </summary>
	/// <returns>The first wisp</returns>
	public GameObject getFirstPlayerWisp(){
		return getFirstPlayer(CreatureRace.WISP, CreatureClass.WISP);
	}

	/// <summary>
	/// Gets the first player-controlled creature of the given type.
	/// </summary>
	/// <returns>The first player-controlled creature</returns>
	/// <param name="cRace">Creature Race</param>
	/// <param name="cClass">Creature Class</param>
	public GameObject getFirstPlayer(CreatureRace cRace, CreatureClass cClass){
		return getPlayer(cRace, cClass, 0);
	}

	/// <summary>
	/// Gets a player-controlled creature belonging to the given type.
	/// If index == 0, returns the first player-controlled creature of this type.
	/// If index == 1, returns the second player-controlled creature of this type and so on.
	/// </summary>
	public GameObject getPlayer(CreatureRace cRace, CreatureClass cClass, int index){
		foreach(GameObject go in m_listPlayer){
			Creature creature = go.GetComponent<Creature>();
			if(creature.CRace == cRace &&
			   creature.CClass == cClass){
				index--;
				if(index == -1)
					return go;
			}
		}

		throw new ArgumentException("Cannot find player-controlled creature");
		return null;
	}

	public Texture2D getMinimapIconTexture(CreatureIconType type){
		switch(type){
		case CreatureIconType.PLAYER:
			return m_playerCreatureIcon;
		case CreatureIconType.ALLY:
			return m_allyCreatureIcon;
		case CreatureIconType.ENEMY:
			return m_enemyCreatureIcon;
		case CreatureIconType.NEUTRAL:
			return m_neutralCreatureIcon;
		}

		throw new ArgumentException("No such icon type");
		return null;
	}

	public Texture2D getMinimapIconTexture(SpawnIconType type){
		switch(type){
		case SpawnIconType.AGI:
			return m_spawnAGI;
		case SpawnIconType.INT:
			return m_spawnINT;
		case SpawnIconType.STR:
			return m_spawnATK;
		}
		
		//throw new ArgumentException("No such icon type");
		return m_spawnAGI;
	}
}
