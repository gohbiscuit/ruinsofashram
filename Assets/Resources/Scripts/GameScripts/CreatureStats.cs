﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Creature Stats
/// </summary>
[System.Serializable]
public class CreatureStats
{
	// Varying Stats
	private float m_curr_hp;
	private float m_curr_mp;

	// EXP
	private int m_exp_given;

	// HP / MP
	private int m_hp;
	private int m_mp;
	private double m_hp_regen;
	private double m_mp_regen;
	
	// Defense / Attack / Speed	
	private int m_phy_atk;
	private int m_mag_atk;
	private int m_phy_def;
	private int m_mag_def;
	private int m_eva;
	private int m_movement_speed;
	private int m_skill_mastery;

	public int EXP_GIVEN
	{
		set { m_exp_given = value; }
		get { return m_exp_given; }
	}

	public float Curr_HP
	{
		set { m_curr_hp = value; }
		get { return m_curr_hp; }
	}
	
	public float Curr_MP
	{
		set { m_curr_mp = value; }
		get { return m_curr_mp; }
	}

	public int HP
	{
		set { m_hp = value; }
		get { return m_hp; }
	}

	public int MP
	{
		set { m_mp = value; }
		get { return m_mp; }
	}

	public double HP_REGEN
	{
		set { m_hp_regen = value; }
		get { return m_hp_regen; }
	}

	public double MP_REGEN
	{
		set { m_mp_regen = value; }
		get { return m_mp_regen; }
	}

	public int PHY_ATK
	{
		set { m_phy_atk = value; }
		get { return m_phy_atk; }
	}

	public int MAG_ATK
	{
		set { m_mag_atk = value; }
		get { return m_mag_atk; }
	}

	public int PHY_DEF
	{
		set { m_phy_def = value; }
		get { return m_phy_def; }
	}

	public int MAG_DEF
	{
		set { m_mag_def = value; }
		get { return m_mag_def; }
	}

	public int EVA
	{
		set { m_eva = value; }
		get { return m_eva; }
	}

	public int MOV
	{
		set { m_movement_speed = value; }
		get { return m_movement_speed; }
	}

	public int SKI
	{
		set { m_skill_mastery = value; }
		get { return m_skill_mastery; }
	}
}