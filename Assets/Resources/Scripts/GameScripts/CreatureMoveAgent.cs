﻿using UnityEngine;
using System.Collections;
using Pathfinding;

/// <summary>
/// Wrapper for pathfinding agent
/// </summary>
public class CreatureMoveAgent {

	const float PATHFINDER_ACCELERATION = 30;
	const float PATHFINDER_SLOWDOWN_TIME = 0.01f;
	const float PATHFINDER_END_REACHED_DISTANCE = 0.3f;
	const float PATHFINDER_WALL_FORCE = 3;
	const float PATHFINDER_WALL_DISTANCE = 1;
	const float PATHFINDER_ROTATIONSPEED = 500;

	
	RichAI m_richAI;					// actual pathfinding agent
	Transform m_destinationTransform;	// reference to the transform that the agent moves to
	bool m_stopped;
	bool m_reached;
	float m_actualSpeed;

	/// <summary>
	/// Initializes a new instance of the <see cref="CreatureMoveAgent"/> class.
	/// </summary>
	/// <param name="m_richAI">Actual Pathfinding Agent</param>
	public CreatureMoveAgent(RichAI m_richAI) {

		// create a new gameobject to use as the destination transform
		GameObject go = new GameObject();
		m_destinationTransform = go.transform;
		m_destinationTransform.position = m_richAI.transform.position;
		m_destinationTransform.name = m_richAI.gameObject.name + "_destination";
		m_destinationTransform.tag = "CreatureDest";

		this.m_richAI = m_richAI;
		m_richAI.target = m_destinationTransform;

		// parameters standardisation for all creatures
		m_richAI.acceleration = PATHFINDER_ACCELERATION;
		m_richAI.slowdownTime = PATHFINDER_SLOWDOWN_TIME;
		m_richAI.endReachedDistance = PATHFINDER_END_REACHED_DISTANCE;
		m_richAI.wallForce = PATHFINDER_WALL_FORCE;
		m_richAI.wallDist = PATHFINDER_WALL_DISTANCE;
		m_richAI.groundMask = 1<<LayerMask.NameToLayer("Ground");
		m_richAI.rotationSpeed = PATHFINDER_ROTATIONSPEED;
	}

	/// <summary>
	/// Sets the destination to move to
	/// </summary>
	/// <param name="targetPoint">point to move to</param>
	public void SetDestination (Vector3 targetPoint) {
		setCurrentMoveSpeed(m_actualSpeed);
		m_richAI.rotationSpeed = PATHFINDER_ROTATIONSPEED;
		m_destinationTransform.position = targetPoint;
		m_reached = false;
		m_richAI.UpdatePath();
		m_richAI.repeatedlySearchPaths = false;
		m_stopped = false;
	}

	/// <summary>
	/// Sets the creature to chase after
	/// </summary>
	/// <param name="creatureTarget">Creature target</param>
	public void SetDestination (Creature creatureTarget) {
		if(creatureTarget == null)
			Stop();
		else {
			m_richAI.rotationSpeed = PATHFINDER_ROTATIONSPEED;
			setCurrentMoveSpeed(m_actualSpeed);
			m_destinationTransform.position = creatureTarget.transform.position;
			m_reached = false;
			m_richAI.repeatedlySearchPaths = true;
			m_stopped = false;
		}
	}

	/// <summary>
	/// Stop the agent from moving
	/// </summary>
	public void Stop(){
		setCurrentMoveSpeed(0);
		m_richAI.repeatedlySearchPaths = false;
		m_reached = true;
		m_richAI.rotationSpeed = 0;
		if(!m_stopped) {
			m_destinationTransform.position = m_richAI.transform.position;
			m_richAI.UpdatePath();
			m_stopped = true;
		}
	}

	/// <summary>
	/// Called when destination is reached
	/// </summary>
	public void OnReachDestination(){
		m_reached = true; 
		m_richAI.repeatedlySearchPaths = false;
	}

	/// <summary>
	/// Returns whether the agent has reached its destination
	/// </summary>
	/// <returns><c>true</c>, if destination is reached, <c>false</c> otherwise.</returns>
	public bool hasReached(){
		return m_reached;
	}

	/// <summary>
	/// Gets the actual speed of the agent.
	/// </summary>
	/// <returns>agent speed</returns>
	public float getSpeed(){
		return m_actualSpeed;
	}

	/// <summary>
	/// Sets the agent speed.
	/// </summary>
	/// <param name="speed">speed to set</param>
	public void setSpeed(float speed){
		m_actualSpeed = speed;
	}

	/// <summary>
	/// Sets the current move speed.
	/// </summary>
	/// <param name="speed">Speed</param>
	void setCurrentMoveSpeed(float speed){
		m_richAI.maxSpeed = speed;
	}

	/// <summary>
	/// Multiplies the speed.
	/// </summary>
	/// <param name="multiplier">multiplier</param>
	public void multiplySpeed(float multiplier){
		m_actualSpeed *= multiplier;
	}

	/// <summary>
	/// Divides the speed.
	/// </summary>
	/// <param name="divisor">divisor</param>
	public void divideSpeed(float divisor){
		m_actualSpeed /= divisor;
	}

	/// <summary>
	/// Returns if it is moving
	/// </summary>
	public bool isMoving(){
		return !(Vector3.SqrMagnitude(m_richAI.Velocity) < 0.01f);
	}

	public void Disable(){
		m_richAI.GetComponent<NavMeshAgent>().enabled = false;
	}

	public void Enable(){
		m_richAI.GetComponent<NavMeshAgent>().enabled = true;
	}
}
