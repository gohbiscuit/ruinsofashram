﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
 
public sealed class AudioManager : MonoBehaviour
{
    private class AudioData
    {
        public AudioSource audioSource { get; set; }
        public GameObject gameObject { get; set; }
    }

    private static readonly AudioManager instance = new AudioManager();
    private List<AudioData> listAudioData;

	private float baseVol = 1.0f;

    public static AudioManager Instance
    {
        get 
        {
            return instance;
        }
    }

    private AudioManager() 
    {
        //Debug.Log("CONSTRUCTOR");
        listAudioData = new List<AudioData>();
    }

    private AudioData Search(string name)
    {
        //Debug.Log("Audio Data Count: " + listAudioData.Count);
        foreach (AudioData audioData in listAudioData)
        {
            if (audioData.audioSource.name == name)
            {
                return audioData;
            }
        }
        //Debug.Log("Search: Audio not found." + name);
        return null;
    }

	// Added by Edwin, Add Unique Sound object. Otherwise every instantiation of skill prefabs will add an audio data to the list, and it will have tons of duplicate of audio data
	private void AddUnique(AudioData data)
	{
		if(listAudioData.Contains(data))
			return;

		listAudioData.Add(data);
	}

    public GameObject Add(string name)
    {
        AudioData audioData = new AudioData();
        audioData.gameObject = new GameObject("Audio: " + name);

        //Source Setting
        audioData.audioSource = (AudioSource)audioData.gameObject.AddComponent<AudioSource>();
        audioData.audioSource.clip = (AudioClip)Resources.Load("SoundFx/" + name);
        audioData.audioSource.name = name;
		audioData.audioSource.rolloffMode = AudioRolloffMode.Linear;	// added by hw - for 3d sound, default log rolloff
																		// will make it too soft when it's some distance away from camera
        
		// commented out this, use add unique instead listAudioData.Add(audioData);
		AddUnique(audioData);

		return audioData.gameObject;
    }

    public bool Play(string name)
    {
        return Play(name, new GameObject().transform, baseVol);
    }
    
    public bool Play(string name, Transform transform)
    {
        return Play(name, transform, baseVol);
    }

    public bool Play(string name, Transform transform, float volume)
    {
        AudioData audioData = Search(name);
        if (audioData == null)
        {
            return false;
        }
		audioData.gameObject.transform.position = transform.position;

        //Source Setting
        audioData.audioSource.volume = volume;
        audioData.audioSource.Play();
        return true;
    }

	public bool PlayOnce(string name, Creature creature, float volume)
	{
		if(creature == null)
			return false;
		return PlayOnce(name, creature.transform, volume);
	}

	public bool PlayOnce(string name, Transform transform, float volume)
	{
		if (transform == null)
			return false;

		AudioData audioData = new AudioData();
		audioData.gameObject = new GameObject("Audio: " + name);
		audioData.gameObject.name = ("Audio: " + name);		// can view in scene, added by edwin
		audioData.gameObject.transform.position = transform.position;
		
		//Source Setting
		AudioSource src = (AudioSource)audioData.gameObject.AddComponent<AudioSource>();
		src.clip = (AudioClip)Resources.Load("SoundFx/" + name);
		src.name = name;
		src.rolloffMode = AudioRolloffMode.Linear;
		src.volume = volume;
		src.Play();

		audioData.gameObject.AddComponent<AudioKiller>();
        return true;
	}

	public bool PlayOnce(string name, float volume)
	{
		AudioData audioData = new AudioData();
		audioData.gameObject = new GameObject("Audio: " + name);
		audioData.gameObject.name = ("Audio: " + name);		// can view in scene, added by edwin
		
		//Source Setting
		AudioSource src = (AudioSource)audioData.gameObject.AddComponent<AudioSource>();
		src.clip = (AudioClip)Resources.Load("SoundFx/" + name);
		src.name = name;
		src.rolloffMode = AudioRolloffMode.Linear;
		src.volume = volume;
		src.Play();
		
		audioData.gameObject.AddComponent<AudioKiller>();
        return true;
	}

    public bool Loop(string name)
    {
        return Loop(name, new GameObject().transform, baseVol);
    }

	public bool Loop(string name, float volume)
	{
		return Loop(name, new GameObject().transform, volume);
	}

    public bool Loop(string name, Transform transform)
    {
        return Loop(name, transform, baseVol);
    }

    public bool Loop(string name, Transform transform, float volume)
    {
        AudioData audioData = Search(name);
        if (audioData == null)
        {
            return false;
        }
        audioData.gameObject.transform.position = transform.position;

        //Source Setting
        audioData.audioSource.volume = volume;
        audioData.audioSource.loop = true;
        audioData.audioSource.Play();
        return true;
    }

    public bool Pause(string name)
    {
        AudioData audioData = Search(name);
        if (audioData == null)
        {
            return false;
        }
        audioData.audioSource.Pause();
        return true;
    }

    public bool Stop(string name)
    {
        AudioData audioData = Search(name);
        if (audioData == null)
        {
            return false;
        }
        audioData.audioSource.Stop();
        return true;
    }

	// Added by edwin, stop a game object audio source, without scanning the entire list, More efficient as O(1). 
	// It is useful for HUD sound
	public bool Stop(GameObject soundInstance)
	{
		if(soundInstance != null)
		{
			AudioSource src = (AudioSource) soundInstance.GetComponent<AudioSource>();
            if (src != null)
            {
                src.Stop();
                return true;
            }
		}
        return false;
	}
	// Added by edwin, Playing a sound specified by it's GO, useful for HUD sound
	public bool Play(GameObject soundInstance, float volume, bool loop = false)
	{
		if(soundInstance != null)
		{
			AudioSource src = (AudioSource) soundInstance.GetComponent<AudioSource>();
			if(src != null && !src.isPlaying )
			{
				src.volume = volume;
				src.Play();
				src.loop = loop;
                return true;
            }
		}
        return false;
	}

	// Added by Edwin, for use in PlayerCommand.cs, and LobbySoundLoader.cs
	public GameObject NewSound(string name)
	{
		AudioData audioData = new AudioData();
		audioData.gameObject = new GameObject("Audio: " + name);
		GameObject parentObject = GameObject.Find ("HUDSound");

		if(parentObject != null)
			audioData.gameObject.transform.parent = parentObject.transform;
		
		//Source Setting
		audioData.audioSource = (AudioSource)audioData.gameObject.AddComponent<AudioSource>();
		audioData.audioSource.clip = (AudioClip)Resources.Load("SoundFx/" + name);
		audioData.audioSource.name = name;
		audioData.audioSource.rolloffMode = AudioRolloffMode.Linear;	// added by hw - for 3d sound, default log rolloff 		// will make it too soft when it's some distance away from camera
		
		return audioData.gameObject;
	}

    public bool IsPlaying(string name)
    {
        AudioData audioData = Search(name);
        if (audioData == null)
        {
            return false;
        }
        return audioData.audioSource.isPlaying;
    }
}