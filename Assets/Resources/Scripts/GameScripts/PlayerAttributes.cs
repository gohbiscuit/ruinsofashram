﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// WARNING
// note that upon any changes to level, str, agi, int and ski,
// you must do a recomputation of the creature stats (creature.RecomputePossessedStatsLater)
// the recomputation will automatically propagate to all other hosts as well

[System.Serializable]
public class PlayerAttributes {
	
	private int m_level = 1;
	private int m_experience = 0;
	private int m_current_exp = 0;


	// old exp to next level private int[] m_next_exp_to_level = {-1, 150, 300, 500, 750, 800, 900, 1000, 1100};			// edited by edwin
	private int[] m_next_exp_to_level = {-1, 250, 500, 750, 950, 1200, 1600, 2200, 3000};			// edited by edwin

	private int m_kills = 0;
	private int m_deaths = 0;

	private int m_strength = 1;
	private int m_agility = 1;
	private int m_intelligence = 1;
	private int m_skill_mastery = 1;

	// Gain experience when killing monsters / players etc
	// returns true if levelled up
	public bool GainEXP(int experience_gained) {
		m_experience += experience_gained;
		m_current_exp += experience_gained;
		bool gainedLevel = false;
		while(m_current_exp >= m_next_exp_to_level[m_level]){
			LevelUp();
			gainedLevel = true;
		}
		return gainedLevel;
	}

	public void ResetAttributes(){
		m_level = 1;
		m_experience = 0;
		m_current_exp = 0;
		
		m_kills = 0;
		m_deaths = 0;
		
		m_strength = 1;
		m_agility = 1;
		m_intelligence = 1;
		m_skill_mastery = 1;
	}
	
	void LevelUp() {
		m_current_exp = m_current_exp - m_next_exp_to_level[m_level];
		m_level += 1;

		Creature controllingCreature = PlayerCommand.Instance.getControlledCreature();
		if(controllingCreature != null){
			controllingCreature.getStats().Curr_HP = controllingCreature.getStats().HP;
			controllingCreature.getStats().Curr_MP = controllingCreature.getStats().MP;
			controllingCreature.RecomputePossessedStatsLater();
		}
	}

	public void IncrementKills() { m_kills++;}
	public void IncrementDeaths() { m_deaths++;}

	// Methods to be used in the GUI
	public void IncrementStrength() { m_strength += 1;}
	public void DecrementStrength() { m_strength -= 1;}
	
	public void IncrementAgility() { m_agility += 1;}
	public void DecrementAgility() { m_agility -= 1;}
	
	public void IncrementInt() { m_intelligence += 1;}
	public void DecrementInt() { m_intelligence -= 1;}
	
	public void IncrementSkillMastery() { m_skill_mastery += 1;}
	public void DecrementSkillMastery() { m_skill_mastery -= 1;}

	public int EXP
	{
		private set { m_experience = value; }
		get { return m_experience; }
	}

	public int CURRENT_EXP
	{
		private set { m_current_exp = value; }
		get { return m_current_exp; }
	}

	public int MAX_EXP
	{
		private set {  m_next_exp_to_level[m_level] = value; }
		get { return  m_next_exp_to_level[m_level]; }
	}
	
	public int CURRENT_LEVEL
	{
		private set { m_level = value; }
		get { return m_level; }
	}

	public int KILLS
	{
		get { return m_kills; }
	}

	public int DEATHS
	{
		get { return m_deaths; }
	}

	public int STRENGTH
	{
		set { m_strength = value; }
		get { return m_strength; }
	}
	
	public int AGILITY
	{
		set { m_agility = value; }
		get { return m_agility; }
	}
	
	public int INTELLIGENCE
	{
		set { m_intelligence = value; }
		get { return m_intelligence; }
	}
	
	public int SKILL_MASTERY
	{
		set { m_skill_mastery = value; }
		get { return m_skill_mastery; }
	}
}
