using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public enum StatusEffect { STUN, SLOW, BATTLE_ROAR, MANA_SHIELD, ORGE_AURA,
							SHIELD_BARRIER, FREEZING_SHIELD, EARTH_SHIELD, RATKIN_EYE, ASSASSIN_LOCK};

/// <summary>
/// Creature status effect.
/// STUN : stop and cannot use commands, overwrites ASSASSIN_LOCK
/// SLOW : 50% movement speed
/// BATTLE_ROAR : PHY_ATK +20 + 5*SKI
/// MANA_SHIELD : HP Damage is converted to MP Damage. Intensity = Amt of Damage that can be absorbed.
///				 Recover LOCALLY when intensity <= 0.
/// OGRE_AURA : Increase in HP_REGEN & MP_REGEN. Intensity = Amt of Increase. No limitation on duration.
///				Recover message has to be broadcasted to all (exit collision).
/// SHIELD_BARRIER : PHY_DEF +30
/// FREEZING_SHIELD : PHY_DEF +30, PHY_ATK +30, 80% movement speed
/// EARTH_SHIELD : HP Damage is converted to Intensity reduction to shield. Intensity = Amt of Damage that can be absorbed.
///				 Recover LOCALLY when intensity <= 0.
/// RATKIN_EYE : Unlimited Vision (TODO)
/// ASSASSIN_LOCK : weaker type of stun that breaks when attacked, overwrites STUN
/// </summary>
[System.Serializable]
public class CreatureStatusEffects {

	const float SLOW_PROPORTION = 0.5f;

	const float OGRE_AURA_REGEN_INCREASE_BASE = 10.0f;
	const float OGRE_AURA_REGEN_INCREASE_PER_SKI = 2;
	const float FREEZING_SHIELD_SPEED_PROPORTION = 0.8f;
	const float FREEZING_SHIELD_PHY_ATK_INCREASE = 30;
	const float FREEZING_SHIELD_PHY_DEF_INCREASE = 30;
	const float BATTLE_ROAR_PHY_ATK_INCREASE_BASE = 20.0f;
	const float BATTLE_ROAR_PHY_ATK_INCREASE_PER_SKI = 5;
	const float SHIELD_BARRIER_PHY_DEF_INCREASE_BASE = 50.0f;
	const float SHIELD_BARRIER_PHY_DEF_INCREASE_PER_SKI = 5;
	const float ORIGINAL_FOW_MIN = 1;
	const float ORIGINAL_FOW_MAX = 10;
	const float RATKIN_EYE_FOW_MIN = 10;
	const float RATKIN_EYE_FOW_MAX = 60;
	const float EARTH_SHIELD_DMG_REDUCTION_PROPORTION_BASE = 0.7f;
	const float EARTH_SHIELD_DMG_REDUCTION_PROPORTION_PER_SKI = 0.05f;
	const float EARTH_SHIELD_MAX_DMG_REDUCTION = 100;

	Creature m_creature;

	// afflicted effects
	bool m_stunned; 		bool m_stunnedLater;			float m_stunDuration;
	bool m_assassinLocked; 	bool m_assassinLockedLater;		float m_assassinLockDuration;
	bool m_slowed; 			bool m_slowedLater;				float m_slowDuration;
	bool m_ogreAuraed;		bool m_ogreAuraedLater;			List<float> m_ogreAuraIntensities;	float m_ogreAuraIntensity;		// list of stacked aura
	bool m_manaShielded;	bool m_manaShieldedLater;		float m_manaShieldDuration;			float m_manaShieldIntensity;	// mana shield absorption left
	bool m_freezingShielded;bool m_freezingShieldedLater;	float m_freezingShieldDuration;
	bool m_earthShielded;	bool m_earthShieldedLater;		float m_earthShieldDuration;		float m_earthShieldIntensity;	// earth shield skill level
	bool m_battleRoared;	bool m_battleRoaredLater;		float m_battleRoarDuration;			float m_battleRoarIntensity;	// battle roar skill level
	bool m_shieldBarriered;	bool m_shieldBarrieredLater;	float m_shieldBarrierDuration;		float m_shieldBarrierIntensity;	// shield barrier skill level
	bool m_ratkinEyed; 		bool m_ratkinEyedLater;			float m_ratkinEyeDuration;

	// FXs
	GameObject m_stunFX;
	GameObject m_assassinLockFX;
	GameObject m_slowFX;
	GameObject m_manaShieldFX;
	GameObject m_freezeShieldFX;
	GameObject m_earthShieldFX;
	GameObject m_battleRoarFX;
	GameObject m_shieldBarrierFX;

	public CreatureStatusEffects(Creature creature){
		this.m_creature = creature;
		m_ogreAuraIntensities = new List<float>();
	}

	/// <summary>
	/// reduces duration of status effects and recover if it's up
	/// </summary>
	/// <param name="durationDecrease">duration decrease</param>
	public void UpdateStatusEffects (float durationDecrease) {

		if(durationDecrease < 0)
			throw new ArgumentException("Inverse decrease in time");

		// STUN
		if(isStunned()){
			m_stunDuration = Mathf.Max(0, m_stunDuration - durationDecrease);
			if(m_stunDuration == 0) StunRecover();
		}

		// ASSASSIN LOCK
		if(isAssassinLocked()){
			m_assassinLockDuration = Mathf.Max(0, m_assassinLockDuration - durationDecrease);
			if(m_assassinLockDuration == 0) AssassinLockRecover();
		}

		// SLOW
		if(isSlowed()){
			m_slowDuration = Mathf.Max(0, m_slowDuration - durationDecrease);
			if(m_slowDuration == 0) SlowRecover();
		}

		// OGRE_AURA (special case)

		// MANA SHIELD
		if(isManaShielded()){
			m_manaShieldDuration = Mathf.Max(0, m_manaShieldDuration - durationDecrease);
			if(m_manaShieldIntensity == -1 || m_manaShieldDuration == 0)
				ManaShieldRecover();
		}

		// FREEZING SHIELD
		if(isFreezingShielded()){
			m_freezingShieldDuration = Mathf.Max(0, m_freezingShieldDuration - durationDecrease);
			if(m_freezingShieldDuration == 0) FreezingShieldRecover();
		}

		// EARTH SHIELD
		if(isEarthShielded()){
			m_earthShieldDuration = Mathf.Max(0, m_earthShieldDuration - durationDecrease);
			if(m_earthShieldDuration == 0) EarthShieldRecover();
		}

		// BATTLE ROAR
		if(isBattleRoared()){
			m_battleRoarDuration = Mathf.Max(0, m_battleRoarDuration - durationDecrease);
			if(m_battleRoarDuration == 0) BattleRoarRecover();
		}

		// SHIELD BARRIER
		if(isShieldBarriered()){
			m_shieldBarrierDuration = Mathf.Max(0, m_shieldBarrierDuration - durationDecrease);
			if(m_shieldBarrierDuration == 0) ShieldBarrierRecover();
		}

		// RATKIN EYE
		if(isRatkinEyed()){
			m_ratkinEyeDuration = Mathf.Max(0, m_ratkinEyeDuration - durationDecrease);
			if(m_ratkinEyeDuration == 0) RatkinEyeRecover();
		}
	}

	/// <summary>
	/// Gets the duration of the status effects.
	/// </summary>
	/// <returns>The status duration</returns>
	/// <param name="effectType">status effect type</param>
	public float getStatusDuration(StatusEffect effectType)
	{
		// add effect later
		switch(effectType)
		{
			case StatusEffect.STUN:
				return m_stunDuration;
				break;
			case StatusEffect.ASSASSIN_LOCK:
				return m_assassinLockDuration;
				break;
			case StatusEffect.SLOW:
				return m_slowDuration;
				break;
			case StatusEffect.ORGE_AURA:
				return -1.0f;	// infinity
				break;
			case StatusEffect.MANA_SHIELD:
				return m_manaShieldDuration;
				break;
			case StatusEffect.FREEZING_SHIELD:
				return m_freezingShieldDuration;
				break;
			case StatusEffect.EARTH_SHIELD:
				return m_earthShieldDuration;
				break;
			case StatusEffect.BATTLE_ROAR:
				return m_battleRoarDuration;
				break;
			case StatusEffect.SHIELD_BARRIER:
				return m_shieldBarrierDuration;
				break;
			case StatusEffect.RATKIN_EYE:
				return m_ratkinEyeDuration;
				break;
		}

		throw new ArgumentException("Invalid status effect");
		return -1.0f;
	}
	
	/// <summary>
	/// affect 'clean' unaffected stats
	/// prereq: stats of the creature must not have been affected
	/// 		by any status effect yet
	/// </summary>
	public void newlyAffect (){

		// get stats
		CreatureStats stats = m_creature.getStats();
		if(stats == null)
			throw new ArgumentException("Invalid Stats");

		// let the stats get affected by the status effect
		if(isStunned())
			StunLocallyNow(m_stunDuration);
		if(isAssassinLocked())
			AssassinLockLocallyNow(m_assassinLockDuration);
		if(isSlowed())
			SlowLocallyNow(m_slowDuration, true);
		if(isOgreAuraed())
			OgreAuraLocallyNow();
		if(isManaShielded())
			ManaShieldLocallyNow(m_manaShieldDuration, m_manaShieldIntensity);
		if(isFreezingShielded())
			FreezingShieldLocallyNow(m_freezingShieldDuration, true);
		if(isEarthShielded())
			EarthShieldLocallyNow(m_earthShieldDuration, m_earthShieldIntensity);
		if(isBattleRoared())
			BattleRoarLocallyNow(m_battleRoarDuration, m_battleRoarIntensity, true);
		if(isShieldBarriered())
			ShieldBarrierLocallyNow(m_shieldBarrierDuration, m_shieldBarrierIntensity, true);
		if(isRatkinEyed())
			RatkinEyeLocallyNow(m_ratkinEyeDuration);
	}

	// ---------------------------------------------

	/// <summary>
	/// Afflicts the status effect after an induced lag.
	/// </summary>
	/// <param name="type">Type of status effect</param>
	/// <param name="duration">Duration of status effect</param>
	/// <param name="intensity">Intensity of status effect (if any)</param>
	public void AfflictStatusEffectLater(StatusEffect type, float duration, float intensity){

		switch(type){
		case StatusEffect.STUN:
			m_stunnedLater = true;
			break;
		case StatusEffect.ASSASSIN_LOCK:
			m_assassinLockedLater = true;
			break;
		case StatusEffect.SLOW:
			m_slowedLater = true;
			break;
		case StatusEffect.ORGE_AURA:
			m_ogreAuraedLater = true;
			break;
		case StatusEffect.MANA_SHIELD:
			m_manaShieldedLater = true;
			break;
		case StatusEffect.FREEZING_SHIELD:
			m_freezingShieldedLater = true;
			break;
		case StatusEffect.EARTH_SHIELD:
			m_earthShieldedLater = true;
			break;
		case StatusEffect.BATTLE_ROAR:
			m_battleRoaredLater = true;
			break;
		case StatusEffect.SHIELD_BARRIER:
			m_shieldBarrieredLater = true;
			break;
		case StatusEffect.RATKIN_EYE:
			m_ratkinEyedLater = true;
			break;
		}

		// store affliction instruction to propagate to all clients
		GameNetworkManager.Instance.StoreAffliction(m_creature.CPhotonView, type, duration, intensity);
	}

	/// <summary>
	/// Afflicts the status effects now.
	/// </summary>
	/// <param name="type">Type of status effect</param>
	/// <param name="duration">Duration of status effect</param>
	/// <param name="intensity">Intensity of status effect (if any)</param>
	public void AfflictStatusEffectsLocallyNow(StatusEffect type, float duration, float intensity){

		switch(type){
		case StatusEffect.STUN:
			StunLocallyNow(duration);
			break;
		case StatusEffect.ASSASSIN_LOCK:
			AssassinLockLocallyNow(duration);
			break;
		case StatusEffect.SLOW:
			SlowLocallyNow(duration, false);
			break;
		case StatusEffect.ORGE_AURA:
			OgreAuraLocallyNow(intensity);
			break;
		case StatusEffect.MANA_SHIELD:
			ManaShieldLocallyNow(duration, intensity);
			break;
		case StatusEffect.FREEZING_SHIELD:
			FreezingShieldLocallyNow(duration, false);
			break;
		case StatusEffect.EARTH_SHIELD:
			EarthShieldLocallyNow(duration, intensity);
			break;
		case StatusEffect.BATTLE_ROAR:
			BattleRoarLocallyNow(duration, intensity, false);
			break;
		case StatusEffect.SHIELD_BARRIER:
			ShieldBarrierLocallyNow(duration, intensity, false);
			break;
		case StatusEffect.RATKIN_EYE:
			RatkinEyeLocallyNow(duration);
			break;
		}
	}
	
	/// <summary>
	/// force to recover from affliction of certain intensity
	/// note that some affliction recovers by itself (through time based or intensity reduction from damage)
	/// so that removal is not necessary
	/// </summary>
	/// <param name="type">Type of status effect to remove</param>
	/// <param name="intensityToRemove">Intensity to remove (if any)</param>
	public void RemoveStatusEffectLater(StatusEffect type, float intensityToRemove){
		switch(type){
		case StatusEffect.STUN:
			m_stunnedLater = false;
			break;
		case StatusEffect.ASSASSIN_LOCK:
			m_assassinLockedLater = false;
			break;
		case StatusEffect.SLOW:
			m_slowedLater = false;
			break;
		case StatusEffect.ORGE_AURA:
			m_ogreAuraedLater = false;
			break;
		case StatusEffect.MANA_SHIELD:
			m_manaShieldedLater = false;
			break;
		case StatusEffect.FREEZING_SHIELD:
			m_freezingShieldedLater = false;
			break;
		case StatusEffect.EARTH_SHIELD:
			m_earthShieldedLater = false;
			break;
		case StatusEffect.BATTLE_ROAR:
			m_battleRoaredLater = false;
			break;
		case StatusEffect.SHIELD_BARRIER:
			m_shieldBarrieredLater = false;
			break;
		case StatusEffect.RATKIN_EYE:
			m_ratkinEyedLater = false;
			break;
		}

		// store affliction removal instruction to propagate to all clients
		GameNetworkManager.Instance.StoreAfflictionRecovery(m_creature.CPhotonView, type, intensityToRemove);
	}
	
	/// <summary>
	/// remove the status effects now. some stackable skills will require specific intensity to be removed.
	/// not necessary to set 'intensityToRemove' for other skills.
	/// </summary>
	/// <param name="type">Type of status effect</param>
	/// <param name="intensityToRemove">Intensity to remove (if stackable)</param>
	public void RemoveStatusEffectsLocallyNow(StatusEffect type, float intensityToRemove){
		switch(type){
		case StatusEffect.STUN:
			StunRecover();
			break;
		case StatusEffect.ASSASSIN_LOCK:
			AssassinLockRecover();
			break;
		case StatusEffect.SLOW:
			SlowRecover();
			break;
		case StatusEffect.ORGE_AURA:
			OgreAuraRecover(intensityToRemove);
			break;
		case StatusEffect.MANA_SHIELD:
			ManaShieldRecover();
			break;
		case StatusEffect.FREEZING_SHIELD:
			FreezingShieldRecover();
			break;
		case StatusEffect.EARTH_SHIELD:
			EarthShieldRecover();
			break;
		case StatusEffect.BATTLE_ROAR:
			BattleRoarRecover();
			break;
		case StatusEffect.SHIELD_BARRIER:
			ShieldBarrierRecover();
			break;
		case StatusEffect.RATKIN_EYE:
			RatkinEyeRecover();
			break;
		}
	}

	// ---------------------------------------------

	/// <summary>
	/// Stuns now
	/// </summary>
	/// <param name="duration">Duration to stun</param>
	void StunLocallyNow(float duration){
		destroyFX(m_stunFX);
		attachFX("Stun", ref m_stunFX, new Vector3(0,1,0));
		m_stunDuration = duration;
		m_creature.StopLocallyNow();
		m_stunned = true;
		AssassinLockRecover();	// will overwrite assassin's lock
	}

	/// <summary>
	/// Recover from stun
	/// </summary>
	void StunRecover(){
		destroyFX(m_stunFX);
		m_stunned = false;
		m_stunnedLater = false;
	}

	/// <summary>
	/// Assassin Lock now.
	/// </summary>
	/// <param name="duration">Duration to assassin lock</param>
	void AssassinLockLocallyNow(float duration){
		destroyFX(m_assassinLockFX);
		attachFX("AssassinLock", ref m_assassinLockFX, new Vector3(0,1,0));
		m_assassinLockDuration = duration;
		if(m_creature != null)
			m_creature.StopLocallyNow();
		m_assassinLocked = true;
		StunRecover();	// will overwrite stun
	}

	/// <summary>
	/// Recover from assassin lock
	/// </summary>
	void AssassinLockRecover(){
		destroyFX(m_assassinLockFX);
		m_assassinLocked = false;
		m_assassinLockedLater = false;
	}

	/// <summary>
	/// Slow now
	/// </summary>
	/// <param name="duration">Duration to slow</param>
	/// <param name="newStats">if stats of creature is new</param>
	void SlowLocallyNow(float duration, bool newStats){
		destroyFX(m_slowFX);
		attachFX("Slow", ref m_slowFX, new Vector3(0,1,0));
		if(m_creature != null && !isSlowed() || newStats)
			m_creature.getAgent().multiplySpeed(SLOW_PROPORTION);
		m_slowDuration = duration;
		m_slowed = true;
	}

	/// <summary>
	/// Recover from slow
	/// </summary>
	void SlowRecover(){
		destroyFX(m_slowFX);
		if(m_creature != null)
			m_creature.getAgent().multiplySpeed(1.0f/SLOW_PROPORTION);
		m_slowed = false;
		m_slowedLater = false;
	}

	/// <summary>
	/// for new ogre aura affliction
	/// </summary>
	void OgreAuraLocallyNow(){
		float highestIntensities = 0;
		foreach(float intensity in m_ogreAuraIntensities)
			if(intensity > highestIntensities)
				highestIntensities = intensity;
		m_ogreAuraIntensity = highestIntensities;
		m_creature.getStats().HP_REGEN += OGRE_AURA_REGEN_INCREASE_BASE + 
			m_ogreAuraIntensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;
		m_creature.getStats().MP_REGEN += OGRE_AURA_REGEN_INCREASE_BASE + 
			m_ogreAuraIntensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;
		m_ogreAuraed = true;
	}

	/// <summary>
	/// Ogre ogre now
	/// </summary>
	/// <param name="intensity">Intensity of ogre aura</param>
	void OgreAuraLocallyNow(float intensity){
		m_ogreAuraIntensities.Add(intensity);

		if(m_ogreAuraIntensities.Count == 1)	// new affliction
			OgreAuraLocallyNow();
		else if(intensity > m_ogreAuraIntensity){	// better affliction

			// return to before intensity increment
			m_creature.getStats().HP_REGEN -= m_ogreAuraIntensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;
			m_creature.getStats().MP_REGEN -= m_ogreAuraIntensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;
			
			// use the new highest intensity
			m_creature.getStats().HP_REGEN += intensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;
			m_creature.getStats().MP_REGEN += intensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;
			
			m_ogreAuraIntensity = intensity;
		}

		m_ogreAuraed = true;
	}

	/// <summary>
	/// Recover from ogre aura
	/// </summary>
	/// <param name="intensityRecover">Intensity to remove</param>
	void OgreAuraRecover(float intensityRecover){
		m_ogreAuraIntensities.Remove(intensityRecover);

		if(m_ogreAuraIntensities.Count == 0){	// no longer afflicted
			m_creature.getStats().HP_REGEN -= OGRE_AURA_REGEN_INCREASE_BASE + 
				intensityRecover*OGRE_AURA_REGEN_INCREASE_PER_SKI;
			m_creature.getStats().MP_REGEN -= OGRE_AURA_REGEN_INCREASE_BASE + 
				intensityRecover*OGRE_AURA_REGEN_INCREASE_PER_SKI;
			m_ogreAuraed = false;
			m_ogreAuraedLater = false;

		} else {

			// if the current ogre aura intensity is the one being recovered and
			// there does not exist another intensity that is the same,
			// recompute the HP/MP regeneration rate
			if(intensityRecover == m_ogreAuraIntensity && !m_ogreAuraIntensities.Contains(intensityRecover)){
				float highestIntensity = 0;
				foreach(float intensity in m_ogreAuraIntensities)
					if(intensity > highestIntensity)
						highestIntensity = intensity;

				// return to before intensity increment
				m_creature.getStats().HP_REGEN -= intensityRecover*OGRE_AURA_REGEN_INCREASE_PER_SKI;
				m_creature.getStats().MP_REGEN -= intensityRecover*OGRE_AURA_REGEN_INCREASE_PER_SKI;

				// use the new highest intensity
				m_creature.getStats().HP_REGEN += highestIntensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;
				m_creature.getStats().MP_REGEN += highestIntensity*OGRE_AURA_REGEN_INCREASE_PER_SKI;

				m_ogreAuraIntensity = highestIntensity;
			}
		}
	}

	/// <summary>
	/// Mana Shield now
	/// </summary>
	/// <param name="duration">Duration of mana shield</param>
	/// <param name="intensity">Intensity of mana shield</param>
	void ManaShieldLocallyNow(float duration, float intensity){
		destroyFX(m_manaShieldFX);
		attachFX("ManaShield", ref m_manaShieldFX, new Vector3(0,1,0));
		m_manaShieldDuration = duration;
		m_manaShieldIntensity = intensity;
		m_manaShielded = true;
	}

	/// <summary>
	/// Recover from mana shield
	/// </summary>
	void ManaShieldRecover(){
		destroyFX(m_manaShieldFX);
		m_manaShielded = false;
		m_manaShieldedLater = false;
	}

	/// <summary>
	/// Freeze Shield now
	/// </summary>
	/// <param name="duration">Duration of freeze shield</param>
	/// <param name="newStats">if stats of creature is new</param>
	void FreezingShieldLocallyNow(float duration, bool newStats){
		destroyFX(m_freezeShieldFX);
		attachFX("FreezeShield", ref m_freezeShieldFX, new Vector3(0,1,0));
		if(m_creature != null && !isFreezingShielded() || newStats){
			m_creature.getAgent().multiplySpeed(FREEZING_SHIELD_SPEED_PROPORTION);
			m_creature.getStats().PHY_ATK += (int)FREEZING_SHIELD_PHY_ATK_INCREASE;
			m_creature.getStats().PHY_DEF += (int)FREEZING_SHIELD_PHY_DEF_INCREASE;
		}
		m_freezingShieldDuration = duration;
		m_freezingShielded = true;
	}

	/// <summary>
	/// Recover from Freezing Shield
	/// </summary>
	void FreezingShieldRecover(){
		destroyFX(m_freezeShieldFX);
		if(m_creature != null){
			m_creature.getAgent().divideSpeed(FREEZING_SHIELD_SPEED_PROPORTION);
			m_creature.getStats().PHY_ATK -= (int)FREEZING_SHIELD_PHY_ATK_INCREASE;
			m_creature.getStats().PHY_DEF -= (int)FREEZING_SHIELD_PHY_DEF_INCREASE;
		}
		m_freezingShielded = false;
		m_freezingShieldedLater = false;
	}

	/// <summary>
	/// Earth Shield now
	/// </summary>
	/// <param name="duration">Duration of earth shield</param>
	/// <param name="intensity">Intensity of earth shield</param>
	void EarthShieldLocallyNow(float duration, float intensity){
		destroyFX(m_earthShieldFX);
		attachFX("EarthShield", ref m_earthShieldFX, new Vector3(0,1,0));
		m_earthShieldDuration = duration;
		m_earthShieldIntensity = intensity;
		m_earthShielded = true;
	}

	/// <summary>
	/// Recover from Earth Shield
	/// </summary>
	void EarthShieldRecover(){
		destroyFX(m_earthShieldFX);
		m_earthShielded = false;
		m_earthShieldedLater = false;
	}

	/// <summary>
	/// Battle Roar now
	/// </summary>
	/// <param name="duration">Duration of battle roar</param>
	/// <param name="intensity">Intensity of battle roar</param>
	/// <param name="newStats">if stats of creature is new</param>
	void BattleRoarLocallyNow(float duration, float intensity, bool newStats){
		destroyFX(m_battleRoarFX);
		attachFX("BattleRoar", ref m_battleRoarFX, new Vector3(0,2.1f,0));

		if(isBattleRoared() && !newStats){
			// if already in battle roar,
			// need to recover first because increment scales by intensity
			BattleRoarRecover();
		}

		// set/add duration and intensity
		m_battleRoarDuration = duration;
		m_battleRoarIntensity = intensity;

		// compute actual rise : PHY_ATK += 20 + SkillLvl*5
		if(m_creature != null)
			m_creature.getStats().PHY_ATK += (int)(BATTLE_ROAR_PHY_ATK_INCREASE_BASE + 
		                                      m_battleRoarIntensity*BATTLE_ROAR_PHY_ATK_INCREASE_PER_SKI);

		m_battleRoared = true;
	}

	/// <summary>
	/// Recover from Battle Roar
	/// </summary>
	void BattleRoarRecover(){
		destroyFX(m_battleRoarFX);
		if(m_creature != null)
			m_creature.getStats().PHY_ATK -= (int)(BATTLE_ROAR_PHY_ATK_INCREASE_BASE + 
		                                       m_battleRoarIntensity*BATTLE_ROAR_PHY_ATK_INCREASE_PER_SKI);
		m_battleRoared = false;
		m_battleRoaredLater = false;
	}
	
	/// <summary>
	/// Shield Barrier now
	/// </summary>
	/// <param name="duration">Duration of shield barrier</param>
	/// <param name="intensity">Intensity of shield barrier</param>
	/// <param name="newStats">if stats of creature is new</param>
	void ShieldBarrierLocallyNow(float duration, float intensity, bool newStats){

		destroyFX(m_shieldBarrierFX);
		attachFX("ShieldBarrier", ref m_shieldBarrierFX, new Vector3(0,1,0));
		
		if(isShieldBarriered() && !newStats){
			// if already in shield barrier,
			// need to recover first because increment scales by intensity
			ShieldBarrierRecover();
		}
		
		// set/add duration and intensity
		m_shieldBarrierDuration = duration;
		m_shieldBarrierIntensity = intensity;
		
		// compute actual rise : PHY_DEF += 30 + SkillLvl*5
		if(m_creature != null)
			m_creature.getStats().PHY_DEF += (int)(SHIELD_BARRIER_PHY_DEF_INCREASE_BASE + 
		                                       m_shieldBarrierIntensity*SHIELD_BARRIER_PHY_DEF_INCREASE_PER_SKI);
		
		m_shieldBarriered = true;
	}

	/// <summary>
	/// Recover from Shield Barrier
	/// </summary>
	void ShieldBarrierRecover(){
		destroyFX(m_shieldBarrierFX);
		if(m_creature != null)
			m_creature.getStats().PHY_DEF -= (int)(SHIELD_BARRIER_PHY_DEF_INCREASE_BASE + 
		                                       m_shieldBarrierIntensity*SHIELD_BARRIER_PHY_DEF_INCREASE_PER_SKI);
		m_shieldBarriered = false;
		m_shieldBarrieredLater = false;
	}

	/// <summary>
	/// Ratkin Eye now
	/// </summary>
	/// <param name="duration">Duration of Ratkin's Eye</param>
	void RatkinEyeLocallyNow(float duration){
		if(m_creature != null)
			m_creature.GetComponent<FOWRevealer>().range = new Vector2(RATKIN_EYE_FOW_MIN, RATKIN_EYE_FOW_MAX);
		m_ratkinEyeDuration = duration;
		m_ratkinEyed = true;
	}

	/// <summary>
	/// Recover from Ratkin's Eye
	/// </summary>
	void RatkinEyeRecover(){
		if(m_creature != null)
			m_creature.GetComponent<FOWRevealer>().range = new Vector2(ORIGINAL_FOW_MIN, ORIGINAL_FOW_MAX);
		m_ratkinEyed = false;
		m_ratkinEyedLater = false;
	}

	// ---------------------------------------------
	
	/// <summary>
	/// Gets the damage left from mana shield.
	/// </summary>
	/// <returns>The damage left from mana shield.</returns>
	/// <param name="totalDamage">Total damage</param>
	public int getDamageLeftFromManaShield(float totalDamage){
		if(m_manaShielded)
			return (int)(totalDamage - getAbsorbedAmtFromManaShield(totalDamage));
		else
			return (int)totalDamage;
	}

	/// <summary>
	/// Absorbs the MP from mana shield.
	/// </summary>
	/// <returns>The damage left from mana shield.</returns>
	/// <param name="totalDamage">Total damage.</param>
	public int absorbMPFromManaShield(float totalDamage){
		if(m_manaShielded){
			float absorbedAmt = getAbsorbedAmtFromManaShield(totalDamage);
			
			// reduction in Mana Shield Intensity
			m_manaShieldIntensity -= absorbedAmt;
			m_creature.getStats().Curr_MP -= absorbedAmt;
			
			// used up mana shield or MP
			if(m_manaShieldIntensity <= 0 || m_creature.getStats().Curr_MP <= 0){
				AudioManager.Instance.PlayOnce("OgrePriest/24_summoner_rally_oba_1", m_creature, 1.0f);
				ManaShieldRecover();
			}
			
			// return what isn't absorbed
			return (int)(totalDamage - absorbedAmt);
		}
		
		// nothing is absorbed
		return (int)totalDamage;
	}

	/// <summary>
	/// Gets the absorbed damage amount from mana shield.
	/// </summary>
	/// <returns>The absorbed amount from mana shield.</returns>
	/// <param name="totalDamage">Total damage.</param>
	float getAbsorbedAmtFromManaShield(float totalDamage){
		if(m_manaShielded)
			return Mathf.Min(totalDamage, m_manaShieldIntensity, m_creature.getStats().Curr_MP);
		return 0;
	}

	/// <summary>
	/// Gets the damage left from earth shield.
	/// </summary>
	/// <returns>The damage left from earth shield.</returns>
	/// <param name="totalDamage">Total damage.</param>
	public int getDamageLeftFromEarthShield(float totalDamage){
		if(m_earthShielded) {

			// reduce damage
			int reducedDamage = (int)((EARTH_SHIELD_DMG_REDUCTION_PROPORTION_BASE - 
			                           m_earthShieldIntensity*EARTH_SHIELD_DMG_REDUCTION_PROPORTION_PER_SKI) * totalDamage);

			// limited by maximum reduction
			if(totalDamage - reducedDamage > EARTH_SHIELD_MAX_DMG_REDUCTION)
				reducedDamage = (int)(totalDamage - EARTH_SHIELD_MAX_DMG_REDUCTION);

			return (int)reducedDamage;
		} else
			return (int)totalDamage;	// nothing shielded
	}

	// ---------------------------------------------

	/// <summary>
	/// Checks if stunned.
	/// </summary>
	/// <returns><c>true</c>, if stunned, <c>false</c> otherwise.</returns>
	public bool isStunned(){
		return m_stunned;
	}

	/// <summary>
	/// Checks if assassin locked
	/// </summary>
	/// <returns><c>true</c>, if assassin locked, <c>false</c> otherwise.</returns>
	public bool isAssassinLocked(){
		return m_assassinLocked;
	}

	/// <summary>
	/// Checks if slow
	/// </summary>
	/// <returns><c>true</c>, if slowed, <c>false</c> otherwise.</returns>
	public bool isSlowed(){
		return m_slowed;
	}

	/// <summary>
	/// Checks if ogre auraed
	/// </summary>
	/// <returns><c>true</c>, if ogre auraed, <c>false</c> otherwise.</returns>
	public bool isOgreAuraed(){
		return m_ogreAuraed;
	}

	/// <summary>
	/// Checks if mana shielded
	/// </summary>
	/// <returns><c>true</c>, if mana shielded, <c>false</c> otherwise.</returns>
	public bool isManaShielded(){
		return m_manaShielded;
	}

	/// <summary>
	/// Gets the amount of mana shield absorption left.
	/// </summary>
	/// <returns>The mana shield absorption left.</returns>
	public float getManaShieldAbsorptionLeft(){
		return m_manaShieldIntensity;
	}

	/// <summary>
	/// Checks if freezing shielded
	/// </summary>
	/// <returns><c>true</c>, if freezing shielded, <c>false</c> otherwise.</returns>
	public bool isFreezingShielded(){
		return m_freezingShielded;
	}

	/// <summary>
	/// Checks if earth shielded.
	/// </summary>
	/// <returns><c>true</c>, if earth shielded, <c>false</c> otherwise.</returns>
	public bool isEarthShielded(){
		return m_earthShielded;
	}

	/// <summary>
	/// Checks if battle roared.
	/// </summary>
	/// <returns><c>true</c>, if battle roared, <c>false</c> otherwise.</returns>
	public bool isBattleRoared(){
		return m_battleRoared;
	}

	/// <summary>
	/// Checks if shield barriered.
	/// </summary>
	/// <returns><c>true</c>, if shield barriered, <c>false</c> otherwise.</returns>
	public bool isShieldBarriered(){
		return m_shieldBarriered;
	}

	/// <summary>
	/// Checks if ratkin eyed.
	/// </summary>
	/// <returns><c>true</c>, if ratkin eyed, <c>false</c> otherwise.</returns>
	public bool isRatkinEyed(){
		return m_ratkinEyed;
	}

	// ---------------------------------------------
	// 'later' version refers to what happens to the status effects after induced lag

	/// <summary>
	/// checks if will be stunned later.
	/// </summary>
	/// <returns><c>true</c>, if will be stunned, <c>false</c> otherwise.</returns>
	public bool isStunnedLater(){
		return m_stunnedLater;
	}

	/// <summary>
	/// checks if will be assassin locked later.
	/// </summary>
	/// <returns><c>true</c>, if will be assassin locked, <c>false</c> otherwise.</returns>
	public bool isAssassinLockedLater(){
		return m_assassinLockedLater;
	}

	/// <summary>
	/// checks if will be slowed later.
	/// </summary>
	/// <returns><c>true</c>, if will be slowed, <c>false</c> otherwise.</returns>
	public bool isSlowedLater(){
		return m_slowedLater;
	}

	/// <summary>
	/// checks if will be ogre auraed later.
	/// </summary>
	/// <returns><c>true</c>, if will be ogre auraed, <c>false</c> otherwise.</returns>
	public bool isOgreAuraedLater(){
		return m_ogreAuraedLater;
	}

	/// <summary>
	/// checks if will be mana shielded later.
	/// </summary>
	/// <returns><c>true</c>, if will be mana shielded, <c>false</c> otherwise.</returns>
	public bool isManaShieldedLater(){
		return m_manaShieldedLater;
	}

	/// <summary>
	/// checks if will be freeze shielded later.
	/// </summary>
	/// <returns><c>true</c>, if will be freeze shielded, <c>false</c> otherwise.</returns>
	public bool isFreezingShieldedLater(){
		return m_freezingShieldedLater;
	}

	/// <summary>
	/// checks if will be earth shielded later.
	/// </summary>
	/// <returns><c>true</c>, if will be earth shielded, <c>false</c> otherwise.</returns>
	public bool isEarthShieldedLater(){
		return m_earthShieldedLater;
	}

	/// <summary>
	/// checks if will be battle roared later.
	/// </summary>
	/// <returns><c>true</c>, if will be battle roared, <c>false</c> otherwise.</returns>
	public bool isBattleRoaredLater(){
		return m_battleRoaredLater;
	}

	/// <summary>
	/// checks if will be shield barriered later.
	/// </summary>
	/// <returns><c>true</c>, if will be shield barriered, <c>false</c> otherwise.</returns>
	public bool isShieldBarrieredLater(){
		return m_shieldBarrieredLater;
	}

	/// <summary>
	/// checks if will be ratkin eyed later.
	/// </summary>
	/// <returns><c>true</c>, if will be ratkin eyed, <c>false</c> otherwise.</returns>
	public bool isRatkinEyedLater(){
		return m_ratkinEyedLater;
	}

	// ---------------------------------------------

	/// <summary>
	/// Destroy the FX
	/// </summary>
	/// <param name="go">FX GameObject to destroy</param>
	void destroyFX(GameObject go){
		if(go != null)
			GameObject.Destroy(go);
	}

	/// <summary>
	/// Attachs the FX to the creature GameObject
	/// </summary>
	/// <param name="prefabPath">Prefab path of the FX</param>
	/// <param name="fx">FX GameObject</param>
	/// <param name="offset">Offset in position to place the FX</param>
	void attachFX(string prefabPath, ref GameObject fx, Vector3 offset){
		GameObject goPrefab = (GameObject)(Resources.Load("Prefab/StatusEffects/" + prefabPath));
		if(goPrefab == null)
			throw new ArgumentException("FX Prefab not found");
		if(m_creature == null)
			return;

		fx = (GameObject)(GameObject.Instantiate(goPrefab, m_creature.transform.position + offset, m_creature.transform.rotation));
		fx.transform.parent = m_creature.transform;
	}
}
