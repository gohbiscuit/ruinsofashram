﻿using UnityEngine;
using System.Collections;

public class AIBaseBehaviour : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public virtual void Act(CreatureInterface creature, GameObject target, bool isAttacked) {
	}

	public virtual bool getIsActing() {
		return true;
	}

	protected virtual void populateSkillList(CreatureInterface creature) {
	}
	
	protected virtual bool isNearTarget (CreatureInterface creature, GameObject target, Skill skill) {
		return true;
	}

	protected virtual void decideAction (CreatureInterface creature, GameObject target, bool isAttacked) {
	}
}
