﻿using UnityEngine;
using System.Collections;

public partial class CreatureAIIdleState : CreatureAIState {

    public CreatureAIIdleState(CreatureInterface creature) : base(creature) { }
    private string stateName = "Idle";
	
    // Typical FSM Activities

    /// <summary>
    /// Called every update()
    /// </summary>
    public override void Do()
    {
 
	}

    /// <summary>
    /// Called when state is entered 
    /// </summary>
    public override void Entry()
    {
        creature.StopLater();
	}

    /// <summary>
    /// Called when state is exited
    /// </summary>
    public override void Exit()
    {
	}

    /// <summary>
    /// Get state name
    /// </summary>
    /// <returns>string AI state name</returns>
	public override string getStateName() {
		return this.stateName;	
	}
}
