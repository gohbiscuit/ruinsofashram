﻿using UnityEngine;
using System.Collections;

public class AIStateManager : MonoBehaviour {
	
	private CreatureInterface creature;
    private Creator creator;

	private CreatureAIState currState;
    private CreatureAIState oldState;
    private CreatureAIIdleState idleState; 
    private CreatureAIPatrolState patrolState;
    private CreatureAIEngageState engageState;
    private CreatureAIReturnState returnState;

	private const float BaseTime = 3.0f;
	private const float IdleThreshold = 0.0f;
	private const float PatrolThreshold = 0.0f;
	private const float RandomRangeA = 0.0f;
	private const float RandomRangeB = 2.0f;

	private const int InsideAggroRange = 9;
    private const int RetreatAggroRange = 14;
	private const int OutsideAggroRange = 25;

	private float idleTime = 3.0f;
	private float patrolTime = 3.0f;

	// Use this for initialization
	void Start () {
		creator = (Creator)gameObject.GetComponent(typeof(Creator));

		creature = (CreatureInterface)(GetComponent<Creature>());

        // initialise concrete states
       	idleState = new CreatureAIIdleState(creature); 
        patrolState = new CreatureAIPatrolState(creature);
        engageState = new CreatureAIEngageState(creature);
        returnState = new CreatureAIReturnState(creature);

        // start state
        oldState = currState = idleState;
	}
	
	/// <summary>
	/// This is where the state changes get updated
	/// </summary>
	void Update () {
		GameObject nearestPlayer = Creator.GetNearestGO(gameObject.transform.position, CreatureType.PLAYER);

		switch (currState.getStateName()) {
			case "Idle":
				idleTime -= Time.deltaTime;
				if (idleTime <= IdleThreshold) {
	                currState = patrolState;
	                idleTime = RestartTime();
	            }
                if (nearestPlayer != null) {
                    if ((gameObject.transform.position - nearestPlayer.transform.position).magnitude < InsideAggroRange ||
                        creature.LastCreatureToHitMe != null) {
                        currState = engageState;
                    }
                }
				break;
			case "Patrol":
				patrolTime -= Time.deltaTime;
	            if (patrolTime <= PatrolThreshold) {
	                currState = idleState;
	                patrolTime = RestartTime();
	            }
                if (nearestPlayer != null) {
                    if ((gameObject.transform.position - nearestPlayer.transform.position).magnitude < InsideAggroRange ||
                        creature.LastCreatureToHitMe != null) {
                        currState = engageState;
                    }
                }
				break;
			case "Engage":
                if (nearestPlayer == null) {
                    currState = returnState;
                }
                else if ((gameObject.transform.position - creature.SpawnPoint.transform.position).magnitude > OutsideAggroRange && 
                    !engageState.getIsInAction()) {
					currState = returnState;
	            }
				break;
			case "Return":
				if (creature.SpawnPoint != null) {
					if ((gameObject.transform.position - creature.SpawnPoint.transform.position).magnitude <= 1.2) {
		                currState = idleState;
					}
                    else if (nearestPlayer != null && (gameObject.transform.position - creature.SpawnPoint.transform.position).magnitude < RetreatAggroRange) {
                        if ((gameObject.transform.position - nearestPlayer.transform.position).magnitude < InsideAggroRange ||
					           creature.LastCreatureToHitMe != null)
						    currState = engageState;
					}
				}
			break;
		default:
			break;
		}
        
        // Check for any state change and update accordingly
		if (oldState != currState) {
            oldState.Exit();
			oldState = currState;
            currState.Entry();
        }
        currState.Do();
	}

	private float RestartTime() {
		return BaseTime + Random.Range(RandomRangeA, RandomRangeB);
	}
}
