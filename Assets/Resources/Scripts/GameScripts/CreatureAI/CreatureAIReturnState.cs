﻿using UnityEngine;
using System.Collections;

public partial class CreatureAIReturnState : CreatureAIState {

    public CreatureAIReturnState(CreatureInterface creature) : base(creature) { }
    
	private string stateName = "Return";
    // Typical FSM Activities

    /// <summary>
    /// Called every update()
    /// </summary>
    public override void Do()
    {
        // Move to spawn point
		if(creature.SpawnPoint != null)
        	creature.MoveLater(creature.SpawnPoint.transform.position);
	}

    /// <summary>
    /// Called when state is entered 
    /// </summary>
    public override void Entry()
    {
        
	}

    /// <summary>
    /// Called when state is exited
    /// </summary>
    public override void Exit()
    {

	}

    /// <summary>
    /// Get state name
    /// </summary>
    /// <returns>string AI state name</returns>
	public override string getStateName() {
		return this.stateName;	
	}
}
