﻿using UnityEngine;
using System.Collections;

public partial class CreatureAIPatrolState : CreatureAIState {

    public CreatureAIPatrolState(CreatureInterface creature) : base(creature) { }

    private Vector3 destination;
    private string stateName = "Patrol";

	private const float PatrolRangeA = -8.0f;
	private const float PatrolRangeB = 8.0f;
	private const float PatrolZNum = 0.0f;
	private const float ReRandomNumA = 1.0f;
	private const float ReRandomNumB = 10.0f;
	// Typical FSM Activities

	/// <summary>
    /// Called every update()
	/// </summary>
    public override void Do()
    {
        creature.MoveLater(destination);
	}
	
    /// <summary>
    /// Called when state is entered 
    /// </summary>
    public override void Entry()
    {
		// patrol in areas around spawn point
		Vector3 creatureSpawnPos = creature.SpawnPoint.transform.position;
		destination = creatureSpawnPos + new Vector3(Random.Range(PatrolRangeA, PatrolRangeB), 
		                          					Random.Range(PatrolRangeA, PatrolRangeB), PatrolZNum);
        Random.seed = (int) Random.Range(ReRandomNumA, ReRandomNumB);
	}
	
    /// <summary>
    /// Called when state is exited
    /// </summary>
    public override void Exit()
    {
	}
    
    /// <summary>
    /// Get state name
    /// </summary>
    /// <returns>string AI state name</returns>
	public override string getStateName() {
		return this.stateName;	
	}
}
