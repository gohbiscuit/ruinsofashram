﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIOgreMageBehaviour : AIBaseBehaviour {
	private List<Skill> skills = null;
	private Creature chosenTarget = null;
	private Skill decision = null;
	private bool isActing = false;
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}

    /// <summary>
    /// Decide whether skill can be used
    /// </summary>
    /// <param name="creature">Current creature</param>
    /// <param name="target">Target enemy</param>
    /// <param name="isAttacked">Currently under attacked or not</param>
	public override void Act(CreatureInterface creature, GameObject target, bool isAttacked) {
		// hongwei - adjusted from 50 to 5 because the change in scale (10 to 1)
		// hongwei - skill 0 is now reserved for possess/release. skill 1-3 will be the actual skills.
		
		// populate local skill list for easy retrieval
		populateSkillList(creature);
		chosenTarget = target.GetComponent<Creature>();
		
		// when decision is empty and not acting,
		if (decision == null) {
            // make a decision on which skill to use
            decideAction(creature, target, isAttacked);
            // use the skill
			creature.UseSkillLater(decision, chosenTarget);
			isActing = true;
		} else {
			if (!creature.isStillUsingSkillLater(decision, chosenTarget) || decision == skills[1]) {
				decision = null;
				isActing = false;
			}
		}
	}

    /// <summary>
    /// Check if creature is currently using a skill
    /// </summary>
    /// <returns>bool acting status</returns>
	public override bool getIsActing() {
		return this.isActing;
	}

    /// <summary>
    /// Retrieve skills
    /// </summary>
	protected override void populateSkillList(CreatureInterface creature) {
		if (skills == null) {
			skills = creature.getSkillList();
		}
	}

    /// <summary>
    /// Use to check if creature is within range before casting skill
    /// </summary>
    /// <param name="creature">Current creature</param>
    /// <param name="target">Target enemy or ally</param>
    /// <param name="skill">Skill to use</param>
    /// <returns>bool within range</returns>
	protected override bool isNearTarget (CreatureInterface creature, GameObject target, Skill skill) {
		if ((creature.GameObjectProperty.transform.position - target.transform.position).magnitude < skill.getRange()) {
			return true;
		}
		return false;
	}

    /// <summary>
    /// Function to decide which skill the AI will use
    /// </summary>
    /// <param name="creature">Current creature</param>
    /// <param name="target">Target enemy or ally</param>
    /// <param name="isAttacked">bool under attack</param>
	protected override void decideAction (CreatureInterface creature, GameObject target, bool isAttacked) {
		if ((isNearTarget(creature, target, skills[2]) || isAttacked) && 
		    creature.canUseSkill(skills[2]) == SkillUsingMessage.SUCCESS) {
			// fireball
			decision = skills[2];
		} else if ((isNearTarget(creature, target, skills[3]) || isAttacked) && 
		           creature.canUseSkill(skills[3]) == SkillUsingMessage.SUCCESS) {
			// infernal scorch
			decision = skills[3];
		} else {
			// basic attack
			decision = skills[1];
		}
	}
}