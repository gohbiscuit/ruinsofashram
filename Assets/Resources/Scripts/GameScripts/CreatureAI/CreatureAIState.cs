﻿using UnityEngine;
using System.Collections;

public class CreatureAIState {

    protected CreatureInterface creature;
	protected string stateName;
	
    public CreatureAIState(CreatureInterface creature)
    {
        this.creature = creature;
    }
    
    // Typical FSM Activities

    /// <summary>
    /// Called every update()
    /// </summary>
	public virtual void Do(){
	}

    /// <summary>
    /// Called when state is entered 
    /// </summary>
	public virtual void Entry(){
	}

    /// <summary>
    /// Called when state is exited
    /// </summary>
	public virtual void Exit(){
	}

    /// <summary>
    /// Get state name
    /// </summary>
    /// <returns>string AI state name</returns>
    public virtual string getStateName() {
		return stateName;
	}
}
