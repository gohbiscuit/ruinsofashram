﻿using UnityEngine;
using System.Collections;

public partial class CreatureAIEngageState : CreatureAIState {

    public CreatureAIEngageState(CreatureInterface creature) : base(creature) { }
    
	private string stateName = "Engage";
	private AIBaseBehaviour behaviour;
	private bool isAttacked;
	private bool isInAction;
	// Typical FSM Activities

    /// <summary>
    /// Called every update()
    /// </summary>
    public override void Do()
    {
		if (isAttacked) {
			behaviour.Act(creature, creature.LastCreatureToHitMe.gameObject, isAttacked);
			isAttacked = false;
		} else {
        	behaviour.Act(creature, Creator.GetNearestGO(creature.GameObjectProperty.transform.position, 
		    	                                         CreatureType.PLAYER), isAttacked);
		}
		isInAction = behaviour.getIsActing();
	}

    /// <summary>
    /// Called when state is entered 
    /// </summary>
    public override void Entry()
    {
		// Check for class and load respective class behaviour
		switch(getCreatureName()) {
		case "OGRE_WARRIOR":
            behaviour = creature.GameObjectProperty.AddComponent<AIOgreWarriorBehaviour>();
			break;
		case "OGRE_ROGUE":
            behaviour = creature.GameObjectProperty.AddComponent<AIOgreRogueBehaviour>();
			break;
		case "OGRE_MAGE":
			behaviour = creature.GameObjectProperty.AddComponent<AIOgreMageBehaviour>();
			break;
		case "OGRE_PRIEST":
			behaviour = creature.GameObjectProperty.AddComponent<AIOgrePriestBehaviour>();
			break;
		case "RATKIN_WARRIOR":
            behaviour = creature.GameObjectProperty.AddComponent<AIRatkinWarriorBehaviour>(); 
			break;
		case "RATKIN_GUNNER":
            behaviour = creature.GameObjectProperty.AddComponent<AIRatkinGunnerBehaviour>();
			break;
		case "RATKIN_MAGE":
            behaviour = creature.GameObjectProperty.AddComponent<AIRatkinMageBehaviour>();
			break;
		case "RATKIN_SHAMAN":
            behaviour = creature.GameObjectProperty.AddComponent<AIRatkinShamanBehaviour>();
			break;
		}
		if (creature.LastCreatureToHitMe != null) {
			isAttacked = true;
		}
	}

    /// <summary>
    /// Called when state is exited
    /// </summary>
    public override void Exit()
    {
		isAttacked = false;
		isInAction = false;
	}

    /// <summary>
    /// Get state name
    /// </summary>
    /// <returns>string AI state name</returns>
	public override string getStateName() {
		return this.stateName;	
	}

    /// <summary>
    /// Get under attacked status
    /// </summary>
    /// <returns>bool is creature under attacked</returns>
	public bool getIsAttacked() {
		return this.isAttacked;
	}

    /// <summary>
    /// Get casting skill status
    /// </summary>
    /// <returns>bool is creature casting a skill</returns>
	public bool getIsInAction() {
		return this.isInAction;
	}

    /// <summary>
    /// Get creature name
    /// </summary>
    /// <returns>string creature name</returns>
	private string getCreatureName() {
		return creature.CRace.ToString()+"_"+creature.CClass.ToString();
	}
}
