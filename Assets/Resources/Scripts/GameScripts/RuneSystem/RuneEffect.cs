﻿using UnityEngine;
using System.Collections;

public enum RuneType {DAMAGE, HASTE, REGENERATION, INVISIBLE};

public class RuneEffect {

	Creature creature;
	
	bool withDamage;
	bool withHaste;
	bool withRegen;
	bool withInvis;
	
	float damageDuration = 60;
	float hasteDuration = 60;
	float regenDuration = 60;
	float invisDuration = 60;
	
	public RuneEffect (Creature creature) {
		this.creature = creature;
	}
	
	public void UpdateRuneEffect (float durationDecrease) {
		if (HasDamageEffect()) {
			damageDuration = Mathf.Max(0, damageDuration - durationDecrease);
			if (damageDuration == 0) {
				DamageRecover();
			}
		}
		
		if (HasHasteEffect()) {
			hasteDuration = Mathf.Max(0, hasteDuration - durationDecrease);
			if (hasteDuration == 0) {
				HasteRecover();
			}
		}
		
		if (HasRegenEffect()) {
			regenDuration = Mathf.Max(0, regenDuration - durationDecrease);
			if (regenDuration == 0) {
				RegenRecover();
			}
		}
		
		if (HasInvisEffect()) {
			invisDuration = Mathf.Max(0, invisDuration - durationDecrease);
			if (invisDuration == 0) {
				InvisRecover();
			}
		}
	}
	
	public void RemoveRuneEffect (RuneType type) {
		switch (type) {
		case RuneType.DAMAGE:
			DamageRecover();
			break;
		case RuneType.HASTE:
			HasteRecover();
			break;
		case RuneType.REGENERATION:
			RegenRecover();
			break;
		case RuneType.INVISIBLE:
			InvisRecover();
			break;
		}
	}
	
	public void AfflictRuneEffect (RuneType type) {
		switch (type) {
		case RuneType.DAMAGE:
			DamageEffect();
			break;
		case RuneType.HASTE:
			HasteEffect();
			break;
		case RuneType.REGENERATION:
			RegenEffect();
			break;
		case RuneType.INVISIBLE:
			InvisEffect();
			break;
		}
	}
	
	// Initialize effect ---------------------------------------------
	
	public void DamageEffect () {
		// increase creature damage
		creature.getStats().PHY_ATK += 30;
		creature.getStats().MAG_ATK += 30;
		withDamage = true;
	}
	
	public void HasteEffect() {
		// increase creature move speed
		creature.getAgent().multiplySpeed(4.0f);
		withHaste = true;
	}
	
	public void RegenEffect() {
		// increase creature hp & mp regeneration
		creature.getStats().HP_REGEN *= 2;
		creature.getStats().MP_REGEN *= 2;
		withRegen = true;
	}
	
	public void InvisEffect() {
		// make creature invisible
		withInvis = true;
	}
	
	// Reset effect ---------------------------------------------
	
	public void DamageRecover() {
		creature.getStats().PHY_ATK -= 30;
		creature.getStats().MAG_ATK -= 30;
		withDamage = false;
	}
	
	public void HasteRecover() {
		creature.getAgent().multiplySpeed(0.25f);
		withHaste = false;
	}
	
	public void RegenRecover() {
		creature.getStats().HP_REGEN /= 2;
		creature.getStats().MP_REGEN /= 2;
		withRegen = false;
	}
	
	public void InvisRecover() {
		withInvis = false;
	}
	
	// Return effect state ---------------------------------------------
	
	public bool HasDamageEffect() {
		return withDamage;
	}
	
	public bool HasHasteEffect() {
		return withHaste;
	}
	
	public bool HasRegenEffect() {
		return withRegen;
	}
	
	public bool HasInvisEffect() {
		return withInvis;
	}
	
	// ---------------------------------------------
}
