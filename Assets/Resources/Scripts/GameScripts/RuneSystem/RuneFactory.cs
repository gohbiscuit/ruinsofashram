﻿using UnityEngine;
using System.Collections;

public class RuneFactory : MonoBehaviour {
	
	private static RuneFactory instance;
	private bool canSpawnRune;
	private float cooldown;
	private Rune currentRune;
	
	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy(this);
		}
	}
	
	void Start () {
		
	}
	
	void Update () {
		// update cooldown
		CheckToSpawn();
		
		// check should spawn rune or not
		if (canSpawnRune) {
			CreateRune();
		}
	}
	
	public static RuneFactory Instance {
		get {
			return instance;
		}
	}
	
	public void Run() {
		canSpawnRune = true;
		cooldown = 60.0f;
		CreateRune();
	}
	
	private void CreateRune() {
		Vector3 runePosition = new Vector3(-2.16f, 0, 0.83f);
		//Quaternion runeRotation = new Quaternion(0, 0, 0, 0);
		
		GameObject tempRune = (GameObject) (Resources.Load("Prefab/RuneObject"));
		currentRune = (Rune) GameObject.Instantiate(tempRune, runePosition, Quaternion.identity);
		
		canSpawnRune = false;
		CountDown();
	}
	
	private void CountDown() {
		Mathf.Max(0, cooldown--);
	}
	
	private void CheckToSpawn() {
		if (!canSpawnRune) {
			CountDown();
		}
		// can spawn when 1) currentRune is not there -> 2) cooldown finishes
		if (currentRune.gameObject == null && cooldown <= 0) {
			canSpawnRune = true;
			cooldown = 60;
		}
	}
}