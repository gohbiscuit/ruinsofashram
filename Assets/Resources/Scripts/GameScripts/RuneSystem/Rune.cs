﻿using UnityEngine;
using System.Collections;

public class Rune : MonoBehaviour {
	// info
	protected string name;
	protected bool isTaken;
	protected Creature grabber;
	
	// Use this for initialization
	void Start () {
		switch (Random.Range(0, 2)) {
		case 0:
			name = "damage";
			break;
		case 1:
			name = "haste";
			break;
		case 2: name = "regen";
			break;
		}
		
		isTaken = false;
	}
	
	// Update is called once per frame
	void Update () {
		// grabber next to rune and is taking it
		if (isTaken) {
			// create RuneEffect on grabber
			switch (name) {
			case "damage":
				grabber.AfflictRuneEffect(RuneType.DAMAGE);
				break;
			case "haste":
				grabber.AfflictRuneEffect(RuneType.HASTE);
				break;
			case "regen":
				grabber.AfflictRuneEffect(RuneType.REGENERATION);
				break;
			}
			Destroy(gameObject);
		} else {
			isTaken = false;
		}
	}
	
	public void grabRune (bool taken, Creature target) {
		if ((gameObject.transform.position - target.transform.position).magnitude < 0.5) {
			isTaken = taken;
			grabber = target;
		}
	}
}
