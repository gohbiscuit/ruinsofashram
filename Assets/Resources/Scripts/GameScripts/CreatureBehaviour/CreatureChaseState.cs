﻿using System;
using UnityEngine;
using System.Collections;
using Pathfinding;

/// <summary>
/// Creature is chasing another creature to use the skill
/// </summary>
public class CreatureChaseState : CreatureState {

	const float CHASE_INNER_OFFSET = 0.6f;
	
	public CreatureChaseState(Creature creature) : base(creature){}

	/// <summary>
	/// called when state is entered
	/// </summary>
	public override void Entry(){
		if(m_creature.animation != null)
			m_creature.animation.wrapMode = WrapMode.Loop;
	}

	/// <summary>
	/// called every frame
	/// </summary>
	public override void Do() {

		// compute animation to play
		if(m_creature.getAgent().isMoving()){
			if(m_creature.WalkAnimation)
				m_creature.animation.CrossFade(m_creature.WalkAnimation.name);
		} else {
			if(m_creature.StopAnimation)
				m_creature.animation.CrossFade(m_creature.StopAnimation.name);
		}

		// get the coordinate of the target/position
		Vector3 targetPoint = m_creature.getTargetPoint();

		// check if close enough to be considered within range to cast skill
		// will go slightly deeper so that it doesn't need to keep chasing
		if(Vector3.Distance(m_creature.transform.position, targetPoint) <= Mathf.Max (0, m_creature.getSkillUsing().getRange()-CHASE_INNER_OFFSET)){
			Transit("Within Range");
			return;
		}

		// set destination to move the pathfinding agent to
		if(m_creature.getTarget() is Creature)
			m_creature.getAgent().SetDestination((Creature)(m_creature.getTarget()));
		else if(m_creature.getTarget() is Vector3)
			m_creature.getAgent().SetDestination(m_creature.getTargetPoint());
		else
			throw new ArgumentException("Invalid Target");
	}

	/// <summary>
	/// called when state is exited
	/// </summary>
	public override void Exit(){

		// stop the pathfinding agent
		m_creature.getAgent().Stop();
	}
}
