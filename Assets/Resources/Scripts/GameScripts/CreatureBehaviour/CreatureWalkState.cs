﻿using System;
using UnityEngine;
using System.Collections;
using Pathfinding;
using Pathfinding.RVO;

/// <summary>
/// Creature is moving to somewhere
/// </summary>
public class CreatureWalkState : CreatureState {
	
	public CreatureWalkState(Creature creature) : base(creature){}

	/// <summary>
	/// called when state is entered
	/// </summary>
	public override void Entry(){

		if(m_creature.animation != null)
			m_creature.animation.wrapMode = WrapMode.Loop;

		// get where it is moving to
		Vector3 targetPoint = m_creature.getTargetPoint();

		// set the position to move pathfinding agent to
		m_creature.getAgent().SetDestination(targetPoint);
	}

	/// <summary>
	/// called every frame
	/// </summary>
	public override void Do() {

		// compute animation to play
		if(m_creature.getAgent().isMoving()){
			if(m_creature.WalkAnimation)
				m_creature.animation.CrossFade(m_creature.WalkAnimation.name);
		} else {
			if(m_creature.StopAnimation)
				m_creature.animation.CrossFade(m_creature.StopAnimation.name);
		}

		if(m_creature.getTarget() is Creature){

			// update path to chase after creature
			Vector3 targetPoint = m_creature.getTargetPoint();
			m_creature.getAgent().SetDestination(targetPoint);

		} else if(m_creature.getTarget() is Vector3) {

			// check if reaching position
			CreatureMoveAgent agent = m_creature.getAgent();
			if (agent.hasReached())
				Transit("Reach Destination");

		} else
			throw new ArgumentException("Invalid Destination");
	}

	/// <summary>
	/// called when state is exited
	/// </summary>
	public override void Exit(){
	}
}
