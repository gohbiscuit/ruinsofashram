﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Creature is charging/channelling the skill until it can be casted/activated
/// </summary>
public class CreatureChargeState : CreatureState {

	const float CHARGE_TURN_SPEED = 12;
	
	public CreatureChargeState(Creature creature) : base(creature){}

	/// <summary>
	/// called when state is entered
	/// </summary>
	public override void Entry(){

		// set animation to its skill animation if there is one
		// otherwise, use the ready animation
		string chargeAnimation = m_creature.getSkillUsing().getChargeAnimation();
		if(chargeAnimation != null)
			m_creature.animation.CrossFade(chargeAnimation);
		else if(m_creature.ReadyAnimation)
			m_creature.animation.CrossFade(m_creature.ReadyAnimation.name);

		// begin charging the skill to use
		m_creature.getSkillUsing().StartCharging();
	}

	/// <summary>
	/// called every frame
	/// </summary>
	public override void Do() {

		// rotate towards target if target is not self
		if(!(m_creature.getTarget() is Creature) || (Creature)(m_creature.getTarget()) != m_creature)
			Helper.SmoothLookAt(m_creature.transform, m_creature.getTargetPoint(), CHARGE_TURN_SPEED);

		// get the skill creature is using
		Skill skill = m_creature.getSkillUsing();

		// checks whether skill has finished charging and is ready to be casted
		if(skill.isCharged()){
			m_creature.getStats().Curr_MP = Mathf.Max(0.0f, ((float)(m_creature.getStats().Curr_MP - skill.getMPCost())));
			Transit("Charge Over");
			return;
		}
	
		// checks if target has escaped and has to chase again
		Vector3 targetPoint = m_creature.getTargetPoint();
		if(Vector3.Distance(m_creature.transform.position, targetPoint) > skill.getRange())
			Transit("Out of Range");
	}

	/// <summary>
	/// called when state is exited
	/// </summary>
	public override void Exit(){
	}
}
