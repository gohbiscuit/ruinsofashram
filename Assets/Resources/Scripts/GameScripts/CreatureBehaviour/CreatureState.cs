﻿using System;
using UnityEngine;
using System.Collections;

/// <summary>
/// Behaviourial state of a creature
/// </summary>
public abstract class CreatureState {

	// reference to the creature that is in this state
	protected Creature m_creature;
	
	public CreatureState(Creature creature){
		this.m_creature = creature;
	}

	/// <summary>
	/// Transit from one state to another given the event
	/// </summary>
	/// <param name="eventToFire">Event to fire</param>
	public void Transit(string eventToFire){
		m_creature.fireEvent(eventToFire);
	}
	
	// Commands

	/// <summary>
	/// Gives a command to stop the creature
	/// </summary>
	public virtual void Player_Stop(){
		Transit("Command - Stop");
	}

	/// <summary>
	/// Gives a command to move the creature to another creature or position
	/// </summary>
	/// <param name="target">creature target or position</param>
	public virtual void Player_Move(object target){

		if(!(target is Vector3 || target is Creature))
			throw new ArgumentException("Invalid move target" + target.GetType().ToString());

		m_creature.setTarget(target);
		Transit("Command - Walk");
	}

	/// <summary>
	/// Gives a command to use skill on a creature or position
	/// </summary>
	/// <param name="skill">Skill to use</param>
	/// <param name="target">creature target or position</param>
	public virtual void Player_UseSkill(Skill skill, object target){

		if(!(target is Vector3 || target is Creature))
			throw new ArgumentException("Invalid target" + target.GetType().ToString());

		m_creature.setSkill(skill);
		m_creature.setTarget(target);
		Transit("Command - Skill");
	}

	// Typical FSM Activities

	/// <summary>
	/// called when state is entered
	/// </summary>
	public abstract void Entry();

	/// <summary>
	/// called every frame
	/// </summary>
	public abstract void Do();

	/// <summary>
	/// called when state is exited
	/// </summary>
	public abstract void Exit();
}

