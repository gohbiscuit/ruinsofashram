﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Creature casts/activates the skill.
/// </summary>
public class CreatureCastState : CreatureState {

	const float CAST_TURN_SPEED = 17;

	Skill m_activatingSkill;

	public CreatureCastState(Creature creature) : base(creature){}

	// override all the commands to stop the execution of the skill
	// before executing them

	/// <summary>
	/// Gives a command to stop the creature
	/// </summary>
	public override void Player_Stop(){
		m_creature.getSkillUsing().stopActivate();
		base.Player_Stop();
	}

	/// <summary>
	/// Gives a command to move the creature to another creature or position
	/// </summary>
	/// <param name="target">creature target or position</param>
	public override void Player_Move(object target){
		m_creature.getSkillUsing().stopActivate();
		base.Player_Move(target);
	}

	/// <summary>
	/// Gives a command to use skill on a creature or position
	/// </summary>
	/// <param name="skill">Skill to use</param>
	/// <param name="target">creature target or position</param>
	public override void Player_UseSkill(Skill skill, System.Object target){
		m_creature.getSkillUsing().stopActivate();
		base.Player_UseSkill(skill, target);
	}

	/// <summary>
	/// called when state is entered
	/// </summary>
	public override void Entry(){
		if(m_creature.animation)
			m_creature.animation.wrapMode = WrapMode.Loop;
	}

	/// <summary>
	/// called every frame
	/// </summary>
	public override void Do() {

		// check if target is gone
		//if(m_creature.getTarget().ToString().Equals("null")){
		//	Transit("Skill End");
		//	return;
		//}

		// check if attacker/target is gone
		if(m_creature == null || 
		   (m_creature.getTarget() is Creature && (Creature)(m_creature.getTarget()) == null)){
			m_creature.getSkillUsing().stopActivate();
			Transit("Skill End");
			return;
		}

		// rotate towards target if target is not self
		if(!(m_creature.getTarget() is Creature) || (Creature)(m_creature.getTarget()) != m_creature)
			Helper.SmoothLookAt(m_creature.transform, m_creature.getTargetPoint(), CAST_TURN_SPEED);

		// get skill creature is using
		Skill skill = m_creature.getSkillUsing();
		m_activatingSkill = skill;
		
		// out of range, stop attack
		Vector3 targetPoint = m_creature.getTargetPoint();
		if(Vector3.Distance(m_creature.transform.position, targetPoint) > m_creature.getSkillUsing().getRange()){
			if(skill.isPersistent()){
				Transit("Skill End & Chase");
			} else {
				m_creature.removeSkillLaterCommand();
				Transit("Skill End");
			}
			return;
		}
		
		// keep activating the skill every frame until the skill has ended
		if(skill.Activate(m_creature.getTarget())){

			// check if skill is persistent - will execute the 
			// same skill again after execution
			if(skill.isPersistent())
				Transit("Skill End & Wait For Cooldown");
			else {
				m_creature.removeSkillLaterCommand();
				Transit("Skill End");

				// for non basic attacks that belong to the current creature player is controlling,
				// make it use basic attack on the same target after executing it
				object target = m_creature.getTarget();
				if(m_creature.CRace != CreatureRace.WISP && PlayerCommand.Instance.isControlling(m_creature) && target != null && 
				   target is Creature && target != m_creature && m_creature.getPendingCommand() == CreatureCommands.NONE){
					m_creature.UseSkillLater(m_creature.getSkillList()[1], target);
					m_creature.ContinueBasicAttack = true;
				}
			}
		}
	}

	/// <summary>
	/// called when state is exited
	/// </summary>
	public override void Exit(){

		// force stop activation of the skill if it hasn't ended
		if(m_activatingSkill != null)
			m_activatingSkill.stopActivate();

		//m_creature.animation.wrapMode = WrapMode.ClampForever;
		if(m_creature.getTarget() is Creature && (Creature)(m_creature.getTarget()) != null){
			if(m_creature.CRace == CreatureRace.OGRE){
				m_creature.animation.Play(m_creature.StopAnimation.name, AnimationPlayMode.Stop);
				m_creature.animation.wrapMode = WrapMode.Loop;
			}
		}
	}
}
