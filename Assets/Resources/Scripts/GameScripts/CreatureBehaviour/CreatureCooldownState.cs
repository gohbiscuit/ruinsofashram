﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Creature is waiting for the skill to finish cooling down
/// </summary>
public class CreatureCooldownState : CreatureState {

	const float COOLDOWN_TURN_SPEED = 10;
	
	public CreatureCooldownState(Creature creature) : base(creature){}

	/// <summary>
	/// called when state is entered
	/// </summary>
	public override void Entry(){

		// set to ready animation
		if(m_creature.ReadyAnimation)
			m_creature.animation.CrossFade(m_creature.ReadyAnimation.name);
	}

	/// <summary>
	/// called every frame
	/// </summary>
	public override void Do() {

		// rotate towards target if target is not self
		if(!(m_creature.getTarget() is Creature) || (Creature)(m_creature.getTarget()) != m_creature)
			Helper.SmoothLookAt(m_creature.transform, m_creature.getTargetPoint(), COOLDOWN_TURN_SPEED);

		// get the skill that creature is using
		Skill skill = m_creature.getSkillUsing();

		// check if skill has cooled
		if(skill.isCooled()){
			Transit("Cooldown Over");
			return;
		}

		// check if target has went out of range and has to chase again
		Vector3 targetPoint = m_creature.getTargetPoint();
		if(Vector3.Distance(m_creature.transform.position, targetPoint) > skill.getRange())
			Transit("Out of Range");
	}

	/// <summary>
	/// called when state is exited
	/// </summary>
	public override void Exit(){
	}
}
