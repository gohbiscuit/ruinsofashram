﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Creature is not moving or doing anything
/// </summary>
public class CreatureStopState : CreatureState {

	const float COUNTER_EXPIRY_TIME = 0.5f;
	
	public CreatureStopState(Creature creature) : base(creature){}

	/// <summary>
	/// called when state is entered
	/// </summary>
	public override void Entry(){

		// set previous command
		m_creature.setPrevCommand(CreatureCommands.STOP);

		// set animation


		//if(m_creature.CRace == CreatureRace.OGRE && m_creature.animation.clip != m_creature.WalkAnimation){
		//	m_creature.animation.Play(m_creature.StopAnimation.name, AnimationPlayMode.Stop);
		//	m_creature.animation.wrapMode = WrapMode.Loop;
		//} else 
		if(m_creature.StopAnimation){
			m_creature.animation.CrossFade(m_creature.StopAnimation.name);
			m_creature.animation.wrapMode = WrapMode.Loop;
		}

		// stop pathfinding agent
		m_creature.getAgent().Stop();
	}

	/// <summary>
	/// called every frame
	/// </summary>
	public override void Do() {

		// if a creature hits it while it's in stop state and is within some range, counter with a basic attack
		Creature creatureToCounter = m_creature.LastCreatureToHitMe;
		if(m_creature.CRace != CreatureRace.WISP && 											// not a wisp
		   !m_creature.UnexecutedCommand &&														// no command is waiting to be executed after induced lag 
		   !m_creature.ContinueBasicAttack && 													// creature is not trying to continue basic attacking after a special skill
		   PlayerCommand.Instance.isControlling(m_creature) &&									// this creature is controlled by this player 
		   creatureToCounter != null && m_creature.TimeSinceLastHit < COUNTER_EXPIRY_TIME && 	// creature attacking it does so recently
		   m_creature.getPendingCommand() == CreatureCommands.NONE &&							// no pending command
		   Vector3.Distance(m_creature.transform.position, creatureToCounter.transform.position) 
		   <= m_creature.getSkillList()[1].getRange())											// basic attack is within range
			m_creature.UseSkillLater(m_creature.getSkillList()[1], creatureToCounter);
	}

	/// <summary>
	/// called when state is exited
	/// </summary>
	public override void Exit(){
		m_creature.ContinueBasicAttack = false;
	}
}
