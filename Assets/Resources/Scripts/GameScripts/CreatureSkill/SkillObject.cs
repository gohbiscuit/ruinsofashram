using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class SkillObject : MonoBehaviour {

	protected int m_skillID;
	protected Creature m_attacker;
	protected object m_target;
	
	protected bool m_initialised = false;
	public GameObject startFX;
	public GameObject endFX;
	
	// creatures within the collider is stored in the set
	// key is the creature, value is dependent on skill
	// only for AOE skills (not used remotely)
	protected Hashtable m_collidingCreatures;

	/// <summary>
	/// Called once when first created.
	/// </summary>
	void Awake(){
		if(GetComponent<Collider>()){

			// 'freeze' the rigidbody to prevent odd movement from physics
			Rigidbody body = gameObject.AddComponent<Rigidbody>();
			body.constraints = RigidbodyConstraints.FreezeAll;
			body.collisionDetectionMode = CollisionDetectionMode.Continuous;
			body.useGravity = false;
			m_collidingCreatures = new Hashtable();
		}
		if(startFX)
			Instantiate(startFX, transform.position + new Vector3(0,1.1f,0), transform.rotation);
	}
	
	/// <summary>
	/// Initialise the user of the skill that spawns this skillobject,
	/// and the target
	/// </summary>
	/// <param name="skillID">Skill ID that creates this skillobject</param>
	/// <param name="attacker">User of the skill</param>
	/// <param name="target">Target to use skill at</param>
	public virtual void initialise(int skillID, Creature attacker, object target){
		
		this.m_skillID = skillID;
		this.m_target = target;
		this.m_attacker = attacker;
		
		m_initialised = true;
	}
	
	/// <summary>
	/// Deal damage to the specified victim after induced lag
	/// </summary>
	/// <param name="victim">Victim of damage</param>
	public void dealDamageLater(Creature victim){
		if(m_attacker.IsMine){
			victim.TakeDamageLater(m_skillID, m_attacker);
		}
	}

	/// <summary>
	/// Deal damage (with multiplier) to the specified victim after induced lag
	/// </summary>
	/// <param name="victim">Victim of damage</param>
	/// <param name="multiplier">damage multiplier</param>
	public void dealDamageLater(Creature victim, float multiplier){
		if(m_attacker.IsMine){
			victim.TakeDamageLater(m_skillID, m_attacker, multiplier);
		}
	}
	
	/// <summary>
	/// Deal damage to the target after induced lag
	/// </summary>
	public void dealDamageLater(){
		if(m_attacker.IsMine){
			if(m_target is Creature)
				((Creature)(m_target)).TakeDamageLater(m_skillID, m_attacker);
			else
				throw new ArgumentException("Cannot deal damage to non-creature");
		}
	}
	
	/// <summary>
	/// Deal damage (with multiplier) to the target after induced lag
	/// </summary>
	/// <param name="multiplier">damage multiplier</param>
	public void dealDamageLater(float multiplier){
		if(m_attacker.IsMine){
			if(m_target is Creature)
				((Creature)(m_target)).TakeDamageLater(m_skillID, m_attacker, multiplier);
			else
				throw new ArgumentException("Cannot deal damage to non-creature");
		}
	}

	/// <summary>
	/// Gives health to the given target after induced lag
	/// </summary>
	/// <param name="target">Target.</param>
	public void gainHealthLater(Creature target){
		if(m_attacker.IsMine){
			target.GainHealthLater(m_skillID, m_attacker);
		}
	}

	/// <summary>
	/// Gives health to the target after induced lag
	/// </summary>
	public void gainHealthLater(){
		if(m_attacker.IsMine){
			if(m_target is Creature)
				((Creature)(m_target)).GainHealthLater(m_skillID, m_attacker);
			else
				throw new ArgumentException("Cannot gain health for non-creature");
		}
	}

	/// <summary>
	/// Afflict status effect (of certain duration (if any) 
	/// & intensity (if any)) to the specified target after induced lag
	/// </summary>
	/// <param name="target">Target to afflict on</param>
	/// <param name="type">Type of status effect</param>
	/// <param name="duration">Duration of status effect</param>
	/// <param name="intensity">Intensity of status effect</param>
	public void afflictStatusEffectsLater(Creature target, StatusEffect type, float duration, float intensity){
		if(m_attacker.IsMine)
			target.AfflictStatusEffectLater(type, duration, intensity);
	}
	
	/// <summary>
	/// Remove status effect (of certain duration (if any) & intensity (if any)) 
	/// to the specified target after induced lag
	/// </summary>
	/// <param name="target">Target to remove from</param>
	/// <param name="type">Type of status effect to remove</param>
	/// <param name="intensityToRemove">Intensity of status effect to remove.</param>
	public void removeStatusEffectsLater(Creature target, StatusEffect type, float intensityToRemove){
		if(m_attacker.IsMine)
			target.RemoveStatusEffectLater(type, intensityToRemove);
	}
	
	/// <summary>
	/// Remove this GameObject.
	/// </summary>
	protected void Remove(){
		if(endFX)
			Instantiate(endFX, transform.position + new Vector3(0,1.1f,0), transform.rotation);
		Destroy(gameObject);
	}
	
	/// <summary>
	/// move at linear speed to a target transform
	/// returns true if reached destination
	/// </summary>
	/// <returns><c>true</c>, if move has reached destination, <c>false</c> otherwise.</returns>
	/// <param name="speed">move speed</param>
	/// <param name="src">Source to move from</param>
	/// <param name="target">Target to move to</param>
	protected bool linearMove(float speed, Transform src, Transform target){

		if(target == null || src == null)
			throw new ArgumentException("Transform(s) are null");

		Vector3 fromToVector = (target.position - src.position);
		if(Vector3.Distance(Vector3.zero, fromToVector) > speed + 0.1f){
			src.position += fromToVector.normalized * speed;
			return false;
		} else {
			src.position = target.position;
			return true;
		}
	}
	
	/// <summary>
	/// move at linear speed to a target transform
	/// </summary>
	/// <param name="speed">move speed</param>
	/// <param name="src">Source to move from</param>
	/// <param name="target">Target to move to</param>
	/// <param name="canDoDamage">Can do damage.</param>
	/// <param name="canDestroy">Can destroy.</param>
	protected void linearMove(float speed, Transform src, Transform target, ref bool canDoDamage, ref bool canDestroy){

		if(target == null || src == null)
			throw new ArgumentException("Transform(s) are null");

		Vector3 fromToVector = (target.position - src.position);
		
		// damage is sent first
		if(!canDoDamage){
			float distLeft = Vector3.Distance(Vector3.zero, fromToVector);
			float updatesLeft = Mathf.Ceil(distLeft/speed);
			if(updatesLeft * Time.fixedDeltaTime < GameNetworkManager.Instance.getTotalLag())
				canDoDamage = true;
		}
		
		// check if should destroy
		if(Vector3.Distance(Vector3.zero, fromToVector) > speed + 0.3f){
			src.position += fromToVector.normalized * speed;
			canDestroy = false;
		} else
			canDestroy = true;
	}
	
	// only AOE skills (that needs to do collision detection) will run the following ----------------------------------
	
	/// <summary>
	/// called automatically when another gameobject enters its collision area
	/// adds to the list of colliding creatures.
	/// </summary>
	/// <param name="info">collision info</param>
	protected virtual void OnCollisionEnter(Collision info){
		if(!(m_attacker.IsMine) || !m_initialised)
			return;
		
		if(info.collider.tag.Equals("Creature"))
			m_collidingCreatures[info.gameObject.GetComponent<Creature>()] = 0;
	}
	
	/// <summary>
	/// called automatically when another gameobject leaves its collision area
	/// removes from the list of colliding creatures.
	/// </summary>
	/// <param name="info">collision info</param>
	protected virtual void OnCollisionExit(Collision info){
		if(!(m_attacker.IsMine) || !m_initialised)
			return;
		
		if(info.collider.tag.Equals("Creature"))
			m_collidingCreatures.Remove(info.gameObject.GetComponent<Creature>());
	}
	
	/// <summary>
	/// return list of all OTHER creatures that are colliding
	/// </summary>
	/// <returns>The other creatures list.</returns>
	protected List<Creature> collidingOtherCreaturesList(){
		List<Creature> creaturesToModify = new List<Creature>();
		foreach(Creature creature in m_collidingCreatures.Keys)
			if(creature != m_attacker)	// exclude caster
				creaturesToModify.Add(creature);
		return creaturesToModify;
	}
}
