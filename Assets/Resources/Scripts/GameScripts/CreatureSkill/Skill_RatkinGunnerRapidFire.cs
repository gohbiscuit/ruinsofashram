﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_RatkinGunnerRapidFire : Skill {

	// inherit base constructor
	public Skill_RatkinGunnerRapidFire(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                   bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                   bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------

	const string atkAnimation = "RGunner_Fire_Belt";
	float nextProjectileTime = 0;
	int numProjectilesCreated = 0;

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.animation[atkAnimation].speed = 5.0f;

		if(Time.time > nextProjectileTime){
			m_attacker.setSkillAnimation(atkAnimation);
			createRangedSkillObject("SkillObject_RatkinGunnerRapidFire", target);
			//AudioManager.Instance.PlayOnce("RatkinGunner/54_caitlyn_piltoverpeacemaker_om", m_attacker, 0.4f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Gunner/(Rapid Fire) GunFire03", m_attacker, 0.5f);
			nextProjectileTime = Time.time + 0.2f;	// 0.5s per fire
			numProjectilesCreated++;
		}

		if(m_attacker.animation.IsPlaying(atkAnimation) && m_attacker.animation[atkAnimation].normalizedTime >= 1){
			m_attacker.setSkillAnimation("RGunner_Combat_Mode_Standing");
			if(numProjectilesCreated >= 5 + 1*(m_attacker.getStats().SKI/2))	// projectiles +1 per 2 SKI
				return true;
		}

		return false;
	}
	
	public override void stopActivate(){
		StartCooldown();
		nextProjectileTime = 0;
		numProjectilesCreated = 0;
	}
	
}
