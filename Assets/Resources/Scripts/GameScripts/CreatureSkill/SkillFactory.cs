﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MathParserTK;

public class SkillFactory : MonoBehaviour
{
	
    private static SkillFactory instance;
	Dictionary<string, List<object[]>> m_parametersList;
	Dictionary<int, string> m_damageFormulaList;

	public static SkillFactory Instance
	{
		get {
			return instance;
		}
	}

	public void Awake(){
		if (instance == null)
		{
			instance = this;
			
			// load all data on skills
			m_parametersList = new Dictionary<string, List<object[]>>();
			m_damageFormulaList = new Dictionary<int, string>();
			Utility.ReadSkillCSV(m_parametersList, m_damageFormulaList);
		}
		else if (instance != this)
			Destroy(this);
	}

	/// <summary>
	/// Retrieve skill of the given creature
	/// </summary>
	/// <returns>The creature skills.</returns>
	/// <param name="CRace">Creature Race.</param>
	/// <param name="creatureClass">Creature Class.</param>
	public List<Skill> getCreatureSkills(CreatureRace creatureRace, 
	                                     CreatureClass creatureClass){

		string creatureName = creatureRace + "_" + creatureClass;
		if(!m_parametersList.ContainsKey(creatureName)){
			throw new ArgumentException("Invalid creature or no skill for the creature");
			return null;
		}

		List<object[]> creatureSkillParameters = m_parametersList[creatureName];

		List<Skill> creatureSkillList = new List<Skill>();
		Skill newSkill = null;
		foreach(object[] para in creatureSkillParameters){
			int id = (int)(para[0]);

			switch(id){		// please suggest if you can think of a better way to do this, all arguments are the same

			case 0: newSkill = new Skill_WispPossess(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 1: newSkill = new Skill_Release(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 2: newSkill = new Skill_WispTeleport(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;

			// RATKIN_WARRIOR
			case 3: newSkill = new Skill_RatkinWarriorBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 4: newSkill = new Skill_RatkinWarriorThunderClap(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 5: newSkill = new Skill_RatkinWarriorDeadlyBlow(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 6: newSkill = new Skill_RatkinWarriorShieldBarrier(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			
			// RATKIN_GUNNER
			case 7: newSkill = new Skill_RatkinGunnerBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 8: newSkill = new Skill_RatkinGunnerRapidFire(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 9: newSkill = new Skill_RatkinGunnerGunnerAssassination(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			case 10: newSkill = new Skill_RatkinGunnerRatkinEye(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;

			// RATKIN_MAGE
			case 11: newSkill = new Skill_RatkinMageBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 12: newSkill = new Skill_RatkinMageArcaneBlast(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 13: newSkill = new Skill_RatkinMageImpactWave(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			case 14: newSkill = new Skill_RatkinMageFreezingShield(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;

			// RATKIN_SHAMAN
			case 15: newSkill = new Skill_RatkinShamanBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 16: newSkill = new Skill_RatkinShamanDivineHeal(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 17: newSkill = new Skill_RatkinShamanChainLightning(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			case 18: newSkill = new Skill_RatkinShamanEarthShield(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			
			// OGRE_WARRIOR
			case 19: newSkill = new Skill_OgreWarriorBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 20: newSkill = new Skill_OgreWarriorEarthQuake(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 21: newSkill = new Skill_OgreWarriorSkullCrusher(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			case 22: newSkill = new Skill_OgreWarriorBattleRoar(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
		
			// OGRE_ROGUE
			case 23: newSkill = new Skill_OgreRogueBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 24: newSkill = new Skill_OgreRogueAssassinLock(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 25: newSkill = new Skill_OgreRogueAssassinStrike(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			case 26: newSkill = new Skill_OgreRogueBackstab(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			
			// OGRE_MAGE
			case 27: newSkill = new Skill_OgreMageBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 28: newSkill = new Skill_OgreMageFireball(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 29: newSkill = new Skill_OgreMageInfernalScorch(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			case 30: newSkill = new Skill_OgreMageBlink(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;

			// OGRE_PRIEST
			case 31: newSkill = new Skill_OgrePriestBasicAttack(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 32: newSkill = new Skill_OgrePriestHolyLight(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;
			case 33: newSkill = new Skill_OgrePriestManaShield(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;			
			case 34: newSkill = new Skill_OgrePriestOgreAura(id, (string)para[1], (string)para[3], (string)para[4], (int)para[2], (bool)para[5], (float)para[6], (float)para[7], (float)para[8], (float)para[9], (float)para[10], (float)para[11], (bool)para[12], (bool)para[13], (bool)para[14], (bool)para[15], (bool)para[16]); break;

			default:
				throw new ArgumentException("Invalid skill ID");
				newSkill = null;
				break;
			}

			creatureSkillList.Add(newSkill);
		}
		
		return creatureSkillList;
    }

	/// <summary>
	/// Computes the damage to deal. Returns -1 if evaded.
	/// </summary>
	/// <returns>The damage.</returns>
	/// <param name="skillID">Skill used</param>
	/// <param name="attacker">Attacker of the skill</param>
	/// <param name="target">Target of the skill</param>
	public int getDamage(int skillID, Creature attacker, Creature target){

		// GAME DESIGN NOTE
		// calculate if target can evade the attack
		if(UnityEngine.Random.Range(0, 100) < target.getStats().EVA/3){
			// evaded!
			return -1;
		}

		return getFormulaOutput(skillID, attacker, target);
	}

	/// <summary>
	/// Gets the amount of HP healed.
	/// </summary>
	/// <returns>The healed amt.</returns>
	/// <param name="skillID">Skill used</param>
	/// <param name="attacker">Healer</param>
	/// <param name="target">Target of the skill</param>
	public int getHealedAmt(int skillID, Creature healer, Creature target){
		return getFormulaOutput(skillID, healer, target);
	}

	/// <summary>
	/// Get the value of the formula after factoring in target's stats and randomness
	/// </summary>
	/// <returns>The formula output.</returns>
	/// <param name="skillID">ID of skill</param>
	/// <param name="attacker">Attacker of skill</param>
	/// <param name="target">Target of skill</param>
	int getFormulaOutput(int skillID, Creature attacker, Creature target){

		// Find and compute a random value for RANGE(x,y)
		string formulaStr = m_damageFormulaList[skillID];
		int rangeIdx = formulaStr.IndexOf("RANGE(");
		while(rangeIdx != -1){
			
			int firstIdx = rangeIdx + 6;
			int nextIdx = formulaStr.IndexOf(",");
			double val1 = double.Parse(formulaStr.Substring(firstIdx, nextIdx-firstIdx));
			
			firstIdx = nextIdx + 1;
			nextIdx = formulaStr.IndexOf(")");
			double val2 = double.Parse(formulaStr.Substring(firstIdx, nextIdx-firstIdx));
			
			formulaStr = formulaStr.Substring(0, rangeIdx) + 
				(UnityEngine.Random.Range((float)val1, (float)val2)).ToString() +
					formulaStr.Substring(nextIdx+1);
			
			rangeIdx = formulaStr.IndexOf("RANGE(");
		}
		
		return getFormulaResultWithoutRange(attacker, target, formulaStr);
	}

	/// <summary>
	/// Gets the base damage range (not affected by target's stats)
	/// at the given skill mastery level
	/// </summary>
	public void getBaseDamage(int skillID, Creature attacker, int skillMastery, out int dmgMin, out int dmgMax){

		// COMPUTE MIN
		// Find and compute a random value for RANGE(x,y)
		string formulaStrMin = m_damageFormulaList[skillID];
		int rangeIdx = formulaStrMin.IndexOf("RANGE(");
		while(rangeIdx != -1){
			
			int firstIdx = rangeIdx + 6;
			int nextIdx = formulaStrMin.IndexOf(",");
			double val1 = double.Parse(formulaStrMin.Substring(firstIdx, nextIdx-firstIdx));
			
			firstIdx = nextIdx + 1;
			nextIdx = formulaStrMin.IndexOf(")");
			
			formulaStrMin = formulaStrMin.Substring(0, rangeIdx) + 
				((float)val1).ToString() + formulaStrMin.Substring(nextIdx+1);
			
			rangeIdx = formulaStrMin.IndexOf("RANGE(");
		}

		dmgMin = getFormulaResultWithoutRange(attacker, formulaStrMin, skillMastery);

		// COMPUTE MAX
		// Find and compute a random value for RANGE(x,y)
		string formulaStrMax = m_damageFormulaList[skillID];
		rangeIdx = formulaStrMax.IndexOf("RANGE(");
		while(rangeIdx != -1){
			
			int firstIdx = rangeIdx + 6;
			int nextIdx = formulaStrMax.IndexOf(",");
			
			firstIdx = nextIdx + 1;
			nextIdx = formulaStrMax.IndexOf(")");
			double val2 = double.Parse(formulaStrMax.Substring(firstIdx, nextIdx-firstIdx));
			
			formulaStrMax = formulaStrMax.Substring(0, rangeIdx) + 
				((float)val2).ToString() + formulaStrMax.Substring(nextIdx+1);
			
			rangeIdx = formulaStrMax.IndexOf("RANGE(");
		}
		
		dmgMax = getFormulaResultWithoutRange(attacker, formulaStrMax, skillMastery);
	}
	
	/// <summary>
	/// returns the value of the formula
	/// prereq : formula must not contain RANGE
	/// </summary>
	/// <returns>The formula result without range.</returns>
	/// <param name="attacker">Attacker.</param>
	/// <param name="target">Target.</param>
	/// <param name="formulaStr">Formula string.</param>
	int getFormulaResultWithoutRange(Creature attacker, Creature target, string formulaStr){

		StringBuilder formula = new StringBuilder(formulaStr);
		
		// Replace variables with target creature's stats
		CreatureStats stats = target.getStats();
		formula.Replace("#HPR", stats.HP_REGEN.ToString());
		formula.Replace("#MPR", stats.MP_REGEN.ToString());
		formula.Replace("#HP", stats.HP.ToString());
		formula.Replace("#MP", stats.MP.ToString());
		formula.Replace("#PA", stats.PHY_ATK.ToString());
		formula.Replace("#MA", stats.MAG_ATK.ToString());
		formula.Replace("#PD", stats.PHY_DEF.ToString());
		formula.Replace("#MD", stats.MAG_DEF.ToString());
		formula.Replace("#EVA", stats.EVA.ToString());
		formula.Replace("#MOV", stats.MOV.ToString());
		formula.Replace("#SKI", stats.SKI.ToString());
		
		// Replace variables with attacker creature's stats
		stats = attacker.getStats();
		formula.Replace("HPR", stats.HP_REGEN.ToString());
		formula.Replace("MPR", stats.MP_REGEN.ToString());
		formula.Replace("HP", stats.HP.ToString());
		formula.Replace("MP", stats.MP.ToString());
		formula.Replace("PA", stats.PHY_ATK.ToString());
		formula.Replace("MA", stats.MAG_ATK.ToString());
		formula.Replace("PD", stats.PHY_DEF.ToString());
		formula.Replace("MD", stats.MAG_DEF.ToString());
		formula.Replace("EVA", stats.EVA.ToString());
		formula.Replace("MOV", stats.MOV.ToString());
		formula.Replace("SKI", stats.SKI.ToString());
		
		// Replace all occurences of "+-" which causes problem
		formula.Replace("+-", "-");
		//Debug.Log("FORMULA!!!: "+formula.ToString());
		
		// Evaluate final damage value
		MathParser mathParser = new MathParser();
		double value = mathParser.Parse(formula.ToString(), false);
		
		return Mathf.Max(0, (int)value);
	}
	
	/// <summary>
	/// returns the value of the formula (with no target stats) and with the given skill mastery
	/// prereq : formula must not contain RANGE
	/// </summary>
	/// <returns>The formula result without range.</returns>
	/// <param name="attacker">Attacker.</param>
	/// <param name="formulaStr">Formula string.</param>
	/// <param name="skillMastery">Skill mastery.</param>
	int getFormulaResultWithoutRange(Creature attacker, string formulaStr, int skillMastery){
		
		StringBuilder formula = new StringBuilder(formulaStr);
		
		// Replace target variables with 0
		formula.Replace("#HPR", "0");
		formula.Replace("#MPR", "0");
		formula.Replace("#HP", "0");
		formula.Replace("#MP", "0");
		formula.Replace("#PA", "0");
		formula.Replace("#MA", "0");
		formula.Replace("#PD", "0");
		formula.Replace("#MD", "0");
		formula.Replace("#EVA", "0");
		formula.Replace("#MOV", "0");
		formula.Replace("#SKI", "0");
		
		// Replace variables with attacker creature's stats
		CreatureStats stats = attacker.getStats();
		formula.Replace("HPR", stats.HP_REGEN.ToString());
		formula.Replace("MPR", stats.MP_REGEN.ToString());
		formula.Replace("HP", stats.HP.ToString());
		formula.Replace("MP", stats.MP.ToString());
		formula.Replace("PA", stats.PHY_ATK.ToString());
		formula.Replace("MA", stats.MAG_ATK.ToString());
		formula.Replace("PD", stats.PHY_DEF.ToString());
		formula.Replace("MD", stats.MAG_DEF.ToString());
		formula.Replace("EVA", stats.EVA.ToString());
		formula.Replace("MOV", stats.MOV.ToString());
		formula.Replace("SKI", skillMastery.ToString());
		
		// Replace all occurences of "+-" which causes problem
		formula.Replace("+-", "-");
		//Debug.Log(formula.ToString());
		
		// Evaluate final damage value
		MathParser mathParser = new MathParser();
		double value = mathParser.Parse(formula.ToString(), false);
		
		return Mathf.Max(0, (int)value);
	}

}