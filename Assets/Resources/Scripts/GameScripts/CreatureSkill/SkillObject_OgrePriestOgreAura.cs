﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillObject_OgrePriestOgreAura : SkillObject {

	void Update () {
		if(!m_initialised)
			return;
	}
	
	void FixedUpdate(){
		if(!m_initialised)
			return;

		float intensityToSend = m_attacker.getStats().SKI;

		List<Creature> toChange = new List<Creature>();
		List<Creature> removeList = new List<Creature>();
		foreach(Creature collidingCreature in m_collidingCreatures.Keys){

			if(collidingCreature == null)
				continue;

			// ally turned enemy will be removed (when possessing a creature inside sphere)
			if(!collidingCreature.areAllies(m_attacker)){
				removeList.Add(collidingCreature);
				continue;
			}

			float intensitySent = (float)(m_collidingCreatures[collidingCreature]);
			if(intensitySent != intensityToSend){
				// remove a previous intensity that was sent if there was one sent
				if(intensitySent != -1.0f)
					removeStatusEffectsLater(collidingCreature, StatusEffect.ORGE_AURA, intensitySent);

				// add a new intensity to the creature
				afflictStatusEffectsLater(collidingCreature, StatusEffect.ORGE_AURA, 0, intensityToSend);

				toChange.Add(collidingCreature);
			}
		}

		// remove ally turned enemy and restoring its status
		foreach(Creature creatureToRemove in removeList){
			float intensitySent = (float)(m_collidingCreatures[creatureToRemove]);
			if(intensitySent != -1.0f)
				removeStatusEffectsLater(creatureToRemove, StatusEffect.ORGE_AURA, ((float)(m_collidingCreatures[creatureToRemove])));
			else
				Debug.Log("You are trying to remove ogre aura from a creature that doesn't have it");
			m_collidingCreatures.Remove(creatureToRemove);
		}

		// refresh changed intensities sent
		foreach(Creature creatureToChange in toChange)
			m_collidingCreatures[creatureToChange] = intensityToSend;

		// if m_attacker has died, restore all status
		if(m_attacker == null){
			removeAllOgreAura();
			Remove();
			return;
		}
	}

	void LateUpdate(){
		if(m_attacker == null){
			removeAllOgreAura();
			Remove();
			return;
		}
		transform.position = m_attacker.transform.position;
	}

	void removeAllOgreAura(){
		foreach(Creature collidingCreature in m_collidingCreatures.Keys){
			if(collidingCreature != null && collidingCreature.areAllies(m_attacker))
				removeOgreAura(collidingCreature);
		}
	}

	void removeOgreAura(Creature target){
		int intensitySent = m_attacker.getStats().SKI;
		removeStatusEffectsLater(target, StatusEffect.ORGE_AURA, intensitySent);
	}

	//int intensitySent = 0;
	protected override void OnCollisionEnter(Collision info){
		if(!(m_attacker.IsMine) || !m_initialised)
			return;

		// if creature comes into the sphere, afflict it with the "ogre aura" status effects
		// based on the skill level
		if(info.collider.tag.Equals("Creature")){
			Creature creatureEntering = info.gameObject.GetComponent<Creature>();
			if(creatureEntering.areAllies(m_attacker))	// only consider allies
				m_collidingCreatures[info.gameObject.GetComponent<Creature>()] = -1.0f;
		}
	}
	
	protected override void OnCollisionExit(Collision info){
		if(!(m_attacker.IsMine) || !m_initialised)
			return;

		// if creature moves away, remove the status effect by simply setting its intensity to nothing
		if(info.collider.tag.Equals("Creature")){

			Creature exitingCreature = info.gameObject.GetComponent<Creature>();
			m_collidingCreatures.Remove(exitingCreature);

			if(exitingCreature.areAllies(m_attacker))
				removeOgreAura(exitingCreature);
		}
	}

}
