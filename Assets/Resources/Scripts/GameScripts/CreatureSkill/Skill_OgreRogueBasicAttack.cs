﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_OgreRogueBasicAttack : Skill {

	List<string> sounds;
	List<string> sounds2;

	// inherit base constructor
	public Skill_OgreRogueBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                  bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                  bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		/*sounds.Add("OgreRogue/13_akali_basicattack_oc_1");
		sounds.Add("OgreRogue/14_akali_basicattack_oc_2");
		sounds.Add("OgreRogue/15_akali_basicattack_oc_3");*/

		sounds.Add ("Ashram/Ogre Rogue/Basic Attack/m1hSwordHitFlesh1a");
		sounds.Add ("Ashram/Ogre Rogue/Basic Attack/m1hSwordHitFlesh1b");
		sounds.Add ("Ashram/Ogre Rogue/Basic Attack/m1hSwordHitFlesh1c");
		//sounds.Add ("Ashram/Ogre Rogue/Basic Attack/m1hSwordHitFleshCrit");
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	bool swingSound = false;
	const string atkAnimation = "Attack01";
	const string atkAnimation2 = "Attack02";
	string currAnimation;

	// return true when the skill finishes
	public override bool Activate(object target){

		// randomise animation
		if(currAnimation == null){
			if(Random.Range(0,2) == 0){
				m_attacker.setSkillAnimation(atkAnimation, 1.5f);
				currAnimation = atkAnimation;
			}else{
				m_attacker.setSkillAnimation(atkAnimation2, 1.5f);
				currAnimation = atkAnimation2;
			}
		}
		
		if(!createdObject && hasReachedTime(m_attacker.animation[currAnimation], 0.3f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.3f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_OgreRogueBasicAttack", target);
		}
		
		if(m_attacker.animation[currAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
		currAnimation = null;
	}
	
}
