﻿using UnityEngine;
using System.Collections;

// possess skill is a special case which doesn't spawn a skill object
public class Skill_WispPossess : Skill {
	
	// inherit base constructor
	public Skill_WispPossess(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------

	bool initPossess = false;

	// return true when the skill finishes
	public override bool Activate(object target){
		if(!initPossess){
			AudioManager.Instance.PlayOnce("Creature/14_ironforgexerath_arcanopulse_o", m_attacker, 1.0f);
			Creator.Instance.PossessCreatureLater(m_attacker.gameObject, ((Creature)(target)).gameObject);
			initPossess = true;
		}
		return false;
	}
	
	public override void stopActivate(){
		StartCooldown();
	}
}
