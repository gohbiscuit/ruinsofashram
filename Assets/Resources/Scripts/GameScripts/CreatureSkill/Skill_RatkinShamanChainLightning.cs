﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_RatkinShamanChainLightning : Skill {

	// inherit base constructor
	public Skill_RatkinShamanChainLightning(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                        bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                        bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "SH_magic_shot_stright";

	List<Creature> targetsHit;

	// return true when the skill finishes
	public override bool Activate(object target){
		m_attacker.setSkillAnimation(atkAnimation, 1.5f);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.4f)){
			createdObject = true;

			Creature creature = ((Creature)(target));

			AudioManager.Instance.PlayOnce("RatkinShaman/43_kennen_lightningrush_oh_5", m_attacker, 0.6f);
			AudioManager.Instance.PlayOnce("RatkinShaman/45_volibear_rollingthunder_oc_02", m_attacker, 1.0f);

			//AudioManager.Instance.PlayOnce("Ashram/Spell Cast/NatureCast", m_attacker.transform, 1.0f);
	
			// instantiation of lightning strike on 'chained' target
			GameObject skillObject = (GameObject)(Resources.Load("Prefab/SkillObject_RatkinShamanChainLightning"));
			GameObject instantiatedSkillObject = (GameObject)(GameObject.Instantiate(skillObject, creature.transform.position, creature.transform.rotation));
			
			SkillObject_RatkinShamanChainLightning obj = instantiatedSkillObject.GetComponent<SkillObject_RatkinShamanChainLightning>();
			obj.initialise(m_skillTypeID, m_attacker, creature);

			targetsHit = new List<Creature>();
			targetsHit.Add(m_attacker);
			targetsHit.Add(creature);
			obj.setupChainLightning(targetsHit, 1.0f);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		// cooldown reduction per SKI
		StartCooldown();
		createdObject = false;
		targetsHit = null;
	}
	
}
