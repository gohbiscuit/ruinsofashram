using UnityEngine;
using System.Collections;

public class Skill_RatkinMageFreezingShield : Skill {

	// inherit base constructor
	public Skill_RatkinMageFreezingShield(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                      bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                      bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Ratking_W_Jump_standing";

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.6f)){
			//AudioManager.Instance.PlayOnce("RatkinMage/18_glacialmalphite_seismicshard_", m_attacker, 1.0f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Mage/(Freezing Shield) ChainsOfIceImpact", m_attacker, 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_RatkinMageFreezingShield", m_attacker);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
