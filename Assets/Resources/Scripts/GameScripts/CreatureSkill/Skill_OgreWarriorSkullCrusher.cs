﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_OgreWarriorSkullCrusher : Skill {

	List<string> sounds;

	// inherit base constructor
	public Skill_OgreWarriorSkullCrusher(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		sounds.Add("OgreWarrior/39_gragas_explosivecask_boom_1");
		sounds.Add("OgreWarrior/40_gragas_explosivecask_boom_2");
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Attack01";

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation, 0.75f);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.35f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.4f);
			//AudioManager.Instance.PlayOnce("Ashram/Ogre Warrior/(Skull Crusher) MaimImpact", m_attacker, 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_OgreWarriorSkullCrusher", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
