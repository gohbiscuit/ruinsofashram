﻿using UnityEngine;
using System.Collections;

public class SkillObject_OgreRogueAssassinLock : SkillObject {

	const float BACK_STAB_ANGLE = 0.3f;
	const float BACK_STAB_MULTIPLIER_BASE = 1.1f;
	const float BACK_STAB_MULTIPLIER_PER_SKI = 0.1f;
	const float ASSASSIN_LOCK_DURATION = 5.0f;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			if(Vector3.Dot(m_attacker.transform.forward,
			               ((Creature)(m_target)).transform.forward) > BACK_STAB_ANGLE) 
			{	// attacker and target is facing approx. same direction (backfacing)
				dealDamageLater(BACK_STAB_MULTIPLIER_BASE + BACK_STAB_MULTIPLIER_PER_SKI * m_attacker.getStats().SKI);	// backstab effect
				Debug.Log("crit");
			} else
				dealDamageLater();
			afflictStatusEffectsLater((Creature)(m_target), StatusEffect.ASSASSIN_LOCK, ASSASSIN_LOCK_DURATION, 0);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
