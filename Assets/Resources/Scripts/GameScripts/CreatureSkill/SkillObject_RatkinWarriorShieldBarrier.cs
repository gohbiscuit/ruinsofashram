﻿using UnityEngine;
using System.Collections;

public class SkillObject_RatkinWarriorShieldBarrier : SkillObject {

	const float SHIELD_BARRIER_DURATION = 30;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			afflictStatusEffectsLater((Creature)(m_target), StatusEffect.SHIELD_BARRIER, 
			                          SHIELD_BARRIER_DURATION, m_attacker.getStats().SKI);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
