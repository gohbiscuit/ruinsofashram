﻿using UnityEngine;
using System.Collections;

public class Skill_RatkinGunnerGunnerAssassination : Skill {

	// inherit base constructor
	public Skill_RatkinGunnerGunnerAssassination(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                             bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                             bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "RGunner_Fire_standing";

	// animation for channelling/charging a skill
	public override string getChargeAnimation(){
		return "RGunner_warning";
	}
	
	// return true when the skill finishes
	public override bool Activate(object target){
		
		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.4f)){
			//AudioManager.Instance.PlayOnce("RatkinGunner/06_caitlyn_aceinthehole_oh_03", m_attacker, 0.6f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Gunner/(Assasination 02) MortarTeamAttack1", m_attacker, 0.6f);
			createdObject = true;
			createRangedSkillObject("SkillObject_RatkinGunnerGunnerAssassination", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1)
			return false;

		return true;
	}

	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
