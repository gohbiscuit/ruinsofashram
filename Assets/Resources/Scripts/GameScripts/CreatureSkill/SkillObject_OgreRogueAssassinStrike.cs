﻿using UnityEngine;
using System.Collections;

public class SkillObject_OgreRogueAssassinStrike : SkillObject {

	const float BACK_STAB_ANGLE = 0.3f;
	const float BACK_STAB_MULTIPLIER_BASE = 1.1f;
	const float BACK_STAB_MULTIPLIER_PER_SKI = 0.1f;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	int m_attacked = 0;
	int m_attackCounter = 0;
	void FixedUpdate(){
		if(!m_initialised)
			return;

		if(m_attacker != null && (Creature)(m_target) != null){
			if(m_attacked < 3 && m_attackCounter%5==0){
				AudioManager.Instance.PlayOnce ("Ashram/Ogre Rogue/(Assasin Strike) 04_wolfman_vs_flesh_4", m_attacker, 1.0f);
				m_attacked++;

				if(Vector3.Dot(m_attacker.transform.forward,
				               ((Creature)(m_target)).transform.forward) > BACK_STAB_ANGLE) {	// attacker and target is facing approx. same direction (backfacing)
					dealDamageLater(BACK_STAB_MULTIPLIER_BASE + BACK_STAB_MULTIPLIER_PER_SKI*m_attacker.getStats().SKI);	// backstab effect
					Debug.Log("crit");
				} else
					dealDamageLater();
			}
		}

		m_attackCounter++;
		
		if(transform.childCount == 0)
			Remove ();
	}
}
