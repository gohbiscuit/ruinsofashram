﻿using UnityEngine;
using System.Collections;

// release skill is a special case which doesn't spawn a skill object
public class Skill_WispTeleport : Skill {
	
	// inherit base constructor
	public Skill_WispTeleport(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){}
	
	// ------------------------------------------------------------------------------------------------
	
	bool initBlink = false;
	
	// return true when the skill finishes
	public override bool Activate(object target){
		if(!initBlink){

			// before teleport FX
			GameObject.Instantiate(Resources.Load("Prefab/Teleport"), m_attacker.transform.position, Quaternion.identity);

			// just make it teleport over
			m_attacker.getAgent().Disable();
			m_attacker.transform.position = (Vector3)(target);
			m_attacker.getAgent().Enable();

			AudioManager.Instance.PlayOnce("Ashram/Wisp/Teleport Skill", m_attacker, 0.7f);

			// after teleport FX
			GameObject.Instantiate(Resources.Load("Prefab/Teleport"), m_attacker.transform.position, Quaternion.identity);

			initBlink = true;
		}
		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		initBlink = false;
	}
}
