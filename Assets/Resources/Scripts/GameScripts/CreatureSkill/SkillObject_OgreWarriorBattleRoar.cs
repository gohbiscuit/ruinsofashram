﻿using UnityEngine;
using System.Collections;

public class SkillObject_OgreWarriorBattleRoar : SkillObject {

	const float BATTLE_ROAR_DURATION = 20.0f;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			afflictStatusEffectsLater(m_attacker, StatusEffect.BATTLE_ROAR, 
			                          BATTLE_ROAR_DURATION, m_attacker.getStats().SKI);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
