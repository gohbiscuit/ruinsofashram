﻿using UnityEngine;
using System.Collections;

// possess skill is a special case which doesn't spawn a skill object
public class Skill_OgrePriestOgreAura : Skill {
	
	// inherit base constructor
	public Skill_OgrePriestOgreAura(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	
	// passive skill - activates once
	public override bool Activate(object target){
		GameObject skillObject = (GameObject)(Resources.Load("Prefab/SkillObject_OgrePriestOgreAura"));
		GameObject instantiatedSkillObject = (GameObject)(GameObject.Instantiate(skillObject, m_attacker.transform.position, m_attacker.transform.rotation));
		instantiatedSkillObject.GetComponent<SkillObject>().initialise(m_skillTypeID, m_attacker, target);
		return true;
	}
	
	public override void stopActivate(){
	}
}
