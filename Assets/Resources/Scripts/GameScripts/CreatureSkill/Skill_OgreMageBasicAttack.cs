﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_OgreMageBasicAttack : Skill {

	List<string> sounds;
	List<string> sounds2;

	// inherit base constructor
	public Skill_OgreMageBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                 bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                 bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		/*sounds = new List<string>();
		sounds.Add("OgreMage/01_ziggs_basicattack_oc_01");
		sounds.Add("OgreMage/02_ziggs_basicattack_oc_02");
		sounds.Add("OgreMage/03_ziggs_basicattack_oc_03");
		sounds.Add("OgreMage/04_ziggs_basicattack_oc_04");
		
		sounds2 = new List<string>();
		sounds2.Add("OgreMage/05_ziggs_basicattack_oh_01");
		sounds2.Add("OgreMage/06_ziggs_basicattack_oh_02");
		sounds2.Add("OgreMage/07_ziggs_basicattack_oh_03");
		sounds2.Add("OgreMage/08_ziggs_basicattack_oh_04");
		sounds2.Add("OgreMage/09_ziggs_basicattack_oh_05");
		sounds2.Add("OgreMage/10_ziggs_basicattack_oh_06");*/

		string path = "Ashram/Ogre Mage/Basic Attack/";

		// Ashram
		sounds = new List<string>();
		sounds.Add("OgreMage/01_ziggs_basicattack_oc_01");
		sounds.Add("OgreMage/02_ziggs_basicattack_oc_02");
		sounds.Add("OgreMage/03_ziggs_basicattack_oc_03");
		sounds.Add("OgreMage/04_ziggs_basicattack_oc_04");

		
		sounds2 = new List<string>();
		sounds2.Add (path + "UnarmedAttackLargeA");
		sounds2.Add (path + "UnarmedAttackLargeB");
		sounds2.Add (path + "UnarmedAttackLargeC");
		sounds2.Add (path + "UnarmedAttackMediumA");
		sounds2.Add (path + "UnarmedAttackMediumB");
		sounds2.Add (path + "UnarmedAttackMediumC");
		sounds2.Add (path + "UnarmedAttackSmallA");
		sounds2.Add (path + "UnarmedAttackSmallB");
		sounds2.Add (path + "UnarmedAttackSmallC");
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	bool swingSound = false;
	const string atkAnimation = "Attack01";
	const string atkAnimation2 = "Attack02";
	string currAnimation;

	// return true when the skill finishes
	public override bool Activate(object target){

		// randomise animation
		if(currAnimation == null){
			if(Random.Range(0,2) == 0){
				m_attacker.setSkillAnimation(atkAnimation, 1.5f);
				currAnimation = atkAnimation;
			}else{
				m_attacker.setSkillAnimation(atkAnimation2, 1.5f);
				currAnimation = atkAnimation2;
			}
		}

		if(!swingSound && hasReachedTime(m_attacker.animation[currAnimation], 0.1f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.2f);
			swingSound = true;
		}
		
		if(!createdObject && hasReachedTime(m_attacker.animation[currAnimation], 0.3f)){
			AudioManager.Instance.PlayOnce(sounds2[Random.Range(0, sounds2.Count)], m_attacker, 0.3f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_GenericMelee", target);
		}
		
		if(m_attacker.animation[currAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
		swingSound = false;
		currAnimation = null;
	}
	
}
