using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_RatkinGunnerBasicAttack : Skill {

	List<string> sounds;

	// inherit base constructor
	public Skill_RatkinGunnerBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		//sounds = new List<string>();
		/*sounds.Add("RatkinGunner/20_caitlyn_basicattack_omc_01");
		sounds.Add("RatkinGunner/21_caitlyn_basicattack_omc_02");
		sounds.Add("RatkinGunner/22_caitlyn_basicattack_omc_03");
		sounds.Add("RatkinGunner/23_caitlyn_basicattack_omc_04");*/

		//sounds.Add("Ashram/Ratkin Gunner/Basic Attack/GunFire01");
		//sounds.Add("Ashram/Ratkin Gunner/Basic Attack/GunFire02");

	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "RGunner_Fire_standing";
	
	// return true when the skill finishes
	public override bool Activate(object target){
		
		m_attacker.setSkillAnimation(atkAnimation, 3.0f);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.4f)){
			//AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.4f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Gunner/Basic Attack/GunFire02", m_attacker, 0.4f);
			createdObject = true;
			createRangedSkillObject("SkillObject_RatkinGunnerBasicAttack", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1)
			return false;

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
