﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillObject_RatkinMageImpactWave : SkillObject {

	void Update () {
		if(!m_initialised)
			return;
	}
	
	void FixedUpdate(){
		if(!m_initialised)
			return;
		
		List<Creature> creaturesToModify = collidingOtherCreaturesList();
		foreach(Creature creature in creaturesToModify){
			if(creature.areAllies(m_attacker))
				continue;

			if(m_collidingCreatures.Contains(creature) && m_collidingCreatures[creature] != null){
				int val = (int)(m_collidingCreatures[creature]);
				
				if(val == 1)
					dealDamageLater(creature);
				
				m_collidingCreatures[creature] = val + 1;
			}
		}

		if(transform.childCount == 0)
			Remove ();
	}

}
