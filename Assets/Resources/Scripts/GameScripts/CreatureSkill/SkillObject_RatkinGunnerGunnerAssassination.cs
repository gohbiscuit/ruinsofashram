﻿using UnityEngine;
using System.Collections;

public class SkillObject_RatkinGunnerGunnerAssassination : SkillObject {

	const float PROJECTILE_SPEED = 0.8f;
	const float DISTANCE_CLOSE = 3.0f;

	void Update () {
		if(!m_initialised)
			return;
	}

	bool dealtDamage = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;

		Creature targetCreature = (Creature)m_target;
		if(targetCreature == null){
			Remove();
			return;
		}

		bool toDestroy = false;
		bool toDealDamage = dealtDamage;
		float speed = PROJECTILE_SPEED;
		Transform src = transform;
		
		Vector3 fromToVector = (targetCreature.transform.position - src.position);
		
		// damage is sent first
		if(!toDealDamage){
			float distLeft = Vector3.Distance(Vector3.zero, fromToVector);
			float updatesLeft = Mathf.Ceil(distLeft/speed);
			if(updatesLeft * Time.fixedDeltaTime < GameNetworkManager.Instance.getTotalLag())
				toDealDamage = true;
		}
		
		// check if should destroy
		if(Vector3.Distance(Vector3.zero, fromToVector) > speed + DISTANCE_CLOSE){
			src.position += fromToVector.normalized * speed;
			toDestroy = false;
		} else {
			src.position = targetCreature.transform.position;
			toDestroy = true;
		}
		
		if(!dealtDamage && toDealDamage){
			//AudioManager.Instance.PlayOnce("RatkinGunner/40_caitlyn_entrapment_oml_02", attacker, 0.3f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Gunner/(Assination 01) GunFire01", m_attacker, 0.5f);
			dealtDamage = true;
			dealDamageLater();
		}
		
		if(toDestroy)
			Remove();
	}
}
