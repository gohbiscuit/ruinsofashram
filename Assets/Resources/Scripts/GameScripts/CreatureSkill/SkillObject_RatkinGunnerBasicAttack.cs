﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillObject_RatkinGunnerBasicAttack : SkillObject {

	const float PROJECTILE_SPEED = 0.5f;
	const float DISTANCE_CLOSE = 3.0f;

	List<string> m_sounds;

	public override void initialise(int skillID, Creature attacker, System.Object target){
		base.initialise(skillID, attacker, target);

		m_sounds = new List<string>();
		m_sounds.Add("RatkinGunner/24_caitlyn_basicattack_oml_01");
		m_sounds.Add("RatkinGunner/25_caitlyn_basicattack_oml_02");
		m_sounds.Add("RatkinGunner/26_caitlyn_basicattack_oml_03");
		m_sounds.Add("RatkinGunner/27_caitlyn_basicattack_oml_04");
	}

	void Update () {
		if(!m_initialised)
			return;
	}

	bool m_dealtDamage = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;

		Creature targetCreature = (Creature)m_target;
		if(targetCreature == null){
			Remove();
			return;
		}

		bool toDestroy = false;
		bool toDealDamage = m_dealtDamage;
		float speed = PROJECTILE_SPEED;
		Transform src = transform;

		Vector3 fromToVector = (targetCreature.transform.position - src.position);
		
		// damage is sent first
		if(!toDealDamage){
			float distLeft = Vector3.Distance(Vector3.zero, fromToVector);
			float updatesLeft = Mathf.Ceil(distLeft/speed);
			if(updatesLeft * Time.fixedDeltaTime < GameNetworkManager.Instance.getTotalLag())
				toDealDamage = true;
		}
		
		// check if should destroy
		if(Vector3.Distance(Vector3.zero, fromToVector) > speed + DISTANCE_CLOSE){
			src.position += fromToVector.normalized * speed;
			toDestroy = false;
		} else {
			src.position = targetCreature.transform.position;
			toDestroy = true;
		}

		if(!m_dealtDamage && toDealDamage){
			AudioManager.Instance.PlayOnce(m_sounds[Random.Range(0, m_sounds.Count)], m_attacker, 0.3f);
			m_dealtDamage = true;
			dealDamageLater();
		}

		if(toDestroy)
			Remove();
	}
}
