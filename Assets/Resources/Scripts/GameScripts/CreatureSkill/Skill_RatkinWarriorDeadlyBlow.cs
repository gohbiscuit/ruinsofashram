using UnityEngine;
using System.Collections;

public class Skill_RatkinWarriorDeadlyBlow : Skill {

	// inherit base constructor
	public Skill_RatkinWarriorDeadlyBlow(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Ratkin_1H_360_Jump_swing";

	// return true when the skill finishes
	public override bool Activate(object target){
		m_attacker.setSkillAnimation(atkAnimation, 1.5f);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.6f)){
			//AudioManager.Instance.PlayOnce("RatkinWarrior/45_kennen_markofthestorm_stun_1", m_attacker, 1.0f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Warrior/(Deadly Blow) DeathCoilTarget", m_attacker, 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_RatkinWarriorDeadlyBlow", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
