﻿using UnityEngine;
using System.Collections;

// release skill is a special case which doesn't spawn a skill object
public class Skill_Release : Skill {
	
	// inherit base constructor
	public Skill_Release(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool initRelease = false;
	
	// return true when the skill finishes
	public override bool Activate(object target){
		if(!initRelease){
			AudioManager.Instance.PlayOnce("Creature/42_ironforgexerath_locusofpower_", m_attacker, 1.0f);
			Creator.Instance.releaseCreatureLater(m_attacker.gameObject);
			initRelease = true;
		}
		return false;
	}
	
	public override void stopActivate(){
		StartCooldown();
	}
}
