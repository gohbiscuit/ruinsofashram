﻿using UnityEngine;
using System.Collections;

public class SkillObject_RatkinShamanEarthShield : SkillObject {

	const float EARTH_SHIELD_DURATION = 30;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			afflictStatusEffectsLater((Creature)(m_target), StatusEffect.EARTH_SHIELD, 
			                          EARTH_SHIELD_DURATION, m_attacker.getStats().SKI);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
