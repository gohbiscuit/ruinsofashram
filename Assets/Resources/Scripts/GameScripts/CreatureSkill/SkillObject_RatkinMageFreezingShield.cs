﻿using UnityEngine;
using System.Collections;

public class SkillObject_RatkinMageFreezingShield : SkillObject {

	const float FREEZING_SHIELD_DURATION_BASE = 10;
	const float FREEZING_SHIELD_DURATION_PER_SKI = 2;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			afflictStatusEffectsLater(m_attacker, StatusEffect.FREEZING_SHIELD, 
			                          FREEZING_SHIELD_DURATION_BASE + FREEZING_SHIELD_DURATION_PER_SKI*m_attacker.getStats().SKI, 0);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
