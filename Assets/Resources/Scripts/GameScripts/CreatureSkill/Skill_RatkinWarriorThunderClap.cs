using UnityEngine;
using System.Collections;

public class Skill_RatkinWarriorThunderClap : Skill {

	// inherit base constructor
	public Skill_RatkinWarriorThunderClap(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                    bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                    bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "1H_Wide_Slash_Combo";

	// animation for channelling/charging a skill
	public override string getChargeAnimation(){
		return "Ratkin_Talking_A";
	}
	
	// return true when the skill finishes
	public override bool Activate(object target){
		
		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.3f)){
			//AudioManager.Instance.PlayOnce("RatkinWarrior/55_volibear_thunderclaws_oh2_01", m_attacker.transform, 1.0f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Warrior/(Thunder Clap)", m_attacker, 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_RatkinWarriorThunderClap", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1)
			return false;

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
