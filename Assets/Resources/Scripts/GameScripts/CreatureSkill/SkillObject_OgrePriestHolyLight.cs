﻿using UnityEngine;
using System.Collections;

public class SkillObject_OgrePriestHolyLight : SkillObject {

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			gainHealthLater();
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
