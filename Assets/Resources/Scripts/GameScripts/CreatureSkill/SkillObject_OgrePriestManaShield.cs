﻿using UnityEngine;
using System.Collections;

public class SkillObject_OgrePriestManaShield : SkillObject {

	const float MANA_SHIELD_DURATION = 20.0f;
	const float MANA_SHIELD_INTENSITY_BASE = 200.0f;
	const float MANA_SHIELD_INTENSITY_PER_SKI = 20.0f;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			afflictStatusEffectsLater((Creature)(m_target), StatusEffect.MANA_SHIELD, 
			                          MANA_SHIELD_DURATION, MANA_SHIELD_INTENSITY_BASE + MANA_SHIELD_INTENSITY_PER_SKI * m_attacker.getStats().SKI);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
