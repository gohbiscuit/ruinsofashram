using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_RatkinWarriorBasicAttack : Skill {

	List<string> sounds;

	// inherit base constructor
	public Skill_RatkinWarriorBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                      bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                      bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		/*sounds.Add("RatkinWarrior/31_garen_basicattack_oc_1");
		sounds.Add("RatkinWarrior/32_garen_basicattack_oc_2");
		sounds.Add("RatkinWarrior/33_garen_basicattack_oc_3");8*/

		sounds.Add("Ashram/Ratkin Warrior/Basic Attack/m2hSwordHitFlesh1a");
		sounds.Add("Ashram/Ratkin Warrior/Basic Attack/m2hSwordHitFlesh1b");
		sounds.Add("Ashram/Ratkin Warrior/Basic Attack/m2hSwordHitFlesh1c");
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Ratkin_1H_sword_swing_high_straight_down";

	// return true when the skill finishes
	public override bool Activate(object target){
		m_attacker.setSkillAnimation(atkAnimation, 1.5f);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.35f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.4f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_GenericMelee", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
