using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_OgrePriestBasicAttack : Skill {

	List<string> sounds;

	// inherit base constructor
	public Skill_OgrePriestBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                   bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                   bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		/*sounds.Add("OgrePriest/01_syndra_justicar_basicattack_o");
		sounds.Add("OgrePriest/02_syndra_justicar_basicattack_o");
		sounds.Add("OgrePriest/03_syndra_justicar_basicattack_o");
		sounds.Add("OgrePriest/04_syndra_justicar_basicattack_o");
		sounds.Add("OgrePriest/05_syndra_justicar_basicattack_o");
		sounds.Add("OgrePriest/06_syndra_justicar_basicattack_o");
		sounds.Add("OgrePriest/07_syndra_justicar_basicattack_o");
		sounds.Add("OgrePriest/08_syndra_justicar_basicattack_o");*/

		sounds.Add("Ashram/Ogre Priest/Basic Attack/1hDaggerHitFleshA");
		sounds.Add("Ashram/Ogre Priest/Basic Attack/1hDaggerHitFleshB");
		sounds.Add("Ashram/Ogre Priest/Basic Attack/1hDaggerHitFleshC");
		//sounds.Add("Ashram/Ogre Priest/Basic Attack/1hDaggerHitFleshCriticalA");		// critical?
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Attack01";
	const string atkAnimation2 = "Attack02";
	string currAnimation;

	// return true when the skill finishes
	public override bool Activate(object target){

		// randomise animation
		if(currAnimation == null){
			if(Random.Range(0,2) == 0){
				m_attacker.setSkillAnimation(atkAnimation, 1.5f);
				currAnimation = atkAnimation;
			}else{
				m_attacker.setSkillAnimation(atkAnimation2, 1.5f);
				currAnimation = atkAnimation2;
			}
		}

		if(!createdObject && hasReachedTime(m_attacker.animation[currAnimation], 0.3f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.3f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_GenericMelee", target);
		}
		
		if(m_attacker.animation[currAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
		currAnimation = null;
	}
	
}
