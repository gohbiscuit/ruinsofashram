using UnityEngine;
using System.Collections;

public class Skill_RatkinShamanDivineHeal : Skill {

	// inherit base constructor
	public Skill_RatkinShamanDivineHeal(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                    bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                    bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "SH_buff_spell_A";

	// animation for channelling/charging a skill
	public override string getChargeAnimation(){
		return "SH_making_potion";
	}
	
	// return true when the skill finishes
	public override bool Activate(object target){
		
		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.4f)){
			//AudioManager.Instance.PlayOnce("RatkinShaman/099_rengar_healthbuff_oba_01", m_attacker, 1.0f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Shaman/(Divine Heal) FlashHeal_Low_Base", m_attacker, 0.8f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_RatkinShamanDivineHeal", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1)
			return false;

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
