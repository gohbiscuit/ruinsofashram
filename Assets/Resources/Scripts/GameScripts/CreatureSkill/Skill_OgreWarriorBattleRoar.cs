using UnityEngine;
using System.Collections;

public class Skill_OgreWarriorBattleRoar : Skill {

	// inherit base constructor
	public Skill_OgreWarriorBattleRoar(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                   bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                   bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Damage";

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject){
			//AudioManager.Instance.PlayOnce("OgreWarrior/43_anivia_blackiceanivia_r_glaci", m_attacker.transform, 1.0f);

			AudioManager.Instance.PlayOnce("Ashram/Ogre Warrior/(Battle Roar) AspectofTheBeast", m_attacker, 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_OgreWarriorBattleRoar", m_attacker);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
