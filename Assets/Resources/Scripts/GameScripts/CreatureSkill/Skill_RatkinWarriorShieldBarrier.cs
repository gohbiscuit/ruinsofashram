using UnityEngine;
using System.Collections;

public class Skill_RatkinWarriorShieldBarrier : Skill {

	// inherit base constructor
	public Skill_RatkinWarriorShieldBarrier(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                        bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                        bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Ratkin_anger";

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject){
			//AudioManager.Instance.PlayOnce("RatkinWarrior/060_thresh_base_lantern_shield_ob", m_attacker.transform, 1.0f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Warrior/(Shield Barrier) DemonArmor", (Creature)(target), 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_RatkinWarriorShieldBarrier", (Creature)(target));
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
