﻿using UnityEngine;
using System.Collections;

public class SkillObject_OgreWarriorSkullCrusher : SkillObject {

	const float STUN_DURATION = 5;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			dealDamageLater();
			afflictStatusEffectsLater((Creature)(m_target), StatusEffect.STUN, STUN_DURATION, 1);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
