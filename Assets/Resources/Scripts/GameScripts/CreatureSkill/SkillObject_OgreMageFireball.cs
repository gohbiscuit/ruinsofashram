﻿using UnityEngine;
using System.Collections;

public class SkillObject_OgreMageFireball : SkillObject {

	const float PROJECTILE_SPEED = 0.2f;

	void Update () {
		if(!m_initialised)
			return;
	}

	bool m_dealtDamage = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
		
		Creature targetCreature = (Creature)m_target;
		if(targetCreature == null){
			Remove();
			return;
		}
		
		bool toDestroy = false;
		bool toDealDamage = false;
		linearMove(PROJECTILE_SPEED, transform, targetCreature.transform, ref toDealDamage, ref toDestroy);
		
		if(!m_dealtDamage && toDealDamage){

			//AudioManager.Instance.PlayOnce("OgreMage/26_ahri_foxfiremissile_oh_02", attacker, 0.8f);

			AudioManager.Instance.PlayOnce("Ashram/Ogre Mage/FireBallImpactA", m_attacker, 0.8f);
			m_dealtDamage = true;
			dealDamageLater();
		}
		
		if(toDestroy)
			Remove();
	}
}
