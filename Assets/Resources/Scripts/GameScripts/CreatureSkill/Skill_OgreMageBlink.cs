﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// release skill is a special case which doesn't spawn a skill object
public class Skill_OgreMageBlink : Skill {

	List<string> sounds;
	
	// inherit base constructor
	public Skill_OgreMageBlink(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		//sounds.Add("OgreMage/10_ezreal_arcaneshift_return_1");
		//sounds.Add("OgreMage/11_ezreal_arcaneshift_return_2");
		//sounds.Add("OgreMage/12_ezreal_arcaneshift_return_3");

		// Ashram sound
		sounds.Add("Ashram/Ogre Mage/(Blink) Teleport");
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool initBlink = false;
	
	// return true when the skill finishes
	public override bool Activate(object target){
		if(!initBlink){

			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 1.0f);

			// before teleport FX
			GameObject.Instantiate(Resources.Load("Prefab/Blink"), m_attacker.transform.position, Quaternion.identity);

			// just make it teleport over
			m_attacker.transform.position = (Vector3)(target);

			// after teleport FX
			GameObject.Instantiate(Resources.Load("Prefab/Blink"), m_attacker.transform.position, Quaternion.identity);

			initBlink = true;
		}
		return true;
	}

	public override void stopActivate(){
		StartCooldown();
		initBlink = false;
	}
}
