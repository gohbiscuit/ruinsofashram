﻿using UnityEngine;
using System.Collections;

public class SkillObject_RatkinGunnerRatkinEye : SkillObject {

	const float RATKIN_EYE_DURATION_BASE = 10.0f;
	const float RATKIN_EYE_DURATION_PER_SKI = 1.0f;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			afflictStatusEffectsLater(m_attacker, StatusEffect.RATKIN_EYE, 
			                          RATKIN_EYE_DURATION_BASE + RATKIN_EYE_DURATION_PER_SKI * m_attacker.getStats().SKI, 0);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
