using UnityEngine;
using System.Collections;

public class Skill_OgreRogueAssassinLock : Skill {

	// inherit base constructor
	public Skill_OgreRogueAssassinLock(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                   bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                   bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "Attack01";

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.3f)){
			//AudioManager.Instance.PlayOnce("OgreRogue/40_akali_pathofap_oba_1", m_attacker.transform, 1.0f);
			AudioManager.Instance.PlayOnce ("Ashram/Ogre Rogue/(Assasin Lock) Kidneyshot", m_attacker, 1.0f);
			createdObject = true;

			Vector3 targetPos = ((Creature)(target)).transform.position;
			Vector3 myPos = m_attacker.transform.position;
			Vector3 createPos = targetPos - (targetPos - myPos).normalized;

			createMeleeSkillObject("SkillObject_OgreRogueAssassinLock", target, createPos);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
