﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillObject_RatkinGunnerRapidFire : SkillObject {

	const float PROJECTILE_SPEED = 1.5f;
	const float DISTANCE_CLOSE = 3.0f;

	void Update () {
		if(!m_initialised)
			return;
	}

	bool m_dealtDamage = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;

		Creature targetCreature = (Creature)m_target;
		if(targetCreature == null){
			Remove();
			return;
		}

		bool toDestroy = false;
		bool toDealDamage = m_dealtDamage;
		float speed = PROJECTILE_SPEED;
		Transform src = transform;
		
		Vector3 fromToVector = (targetCreature.transform.position - src.position);
		
		// damage is sent first
		if(!toDealDamage){
			float distLeft = Vector3.Distance(Vector3.zero, fromToVector);
			float updatesLeft = Mathf.Ceil(distLeft/speed);
			if(updatesLeft * Time.fixedDeltaTime < GameNetworkManager.Instance.getTotalLag())
				toDealDamage = true;
		}
		
		// check if should destroy
		if(Vector3.Distance(Vector3.zero, fromToVector) > speed + DISTANCE_CLOSE){
			src.position += fromToVector.normalized * speed;
			toDestroy = false;
		} else {
			src.position = targetCreature.transform.position;
			toDestroy = true;
		}
		
		if(!m_dealtDamage && toDealDamage){
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Gunner/(Rapid Fire) GunFire03", m_attacker, 0.6f);
			m_dealtDamage = true;
			dealDamageLater();
		}
		
		if(toDestroy)
			Remove();
	}
}
