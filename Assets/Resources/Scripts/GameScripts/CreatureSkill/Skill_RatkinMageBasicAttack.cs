using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_RatkinMageBasicAttack : Skill {

	List<string> sounds;

	// inherit base constructor
	public Skill_RatkinMageBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                   bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                   bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		/*sounds.Add("RatkinMage/13_draven_basicattack_ohw_01");
		sounds.Add("RatkinMage/14_draven_basicattack_ohw_02");
		sounds.Add("RatkinMage/15_draven_basicattack_ohw_03");
		sounds.Add("RatkinMage/16_draven_basicattack_ohw_04");*/

		sounds.Add ("Ashram/Ratkin Mage/Basic Attack/1hMaceMetalHitFlesh1a");
		sounds.Add ("Ashram/Ratkin Mage/Basic Attack/1hMaceMetalHitFlesh1b");
		sounds.Add ("Ashram/Ratkin Mage/Basic Attack/1hMaceMetalHitFlesh1c");
	}
	
	// ------------------------------------------------------------------------------------------------

	bool createdObject = false;
	const string atkAnimation = "Ratkin_2W_L_swing_left";
	const string atkAnimation2 = "Ratkin_2W_L_swing_right";
	string currAnimation;
	
	// return true when the skill finishes
	public override bool Activate(object target){
		
		// randomise animation
		if(currAnimation == null){
			if(Random.Range(0,2) == 0){
				m_attacker.setSkillAnimation(atkAnimation, 1.5f);
				currAnimation = atkAnimation;
			}else{
				m_attacker.setSkillAnimation(atkAnimation2, 1.5f);
				currAnimation = atkAnimation2;
			}
		}

		if(!createdObject && hasReachedTime(m_attacker.animation[currAnimation], 0.35f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.3f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_GenericMelee", target);
		}
		
		if(m_attacker.animation[currAnimation].normalizedTime < 1){
			return false;
		}
		
		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
		currAnimation = null;
	}
	
}
