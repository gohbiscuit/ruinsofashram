﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkillObject_RatkinShamanChainLightning : SkillObject {

	const int FRAMES_TILL_NEXT_STRIKE = 7;
	const float CHAIN_RANGE = 12.0f;

	// these 2 will determine how many strikes will be made
	const float MIN_MULTIPLIER = 0.8f;
	const float MULTIPLIER_REDUCTION_PER_STRIKE = 0.1f;

	List<Creature> m_targetsHit;
	float m_multiplier;
	bool m_setup;
	bool m_attacked = false;
	int m_attackCounter = 0;

	void Update () {
		if(!m_initialised)
			return;
	}

	public void setupChainLightning(List<Creature> targetsHit, float multiplier){
		m_setup = true;
		this.m_targetsHit = targetsHit;
		this.m_multiplier = multiplier;
	}

	void FixedUpdate(){
		if(!m_initialised || !m_setup)
			return;

		m_attackCounter++;
		if(!m_attacked && m_attackCounter >= FRAMES_TILL_NEXT_STRIKE){
			m_attacked = true;
			dealDamageLater(m_multiplier);

			if(m_multiplier > MIN_MULTIPLIER) {

				// find closest creature that is not an ally
				GameObject[] allCreatureGOs = GameObject.FindGameObjectsWithTag("Creature");
				float minDist = 9999.9f;
				Creature closestCreature = null;
				foreach(GameObject creatureGO in allCreatureGOs){
					Creature creature = creatureGO.GetComponent<Creature>();
					if(creature == null || m_targetsHit.Contains(creature) || m_attacker.areAllies(creature))	// don't attack allies / creatures that were hit
						continue;
					float dist = Vector3.Distance(transform.position, creature.transform.position);
					if(dist < minDist){
						closestCreature = creature;
						minDist = dist;
					}
				}

				// no creature in the vicinity (impossible)
				// or the closest creature is too far away, then don't chain
				if(closestCreature != null && minDist <= CHAIN_RANGE){

					// instantiation of lightning strike on 'chained' target
					GameObject skillObject = (GameObject)(Resources.Load("Prefab/SkillObject_RatkinShamanChainLightning"));
					GameObject instantiatedSkillObject = (GameObject)(GameObject.Instantiate(skillObject, closestCreature.transform.position, closestCreature.transform.rotation));

					AudioManager.Instance.PlayOnce("RatkinShaman/44_kennen_lightningrush_oh_6", instantiatedSkillObject.transform, 0.5f);
					//AudioManager.Instance.PlayOnce("Ashram/Ratkin Shaman/(Chain Lightning Impact) LightningBoltImpact", instantiatedSkillObject.transform, 0.8f);

					SkillObject_RatkinShamanChainLightning obj = instantiatedSkillObject.GetComponent<SkillObject_RatkinShamanChainLightning>();
					obj.initialise(m_skillID, m_attacker, closestCreature);

					m_targetsHit.Add(closestCreature);
					obj.setupChainLightning(m_targetsHit, m_multiplier-MULTIPLIER_REDUCTION_PER_STRIKE);
				}

			}
		}
		
		if(transform.childCount == 0 && m_attacked)
			Remove ();
	}
}
