using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_RatkinShamanBasicAttack : Skill {

	List<string> sounds;

	// inherit base constructor
	public Skill_RatkinShamanBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                      bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                      bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		/*sounds.Add("RatkinShaman/01_xenzhao_basicattack_ohf_1");
		sounds.Add("RatkinShaman/02_xenzhao_basicattack_ohf_2");
		sounds.Add("RatkinShaman/03_xenzhao_basicattack_ohf_3");*/

		sounds.Add("Ashram/Ratkin Shaman/Basic Attack/2hMaceMetalHitFlesh1a");
		sounds.Add("Ashram/Ratkin Shaman/Basic Attack/2hMaceMetalHitFlesh1b");
		sounds.Add("Ashram/Ratkin Shaman/Basic Attack/2hMaceMetalHitFlesh1c");
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "SH_swing_right";

	// return true when the skill finishes
	public override bool Activate(object target){
		m_attacker.setSkillAnimation(atkAnimation, 1.5f);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.35f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_GenericMelee", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
