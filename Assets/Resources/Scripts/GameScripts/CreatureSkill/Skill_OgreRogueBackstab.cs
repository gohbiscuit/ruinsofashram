﻿using UnityEngine;
using System.Collections;

public class Skill_OgreRogueBackstab : Skill {

	// inherit base constructor
	public Skill_OgreRogueBackstab(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                               bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                               bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){}
	
	// ------------------------------------------------------------------------------------------------

	// return true when the skill finishes
	public override bool Activate(object target){
		return true;
	}
	
	public override void stopActivate(){
	}
	
}
