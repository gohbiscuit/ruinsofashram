﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// Skill class, each creature will have a set of skill objects unique to it.
/// when a skill is activated, skill objects would be created which does the actual effect (damage, heal and so on)
/// </summary>
public class Skill {
	
	// stats
	string m_skillName;						// skill of name. need not be unique.
	protected int m_skillTypeID;			// id of skill, determined by skill.csv.
	protected string m_skillDesc1;			// description of skill
	protected string m_skillDesc2;			// description of skill
	protected int m_mpCost;					// cost of using the skill. use up MP the moment skill is cast.
	protected float m_maxBaseCooldown;		// cooldown time (at level 0). also refers to the time between persistent basic attacks.
	protected float m_cooldownReduction;	// cooldown time reduction per SKI
	protected float m_chargeTime;			// time to charge/channel before actually casting the skill. (at level 0)
	protected float m_chargeReduction;		// charge time reduction per SKI
	protected float m_range;				// range before skill is used. (at level 0)
	protected float m_rangeIncrease;		// range increase per SKI
	protected bool m_persistent;			// generally for basic attacks. will keep executing skills relentlessly as soon as cooldown is over.
	protected bool m_executionOnRelease;	// the moment the skill is selected, it is used immediately without needing to select target
	protected bool m_passive;				// passive skills are activated ONCE the moment the creature awakes and stay forever.
											// passive skills cannot be used after which.

	// possible targets
	protected bool m_canSelectPoint;		// can specify a world coordinate as target.
	protected bool m_canSelectEnemy;		// can specify an enemy as a target. (opposite team or neutral creatures)
	protected bool m_canSelectAlly;			// can specify an ally (includes yourself) as a target.

	// animation (override in child to specify)
	string m_channelAnimation;
	
	// variables
	protected Creature m_attacker;
	protected float m_cooldown;
	protected float m_charge;
	
	public Skill(int skillTypeID, string name, string desc1, string desc2, int mpCost, bool persistent, 
	    float range, float rangeIncrease, float maxCooldown, float cooldownReduction,
	    float chargeTime, float chargeReduction,
	    bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, 
	    bool executionOnRelease, bool passive){
		
		this.m_skillTypeID = skillTypeID;
		this.m_skillName = name;
		this.m_skillDesc1 = desc1;
		this.m_skillDesc2 = desc2;

		this.m_mpCost = mpCost;
		this.m_persistent = persistent;
		this.m_range = range;
		this.m_rangeIncrease = rangeIncrease;
		this.m_maxBaseCooldown = maxCooldown;
		this.m_cooldownReduction = cooldownReduction;
		this.m_chargeTime = chargeTime;
		this.m_chargeReduction = chargeReduction;
		this.m_executionOnRelease = executionOnRelease;
		this.m_passive = passive;

		this.m_canSelectPoint = canSelectPoint;
		this.m_canSelectEnemy = canSelectEnemy;
		this.m_canSelectAlly = canSelectAlly;
	
		//StartCooldown();
	}
	
	/// <summary>
	/// Sets the user of this skill and starts cooling down
	/// </summary>
	/// <param name="attacker">Attacker.</param>
	public void setUser(Creature attacker){
		this.m_attacker = attacker;
	}
	
	/// <summary>
	/// Returns the description of the skill
	/// </summary>
	/// <returns>The skill description.</returns>
	public string getDescription(){
		return m_skillDesc1;
	}

	/// <summary>
	/// Returns the other description of the skill
	/// </summary>
	/// <returns>The other description.</returns>
	public string getDescription2(){
		return m_skillDesc2;
	}
	
	/// <summary>
	/// Starts channeling/charging the skill.
	/// </summary>
	public void StartCharging(){
		int SKI = 0;
		if(m_attacker != null)
			SKI = m_attacker.getStats().SKI;
		m_charge = m_chargeTime - SKI*m_chargeReduction;
	}
	
	/// <summary>
	/// Gets the maximum charge/cast time of the skill (subjected to level)
	/// </summary>
	/// <returns>The max charge time after SKI reduction.</returns>
	public float getMaxChargeTime(){
		if(m_attacker != null && m_attacker.getStats() != null)
			return m_chargeTime - m_attacker.getStats().SKI*m_chargeReduction;
		else
			return m_chargeTime;
	}
	
	/// <summary>
	/// Gets the decrease in charge/cast time with single point increase in SKI level.
	/// </summary>
	/// <returns>The charge time reduction per SKI.</returns>
	public float getChargeTimeReduction(){
		return m_chargeReduction;
	}
	
	/// <summary>
	/// Charge the skill. Called every frame.
	/// </summary>
	/// <param name="chargeAmt">amt to charge</param>
	public void Charge(float chargeAmt){
		if(chargeAmt < 0)
			throw new ArgumentException("Negative Charge");
		m_charge = Mathf.Max(0, m_charge - chargeAmt);
	}
	
	/// <summary>
	/// Returns whether the skill has finish charging.
	/// </summary>
	/// <returns><c>true</c>, if skill is charged, <c>false</c> otherwise.</returns>
	public bool isCharged(){
		return (m_charge == 0);
	}
	
	/// <summary>
	/// Gets the current charge time.
	/// </summary>
	/// <returns>The current charge time.</returns>
	public float getCurrentChargeTime(){
		return m_charge;
	}
	
	/// <summary>
	/// Starts the cooldown.
	/// </summary>
	protected void StartCooldown(){
		m_cooldown = getMaxCooldown();
	}
	
	/// <summary>
	/// Cool the skill down. Called every frame.
	/// </summary>
	/// <param name="coolAmt">amt to cool</param>
	public void Cooldown(float coolAmt){
		if(coolAmt < 0)
			throw new ArgumentException("Negative Cooldown");
		m_cooldown = Mathf.Max(0, m_cooldown - coolAmt);
	}
	
	/// <summary>
	/// Gets the current cooldown of the skill
	/// </summary>
	/// <returns>The cooldown.</returns>
	public float getCooldown(){
		return m_cooldown;
	}
	
	/// <summary>
	/// Gets the decrease in cooldown time with single point increase in SKI level.
	/// </summary>
	/// <returns>The cooldown reduction.</returns>
	public float getCooldownReduction(){
		return m_cooldownReduction;
	}
	
	/// <summary>
	/// Gets the maximum cooldown value of the skill (subjected to level)
	/// </summary>
	/// <returns>The max cooldown after SKI reduction.</returns>
	public float getMaxCooldown(){
		if(m_attacker != null && m_attacker.getStats() != null)
			return m_maxBaseCooldown - m_attacker.getStats().SKI * m_cooldownReduction;
		else
			return m_maxBaseCooldown;
	}
	
	/// <summary>
	/// Returns whether the skill has finished cooling down.
	/// </summary>
	/// <returns><c>true</c>, if skill is cooled, <c>false</c> otherwise.</returns>
	public bool isCooled(){
		return (m_cooldown == 0);
	}
	
	/// <summary>
	/// Gets the range before the skill starts casting. (subjected to level)
	/// </summary>
	/// <returns>The range after SKI increment.</returns>
	public float getRange(){
		if(m_attacker != null)
			return m_range + m_attacker.getStats().SKI * m_rangeIncrease;
		else
			return m_range;
	}
	
	/// <summary>
	/// Gets the increase in range with single point increase in SKI level.
	/// </summary>
	/// <returns>The range increase per SKI.</returns>
	public float getRangeIncrease(){
		return m_rangeIncrease;
	}
	
	/// <summary>
	/// Returns whether skill is activated automatically
	/// right after casting (basic attack)
	/// </summary>
	/// <returns><c>true</c>, if skill if persistent, <c>false</c> otherwise.</returns>
	public bool isPersistent(){
		return m_persistent;
	}
	
	/// <summary>
	/// Gets the ID of the skill.
	/// </summary>
	/// <returns>The skill type ID</returns>
	public int getSkillTypeID(){
		return m_skillTypeID;
	}
	
	/// <summary>
	/// Returns whether a coordinate can be selected
	/// with this skill.
	/// </summary>
	/// <returns><c>true</c>, if skill is point selectable, <c>false</c> otherwise.</returns>
	public bool isPointSelectable(){
		return m_canSelectPoint;
	}

	/// <summary>
	/// Returns whether a skill is an AOE or not
	/// </summary>
	/// <returns><c>true</c>, if skill is AOE, <c>false</c> otherwise.</returns>
	public bool isAOE(){
		// temporary but working implementation
		if(m_canSelectPoint && !m_skillName.Equals("Teleport") && !m_skillName.Equals("Blink"))
			return true;
		else
			return false;
	}
	
	/// <summary>
	/// Returns whether an enemy can be selected
	/// with this skill.
	/// </summary>
	/// <returns><c>true</c>, if skill is enemy selectable, <c>false</c> otherwise.</returns>
	public bool isEnemySelectable(){
		return m_canSelectEnemy;
	}
	
	/// <summary>
	/// Returns whether an ally can be selected
	/// with this skill.
	/// </summary>
	/// <returns><c>true</c>, if skill is ally selectable, <c>false</c> otherwise.</returns>
	public bool isAllySelectable(){
		return m_canSelectAlly;
	}
	
	/// <summary>
	/// Returns whether the skill is used immediately
	/// after selecting the skill
	/// </summary>
	/// <returns><c>true</c>, if skill is executed on release, <c>false</c> otherwise.</returns>
	public bool isExecutedOnRelease(){
		return m_executionOnRelease;
	}
	
	/// <summary>
	/// Returns whether the skill is a passive (ongoing) skill.
	/// </summary>
	/// <returns><c>true</c>, if skill is passive, <c>false</c> otherwise.</returns>
	public bool isPassive(){
		return m_passive;
	}

	/// <summary>
	/// Gets the MP cost.
	/// </summary>
	/// <returns>The MP cost.</returns>
	public int getMPCost(){
		return m_mpCost;
	}
	
	/// <summary>
	/// Computes and return the damage range for the current level
	/// with no regards to the target's stats. Note that this is quite computational expensive.
	/// </summary>
	/// <param name="minDamage">Minimum damage that can be dealt.</param>
	/// <param name="maxDamage">Max damage that can be dealt.</param>
	public void computeBaseDamage(out int minDamage, out int maxDamage){
		SkillFactory.Instance.getBaseDamage(m_skillTypeID, m_attacker, m_attacker.getStats().SKI, out minDamage, out maxDamage);
	}
	
	/// <summary>
	/// get the animation for channelling/charging a skill
	/// override in individual inherited
	/// </summary>
	/// <returns>The charge animation</returns>
	public virtual string getChargeAnimation(){
		return null;
	}
	
	/// <summary>
	/// Uses the skill. Returns true once the skill has finished.
	/// To be overridden by all skills
	/// </summary>
	/// <param name="target">Target to use on</param>
	public virtual bool Activate(object target){
		StartCooldown();
		return true;
	}

	/// <summary>
	/// Checks if the time of the animation is reached
	/// </summary>
	/// <returns><c>true</c>, if reached time, <c>false</c> otherwise.</returns>
	/// <param name="animation">Animation to test</param>
	/// <param name="normalisedTime">Normalised time.</param>
	protected bool hasReachedTime(AnimationState animation, float normalisedTime){
		float timeToExecute = animation.length * normalisedTime  - GameNetworkManager.Instance.getTotalLag();	// the instant where the skillobject is created
		return animation.time >= timeToExecute;
	}
	
	/// <summary>
	/// Called whenever the skill is forced to deactivate after activation has occurred.
	/// </summary>
	public virtual void stopActivate(){
	}

	/// <summary>
	/// Can be called eric ranged skills to
	/// create a skill object which would travel from the attacker to its target
	/// </summary>
	/// <param name="prefabName">Prefab name of skillobject</param>
	/// <param name="target">Target to hit</param>
	protected void createRangedSkillObject(string prefabName, object target){
		GameObject skillObject = (GameObject)(Resources.Load("Prefab/" + prefabName));
		if(skillObject == null)
			throw new ArgumentException("Invalid skillobject prefab");

		GameObject instantiatedSkillObject = (GameObject)(GameObject.Instantiate(skillObject, m_attacker.transform.position, m_attacker.transform.rotation));
		instantiatedSkillObject.GetComponent<SkillObject>().initialise(m_skillTypeID, m_attacker, target);
	}

	/// <summary>
	/// Can be called by generic melee skills to
	/// create a skill object on the target itself
	/// </summary>
	/// <param name="prefabName">Prefab name of skillobject.</param>
	/// <param name="target">Target to hit</param>
	protected void createMeleeSkillObject(string prefabName, object target){
		Vector3 destination;
		if(target is Creature){
			if((Creature)(target) == null)
				return;
			destination = ((Creature)(target)).transform.position;
		} else if(target is Vector3)
			destination = (Vector3)target;
		else
			throw new ArgumentException("Invalid target");
		
		GameObject skillObject = (GameObject)(Resources.Load("Prefab/" + prefabName));
		if(skillObject == null)
			throw new ArgumentException("Invalid skillobject prefab");

		GameObject instantiatedSkillObject = (GameObject)(GameObject.Instantiate(skillObject, destination, m_attacker.transform.rotation));
		instantiatedSkillObject.GetComponent<SkillObject>().initialise(m_skillTypeID, m_attacker, target);
	}
	
	/// <summary>
	/// Can be called by generic melee skills to
	/// create a skill object on the target itself
	/// </summary>
	/// <param name="prefabName">Prefab name of skillobject</param>
	/// <param name="target">Target to hit</param>
	/// <param name="position">Position to spawn skill object</param>
	protected void createMeleeSkillObject(string prefabName, object target, Vector3 position){
		GameObject skillObject = (GameObject)(Resources.Load("Prefab/" + prefabName));
		if(skillObject == null)
			throw new ArgumentException("Invalid skillobject prefab");

		GameObject instantiatedSkillObject = (GameObject)(GameObject.Instantiate(skillObject, position, m_attacker.transform.rotation));
		instantiatedSkillObject.GetComponent<SkillObject>().initialise(m_skillTypeID, m_attacker, target);
	}

	/// <summary>
	/// Gets the name of the skill.
	/// </summary>
	/// <value>The name of the skill</value>
	public string SkillName{
		get {return m_skillName;}
	}
}
