using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skill_OgreWarriorBasicAttack : Skill {

	List<string> sounds;
	List<string> sounds2;

	// inherit base constructor
	public Skill_OgreWarriorBasicAttack(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                    bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                    bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){

		sounds = new List<string>();
		sounds.Add("OgreWarrior/13_Gragas_basicattack_oc_1");
		sounds.Add("OgreWarrior/14_Gragas_basicattack_oc_2");
		sounds.Add("OgreWarrior/15_Gragas_basicattack_oc_3");

		/*sounds.Add ("Ashram/Ogre Warrior/Basic Attack/SwingWeaponSpecialWarriorA");
		sounds.Add ("Ashram/Ogre Warrior/Basic Attack/SwingWeaponSpecialWarriorB");
		sounds.Add ("Ashram/Ogre Warrior/Basic Attack/SwingWeaponSpecialWarriorC");
		sounds.Add ("Ashram/Ogre Warrior/Basic Attack/SwingWeaponSpecialWarriorD");
		sounds.Add ("Ashram/Ogre Warrior/Basic Attack/SwingWeaponSpecialWarriorE");*/

		sounds2 = new List<string>();
		sounds2.Add("OgreWarrior/07_Gragas_basicattack_ohs_1");
		sounds2.Add("OgreWarrior/08_Gragas_basicattack_ohs_2");
		sounds2.Add("OgreWarrior/09_Gragas_basicattack_ohs_3");
		/*sounds2.Add ("Ashram/Ogre Warrior/Basic Attack/m1hAxeHitFlesh1a");
		sounds2.Add ("Ashram/Ogre Warrior/Basic Attack/m1hAxeHitFlesh1b");
		sounds2.Add ("Ashram/Ogre Warrior/Basic Attack/m1hAxeHitFlesh1c");*/
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	bool swingSound = false;
	
	const string atkAnimation = "Attack01";
	const string atkAnimation2 = "Attack02";
	string currAnimation;

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation);

		if(!swingSound && hasReachedTime(m_attacker.animation[atkAnimation], 0.2f)){
			AudioManager.Instance.PlayOnce(sounds[Random.Range(0, sounds.Count)], m_attacker, 0.8f);
			swingSound = true;
		}

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.3f)){
			AudioManager.Instance.PlayOnce(sounds2[Random.Range(0, sounds2.Count)], m_attacker, 0.8f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_GenericMelee", target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
		swingSound = false;
	}
	
}
