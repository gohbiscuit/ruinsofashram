﻿using UnityEngine;
using System.Collections;

public class SkillObject_RatkinWarriorDeadlyBlow : SkillObject {

	const float SLOW_DURATION = 5;

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!attacked){
			attacked = true;
			dealDamageLater();
			afflictStatusEffectsLater((Creature)(m_target), StatusEffect.SLOW, SLOW_DURATION, 1);
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
