using UnityEngine;
using System.Collections;

public class Skill_RatkinShamanEarthShield : Skill {

	// inherit base constructor
	public Skill_RatkinShamanEarthShield(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                     bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                     bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "SH_magic_light_spell";

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject && hasReachedTime(m_attacker.animation[atkAnimation], 0.4f)){
			//AudioManager.Instance.PlayOnce("RatkinShaman/069_rengar_battleroar_armorbuff_o", m_attacker, 1.0f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Shaman/(Earth Shield) FireShield", (Creature)target, 1.0f);
			createdObject = true;
			createMeleeSkillObject("SkillObject_RatkinShamanEarthShield", (Creature)target);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
