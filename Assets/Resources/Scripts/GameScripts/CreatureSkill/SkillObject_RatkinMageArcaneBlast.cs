﻿using UnityEngine;
using System.Collections;

public class SkillObject_RatkinMageArcaneBlast : SkillObject {

	const float PROJECTILE_SPEED = 0.25f;

	void Update () {
		if(!m_initialised)
			return;
	}

	bool m_dealtDamage = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;

		Creature targetCreature = (Creature)m_target;
		if(targetCreature == null){
			Remove();
			return;
		}

		bool toDestroy = false;
		bool toDealDamage = false;
		linearMove(PROJECTILE_SPEED, transform, targetCreature.transform, ref toDealDamage, ref toDestroy);

		if(!m_dealtDamage && toDealDamage){
			//AudioManager.Instance.PlayOnce("RatkinMage/34_leblanc_leblancpassive_oba_a0", attacker, 0.8f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Mage/ArcaneMissileImpact1a", m_attacker, 1.0f);
			m_dealtDamage = true;
			dealDamageLater();
		}
		
		if(toDestroy)
			Remove();
	}
}
