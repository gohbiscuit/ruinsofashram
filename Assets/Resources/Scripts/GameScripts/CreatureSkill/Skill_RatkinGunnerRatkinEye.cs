using UnityEngine;
using System.Collections;

public class Skill_RatkinGunnerRatkinEye : Skill {

	// inherit base constructor
	public Skill_RatkinGunnerRatkinEye(int skillTypeID, string name, string desc1, string desc2, int mpCost,
	                                   bool persistent, float range, float rangeIncrease, float maxCooldown, float cooldownReduction, float chargeTime, float chargeReduction,
	                                   bool canSelectPoint, bool canSelectEnemy, bool canSelectAlly, bool executionOnRelease, bool passive) : 
		base(skillTypeID, name, desc1, desc2, mpCost, persistent, range, rangeIncrease, maxCooldown, cooldownReduction, chargeTime, chargeReduction, canSelectPoint, 
		    canSelectEnemy, canSelectAlly, executionOnRelease, passive){
	}
	
	// ------------------------------------------------------------------------------------------------
	
	bool createdObject = false;
	const string atkAnimation = "RGunner_Stretching";

	// return true when the skill finishes
	public override bool Activate(object target){

		m_attacker.setSkillAnimation(atkAnimation);

		if(!createdObject){
			createdObject = true;
			//AudioManager.Instance.PlayOnce("RatkinGunner/24_item_glacialspike_oc_03", m_attacker, 1.0f);
			AudioManager.Instance.PlayOnce("Ashram/Ratkin Gunner/(Ratkin Eye02) UnyieldingWillTarget", m_attacker, 1.0f);
			createMeleeSkillObject("SkillObject_RatkinGunnerRatkinEye", m_attacker);
		}
		
		if(m_attacker.animation[atkAnimation].normalizedTime < 1){
			return false;
		}

		return true;
	}
	
	public override void stopActivate(){
		StartCooldown();
		createdObject = false;
	}
	
}
