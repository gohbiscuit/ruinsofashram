﻿using UnityEngine;
using System.Collections;

public class SkillObject_GenericInstantDamageWithEffects : SkillObject {

	void Update () {
		if(!m_initialised)
			return;
	}
	
	bool m_attacked = false;
	void FixedUpdate(){
		if(!m_initialised)
			return;
			
		if(!m_attacked){
			m_attacked = true;
			dealDamageLater();
		}
		
		if(transform.childCount == 0)
			Remove ();
	}
}
