﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// very basic script for testing only
public class OldPlayerCommand : MonoBehaviour {
	/*
	private static OldPlayerCommand instance;

	PlayerAttributes attrs;
	bool initialised = false;

	Creature creature;

	// stuff for displaying stuff, not essential
	bool selectingSkill1 = false;
	bool selectingSkill2 = false;
	bool selectingSkill3 = false;
	bool selectingPossessionRelease = false;
	Skill selectedSkill;
	Skill skillPossessRelease;
	Skill skill1;
	Skill skill2;
	Skill skill3;
	Creature lastHoveredCreature;
	public List<string> damageMessages = new List<string>();

	public static OldPlayerCommand Instance
	{
		get {
			return instance;
		}
	}

	void Awake(){

		instance = this;
		attrs = new PlayerAttributes();
	}
	
	void Update(){
		
		if(creature == null)
			return;
		else if(!initialised)
			initialised = true;

		/*if(creature.creatureRace != CreatureRace.WISP)
			Debug.Log(creature.isStillUsingSkill(creature.getSkillList()[2], creature.getTarget()));*/
		
		// horrible way of doing things i know, just placeholders
		bool selectingSkill = false;
		/*if(selectingSkill1 || selectingSkill2 || selectingSkill3 || selectingPossessionRelease){
			
			if(selectingSkill1)
				selectedSkill = skill1;
			else if(selectingSkill2)
				selectedSkill = skill2;
			else if(selectingSkill3)
				selectedSkill = skill3;
			else if(selectingPossessionRelease)
				selectedSkill = skillPossessRelease;
			
			selectingSkill = true;
		}
		
		// after clicking "Skill 1"
		if(selectingSkill){
			if(Input.GetMouseButtonDown(0)){
				selectingSkill1 = false;
				selectingSkill2 = false;
				selectingSkill3 = false;
				selectingPossessionRelease = false;
				
				RaycastHit hit;
				Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
				Physics.Raycast(ray.origin, ray.direction, out hit, 500, (1<<10) | (1<<9));
				
				if(hit.transform.tag == "Creature"){
					
					// select a creature
					Creature selectedCreature = hit.transform.GetComponent<Creature>();
					creature.UseSkillLater(selectedSkill, selectedCreature);
				
				} else {
					
					// select a point
					System.Object pos = (System.Object)(hit.point);
					creature.UseSkillLater(selectedSkill, pos);
				
				}
			}
		}
		
		// after right click
		if(Input.GetMouseButtonUp(1)){
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			Physics.Raycast(ray.origin, ray.direction, out hit, 500, 1<<10 | 1<<9);

			// using basic attack
			if(hit.collider.tag.Equals("Creature")){
				if(creature.getSkillList().Count > 1)
					creature.UseSkillLater(creature.getSkillList()[1], hit.transform.GetComponent<Creature>());
			} else 
				// use move
				creature.MoveLater(hit.point);
		}

		// hovering over which creature (to show stats)
		Ray rayMouse = Camera.main.ScreenPointToRay(Input.mousePosition);
		RaycastHit hitMouse;
		Physics.Raycast(rayMouse.origin, rayMouse.direction, out hitMouse, 500, 1<<10 | 1<<9);
		if(hitMouse.collider.tag.Equals("Creature"))
			lastHoveredCreature = hitMouse.transform.GetComponent<Creature>();
	}

	void OnGUI(){

		if(creature == null)
			return;

		if(GUI.Button(new Rect(20,170,100,40), skillPossessRelease.skillName + " (" + skillPossessRelease.getCooldown() + ")")) {
			if(skillPossessRelease.isExecutedOnRelease())
				creature.UseSkillLater(skillPossessRelease, creature);
			else {
				selectingPossessionRelease = true;
				selectingSkill1 = false;
				selectingSkill2 = false;
				selectingSkill3 = false;
			}
		}

		if(skill1 != null && GUI.Button(new Rect(20,70,140,40), skill1.skillName + " (" + skill1.getCooldown() + ")")) {
			if(skill1.isExecutedOnRelease())
				creature.UseSkillLater(skill1, creature);
			else {
				selectingPossessionRelease = false;
				selectingSkill1 = true;
				selectingSkill2 = false;
				selectingSkill3 = false;
			}
		}
		
		if(skill2 != null && GUI.Button(new Rect(160,70,140,40), skill2.skillName + " (" + skill2.getCooldown() + ")")) {
			if(skill2.isExecutedOnRelease())
				creature.UseSkillLater(skill2, creature);
			else {
				selectingPossessionRelease = false;
				selectingSkill2 = true;
				selectingSkill1 = false;
				selectingSkill3 = false;
			}
		}

		if(skill3 != null && GUI.Button(new Rect(300,70,140,40), skill3.skillName + " (" + skill3.getCooldown() + ")")) {
			if(skill3.isExecutedOnRelease())
				creature.UseSkillLater(skill3, creature);
			else {
				selectingPossessionRelease = false;
				selectingSkill3 = true;
				selectingSkill1 = false;
				selectingSkill2 = false;
			}
		}
		
		if(GUI.Button(new Rect(20,120,100,40), "Stop")) {
			creature.StopLater();
		}
		
		if(selectingPossessionRelease || selectingSkill1 || selectingSkill2 || selectingSkill3){
			GUI.Label(new Rect(20,300,100,200), "Left click to use skill");
		}

		/*
		// Levelling, increasing attributes
		if(GUI.Button(new Rect(20,480,100,30), "+100EXP"))
			gainEXP(100);
		if(GUI.Button(new Rect(20,510,100,30), "+STR"))
			incrementStrength();
		if(GUI.Button(new Rect(20,540,100,30), "+INT"))
			incrementIntelligence();
		if(GUI.Button(new Rect(20,570,100,30), "+AGI"))
			incrementAgility();
		if(GUI.Button(new Rect(20,600,100,30), "+SKI"))
			incrementSkillMastery();


		// show player attributes box
		string displayAttrs = "";
		displayAttrs += "Player's Attributes";
		displayAttrs += "\nLvl :" + attrs.CURRENT_LEVEL;
		displayAttrs += "\nExp :" + attrs.EXP;
		displayAttrs += "\nStr :" + attrs.STRENGTH;
		displayAttrs += "\nInt :" + attrs.INTELLIGENCE;
		displayAttrs += "\nAgi :" + attrs.AGILITY;
		displayAttrs += "\nSki :" + attrs.SKILL_MASTERY;
		GUI.Button(new Rect(150,480,200,200), displayAttrs);

		// show creature stats box
		string displayStats = "(Hover over to see stats)\n";
		CreatureStats stats = lastHoveredCreature.getStats();
		if(lastHoveredCreature == creature)
			displayStats += "(possessed by you)\n";
		displayStats += lastHoveredCreature.creatureRace.ToString() + "_" + lastHoveredCreature.creatureClass.ToString();
		displayStats += "\nHP :" + ((int)(stats.Curr_HP)) + "/" + stats.HP;
		displayStats += "\nMP :" + ((int)(stats.Curr_MP)) + "/" + stats.MP;
		displayStats += "\nHPRegen :" + stats.HP_REGEN;
		displayStats += "\nMPRegen :" + stats.MP_REGEN;
		displayStats += "\nPhyAtk :" + stats.PHY_ATK;
		displayStats += "\nMagAtk :" + stats.MAG_ATK;
		displayStats += "\nPhyDef :" + stats.PHY_DEF;
		displayStats += "\nMagDef :" + stats.MAG_DEF;
		displayStats += "\nEvasion :" + stats.EVA;
		displayStats += "\nMovSpeed :" + stats.MOV;
		displayStats += "\nSkill :" + stats.SKI;
		GUI.Button(new Rect(400,480,200,230), displayStats);*/

		// show damage messages
		/*if(damageMessages.Count > 5)
			damageMessages.RemoveAt(0);
		string toDisplay = "";
		foreach(string msg in damageMessages)
			toDisplay += msg + "\n";
		GUI.Label(new Rect(20,210,350,200), toDisplay);

		// skill effects test ----------------------------------

		// STUN
		if(creature.hasStatusEffectNow(StatusEffect.STUN))
			GUI.Label(new Rect(20,300,350,20), "STUN");
		//if(GUI.Button(new Rect(150,300,150,20), "Stun 3s"))
		//	creature.AfflictStatusEffectLater(StatusEffect.STUN, 3.0f, 1);

		// SLOW
		if(creature.hasStatusEffectNow(StatusEffect.SLOW))
			GUI.Label(new Rect(20,320,350,20), "SLOW");
		//if(GUI.Button(new Rect(150,320,150,20), "Slow 3s"))
		//	creature.AfflictStatusEffectLater(StatusEffect.SLOW, 3.0f, 1);

		// OGRE AURA (Cannot activate from here cos' it's passive)
		if(creature.hasStatusEffectNow(StatusEffect.ORGE_AURA))
			GUI.Label(new Rect(20,320,350,20), "OGRE AURA");

		// MANA SHIELD
		if(creature.hasStatusEffectNow(StatusEffect.MANA_SHIELD))
			GUI.Label(new Rect(20,340,350,20), "MANA SHIELD");
		//if(GUI.Button(new Rect(150,340,150,20), "Mana Shield 100"))
		//	creature.AfflictStatusEffectLater(StatusEffect.MANA_SHIELD, 0, 100);

		// FREEZING SHIELD
		if(creature.hasStatusEffectNow(StatusEffect.FREEZING_SHIELD))
			GUI.Label(new Rect(20,360,350,20), "FREEZING SHIELD");
		//if(GUI.Button(new Rect(150,360,150,20), "Freezing Shield 10s"))
		//	creature.AfflictStatusEffectLater(StatusEffect.FREEZING_SHIELD, 10, 1);

		// EARTH SHIELD
		if(creature.hasStatusEffectNow(StatusEffect.EARTH_SHIELD))
			GUI.Label(new Rect(20,380,350,20), "EARTH SHIELD");
		//if(GUI.Button(new Rect(150,380,150,20), "Earth Shield 10s Lv3"))
		//	creature.AfflictStatusEffectLater(StatusEffect.EARTH_SHIELD, 10, 3);

		// BATTLE ROAR
		if(creature.hasStatusEffectNow(StatusEffect.BATTLE_ROAR))
			GUI.Label(new Rect(20,400,350,20), "BATTLE ROAR");
		//if(GUI.Button(new Rect(150,400,150,20), "Battle Roar 10s Lv3"))
		//	creature.AfflictStatusEffectLater(StatusEffect.BATTLE_ROAR, 10, 3);

		// SHIELD BARRIER
		if(creature.hasStatusEffectNow(StatusEffect.SHIELD_BARRIER))
			GUI.Label(new Rect(20,420,350,20), "SHIELD BARRIER");
		//if(GUI.Button(new Rect(150,420,150,20), "Shield Barrier 10s Lv3"))
		//	creature.AfflictStatusEffectLater(StatusEffect.SHIELD_BARRIER, 10, 3);

		// ASSASSIN LOCK
		if(creature.hasStatusEffectNow(StatusEffect.ASSASSIN_LOCK))
			GUI.Label(new Rect(20,440,350,20), "ASSASSIN LOCK");
		//if(GUI.Button(new Rect(150,440,150,20), "Assassin Lock 5s"))
		//	creature.AfflictStatusEffectLater(StatusEffect.ASSASSIN_LOCK, 5, 1);

	}

	/// <summary>
	/// Sets the creature that the player is controlling to the given creature.
	/// </summary>
	public void setControlledCreature(Creature creature){
		
		creature.isMine = true;
		creature.name = "Possessed " + creature.name;	// not used, for debugging only
		
		skillPossessRelease = creature.getSkillList()[0];	// first skill is always possess/release
		int numSkills = creature.getSkillList().Count;
		if(creature.creatureRace == CreatureRace.WISP){
			skill1 = creature.getSkillList()[1];	// wisp doesn't have a basic attack
			skill2 = null;
			skill3 = null;
		} else {
			skill1 = (numSkills >= 3)? creature.getSkillList()[2] : null;
			skill2 = (numSkills >= 4)? creature.getSkillList()[3] : null;
			skill3 = (numSkills >= 5)? creature.getSkillList()[4] : null;
		}
		this.creature = creature;
		
		lastHoveredCreature = creature;
	}

	/// <summary>
	/// Gains experience
	/// </summary>
	public void gainEXP(int exp){
		if(attrs.GainEXP(exp))
			creature.RecomputePossessedStatsLater();
	}

	/// <summary>
	/// Adds 1 point to player's STR attribute
	/// </summary>
	public void incrementStrength(){
		attrs.IncrementStrength();
		creature.RecomputePossessedStatsLater();
	}

	/// <summary>
	/// Adds 1 point to player's INT attribute
	/// </summary>
	public void incrementIntelligence(){
		attrs.IncrementInt();;
		creature.RecomputePossessedStatsLater();
	}


	/// <summary>
	/// Adds 1 point to player's AGI attribute
	/// </summary>
	public void incrementAgility(){
		attrs.IncrementAgility();
		creature.RecomputePossessedStatsLater();
	}

	/// <summary>
	/// Adds 1 point to player's SKI attribute
	/// </summary>
	public void incrementSkillMastery(){
		attrs.IncrementSkillMastery();
		creature.RecomputePossessedStatsLater();
	}

	/// <summary>
	/// Gets the name of the player.
	/// </summary>
	public string getPlayerName(){
		return PhotonNetwork.playerName;
	}

	/// <summary>
	/// Gets the player attributes (STR, INT, AGI, SKI, LVL, EXP)
	/// </summary>
	public PlayerAttributes getPlayerAttributes(){
		return attrs;
	}

	/// <summary>
	/// Checks if the given creature is the one that the player is controlling
	/// </summary>
	public bool isControlling(Creature creature){
		return this.creature == creature;
	}

	/// <summary>
	/// Returns the creature that the player is controlling
	/// </summary>
	public Creature getControlledCreature(){
		return creature;
	}

	/// <summary>
	/// Checks if the player is alive
	/// </summary>
	public bool isAlive(){
		return creature != null;
	}*/
}
