﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class that is in-charge of decaying the corpse e.g. move it underground after a creature has died
/// </summary>
public class CorpseDecay : MonoBehaviour {

	bool m_isDecaying;

	// Use this for initialization
	void Start () {
		StartCoroutine(Decay());
	}

	IEnumerator Decay(){
		yield return new WaitForSeconds(10);
		m_isDecaying = true;
		yield return new WaitForSeconds(10);
		Destroy(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
		if(m_isDecaying)
			transform.position += new Vector3(0, -Time.deltaTime/3, 0);
	}
}
