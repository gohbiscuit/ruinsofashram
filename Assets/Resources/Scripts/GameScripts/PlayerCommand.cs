using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Player command class is the core class that interface with creature.
/// </summary>
public class PlayerCommand : MonoBehaviour {

	private static PlayerCommand instance;

	PlayerAttributes attrs;
	bool initialised = false;

	public Creature creature;

	private float m_timeLeftToRespawn;

	// for desaturation effect when player dies
	[SerializeField] DesaturateEffect m_desatFX;
	bool m_isSaturated;

	// stuff for displaying stuff, not essential
	bool selectingSkill1 = false;
	bool selectingSkill2 = false;
	bool selectingSkill3 = false;
	bool selectingPossessionRelease = false;
	Skill selectedSkill;
	Skill skillPossessRelease;
	Skill skill1;
	Skill skill2;
	Skill skill3;
	Creature lastHoveredCreature;
	//public List<string> damageMessages = new List<string>();


	[SerializeField] HUDHandler m_hudHandler;

	[SerializeField] CustomCursor m_cursor;
	[SerializeField] GameObject m_skillChargeBarGO;
	[SerializeField] UISlider m_skillChargeBar;
	[SerializeField] UILabel m_skillChargeName;

	[SerializeField] GameObject m_targetMarker;

	[SerializeField] HUDErrorMessage m_errorMessage;
	[SerializeField] HUDExploredText m_exploreMessage;
	[SerializeField] HUDRespawnMessage m_combatLogMessage;

	[SerializeField] GameObject m_statusEffectTemplate;
	[SerializeField] GameObject m_statusEffectHUDParent;
	private Dictionary<string, GameObject> statusEffectDict = new Dictionary<string, GameObject>();	
	private int statusCounter = 0;

	private Dictionary<string, GameObject> hudSoundDict = new Dictionary<string, GameObject>();

	private List<string> ratkin_gunner_hudSoundList = new List<string>();
	private List<string> ratkin_warrior_hudSoundList = new List<string>();
	private List<string> ratkin_mage_hudSoundList = new List<string>();
	private List<string> ratkin_shaman_hudSoundList = new List<string>();

	private List<string> ogre_warrior_hudSoundList = new List<string>();
	private List<string> ogre_rogue_hudSoundList = new List<string>();
	private List<string> ogre_priest_hudSoundList = new List<string>();
	private List<string> ogre_mage_hudSoundList = new List<string>();
	private List<string> wisp_hudSoundList = new List<string>();

	private List<string> moveSoundList = new List<string>();
	private bool isMoveSoundPlaying = false;
	float hudMoveStartTime = -4.0f;	// set intial to -4 so first movement sound will be played
	float hudMoveSoundTimerLimit = 4.0f;
	private bool isExploredSoundPlayed = false;

	private GameObject currentChargeSound;
	private GameObject currentMoveSound;
	

	// Attributes Bar
	[SerializeField] GameObject m_attributesStrengthButton;
	[SerializeField] GameObject m_attributesAgilityButton;
	[SerializeField] GameObject m_attributesIntelligenceButton;
	[SerializeField] GameObject m_attributesMagicButton;


	// Respawn Text
	[SerializeField] GameObject m_respawnPanel;
	[SerializeField] UILabel m_killText;
	[SerializeField] UILabel m_respawnText;


	private bool isGameEnded = false;

	public static PlayerCommand Instance
	{
		get {
			return instance;
		}
	}

	/// <summary>
	/// Inits the audio sound for all the HUD components
	/// </summary>
	void InitAudioSound()
	{
		hudSoundDict.Add("fire_cast", AudioManager.Instance.NewSound("Ashram/Spell Charging/PreCastFireHigh") );
		hudSoundDict.Add("frost_cast", AudioManager.Instance.NewSound("Ashram/Spell Charging/PreCastFrostMagicMedium"));
		hudSoundDict.Add("heal_cast", AudioManager.Instance.NewSound("Ashram/Spell Charging/PreCastHolyMagicHigh"));
		hudSoundDict.Add("heal_lightcast", AudioManager.Instance.NewSound("Ashram/Spell Charging/PreCastHolyMagicMedium"));
		hudSoundDict.Add("general_cast", AudioManager.Instance.NewSound("Ashram/Spell Charging/PreCastMageLow"));
		hudSoundDict.Add("arcane_cast", AudioManager.Instance.NewSound("Ashram/Spell Charging/PreCastShadowMagicHigh"));
		hudSoundDict.Add("nature_cast", AudioManager.Instance.NewSound("Ashram/Spell Charging/PreCastNatureMagicHigh"));

		currentChargeSound = hudSoundDict["fire_cast"];

		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_01");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_02");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_03");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_04");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_05");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_06");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_07");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_08");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_09");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_10");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_11");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_12");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_13");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_14");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_15");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_16");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_17");
		ratkin_warrior_hudSoundList.Add ("Ashram/Ratkin Warrior/Move/lone_druid_move_18");

		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_01");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_12");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_13");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_14");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_15");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_16");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_17");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_18");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_19");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_20");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_21");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_22");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_23");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_24");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_25");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_26");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_27");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_29");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_30");
		ratkin_gunner_hudSoundList.Add ("Ashram/Ratkin Gunner/Move/snip_move_28");

		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_01");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_02");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_03");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_04");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_05");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_06");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_07");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_08");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_09");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_10");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_12");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_13");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_14");
		ratkin_mage_hudSoundList.Add ("Ashram/Ratkin Mage/Move/drag_move_15");

		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_01");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_02");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_03");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_04");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_05");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_06");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_07");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_08");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_09");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_10");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_11");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_12");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_13");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_14");
		ratkin_shaman_hudSoundList.Add ("Ashram/Ratkin Shaman/Move/abad_move_15");


		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_01");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_02");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_03");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_04");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_05");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_06");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_07");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_08");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_09");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_10");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_11");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_12");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_13");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_14");
		ogre_warrior_hudSoundList.Add("Ashram/Ogre Warrior/Move/spir_move_15");

		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_01");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_02");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_03");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_04");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_05");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_06");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_07");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_08");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_09");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_10");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_11");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_12");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_13");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_14");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_15");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_16");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_17");
		ogre_rogue_hudSoundList.Add ("Ashram/Ogre Rogue/Move/riki_move_18");

		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_attack_01");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_attack_02");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_attack_03");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_attack_04");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_attack_05");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_attack_01");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_01");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_02");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_03");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_04");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_05");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_06");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_07");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_08");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_09");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_10");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_11");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_12");
		ogre_priest_hudSoundList.Add ("Ashram/Ogre Priest/Move/wdoc_move_13");

		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_01");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_02");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_03");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_04");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_05");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_06");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_07");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_08");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_09");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_10");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_11");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_12");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_13");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_14");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_15");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_16");
		ogre_mage_hudSoundList.Add ("Ashram/Ogre Mage/Move/shad_move_17");

		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispPissed1");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispPissed2");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispPissed3");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispReady1");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispWhat1");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispWhat2");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispWhat3");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispYes1");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispYes2");
		wisp_hudSoundList.Add ("Ashram/Wisp/Move/WispYes3");
	}

	/// <summary>
	/// Awake, Init this instance.
	/// </summary>
	void Awake()
	{
		instance = this;
		attrs = new PlayerAttributes();

		m_skillChargeBarGO.SetActive(false);
		InitAudioSound();

		// reset all existing player data for the match
		ExitGames.Client.Photon.Hashtable updatedPlayerData = new ExitGames.Client.Photon.Hashtable();
		updatedPlayerData.Add("match_level", attrs.CURRENT_LEVEL);
		updatedPlayerData.Add("match_kills", attrs.KILLS);
		updatedPlayerData.Add("match_deaths", attrs.DEATHS);
		updatedPlayerData.Add("match_isAlive", true);
		updatedPlayerData.Add("match_respawn_timer", 0);
		PhotonNetwork.SetPlayerCustomProperties(updatedPlayerData);
	}

	public void DisplayAttributesButton(bool isActive = true)
	{
		m_attributesStrengthButton.SetActive(isActive);
		m_attributesAgilityButton.SetActive(isActive);
		m_attributesIntelligenceButton.SetActive(isActive);
		m_attributesMagicButton.SetActive(isActive);
	}

	/// <summary>
	/// Adds the explored message.
	/// </summary>
	/// <param name="areaExplored">Area explored.</param>
	void AddExploredMessage(string areaExplored)
	{
		AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/(Exploration) HumanExploration", 1.0f);
		m_exploreMessage.AddExploredText(areaExplored);
	}

	public void DisplayRuinsOfAshram()
	{
		AddExploredMessage("Ruins of Ashram");
		isExploredSoundPlayed = true;
	}

	public void DisplayCryptsOfAshram()
	{
		AddExploredMessage("Crypts of Ashram");
		isExploredSoundPlayed = true;
	}

	/// <summary>
	/// Update logic that updates the ping timer as well as skill charging bar HUD
	/// </summary>
	float pingUpdateTimer = 5;
	void Update(){

		if(m_isSaturated)
		{
			if(creature != null)
			{
			   m_desatFX.enabled = false;
			   m_isSaturated = false;
			}
		} 
		else
		{
			if(creature == null)
			{
				// Hide skill casting
				if(m_skillChargeBarGO.activeSelf)
				{
					m_skillChargeBarGO.SetActive(false);
				}

				m_desatFX.enabled = true;
				m_isSaturated = true;
			}
		}

		// update ping every 5s
		pingUpdateTimer += Time.deltaTime;
		if(pingUpdateTimer > 5){
			ExitGames.Client.Photon.Hashtable updatedPlayerData = new ExitGames.Client.Photon.Hashtable();
			updatedPlayerData.Add("ping", PhotonNetwork.GetPing());
			PhotonNetwork.SetPlayerCustomProperties(updatedPlayerData);
			pingUpdateTimer = 0;
		}
		
		if(creature == null)
			return;
		else if(!initialised)
			initialised = true;

		// Skill currently is charging
		if(creature.isChargingSkill() && selectedSkill != null && !selectedSkill.isCharged())
		{
			float chargeTime = selectedSkill.getCurrentChargeTime();
			float maxChargeTime = selectedSkill.getMaxChargeTime();

			if(chargeTime > 0)
			{
				float percent = 1f - chargeTime/maxChargeTime;
				m_skillChargeBar.sliderValue = percent;
			}

			if(m_skillChargeBarGO.activeSelf == false)
			{
				m_skillChargeBarGO.SetActive(true);
			}

			m_skillChargeName.text = selectedSkill.SkillName + " " + chargeTime.ToString("f1");	// round to float 1 decimal place

			if(selectedSkill.SkillName == "Fireball" || selectedSkill.SkillName == "Infernal Scorch")
			{
				currentChargeSound = hudSoundDict["fire_cast"];
				AudioManager.Instance.Play(currentChargeSound, 1.0f);
			}
			else if(selectedSkill.SkillName == "Holy Light" || selectedSkill.SkillName == "Divine Heal")
			{
				currentChargeSound = hudSoundDict["heal_cast"];
				AudioManager.Instance.Play(currentChargeSound, 1.0f);
			}
			else if(selectedSkill.SkillName == "Gunner's Assassination" || selectedSkill.SkillName == "Teleport")
			{
				currentChargeSound = hudSoundDict["general_cast"];
				AudioManager.Instance.Play(currentChargeSound, 1.0f);
			}
			else if(selectedSkill.SkillName == "Arcane Blast")
			{
				currentChargeSound = hudSoundDict["arcane_cast"];
				AudioManager.Instance.Play(currentChargeSound, 1.0f);
			}
			else if(selectedSkill.SkillName == "Chain Lightning")
			{
				currentChargeSound = hudSoundDict["arcane_cast"];
				AudioManager.Instance.Play(currentChargeSound, 1.0f);
			}
			else if(selectedSkill.SkillName == "Thunder Clap")
			{
				currentChargeSound = hudSoundDict["arcane_cast"];
				AudioManager.Instance.Play(currentChargeSound, 1.0f);
			}
		}
		else if(m_skillChargeBarGO.activeSelf)
		{
			m_skillChargeBarGO.SetActive(false);
			AudioManager.Instance.Stop(currentChargeSound);
		}
	}

	/// <summary>
	/// Stops the player.
	/// </summary>
	public void StopPlayer()
	{
		if(creature != null)
			creature.StopLater();
	}

	/// <summary>
	/// Moves the creature to position.
	/// </summary>
	/// <param name="targetPosition">Target position.</param>
	public void MoveCreatureToPosition(Vector3 targetPosition)
	{
		if(creature != null){

			float timeElapsed = Time.time - hudMoveStartTime;

			// Reset the sound timer every 5 seconds
			if (timeElapsed >= hudMoveSoundTimerLimit)
			{
				hudMoveStartTime = Time.time;
				timeElapsed = 0;
				isMoveSoundPlaying = false;
			}
			else
			{
				isMoveSoundPlaying = true;
			}

			creature.MoveLater(targetPosition);

			// If sound is still playing, exit, otherwise play move sound
			if(isMoveSoundPlaying)
			{
				return;
			}

			PlayMovementSound();
		}
	}

	/// <summary>
	/// Plaies the movement sound.
	/// </summary>
	private void PlayMovementSound()
	{
		
		float soundVolume = 1.0f;
		
		if(creature.CRace == CreatureRace.RATKIN)
		{
			if(creature.CClass == CreatureClass.WARRIOR)
			{
				soundVolume = 0.6f;
				moveSoundList = ratkin_warrior_hudSoundList;
			}
			else if(creature.CClass == CreatureClass.GUNNER)
			{
				soundVolume = 0.6f;
				moveSoundList = ratkin_gunner_hudSoundList;
			}
			else if(creature.CClass == CreatureClass.MAGE)
			{
				soundVolume = 0.6f;
				moveSoundList = ratkin_mage_hudSoundList;
			}
			else if(creature.CClass == CreatureClass.SHAMAN)
			{
				soundVolume = 0.6f;
				moveSoundList = ratkin_shaman_hudSoundList;
			}
			
		} 
		else if(creature.CRace == CreatureRace.OGRE)
		{
			if(creature.CClass == CreatureClass.ROGUE)
			{
				soundVolume = 0.6f;
				moveSoundList = ogre_rogue_hudSoundList;
			}
			else if(creature.CClass == CreatureClass.PRIEST)
			{
				soundVolume = 0.6f;
				moveSoundList = ogre_priest_hudSoundList;
			}
			else if(creature.CClass == CreatureClass.MAGE)
			{
				soundVolume = 0.6f;
				moveSoundList = ogre_mage_hudSoundList;
			}
			else if(creature.CClass == CreatureClass.WARRIOR)
			{
				soundVolume = 0.6f;
				moveSoundList = ogre_warrior_hudSoundList;
			}
		}
		else if(creature.CRace == CreatureRace.WISP && creature.CClass == CreatureClass.WISP)
		{
			soundVolume = 0.8f;
			moveSoundList = wisp_hudSoundList;
		}
		
		AudioManager.Instance.PlayOnce(moveSoundList[Random.Range(0, moveSoundList.Count)], soundVolume);
	}

	/// <summary>
	/// Uses the skill effect, display the target marker to the creature or a point
	/// </summary>
	/// <param name="isAOE">If set to <c>true</c> is AO.</param>
	/// <param name="target">Target.</param>
	void UseSkillEffect(bool isAOE, object target){

		// Target Marker

		// remove any existing marker
		GameObject[] otherTargetMarkers = GameObject.FindGameObjectsWithTag("TargetMarker");
		foreach(GameObject otherTargetMarker in otherTargetMarkers)
			Destroy(otherTargetMarker);

		// create new marker
		GameObject targetMarker = null;
		if(target is Creature){
			Creature targetCreature = (Creature)(target);
			targetMarker = (GameObject)(Instantiate(m_targetMarker, targetCreature.transform.position, Quaternion.identity));
			targetMarker.GetComponent<TargetMarker>().setTarget(targetCreature.GetComponent<Creature>());
		} else if(target is Vector3){	// is an AOE
			Vector3 targetPoint = (Vector3)(target) + new Vector3(0,0.5f,0);
			targetMarker = (GameObject)(Instantiate(m_targetMarker, targetPoint + new Vector3(0,0.1f,0), Quaternion.identity));;
			targetMarker.GetComponent<TargetMarker>().setTarget(targetPoint);
		}

		// enlarge marker if AOE
		if(isAOE && targetMarker != null)
			targetMarker.transform.localScale = new Vector3(7.5f,0,7.5f);
	}

	/// <summary>
	/// Basics attack 
	/// </summary>
	/// <param name="targetCreature">Target creature.</param>
	public void BasicAttack(Transform targetCreature)
	{
		// special case : wisp don't have a basic attack
		if(creature.CRace == CreatureRace.WISP)
			return;

		if(creature != null && creature.getSkillList().Count > 1)
		{
			SkillUsingMessage message = creature.canUseSkill(creature.getSkillList()[1], targetCreature.GetComponent<Creature>());
			message = DisplayFeedbackMessage(message);
			
			if(message == SkillUsingMessage.SUCCESS)
			{
				Skill skill = creature.getSkillList()[1];
				UseSkillEffect(false, targetCreature.GetComponent<Creature>());
				creature.UseSkillLater(skill, targetCreature.GetComponent<Creature>());
			}
		}
	}

	/// <summary>
	/// Uses the possess release skill_ at point.
	/// </summary>
	/// <param name="selectedPoint">Selected point.</param>
	public void UsePossessReleaseSkill_AtPoint(Vector3 selectedPoint)
	{
		if(creature != null)
		{
			SkillUsingMessage message = creature.canUseSkill(skillPossessRelease, (System.Object) selectedPoint);
			message = DisplayFeedbackMessage(message);

			if(message == SkillUsingMessage.SUCCESS)
			{
				UseSkillEffect(false, selectedPoint);
				creature.UseSkillLater(skillPossessRelease, (System.Object) selectedPoint);
				selectedSkill = skillPossessRelease;
			}
		}
	}

	/// <summary>
	/// Uses the special skill_1_ at point.
	/// </summary>
	/// <param name="selectedPoint">Selected point.</param>
	public void UseSpecialSkill_1_AtPoint(Vector3 selectedPoint)
	{
		if(creature != null)
		{
			SkillUsingMessage message = creature.canUseSkill(skill1, (System.Object) selectedPoint);
			message = DisplayFeedbackMessage(message);

			if(message == SkillUsingMessage.SUCCESS)
			{
				UseSkillEffect(skill1.isAOE(), selectedPoint);
				creature.UseSkillLater(skill1, (System.Object) selectedPoint);
				selectedSkill = skill1;
			}
		}
	}

	/// <summary>
	/// Uses the special skill_2_ at point.
	/// </summary>
	/// <param name="selectedPoint">Selected point.</param>
	public void UseSpecialSkill_2_AtPoint(Vector3 selectedPoint)
	{
		if(creature != null)
		{
			SkillUsingMessage message = creature.canUseSkill(skill2, (System.Object) selectedPoint);
			message = DisplayFeedbackMessage(message);

			if(message == SkillUsingMessage.SUCCESS)
			{
				UseSkillEffect(skill2.isAOE(), selectedPoint);
				creature.UseSkillLater(skill2, (System.Object) selectedPoint);
				selectedSkill = skill2;
			}
		}
	}

	/// <summary>
	/// Uses the special skill_3_ at point.
	/// </summary>
	/// <param name="selectedPoint">Selected point.</param>
	public void UseSpecialSkill_3_AtPoint(Vector3 selectedPoint)
	{
		if(creature != null)
		{
			SkillUsingMessage message = creature.canUseSkill(skill3, (System.Object) selectedPoint);
			message = DisplayFeedbackMessage(message);

			if(message == SkillUsingMessage.SUCCESS)
			{
				UseSkillEffect(skill3.isAOE(), selectedPoint);
				creature.UseSkillLater(skill3, (System.Object) selectedPoint);
				selectedSkill = skill3;
			}
		}
	}

	/// <summary>
	/// Uses the possess release skill.
	/// </summary>
	/// <param name="selectedCreature">Selected creature.</param>
	/// <param name="needToSelectTarget">If set to <c>true</c> need to select target.</param>
	public void UsePossessReleaseSkill(Creature selectedCreature = null, bool needToSelectTarget = false)
	{
		if(creature != null)
		{
			if(needToSelectTarget == false)
			{
				Debug.LogWarning ("Skill: "+skillPossessRelease.SkillName+" is executed on release!");
				//creature.UseSkillLater(skillPossessRelease, creature);
				SkillUsingMessage message = creature.canUseSkill(skillPossessRelease, creature);
				message = DisplayFeedbackMessage(message);

				if(message == SkillUsingMessage.SUCCESS)
				{
					creature.UseSkillLater(skillPossessRelease, creature);
					selectedSkill = skillPossessRelease;
				}
			}
			else
			{
				Debug.LogWarning ("Skill: "+skillPossessRelease.SkillName+" is executed on SELECT!");

				SkillUsingMessage message = creature.canUseSkill(skillPossessRelease, selectedCreature);
				message = DisplayFeedbackMessage(message);
				
				if(message == SkillUsingMessage.SUCCESS)
				{
					UseSkillEffect(false, selectedCreature);
					creature.UseSkillLater(skillPossessRelease, selectedCreature);
					selectedSkill = skillPossessRelease;
				}
			}
		}
	}

	/// <summary>
	/// Uses the special skill_1.
	/// </summary>
	/// <param name="selectedCreature">Selected creature.</param>
	/// <param name="needToSelectTarget">If set to <c>true</c> need to select target.</param>
	public void UseSpecialSkill_1(Creature selectedCreature = null, bool needToSelectTarget = false)
	{
		if(creature != null)
		{
			if(needToSelectTarget == false)
			{
				Debug.LogWarning ("Skill: "+skill1.SkillName+" is executed on release!");
				//creature.UseSkillLater(skill1, creature);

				SkillUsingMessage message = creature.canUseSkill(skill1, creature);
				message = DisplayFeedbackMessage(message);
				
				if(message == SkillUsingMessage.SUCCESS)
				{
					creature.UseSkillLater(skill1, creature);
					selectedSkill = skill1;
				}
			}
			else
			{
				Debug.LogWarning ("Skill: "+skill1.SkillName+" is executed on SELECT!");
				//creature.UseSkillLater(skill1, selectedCreature);

				SkillUsingMessage message = creature.canUseSkill(skill1, selectedCreature);
				message = DisplayFeedbackMessage(message);
				
				if(message == SkillUsingMessage.SUCCESS)
				{
					UseSkillEffect(skill1.isAOE(), selectedCreature);
					creature.UseSkillLater(skill1, selectedCreature);
					selectedSkill = skill1;
				}
			}
		}
	}

	/// <summary>
	/// Uses the special skill_2.
	/// </summary>
	/// <param name="selectedCreature">Selected creature.</param>
	/// <param name="needToSelectTarget">If set to <c>true</c> need to select target.</param>
	public void UseSpecialSkill_2(Creature selectedCreature = null, bool needToSelectTarget = false)
	{
		if(creature != null)
		{
			if(needToSelectTarget == false)
			{
				Debug.LogWarning("Skill: "+skill2.SkillName+" is executed on release!");
				//creature.UseSkillLater(skill2, creature);

				SkillUsingMessage message = creature.canUseSkill(skill2, creature);
				message = DisplayFeedbackMessage(message);
				
				if(message == SkillUsingMessage.SUCCESS)
				{
					creature.UseSkillLater(skill2, creature);
					selectedSkill = skill2;
				}
			}
			else
			{
				Debug.LogWarning("Skill: "+skill2.SkillName+" is executed on SELECT!");
				//creature.UseSkillLater(skill2, selectedCreature);

				SkillUsingMessage message = creature.canUseSkill(skill2, selectedCreature);
				message = DisplayFeedbackMessage(message);
				
				if(message == SkillUsingMessage.SUCCESS)
				{
					UseSkillEffect(skill2.isAOE(), selectedCreature);
					creature.UseSkillLater(skill2, selectedCreature);
					selectedSkill = skill2;
				}
			}
		}
	}

	/// <summary>
	/// Uses the special skill_3.
	/// </summary>
	/// <param name="selectedCreature">Selected creature.</param>
	/// <param name="needToSelectTarget">If set to <c>true</c> need to select target.</param>
	public void UseSpecialSkill_3(Creature selectedCreature = null, bool needToSelectTarget = false)
	{
		if(creature != null)
		{
			if(needToSelectTarget == false)
			{
				Debug.LogWarning ("Skill: "+skill3.SkillName+" is executed on release!");
				//creature.UseSkillLater(skill3, creature);

				SkillUsingMessage message = creature.canUseSkill(skill3, creature);
				message = DisplayFeedbackMessage(message);
				
				if(message == SkillUsingMessage.SUCCESS)
				{
					creature.UseSkillLater(skill3, creature);
					selectedSkill = skill3;
				}
			}
			else
			{
				Debug.LogWarning ("Skill: "+skill3.SkillName+" is executed on SELECT!");
				//creature.UseSkillLater(skill3, selectedCreature);

				SkillUsingMessage message = creature.canUseSkill(skill3, selectedCreature);
				message = DisplayFeedbackMessage(message);
				
				if(message == SkillUsingMessage.SUCCESS)
				{
					UseSkillEffect(skill3.isAOE(), selectedCreature);
					creature.UseSkillLater(skill3, selectedCreature);
					selectedSkill = skill3;
				}
			}
		}
	}

	/// <summary>
	/// Displays HUD error, feedback message. If there is error, otherwise no error message
	/// </summary>
	/// <returns>The feedback message.</returns>
	/// <param name="message">Message.</param>
	public SkillUsingMessage DisplayFeedbackMessage(SkillUsingMessage message)
	{
		if(message == SkillUsingMessage.SUCCESS)
		{
			// skill is used successfully
			//Debug.LogWarning("Skill is used successfully!!");
			return SkillUsingMessage.SUCCESS;
		}
		else if(message == SkillUsingMessage.INVALID_TARGET)
		{
			m_errorMessage.AddErrorMessage("Invalid Target.");
			AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/(HUD Spell_Fail) Error", 1.0f);
			return SkillUsingMessage.INVALID_TARGET;
		}
		else if(message == SkillUsingMessage.IS_PASSIVE)
		{
			m_errorMessage.AddErrorMessage("This is a Passive Skill!");
			AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/(HUD Spell_Fail) Error", 1.0f);
			return SkillUsingMessage.IS_PASSIVE;
		}
		else if(message == SkillUsingMessage.IS_STUNNED)
		{
			m_errorMessage.AddErrorMessage("Player is currently being stunned.");
			AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/(HUD Spell_Fail) Error", 1.0f);
			return SkillUsingMessage.IS_STUNNED;
		}
		else if(message == SkillUsingMessage.NOT_COOLED)
		{
			m_errorMessage.AddErrorMessage("Skill is still on cooldown");
			AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/(HUD Spell_Fail) Error", 1.0f);
			return SkillUsingMessage.NOT_COOLED;
		}
		else if(message == SkillUsingMessage.NOT_ENOUGH_MP)
		{
			m_errorMessage.AddErrorMessage("Not enough mana.");
			AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/(HUD Spell_Fail) Error", 1.0f);
			return SkillUsingMessage.NOT_ENOUGH_MP;
		}

		return SkillUsingMessage.INVALID_TARGET;
	}


	/// <summary>
	/// The creature that take damage. Can be any creature.
	/// </summary>
	public void creatureTookDamage(Creature damagedCreature, int damage){

		//damagedCreature.gameObject.camera.

		if(damagedCreature == null)
		{
			Debug.LogError("Damaged creature is NULL");
			return;
		}

		// Game Object not being rendered in player's camera

		Renderer renderChild = damagedCreature.gameObject.GetComponentInChildren<Renderer>();
		if(renderChild.isVisible == false)
		{
			return;
		}

		// Display the HUD text on the creature that is damaged
		HUDDisplayText damageText = damagedCreature.gameObject.GetComponent<HUDDisplayText>();

		if(damageText != null)
		{
			if(damage > 0)
			{
				damageText.AddDamageText(damage);
			}
			else if(damage == 0)
			{
				damageText.AddAbsorbText();
			}
			else
			{
				damageText.AddEvasionText();
			}
		}
	}

	/// <summary>
	/// Player creature gain health
	/// </summary>
	public void creatureGainHealth(Creature healedCreature, int gain){
		if(healedCreature == null)
		{
			Debug.LogError("Healed creature is NULL");
			return;
		}
		
		// Display the HUD text on the creature that is damaged
		HUDDisplayText healText = healedCreature.gameObject.GetComponent<HUDDisplayText>();
		
		if(healText != null)
		{
			healText.AddHealText(gain);
		}
	}

	/// <summary>
	/// Check whether I win the game, e.g. for death-match is top-scorer wins. Team-deathmatch is team with most kills will win
	/// </summary>
	/// <returns><c>true</c>, if top scorer was ised, <c>false</c> otherwise.</returns>
	private bool isWinner()
	{
		int maxKill = -999;
		int myPlayerKill = 0;
		Team teamWithMostKills = Team.ONE;
		Team playerTeam = Team.ONE;

		bool isDeathMatch = true;
		bool isTopScorer = false;

		foreach(PhotonPlayer player in PhotonNetwork.playerList)
		{
			ExitGames.Client.Photon.Hashtable playerData = player.customProperties;
			int kill = (int) playerData["match_kills"];
			Team gameMode = (Team) playerData["Team"];

			// Death-Match
			if(gameMode != null && gameMode == Team.ON_YOUR_OWN)
			{
				isDeathMatch = true;

				if(kill >= maxKill)
					maxKill = kill;
				
				if(player.name == PhotonNetwork.playerName)
					myPlayerKill = kill;
			}
			// Team Death-Match
			else if(gameMode != null && gameMode == Team.ONE)
			{
				isDeathMatch = false;

				if(player.name == PhotonNetwork.playerName)
				{
					playerTeam = Team.ONE;
				}

				if(kill >= maxKill)
				{
					maxKill = kill;
					teamWithMostKills = Team.ONE;
				}
			}
			else if(gameMode != null && gameMode == Team.TWO)
			{
				isDeathMatch = false;

				if(player.name == PhotonNetwork.playerName)
				{
					playerTeam = Team.TWO;
				}

				if(kill >= maxKill)
				{
					maxKill = kill;
					teamWithMostKills = Team.TWO;
				}
			}
		}

		if(isDeathMatch)
		{
			return (myPlayerKill == maxKill);
		}
		else
		{
			return (playerTeam == teamWithMostKills);
		}
	}

	/// <summary>
	/// In-charge of updating the winrate after the game ends
	/// </summary>
	public void DoUpdateWinRate()
	{
		ExitGames.Client.Photon.Hashtable playerData = PhotonNetwork.player.customProperties;
		string playerEmail = playerData["email"].ToString();
		StartCoroutine( DoWinRate(playerEmail) );
	}

	/// <summary>
	/// Updating of win-rate with the server
	/// </summary>
	/// <returns>The window rate.</returns>
	/// <param name="playerEmail">Player email.</param>
	IEnumerator DoWinRate(string playerEmail) 
	{
		string DB_UPDATEPROFILE_URL = "http://ashram.net78.net/ashram/register/do-win_rate.php"; 
		
		//email, win, lose, win_rate, exp
		
		// after every match increment exp by 100
		Player.EXP += 100;
		
		if( isWinner() )
			Player.WINCOUNT++;
		else
			Player.LOSECOUNT++;
		
		// calculate win_rate
		float total_matches_played = Player.WINCOUNT + Player.LOSECOUNT;
		float win_ratio = 0.0f;
		if(total_matches_played > 0f)
		{
			win_ratio = Player.WINCOUNT / total_matches_played * 100f;
		}
		
		WWWForm form = new WWWForm(); //here you create a new form connection
		form.AddField( "email", playerEmail );
		form.AddField( "win", Player.WINCOUNT.ToString() );
		form.AddField( "lose", Player.LOSECOUNT.ToString() );
		form.AddField( "win_rate", win_ratio.ToString() );
		form.AddField( "exp", Player.EXP.ToString() );
		
		WWW updateResult = new WWW(DB_UPDATEPROFILE_URL, form); //here we create a var called 'loginResult' and we sync with our URL and the form
		yield return updateResult; //we wait for the form to check the PHP file, so our game dont just hang
		
		string resultMessage = "";
		
		if ( !string.IsNullOrEmpty(updateResult.error) )
		{
			//Debug.LogError("Network Error: "+updateResult.error); //if there is an error, tell us
		}
		else  // Either successfully login or user pw does not match
		{	
			string php_result_text = updateResult.data; //here we return the data our PHP told us
			updateResult.Dispose(); 				//clear our form in game
			
			//Debug.LogError("Result Text from PHP: "+php_result_text);
			
			if(php_result_text.Contains ("win rate updated"))
			{
				// win rate is successfully updated
			}
			else
			{
				// win-rate updating is fail
			}
		}
	}
	
	/// <summary>
	/// Shows the end score board, when the game ends
	/// </summary>
	public void showEndScoreBoard()
	{
		if(isGameEnded == false)
		{
			AudioManager.Instance.PlayOnce("Ashram/User Interface/HUD/PVPVictoryAllianceMono (Ratkin Victory)", 0.8f);
			isGameEnded = true;

			DoUpdateWinRate();
		}
	}

	void OnGUI(){

		// show score board upon pressing space
		if(Input.GetKey(KeyCode.Tab) || isGameEnded)
		{
			m_hudHandler.DisplayScoreBoard(isGameEnded);
		}
		else if(Input.GetKeyUp(KeyCode.Tab))
		{
			m_hudHandler.HideScoreBoard();
		}


		if(creature == null)
			return;
		
		// skill effects test ----------------------------------
		
		statusCounter = 0;
		if(creature.hasStatusEffectNow(StatusEffect.STUN))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.SLOW))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.ORGE_AURA))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.MANA_SHIELD))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.FREEZING_SHIELD))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.EARTH_SHIELD))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.BATTLE_ROAR))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.SHIELD_BARRIER))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.ASSASSIN_LOCK))
			statusCounter++;
		if(creature.hasStatusEffectNow(StatusEffect.RATKIN_EYE))
			statusCounter++;
		
		// STUN
		if(creature.hasStatusEffectNow(StatusEffect.STUN))
		{
			UpdateStatusEffect(StatusEffect.STUN, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.STUN);
		
		// SLOW
		if(creature.hasStatusEffectNow(StatusEffect.SLOW))
		{
			UpdateStatusEffect(StatusEffect.SLOW, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.SLOW);
		
		// OGRE AURA (Cannot activate from here cos' it's passive)
		if(creature.hasStatusEffectNow(StatusEffect.ORGE_AURA))
		{
			UpdateStatusEffect(StatusEffect.ORGE_AURA, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.ORGE_AURA);
		
		// MANA SHIELD
		if(creature.hasStatusEffectNow(StatusEffect.MANA_SHIELD))
		{
			UpdateStatusEffect(StatusEffect.MANA_SHIELD, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.MANA_SHIELD);
		
		// FREEZING SHIELD
		if(creature.hasStatusEffectNow(StatusEffect.FREEZING_SHIELD))
		{
			UpdateStatusEffect(StatusEffect.FREEZING_SHIELD, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.FREEZING_SHIELD);
		
		// EARTH SHIELD
		if(creature.hasStatusEffectNow(StatusEffect.EARTH_SHIELD))
		{
			UpdateStatusEffect(StatusEffect.EARTH_SHIELD, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.EARTH_SHIELD);
		
		// BATTLE ROAR
		if(creature.hasStatusEffectNow(StatusEffect.BATTLE_ROAR))
		{
			UpdateStatusEffect(StatusEffect.BATTLE_ROAR, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.BATTLE_ROAR);
		
		// SHIELD BARRIER
		if(creature.hasStatusEffectNow(StatusEffect.SHIELD_BARRIER))
		{
			UpdateStatusEffect(StatusEffect.SHIELD_BARRIER, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.SHIELD_BARRIER);
		
		// ASSASSIN LOCK
		if(creature.hasStatusEffectNow(StatusEffect.ASSASSIN_LOCK))
		{
			UpdateStatusEffect(StatusEffect.ASSASSIN_LOCK, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.ASSASSIN_LOCK);
		
		// RATKIN EYE
		if(creature.hasStatusEffectNow(StatusEffect.RATKIN_EYE))
		{
			UpdateStatusEffect(StatusEffect.RATKIN_EYE, creature);
		}
		else
			DeleteStatusEffect(StatusEffect.RATKIN_EYE);


		// skill effects test ----------------------------------
		
		// STUN
		//if(creature.hasStatusEffectNow(StatusEffect.STUN))
		//GUI.Label(new Rect(20,300,350,20), "STUN");
	}

	/// <summary>
	/// Deletes the status effect of the player
	/// </summary>
	/// <param name="statusEffect">Status effect.</param>
	private void DeleteStatusEffect(StatusEffect statusEffect)
	{
		if( statusEffectDict.ContainsKey(statusEffect.ToString()) )
		{
			GameObject statusEffectInstance = statusEffectDict[statusEffect.ToString ()];
			statusEffectDict.Remove(statusEffect.ToString());
			Destroy(statusEffectInstance);
		}
	}

	/// <summary>
	/// Display the status effect text of the creature
	/// </summary>
	/// <param name="currentCreature">Current creature.</param>
	/// <param name="statusText">Status text.</param>
	private void AddStatusEffectText(Creature currentCreature, string statusText)
	{
		// Display the HUD text on the creature that is damaged
		HUDDisplayText displayText = currentCreature.gameObject.GetComponent<HUDDisplayText>();
		
		if(displayText != null)
		{
			displayText.AddStatusText(statusText);
		}
	}

	/// <summary>
	/// Updates the status effect of the player E.g. (Stun, Slow, Ogre Aura etc)
	/// </summary>
	/// <param name="statusEffect">Status effect.</param>
	/// <param name="currentCreature">Current creature.</param>
	private void UpdateStatusEffect(StatusEffect statusEffect, Creature currentCreature)
	{
		if(statusCounter == 0)
			return;

		string spriteName = "";
		string statusText = "";
		float maxDuration = 10f;

		if(statusEffect == StatusEffect.ASSASSIN_LOCK)
		{
			spriteName = "Status_Assassinlock";
			statusText = "Assassin Lock";
			maxDuration = 5f;
		}
		else if(statusEffect == StatusEffect.BATTLE_ROAR)
		{
			spriteName = "Status_Battleroar";
			statusText = "Battle Roar";
			maxDuration = 30f;
		}
		else if(statusEffect == StatusEffect.EARTH_SHIELD)
		{
			spriteName = "Status_Earthshield";
			statusText = "Earth Shield";
			maxDuration = 30f;
		}
		else if(statusEffect == StatusEffect.FREEZING_SHIELD)
		{
			spriteName = "Status_Freezingshield";
			statusText = "Freezing Shield";
			maxDuration = 30f;	// level increase +2s duration
		}
		else if(statusEffect == StatusEffect.MANA_SHIELD)
		{
			spriteName = "Status_Manashield";
			statusText = "Mana Shield";
			maxDuration = 20f;
		}
		else if(statusEffect == StatusEffect.ORGE_AURA)
		{
			spriteName = "Status_Ogreaura";
			statusText = "Ogre Aura";
			maxDuration = -1; // -1, the label will be empty text ""
		}
		else if(statusEffect == StatusEffect.RATKIN_EYE)
		{
			spriteName = "Status_Ratkineye";
			statusText = "Ratkin Eye";
			maxDuration = 10f;		// level increase +2s duration
		}
		else if(statusEffect == StatusEffect.SHIELD_BARRIER)
		{
			spriteName = "Status_Shieldbarrier";		// duration 30s
			statusText = "Shield Barrier";
			maxDuration = 30f;
		}
		else if(statusEffect == StatusEffect.SLOW)
		{
			spriteName = "Status_Slow";
			statusText = "Slow";
			maxDuration = 5f;
		}
		else if(statusEffect == StatusEffect.STUN)
		{
			spriteName = "Stun";
			statusText = "Stun";
			maxDuration = 5f;		// 5 second stun from assasin lock
		}

		if(statusEffectDict.ContainsKey(statusEffect.ToString()) == false)
		{
			GameObject statusEffectInstance = NGUITools.AddChild (m_statusEffectHUDParent, m_statusEffectTemplate);
			statusEffectInstance.name = statusEffect.ToString();

			Vector3 statusVector = new Vector3(-200f + statusCounter * 40f, -295f, 0f);

			statusEffectInstance.transform.localPosition = statusVector;
			statusEffectDict.Add(statusEffect.ToString(), statusEffectInstance);
			string statusTooltipName = spriteName.Replace("Status_", "");
			statusEffectInstance.GetComponent<StatusEffectButton>().SetName(statusTooltipName);
			statusEffectInstance.GetComponent<StatusEffectButton>().setSprite(spriteName);
			statusEffectInstance.GetComponent<StatusEffectButton>().maxCooldown = maxDuration;

			AddStatusEffectText(currentCreature,  statusText);
		}
		else
		{
			GameObject statusEffectInstance = statusEffectDict[statusEffect.ToString ()];
			if(statusEffectInstance != null)
			{
				float currentCooldown = currentCreature.getStatusDuration(statusEffect);
				statusEffectInstance.GetComponent<StatusEffectButton>().setCurrentCooldown(currentCooldown);
			}
		}
	}

	/// <summary>
	/// Sets the creature that the player is controlling to the given creature.
	/// </summary>
	public void setControlledCreature(Creature creature){

		// used to control no creature, then if set to one,
		// that means it has respawned. status is synchronised to all other clients.
		if(this.creature == null){
			ExitGames.Client.Photon.Hashtable updatedPlayerData = new ExitGames.Client.Photon.Hashtable();
			updatedPlayerData.Add("match_isAlive", true);
			PhotonNetwork.SetPlayerCustomProperties(updatedPlayerData);
		}
		
		creature.IsMine = true;
		creature.name = "Possessed " + creature.name;	// not used, for debugging only
		
		skillPossessRelease = creature.getSkillList()[0];	// first skill is always possess/release
		int numSkills = creature.getSkillList().Count;
		if(creature.CRace == CreatureRace.WISP){
			skill1 = creature.getSkillList()[1];	// wisp doesn't have a basic attack
			skill2 = null;
			skill3 = null;
		} else {
			skill1 = (numSkills >= 3)? creature.getSkillList()[2] : null;
			skill2 = (numSkills >= 4)? creature.getSkillList()[3] : null;
			skill3 = (numSkills >= 5)? creature.getSkillList()[4] : null;
		}
		this.creature = creature;

		lastHoveredCreature = creature;
	}

	/// <summary>
	/// Killed a creature and gains experience
	/// </summary>
	public void madeKill(int exp, bool victimIsPlayer)
	{
		if(creature == null)
		{
			Debug.LogError("Creature is NULL");
			return;
		}

		HUDDisplayText hudDisplayText = creature.gameObject.GetComponent<HUDDisplayText>();
		if(hudDisplayText != null)
		{
			hudDisplayText.AddExpGainedText(exp);
		}

		// up kill if player
		if(victimIsPlayer)
			attrs.IncrementKills();

		bool isLevelUp = attrs.GainEXP(exp);
		if(isLevelUp)
		{
			creature.RecomputePossessedStatsLater();

			// level up FX
			GameObject goPrefab = (GameObject)(Resources.Load("Prefab/StatusEffects/LevelUp"));
			GameObject fx = (GameObject)(GameObject.Instantiate(goPrefab, creature.transform.position, creature.transform.rotation));
			fx.transform.parent = creature.transform;

			// levelling up sound
			AudioManager.Instance.PlayOnce("Creature/090_levelup", creature, 1.0f);
			AudioManager.Instance.PlayOnce("Creature/5502_Tutorial_LevelUp", 0.6f);


			// Update players level on HUD
			m_hudHandler.UpdatePlayersLevel(attrs.CURRENT_LEVEL.ToString());

			// Display attributes button
			DisplayAttributesButton();
		}

		// Update the EXP bar, after attributes has gain the exp
		m_hudHandler.UpdateExpBar(attrs.CURRENT_EXP, attrs.MAX_EXP);

		// synchronise new kills and levels with other players
		ExitGames.Client.Photon.Hashtable updatedPlayerData = new ExitGames.Client.Photon.Hashtable();
		updatedPlayerData.Add("match_kills", attrs.KILLS);			// update kills made
		updatedPlayerData.Add("match_level", attrs.CURRENT_LEVEL);	// update level
		PhotonNetwork.SetPlayerCustomProperties(updatedPlayerData);
	}

	/// <summary>
	/// Triggers upon death
	/// </summary>
	public void Dying(){

		// up death
		attrs.IncrementDeaths();

		// synchronise new death count with other players
		ExitGames.Client.Photon.Hashtable updatedPlayerData = new ExitGames.Client.Photon.Hashtable();
		updatedPlayerData.Add("match_deaths", attrs.DEATHS);	// update deaths made
		updatedPlayerData.Add("match_isAlive", false);
		PhotonNetwork.SetPlayerCustomProperties(updatedPlayerData);
	}

	/// <summary>
	/// Adds 1 point to player's STR attribute
	/// </summary>
	public void incrementStrength(){
		attrs.IncrementStrength();
		creature.RecomputePossessedStatsLater();
	}

	/// <summary>
	/// Adds 1 point to player's INT attribute
	/// </summary>
	public void incrementIntelligence(){
		attrs.IncrementInt();;
		creature.RecomputePossessedStatsLater();
	}


	/// <summary>
	/// Adds 1 point to player's AGI attribute
	/// </summary>
	public void incrementAgility(){
		attrs.IncrementAgility();
		creature.RecomputePossessedStatsLater();
	}

	/// <summary>
	/// Adds 1 point to player's SKI attribute
	/// </summary>
	public void incrementSkillMastery(){
		attrs.IncrementSkillMastery();
		creature.RecomputePossessedStatsLater();
	}

	/// <summary>
	/// Gets the name of the player.
	/// </summary>
	public string getPlayerName(){
		return PhotonNetwork.playerName;
	}

	/// <summary>
	/// Gets the player attributes (STR, INT, AGI, SKI, LVL, EXP)
	/// </summary>
	public PlayerAttributes getPlayerAttributes(){
		return attrs;
	}

	/// <summary>
	/// Checks if the given creature is the one that the player is controlling
	/// </summary>
	public bool isControlling(Creature creature){
		return this.creature == creature;
	}

	/// <summary>
	/// Returns the creature that the player is controlling
	/// </summary>
	public Creature getControlledCreature(){
		return creature;
	}

	/// <summary>
	/// Checks if the player is alive
	/// </summary>
	public bool isAlive(){
		return creature != null;
	}

	/// <summary>
	/// Displays the combat message, when a player kills another player
	/// </summary>
	/// <param name="attackerName">Attacker name.</param>
	/// <param name="msg">Message.</param>
	/// <param name="victimName">Victim name.</param>
	public void DisplayCombatMessage(string attackerName, string msg, string victimName)
	{
		string displayText = "";
		if(creature != null)
		{

			Debug.LogWarning(attackerName + " " + msg + " " + victimName);
			//m_respawnMessage.AddKillText(displayText);

			if(victimName == "You")
			{
				AudioManager.Instance.PlayOnce("Creature/0046_female1_OnChampionKillHeroYou", 0.6f);

				if(m_respawnPanel != null){
					m_respawnPanel.SetActive(true);
					displayText = victimName + " " + msg + " " + TextUtil.UppercaseFirst(attackerName);
					m_killText.text = displayText;
				}
			}
			else
			{
				AudioManager.Instance.PlayOnce("Creature/0042_female1_OnChampionKillHeroHer", 0.6f);

				displayText = attackerName + " " + msg + " " + victimName;
				m_combatLogMessage.AddKillText(displayText);			// Display kill message of who kills who
			}
		}
	}

	/// <summary>
	/// Display the respawn text given by player
	/// </summary>
	/// <param name="secondsToRespawn">Seconds to respawn.</param>
	public void DisplayRespawnText(int secondsToRespawn)
	{
		m_respawnPanel.SetActive(true);
		m_respawnText.text = "Respawn in " + secondsToRespawn;
	}

	/// <summary>
	/// Hides the respawn message.
	/// </summary>
	public void HideRespawnMessage()
	{
		m_respawnPanel.SetActive(false);
	}
}
