﻿using UnityEngine;
using System.Collections;

public class Player {
	
	private static string m_playerName;
	private static string m_email;
	private static int m_numberOfWins;
	private static int m_numberOfLoses;
	private static double m_winRatio;
	private static int m_experience;
	private static int m_level;
	
	public static string USERNAME
    {
		set { m_playerName = value; }
        get { return m_playerName; }
    }
	
	public static string EMAIL
    {
		set { m_email = value; }
        get { return m_email; }
    }
	
	public static int WINCOUNT
    {
		set { m_numberOfWins = value; }
        get { return m_numberOfWins; }
    }
	
	public static int LOSECOUNT
    {
		set { m_numberOfLoses = value; }
        get { return m_numberOfLoses; }
    }
	
	public static double WINRATE
    {
		set { m_winRatio = value; }
        get { return m_winRatio; }
    }
	
	public static int EXP
    {
		set { m_experience = value; }
        get { return m_experience; }
    }
	
	public static int LEVEL
    {
		set { m_level = value; }
        get { return m_level; }
    }
	
	public static void UpdateUserProfile(string username, string email, string wins, string loses, string winrate, string exp)
	{
		//int memeValue;
		// attempt to parse the value using the TryParse functionality of the integer type
		//int.TryParse(sillyMeme, out memeValue);
		

		m_playerName = username;
		m_email = email;
		int.TryParse(wins, out m_numberOfWins);
		int.TryParse(loses, out m_numberOfLoses);
		double.TryParse(winrate, out m_winRatio);
		int.TryParse(exp, out m_experience);
		//m_level = level;
	}
}


