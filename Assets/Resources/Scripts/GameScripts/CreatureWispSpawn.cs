﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CreatureWispSpawn : CreatureSpawn {

	bool spawned;
	bool isActive;

	private float[] m_spawn_duration = {5f, 8f, 12f, 15f, 18f, 20f, 22f, 26f, 28f, 30f, 32f};		// this spawn duration is based on level e.g. first element is 5 seconds

	void Start(){
		renderer.enabled = false;

		// Set whether this spawnpoint belongs to this client
		// PhotonNetwork.player.id returns this player's ID of [1, maxPlayer]
		int spawnPointID;
		bool isDeathMatchSpawnPoint = false;
		if(!int.TryParse("" + gameObject.name[13], out spawnPointID)){	// is for team deathmatch
			int.TryParse("" + gameObject.name[15], out spawnPointID);	// is for deathmatch
			isDeathMatchSpawnPoint = true;
		}

		PhotonPlayer player = PhotonNetwork.player;
		Team playerTeam = (Team)(PhotonNetwork.player.customProperties["Team"]);

		// check if the spawnpoint is responsible for the team
		bool isResponsibleForTeam = false;

		if((!isDeathMatchSpawnPoint && playerTeam == Team.ONE && spawnPointID < 5) ||
		   (!isDeathMatchSpawnPoint && playerTeam == Team.TWO && spawnPointID >= 5) ||
		   (isDeathMatchSpawnPoint && playerTeam == Team.ON_YOUR_OWN))
			isResponsibleForTeam = true;

		if(!isResponsibleForTeam){
			enabled = false;
			return;
		}

		int playerTeamID = -1;
		if(isDeathMatchSpawnPoint)
			playerTeamID = PhotonNetwork.player.ID;
		else {

			if(playerTeam == Team.TWO)
				spawnPointID -= 4;

			List<int> teamIDs = new List<int>();

			// add IDs of all players in the same team
			teamIDs.Add(player.ID);
			foreach(PhotonPlayer otherPlayer in PhotonNetwork.otherPlayers){
				if((Team)(otherPlayer.customProperties["Team"]) == playerTeam)
					teamIDs.Add(otherPlayer.ID);
			}

			// sort in order of ID
			teamIDs.Sort();

			int teamID = 1;
			foreach(int id in teamIDs){
				if(player.ID == id){
					playerTeamID = teamID;
					break;
				}
				teamID++;
			}
		}

		if(playerTeamID != spawnPointID)
			enabled = false;
		else
			Debug.Log(gameObject.name);
	}

	/// <summary>
	/// Called when the spawnpoint is ready to spawn.
	/// </summary>
	public override void OnReadyToSpawn(){

		if(!enabled)	// if this client not in charge of this creature, ignore
			return;
	
		// create wisp
		Creator.Instance.createMyWispLater(this, transform.position, Quaternion.identity);
	}

	/// <summary>
	/// Called whenever a creature spawned by this spawnpoint dies.
	/// </summary>
	/// <param name="dyingCreature">Creature that is going to die</param>
	public override void OnMyCreatureDeath(Creature dyingCreature){

		if(!enabled)	// if this client not in charge of this creature, ignore
			return;
		// spawn after 10s
		StartCoroutine(SpawnWispAfterSomeTime());
	}


	// Edited by edwin, the spawning time is dependant on player's level

	/// <summary>
	/// Spawns the wisp after some time.
	/// </summary>
	/// <returns>The wisp after some time.</returns>
	IEnumerator SpawnWispAfterSomeTime()
	{
		float spawnTime = 10.0f;

		ExitGames.Client.Photon.Hashtable playerData = null;
	

		// Added by edwin, spawn wisp based on it's level and add a respawn timer to display when user presses tab
		foreach(PhotonPlayer player in PhotonNetwork.playerList)
		{
			playerData = player.customProperties;
			if(player.name == PhotonNetwork.playerName)
			{
				int level = (int) playerData["match_level"];

				if(level > 0 && level <= 10)
				{
					spawnTime = m_spawn_duration[level - 1];		// index starts from 0
					break;
				}
			}
		}

		for(int timer = (int) spawnTime; timer > 0; timer--)
		{
			if(playerData != null)
			{
				playerData["match_respawn_timer"] = timer;
				PhotonNetwork.SetPlayerCustomProperties(playerData);
			}

			PlayerCommand.Instance.DisplayRespawnText(timer);		// set timer left to respawn in HUD
			yield return new WaitForSeconds(1);
		}

		PlayerCommand.Instance.HideRespawnMessage();

		//yield return new WaitForSeconds(spawnTime); obsolete
		Creator.Instance.createMyWispLater(this, transform.position, Quaternion.identity);
	}
}