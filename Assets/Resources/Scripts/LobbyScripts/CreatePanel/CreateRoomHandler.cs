﻿
using UnityEngine;
using System.Collections;

/// <summary>
/// Create room handler that handles the event when user presses the Create Room button
/// </summary>
public class CreateRoomHandler : Photon.MonoBehaviour {

	[SerializeField] UIInput m_roomName;

	// Game Modes
	[SerializeField] RadioButtonsSelection m_deathMatch;
	[SerializeField] RadioButtonsSelection m_teamDeathMatch;
	[SerializeField] RadioButtonsSelection m_captureTheFlag;

	// Game Time
	[SerializeField] RadioButtonsSelection m_tenMin;
	[SerializeField] RadioButtonsSelection m_fifiteenMin;
	[SerializeField] RadioButtonsSelection m_twentyMin;

	// Game Map
	[SerializeField] RadioButtonsSelection m_ruinsOfAshram;
	[SerializeField] RadioButtonsSelection m_cryptsOfAshram;

	[SerializeField] PopupDialogHandler m_dialog;

	string roomName = "";
	string gameMode = "";
	int gameTime = 0;
	string gameMap = "";

	ExitGames.Client.Photon.Hashtable customPropertiesToSet = null;
	string[] customPropertiesForLobby = new string[22];

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		roomName = m_roomName.text;

		if(string.IsNullOrEmpty(roomName))
		{
			m_dialog.InitPopupBox("The room name cannot be empty");
			Debug.LogError("Room NAME IS EMPTY!!!");
			return;
		}

		if(m_deathMatch.isSelected)
		{
			gameMode = "Death-Match Classic";
		}
		else if(m_teamDeathMatch.isSelected)
		{
			gameMode = "Team Death-Match";
		}
		else if(m_captureTheFlag.isSelected)
		{
			gameMode = "Capture the Flag";
		}

		if(m_tenMin.isSelected)
		{
			gameTime = 10;
		}
		else if(m_fifiteenMin.isSelected)
		{
			gameTime = 15;
		}
		else if(m_twentyMin.isSelected)
		{
			gameTime = 20;
		}

		if(m_ruinsOfAshram.isSelected)
		{
			gameMap = "Ruins of Ashram";
		}
		else if(m_cryptsOfAshram.isSelected)
		{
			gameMap = "Crypts of Ashram";
		}

		// If i am currently in the lobby chat channel, leave that room before joining a new room
		/*if(PhotonNetwork.room != null && PhotonNetwork.room.name == TextUtil.ASHRAM_MAIN_CHANNEL)
		{
			PhotonNetwork.LeaveRoom();
		}*/

		if(PhotonNetwork.room == null)
		{
			SetRoomProperties();
			PhotonNetwork.CreateRoom(roomName, true, true, 8, customPropertiesToSet, customPropertiesForLobby);
		}
	}

	/// <summary>
	/// Sets the room properties.
	/// </summary>
	void SetRoomProperties()
	{
		customPropertiesToSet = new ExitGames.Client.Photon.Hashtable();
		customPropertiesToSet.Add ("room_name", roomName);
		customPropertiesToSet.Add ("game_mode", gameMode);
		customPropertiesToSet.Add ("game_map", gameMap);
		customPropertiesToSet.Add ("game_time", gameTime.ToString());
		customPropertiesToSet.Add ("no_of_players", "1");		// 1 player in room
		customPropertiesToSet.Add ("is_playing", false);
		
		// player list contains a list of players of string
		customPropertiesToSet.Add ("player_host", "");		
		customPropertiesToSet.Add ("player_1", "");	
		customPropertiesToSet.Add ("player_2", "");		
		customPropertiesToSet.Add ("player_3", "");		
		customPropertiesToSet.Add ("player_4", "");		
		customPropertiesToSet.Add ("player_5", "");	
		customPropertiesToSet.Add ("player_6", "");		
		customPropertiesToSet.Add ("player_7", "");	
		
		customPropertiesToSet.Add ("slot_1", "");
		customPropertiesToSet.Add ("slot_2", "");
		customPropertiesToSet.Add ("slot_3", "");
		customPropertiesToSet.Add ("slot_4", "");
		customPropertiesToSet.Add ("slot_5", "");
		customPropertiesToSet.Add ("slot_6", "");
		customPropertiesToSet.Add ("slot_7", "");
		customPropertiesToSet.Add ("slot_8", "");
		customPropertiesToSet.Add ("number_of_slots_taken", 0);
		
		customPropertiesForLobby = new string[23];
		customPropertiesForLobby[0] = "room_name";
		customPropertiesForLobby[1] = "game_mode";
		customPropertiesForLobby[2] = "game_map";
		customPropertiesForLobby[3] = "game_time";
		customPropertiesForLobby[4] = "no_of_players";
		customPropertiesForLobby[5] = "is_playing";
		
		// Player room info
		customPropertiesForLobby[6] = "player_host";
		customPropertiesForLobby[7] = "player_1";
		customPropertiesForLobby[8] = "player_2";
		customPropertiesForLobby[9] = "player_3";
		customPropertiesForLobby[10] = "player_4";
		customPropertiesForLobby[11] = "player_5";
		customPropertiesForLobby[12] = "player_6";
		customPropertiesForLobby[13] = "player_7";
		
		// Player room info
		customPropertiesForLobby[14] = "slot_1";
		customPropertiesForLobby[15] = "slot_2";
		customPropertiesForLobby[16] = "slot_3";
		customPropertiesForLobby[17] = "slot_4";
		customPropertiesForLobby[18] = "slot_5";
		customPropertiesForLobby[19] = "slot_6";
		customPropertiesForLobby[20] = "slot_7";
		customPropertiesForLobby[21] = "slot_8";
		customPropertiesForLobby[22] = "number_of_slots_taken";
	}

	/// <summary>
	/// Raises the created room event.
	/// </summary>
	void OnCreatedRoom()
	{
		Debug.Log("OnCreatedRoom: "+PhotonNetwork.room.customProperties.ToString());

		ExitGames.Client.Photon.Hashtable roomProperties = PhotonNetwork.room.customProperties;
	
		roomProperties["player_host"] = PhotonNetwork.playerName;

		PhotonNetwork.room.SetCustomProperties(roomProperties);

		// Set message queue to false before loading the scene
		//PhotonNetwork.isMessageQueueRunning = false;
		PhotonNetwork.LoadLevel("GameRoom");
	}

	void OnPhotonCreateRoomFailed()
	{
		m_dialog.InitPopupBox("There is already an existing room: " + roomName + ")");
	}
	
	void OnHover( bool isOver )
	{
	}
	
}
