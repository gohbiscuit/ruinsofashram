﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Radio buttons selection for Create Game Room panel
/// </summary>
public class RadioButtonsSelection : MonoBehaviour {

	// First and second button to disable for radio buttons selection
	[SerializeField] GameObject m_firstDisabledButton;
	[SerializeField] GameObject m_secondDisabledButton;
	
	float fade_alpha = 0.2f;

	public bool isSelected = false;

	// Use this for initialization
	void Start () {
	
		if(isSelected == false)
		{
			HideObject(0.01f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Hides the object.
	/// </summary>
	/// <param name="duration">Duration.</param>
	public void HideObject(float duration)
	{
		TweenAlpha.Begin(gameObject, duration, fade_alpha);
		isSelected = false;
	}

	/// <summary>
	/// Selects the object.
	/// </summary>
	public void SelectObject()
	{
		TweenAlpha.Begin(gameObject, 0.0f, 1.0f);
		isSelected = true;
	}

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		if(m_firstDisabledButton)
		{
			//m_firstDisabledButton.GetComponent<RadioButtonsSelection>().isSelected = false;
			//TweenAlpha.Begin(m_firstDisabledButton, 0.2f, fade_alpha);
			m_firstDisabledButton.GetComponent<RadioButtonsSelection>().HideObject(0.2f);
		}

		if(m_secondDisabledButton)
		{
			////m_secondDisabledButton.GetComponent<RadioButtonsSelection>().isSelected = false;
			//TweenAlpha.Begin(m_secondDisabledButton, 0.2f, fade_alpha);

			m_secondDisabledButton.GetComponent<RadioButtonsSelection>().HideObject(0.2f);
		}

		SelectObject();
	}

	/// <summary>
	/// Raises the hover event.
	/// </summary>
	/// <param name="isOver">If set to <c>true</c> is over.</param>
	void OnHover( bool isOver )
	{
		// Roll Out
		if( isOver == false )
		{
			if( !isSelected )
			{
				HideObject(0.2f);
			}
		}
	}
}
