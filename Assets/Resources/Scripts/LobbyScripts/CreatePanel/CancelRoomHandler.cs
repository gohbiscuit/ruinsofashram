﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Cancel room handler that handles the event for Cancel button when a player cancels the create game room process
/// </summary>
public class CancelRoomHandler : MonoBehaviour {

	[SerializeField] GameObject m_createRoomPanel;

	[SerializeField] GameObject m_playPanel;
	[SerializeField] GameObject m_playPanelBG;

	[SerializeField] UIInput m_roomName;
	
	// Game Modes
	[SerializeField] RadioButtonsSelection m_deathMatch;
	[SerializeField] RadioButtonsSelection m_teamDeathMatch;
	[SerializeField] RadioButtonsSelection m_captureTheFlag;
	
	// Game Time
	[SerializeField] RadioButtonsSelection m_tenMin;
	[SerializeField] RadioButtonsSelection m_fifiteenMin;
	[SerializeField] RadioButtonsSelection m_twentyMin;
	
	// Game Map
	[SerializeField] RadioButtonsSelection m_ruinsOfAshram;
	[SerializeField] RadioButtonsSelection m_cryptsOfAshram;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		// Reset the fields
		m_roomName.text = "";

		m_deathMatch.SelectObject();
		m_teamDeathMatch.HideObject(0.01f);
		m_captureTheFlag.HideObject(0.01f);

		m_tenMin.SelectObject();
		m_fifiteenMin.HideObject(0.01f);
		m_twentyMin.HideObject(0.01f);

		m_ruinsOfAshram.SelectObject();
		m_cryptsOfAshram.HideObject(0.01f);


		m_playPanel.SetActive(true);
		m_playPanelBG.SetActive(true);
		m_createRoomPanel.SetActive(false);
	}

	void OnHover()
	{
	}
}
