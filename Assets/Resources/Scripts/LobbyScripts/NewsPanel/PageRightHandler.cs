﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Page right handler class handles the right page arrow functionality
/// </summary>
public class PageRightHandler : MonoBehaviour {
	
	[SerializeField] int m_pageNum;
	[SerializeField] UIDraggablePanel m_dragPanel;
	
	float pageSize = 641.33333333f;		// width of the image by default 1080p is 962
	float dragStrength = 8f;

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		AudioManager.Instance.PlayOnce("Ashram/User Interface/(Panel Left-Right) Selection", 0.5f);
		Vector3 panelPos = m_dragPanel.transform.localPosition;
        panelPos.x += - (pageSize);
        SpringPanel.Begin(m_dragPanel.gameObject, panelPos, dragStrength);
	}
	
	/*void OnPress(bool isPressed)
    {
        if(!isPressed && (DragPanel != null))
        {
            Vector3 panelPos = DragPanel.localPosition;
            panelPos.x = getPage() * -pageSize;
            SpringPanel.Begin(DragPanel.gameObject, panelPos, 13f);
        }
    }*/

	void OnDrag()
	{
	}
	
	void OnHover(bool isOver)
	{
	}
}
