﻿using UnityEngine;
using System.Collections;

enum PANEL_TYPE {NEWS, PLAY, LEARN};

/// <summary>
/// Panel toggle handler that handles the toggling of panel
/// </summary>
public class PanelToggleHandler : MonoBehaviour {
	
	[SerializeField] PANEL_TYPE panelType;
	
	[SerializeField] UIImageButton m_newsButton;
	[SerializeField] UISprite m_newsSprite;
	[SerializeField] GameObject m_newsPanel;
	[SerializeField] GameObject m_newsPanelBG;	
	
	[SerializeField] UIImageButton m_playButton;
	[SerializeField] UISprite m_playSprite;
	[SerializeField] GameObject m_playPanel;
	[SerializeField] GameObject m_playPanelBG;
	
	[SerializeField] UIImageButton m_learnButton;
	[SerializeField] UISprite m_learnSprite;
	[SerializeField] GameObject m_learnPanel;
	[SerializeField] GameObject m_learnPanelBG;

	[SerializeField] GameObject m_createPanel;

	[SerializeField] GameObject m_playerListLoader;
	
	const string NEWS_SPRITE_NORMAL = "News_Btn_Normal"; 
	const string NEWS_SPRITE_HOVER = "News_Btn_Hover";
	
	const string PLAY_SPRITE_NORMAL = "Play_Btn_Normal"; 
	const string PLAY_SPRITE_HOVER = "Play_Btn_Hover"; 
	
	const string LEARN_SPRITE_NORMAL = "Learn_Btn_Normal"; 
	const string LEARN_SPRITE_HOVER = "Learn_Btn_Hover"; 

	private string toggleSound;

	/// <summary>
	/// Toggles the display.
	/// </summary>
	void ToggleDisplay()
	{
		// Toggle
		/*switch(panelType)
		{
			/*case PANEL_TYPE.NEWS:
			TogglePanel (panelType);
			JoinOrLeaveLobby(panelType);
			break;
			
			case PANEL_TYPE.PLAY:
			TogglePanel (panelType);
			JoinOrLeaveLobby(panelType);
			break;
			
			case PANEL_TYPE.LEARN:
			TogglePanel (panelType);
			JoinOrLeaveLobby(panelType);
			break;

			TogglePanel (panelType);
			JoinOrLeaveLobby(panelType);
		}*/

		TogglePanel (panelType);
		JoinOrLeaveLobby(panelType);
	}

	/// <summary>
	/// Toggles the learn panel
	/// </summary>
	void ToggleLearn()
	{
		toggleSound = (m_learnPanel.activeSelf == false) ? "Ashram/User Interface/(Panel Open) uCharacterSheetOpen" : "Ashram/User Interface/(Panel Close) uCharacterSheetClose";
		AudioManager.Instance.PlayOnce(toggleSound, 1.0f);

		m_learnPanel.SetActive( !m_learnPanel.activeSelf );
		m_learnPanelBG.SetActive( !m_learnPanelBG.activeSelf );
		m_learnPanel.GetComponent<UIDraggablePanel>().ResetPosition();
		
		m_learnButton.normalSprite = (m_learnPanel.activeSelf) ? LEARN_SPRITE_HOVER : LEARN_SPRITE_NORMAL;
		m_newsButton.normalSprite = NEWS_SPRITE_NORMAL;
		m_playButton.normalSprite = PLAY_SPRITE_NORMAL;
		
		m_newsSprite.spriteName = NEWS_SPRITE_NORMAL;
		m_playSprite.spriteName = PLAY_SPRITE_NORMAL;
		
		m_newsPanel.SetActive( false );
		m_newsPanelBG.SetActive( false );
		m_playPanel.SetActive( false );
		m_playPanelBG.SetActive( false );
		
		m_createPanel.SetActive( false );
	}

	/// <summary>
	/// Toggles the play panel
	/// </summary>
	void TogglePlay()
	{
		toggleSound = (m_playPanel.activeSelf == false) ? "Ashram/User Interface/(Panel Open) uCharacterSheetOpen" : "Ashram/User Interface/(Panel Close) uCharacterSheetClose";
		AudioManager.Instance.PlayOnce(toggleSound, 1.0f);

		m_playPanel.SetActive( !m_playPanel.activeSelf );
		m_playPanelBG.SetActive( !m_playPanelBG.activeSelf );
		
		m_playButton.normalSprite = (m_playPanel.activeSelf) ? PLAY_SPRITE_HOVER : PLAY_SPRITE_NORMAL;
		m_newsButton.normalSprite = NEWS_SPRITE_NORMAL;
		m_learnButton.normalSprite = LEARN_SPRITE_NORMAL;
		
		m_newsSprite.spriteName = NEWS_SPRITE_NORMAL;
		m_learnSprite.spriteName = LEARN_SPRITE_NORMAL;
		
		m_newsPanel.SetActive( false );
		m_newsPanelBG.SetActive( false );
		m_learnPanel.SetActive( false );
		m_learnPanelBG.SetActive( false );
		m_createPanel.SetActive( false );
	}

	/// <summary>
	/// Toggles the news panel
	/// </summary>
	void ToggleNews()
	{
		toggleSound = (m_newsPanel.activeSelf == false) ? "Ashram/User Interface/(Panel Open) uCharacterSheetOpen" : "Ashram/User Interface/(Panel Close) uCharacterSheetClose";
		AudioManager.Instance.PlayOnce(toggleSound, 1.0f);

		m_newsPanel.SetActive( !m_newsPanel.activeSelf );
		m_newsPanelBG.SetActive( !m_newsPanelBG.activeSelf );
		m_newsPanel.GetComponent<UIDraggablePanel>().ResetPosition();
		
		// Change sprite according to it's active
		m_newsButton.normalSprite = (m_newsPanel.activeSelf) ? NEWS_SPRITE_HOVER : NEWS_SPRITE_NORMAL;
		m_playButton.normalSprite = PLAY_SPRITE_NORMAL;
		m_learnButton.normalSprite = LEARN_SPRITE_NORMAL;
		
		m_playSprite.spriteName = PLAY_SPRITE_NORMAL;
		m_learnSprite.spriteName = LEARN_SPRITE_NORMAL;
		
		m_playPanel.SetActive( false );
		m_playPanelBG.SetActive( false );
		m_learnPanel.SetActive( false );
		m_learnPanelBG.SetActive( false );
		
		m_createPanel.SetActive( false );
	}

	/// <summary>
	/// Toggles the panel.
	/// </summary>
	/// <param name="panelType">Panel type.</param>
	void TogglePanel(PANEL_TYPE panelType)
	{
		switch(panelType)
		{
			case PANEL_TYPE.NEWS:
				ToggleNews();
				break;
				
			case PANEL_TYPE.PLAY:
				TogglePlay();
				break;
				
			case PANEL_TYPE.LEARN:
				ToggleLearn ();
				break;
		}
	}

	/// <summary>
	/// Joins the or leave lobby.
	/// </summary>
	/// <param name="panelType">Panel type.</param>
	void JoinOrLeaveLobby(PANEL_TYPE panelType)
	{
		// toggling between join lobby
		if(m_playPanel.activeSelf)
		{
			StartCoroutine( WaitToLeaveLobby(panelType) );
		}
		else
		{
			StartCoroutine( WaitToJoinLobby(panelType) );
		}
	}

	/// <summary>
	/// Waits to leave lobby.
	/// </summary>
	/// <returns>The to leave lobby.</returns>
	/// <param name="type">Type.</param>
	IEnumerator WaitToLeaveLobby(PANEL_TYPE type)
	{
		// wait until lobby is true
		while(m_playerListLoader.GetComponent<PlayerListLoader>().isInLobby == false)
		{
			yield return null;
		}
		
		m_playerListLoader.GetComponent<PlayerListLoader>().isSearchingForRoom = true;
		PhotonNetwork.LeaveRoom();

		// wait until I have left the lobby
		/*while(m_playerListLoader.GetComponent<PlayerListLoader>().isInLobby)
		{
			yield return null;
		}

		TogglePanel(type);*/
	}

	/// <summary>
	/// Waits to join lobby.
	/// </summary>
	/// <returns>The to join lobby.</returns>
	/// <param name="type">Type.</param>
	IEnumerator WaitToJoinLobby(PANEL_TYPE type)
	{
		// wait until I left the lobby
		while(m_playerListLoader.GetComponent<PlayerListLoader>().isInLobby)
		{
			yield return null;
		}
		
		// join lobby room
		m_playerListLoader.GetComponent<PlayerListLoader>().JoinMainChannelRoom();

		// Wait till i joined the lobby
		/*while(m_playerListLoader.GetComponent<PlayerListLoader>().isInLobby == false)
		{
			yield return null;
		}

		TogglePanel(type);

		//yield break;*/
	}
	
	void OnClick()
	{
		//Debug.Log("Clicked Object: "+gameObject.name);
		ToggleDisplay();
	}
	
	void OnHover(bool isOver)
	{
	}
}
