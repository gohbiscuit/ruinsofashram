﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Page left handler that is in-charge of left page button functionality
/// </summary>
public class PageLeftHandler : MonoBehaviour {
	
	[SerializeField] int m_pageNum;
	[SerializeField] UIDraggablePanel m_dragPanel;
	
	float pageSize = 641.33333333f;		// width of the image by default 1080p is 962
	float dragStrength = 8f;

	void OnClick()
	{

		AudioManager.Instance.PlayOnce("Ashram/User Interface/(Panel Left-Right) Selection", 0.5f);

		Vector3 panelPos = m_dragPanel.transform.localPosition;
        panelPos.x += (pageSize);
        SpringPanel.Begin(m_dragPanel.gameObject, panelPos, dragStrength);
	}
	
	void OnHover(bool isOver)
	{
	}
}
