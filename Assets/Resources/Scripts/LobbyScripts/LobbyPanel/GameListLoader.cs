﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Game list loader that is in-charge of loading and displaying of the available games hosted by the player
/// </summary>
public class GameListLoader : MonoBehaviour {

	[SerializeField] GameObject m_gameListGrid;
	[SerializeField] GameObject m_gameItemTemplate;
	
	// Use this for initialization
	void Start () 
	{
		/*AddGameToList(1, "Test Lobby", "Death-Match", "Ruins of Ashram", 1, 8, true);
		AddGameToList(2, "Test Lobby", "Death-Match", "Ruins of Ashram", 4, 8);
		AddGameToList(3, "Test Lobby", "Death-Match", "Ruins of Ashram", 5, 8);
		AddGameToList(4, "Test Lobby", "Death-Match", "Ruins of Ashram", 6, 6);*/
	}

	/// <summary>
	/// Clears the game list.
	/// </summary>
	public void ClearGameList()
	{
		Utility.DeleteAllNGUIChilds(m_gameListGrid, true);
	}

	/// <summary>
	/// Adds the game to list.
	/// </summary>
	/// <param name="gameRoomID">Game room I.</param>
	/// <param name="roomName">Room name.</param>
	/// <param name="gameMode">Game mode.</param>
	/// <param name="map">Map.</param>
	/// <param name="numOfPlayers">Number of players.</param>
	/// <param name="maxPlayersInRoom">Max players in room.</param>
	/// <param name="isPlaying">If set to <c>true</c> is playing.</param>
	public void AddGameToList(int gameRoomID, string roomName, string gameMode, string map, string numOfPlayers, int maxPlayersInRoom, bool isPlaying = false)
	{		
		// If duplicate exist
		if(Utility.IsChildExist(m_gameListGrid, gameRoomID.ToString()))
			return;

		string playersRoomStr = numOfPlayers + "/" + maxPlayersInRoom;
		string statusStr = (isPlaying) ? "Playing" : "Waiting";
		
		GameObject newGameListGO = Instantiate(m_gameItemTemplate, m_gameItemTemplate.transform.position, m_gameItemTemplate.transform.rotation) as GameObject;
		newGameListGO.SetActive(true);
		
		// essential for sorting. As reposition will sort the name of the template
		newGameListGO.name = gameRoomID.ToString();
		
		newGameListGO.transform.parent = m_gameListGrid.transform;
		newGameListGO.transform.localPosition = new Vector3(0.0f, 0.0f, -1.0f);
		newGameListGO.transform.transform.localScale = Vector3.one;

		UILabel roomNoLabel = Utility.FindChild(newGameListGO, "RoomNo").GetComponent<UILabel>();
		UILabel roomNameLabel = Utility.FindChild(newGameListGO, "RoomName").GetComponent<UILabel>();
		UILabel modeLabel = Utility.FindChild(newGameListGO, "Mode").GetComponent<UILabel>();
		UILabel mapLabel = Utility.FindChild(newGameListGO, "Map").GetComponent<UILabel>();
		UILabel playersLabel = Utility.FindChild(newGameListGO, "Players").GetComponent<UILabel>();
		UILabel statusLabel = Utility.FindChild(newGameListGO, "Status").GetComponent<UILabel>();
		
		roomNoLabel.text = gameRoomID.ToString();
		roomNameLabel.text = roomName;
		modeLabel.text = gameMode;
		mapLabel.text = map;
		playersLabel.text = playersRoomStr;
		statusLabel.text = statusStr;
	}

	/// <summary>
	/// Repositions the grid.
	/// </summary>
	public void RepositionGrid()
	{
		m_gameListGrid.GetComponent<UIGrid>().Reposition();
		//m_gameListGrid.transform.parent.GetComponent<UIDraggablePanel>().ResetPosition();
	}
}
