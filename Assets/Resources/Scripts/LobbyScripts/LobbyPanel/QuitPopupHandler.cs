﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Quit popup handler that is responsible for showing the quit popup
/// </summary>
public class QuitPopupHandler : MonoBehaviour 
{
	[SerializeField] GameObject m_quitPopup;	
	private string quitPopupSound;
	
	void OnClick()
	{
		quitPopupSound = (m_quitPopup.activeSelf == false) ? "Ashram/User Interface/(Close Dialog Open) uEscapeScreenOpen" : "Ashram/User Interface/(Close Dialog Close) uEscapeScreenClose";
		AudioManager.Instance.PlayOnce(quitPopupSound, 1.0f);

		m_quitPopup.SetActive( !m_quitPopup.activeSelf );
	}
}
