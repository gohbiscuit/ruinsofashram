﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Create button handler that is attached to CreateButton that handles hides/show the panel
/// </summary>
public class CreateButtonHandler : MonoBehaviour {

	[SerializeField] GameObject m_createPanel;
	[SerializeField] GameObject m_playPanel;
	[SerializeField] GameObject m_playPanelBG;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	/// <summary>
	/// Raises the click event. Deactivates play panel and set Create panel to true
	/// </summary>
	void OnClick()
	{
		m_playPanel.SetActive(false);
		m_playPanelBG.SetActive(false);
		m_createPanel.SetActive(true);
	}

	void OnHover()
	{
	}
}
