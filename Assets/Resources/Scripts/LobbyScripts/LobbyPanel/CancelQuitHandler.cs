﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Cancel quit handler that is attached to quit button
/// </summary>
public class CancelQuitHandler : MonoBehaviour 
{
	[SerializeField] GameObject m_quitBox;

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		m_quitBox.SetActive(false);
	}
}
