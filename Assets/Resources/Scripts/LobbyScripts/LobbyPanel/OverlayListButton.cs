﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Overlay list button.
/// </summary>
public class OverlayListButton : MonoBehaviour {
	
	public bool isSelected = false;
	[SerializeField] GameObject m_gridList;
	[SerializeField] JoinRoomHandler m_joinRoomHandler;

	private float lastClick = 0f;

	// Use this for initialization
	void Start () {
		// hide button at the start
		ToggleOverlayButton(gameObject, false, 0.3f);
	}

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		ResetAllList();
	
		isSelected = true;
		ToggleOverlayButton(gameObject, true, 1.0f);

		bool isDoubleClick = (Time.time - lastClick < 0.3);

		if(isDoubleClick)
		{
			// join room
			m_joinRoomHandler.DoJoinRoom();
		}

		lastClick = Time.time;
	}

	/// <summary>
	/// Toggles the overlay button, hiding or showing when mouse is over
	/// </summary>
	/// <param name="parent">Parent.</param>
	/// <param name="isVisible">If set to <c>true</c> is visible.</param>
	/// <param name="alpha">Alpha.</param>
	void ToggleOverlayButton(GameObject parent, bool isVisible, float alpha)
	{
		GameObject overlayButton = Utility.FindChild(parent, "OverlayButton");
		if(overlayButton == null)
		{
			Debug.LogError("Child button is NULL");
			return;
		}

		TweenAlpha.Begin(overlayButton, 0.0f, alpha);

		if(isVisible)
		{
			overlayButton.SetActive(true);
		}
		else
		{
			overlayButton.SetActive(false);
		}
	}

	/// <summary>
	/// Resets all list.
	/// </summary>
	void ResetAllList()
	{
		foreach(Transform child in m_gridList.transform)
		{
			ToggleOverlayButton(child.gameObject, false, 0.3f);
			child.GetComponent<OverlayListButton>().isSelected = false;
		}
	}

	/// <summary>
	/// Raises the hover event.
	/// </summary>
	/// <param name="isOver">If set to <c>true</c> is over.</param>
	void OnHover( bool isOver )
	{
		if(isSelected)
		{
			// do nothing
		}
		else
		{
			if(isOver)
			{
				ToggleOverlayButton(gameObject, true, 0.3f);
			}
			else
			{
				ToggleOverlayButton(gameObject, false, 0.3f);
			}
		}
	}
}
