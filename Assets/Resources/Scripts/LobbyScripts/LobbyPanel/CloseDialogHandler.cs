﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class that is in-charge of event handling when the close dialog popup button is being pressed
/// </summary>
public class CloseDialogHandler : MonoBehaviour 
{
	[SerializeField] GameObject m_closeDialog;

	void OnClick()
	{
		gameObject.transform.parent.gameObject.SetActive(false);
	}
}
