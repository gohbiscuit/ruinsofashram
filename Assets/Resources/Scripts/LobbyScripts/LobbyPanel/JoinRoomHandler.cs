﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Join room handler attached to join room button that handles join room functionality
/// </summary>
public class JoinRoomHandler : Photon.MonoBehaviour {

	[SerializeField] GameObject m_gridContent;
	[SerializeField] PopupDialogHandler m_dialog;

	private ExitGames.Client.Photon.Hashtable roomProperties = null;

	void OnClick()
	{
		DoJoinRoom();
	}

	/// <summary>
	/// Dos the join room, iterate all the rooms and joins the selected room
	/// </summary>
	public void DoJoinRoom()
	{
		// Getting the selected room name from GUI
		foreach (Transform childTransform in m_gridContent.transform) 
		{
			if(childTransform.gameObject.activeSelf)
			{
				OverlayListButton buttonSelected = childTransform.gameObject.GetComponent<OverlayListButton>();
				if(buttonSelected.isSelected)
				{
					GameObject gameRoomName = Utility.FindChild (childTransform.gameObject, "RoomName");
					string roomName = gameRoomName.GetComponent<UILabel>().text;
					if( !string.IsNullOrEmpty(roomName) )
					{
						JoinSelectedRoom(roomName);
					}
				}
			}
		}
	}

	/// <summary>
	/// Joins the selected room based on the room name
	/// </summary>
	/// <param name="roomName">Room name.</param>
	void JoinSelectedRoom(string roomName)
	{
		int roomID = 1;

		foreach (RoomInfo room in PhotonNetwork.GetRoomList())
		{
			if(room.name == TextUtil.ASHRAM_MAIN_CHANNEL)
				continue;

			roomProperties = room.customProperties;
			if(roomProperties == null || roomProperties.Count == 0)
			{
				Debug.LogError("Room hash table is EMPTY: " + room.ToString());
				return;
			}

			// If the room still exist in the network AND not full join the room
			if(room.name == roomName)
			{
				if(room.playerCount < room.maxPlayers)
				{
					// If you have successfully left the main lobby
					if(PhotonNetwork.room == null)
					{
						PhotonNetwork.JoinRoom (roomName);
					}
				}
				else
				{
					Debug.LogError("Room is FULL!!!");
					m_dialog.InitPopupBox("The room is currently full.");
				}
			}
			else
			{
				m_dialog.InitPopupBox("The room you want to join does not exist.");
				Debug.LogError("Room DOES NOT EXIST!!!");
			}

			roomID++;
		}
	}

	/// <summary>
	/// Sets the room properties.
	/// </summary>
	void SetRoomProperties()
	{
		if( string.IsNullOrEmpty( (string) roomProperties["player_1"])  )
			roomProperties["player_1"] = PhotonNetwork.playerName;
		else if( string.IsNullOrEmpty( (string) roomProperties["player_2"]) )
			roomProperties["player_2"] = PhotonNetwork.playerName;
		else if( string.IsNullOrEmpty( (string) roomProperties["player_3"]) )
			roomProperties["player_3"] = PhotonNetwork.playerName;
		else if( string.IsNullOrEmpty( (string) roomProperties["player_4"]) )
			roomProperties["player_4"] = PhotonNetwork.playerName;
		else if( string.IsNullOrEmpty( (string) roomProperties["player_5"]) )
			roomProperties["player_5"] = PhotonNetwork.playerName;
		else if( string.IsNullOrEmpty( (string) roomProperties["player_6"]) )
			roomProperties["player_6"] = PhotonNetwork.playerName;
		else if( string.IsNullOrEmpty( (string) roomProperties["player_7"]) )
			roomProperties["player_7"] = PhotonNetwork.playerName;

		roomProperties["no_of_players"] = PhotonNetwork.room.playerCount.ToString();

		// Set the property when you successfully joined the room
		PhotonNetwork.room.SetCustomProperties(roomProperties);
	}

	/// <summary>
	/// Raises the joined room event.
	/// </summary>
 	void OnJoinedRoom()
	{
		// If the room i join is not the main lobby
		if(PhotonNetwork.room != null && PhotonNetwork.room.name != TextUtil.ASHRAM_MAIN_CHANNEL)
		{
			SetRoomProperties();

			// Set message queue to false before loading the scene
			//PhotonNetwork.isMessageQueueRunning = false;
			PhotonNetwork.LoadLevel("GameRoom");
		}
	}

	void OnHover()
	{
	}
}
