﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Lobby sound loader in-charge of loading the lobby loop music
/// </summary>
public class LobbySoundLoader : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		GameObject soundInstance = AudioManager.Instance.NewSound("Ashram/User Interface/(Lobby) Intro");
		bool loopSound = true;
		AudioManager.Instance.Play (soundInstance, 1.0f, loopSound); 
	}
}
