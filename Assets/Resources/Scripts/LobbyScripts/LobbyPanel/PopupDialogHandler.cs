﻿using UnityEngine;
using System.Collections;

public class PopupDialogHandler : MonoBehaviour {
		
	[SerializeField] UILabel m_popTitle;
	 

	public void InitPopupBox(string title)
	{
		m_popTitle.text = title;
		gameObject.SetActive(true);
	}

	void ClosePopupBox()
	{
		gameObject.SetActive(false);
	}
}
