﻿using UnityEngine;
using System.Collections;

public class ShowPopupHandler : MonoBehaviour 
{
	[SerializeField] PopupDialogHandler m_dialog;
	[SerializeField] string m_message;

	void OnClick()
	{
		m_dialog.InitPopupBox(m_message);
	}
}
