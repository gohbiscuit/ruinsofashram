﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Quit game handler script that is attached to quit button that allows quitting of game
/// </summary>
public class QuitGameHandler : MonoBehaviour 
{
	void OnClick()
	{
		Application.Quit();
	}
}
