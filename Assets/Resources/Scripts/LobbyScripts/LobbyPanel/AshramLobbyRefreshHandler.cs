﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Ashram lobby refresh handler, attached to refresh button for functionality
/// </summary>
public class AshramLobbyRefreshHandler : Photon.MonoBehaviour {

	[SerializeField] GameListLoader m_gameListScript;

	// Use this for initialization
	void Start () 
	{
		RefreshLobby();
	}

	/// <summary>
	/// Note, this is only called when you are in a LOBBY STATE. E.g. have not join a room yet
	/// </summary>
	void OnReceivedRoomListUpdate()
	{
		RefreshLobby();
	}

	void RefreshLobby()
	{
		// Clear the previous room list
		m_gameListScript.ClearGameList();

		ExitGames.Client.Photon.Hashtable roomProperties = null;

		int roomID = 1;

		foreach (RoomInfo room in PhotonNetwork.GetRoomList())
		{
			// Skip the main lobby room
			if(room.name == TextUtil.ASHRAM_MAIN_CHANNEL)
			{
				continue;
			}

			roomProperties = room.customProperties;
			if(roomProperties == null || roomProperties.Count == 0)
			{
				Debug.LogError("Room hash table is EMPTY: " + room.ToString());
				return;
			}
		
			//(string) roomProperties["no_of_players"]
			m_gameListScript.AddGameToList(roomID, (string) roomProperties["room_name"], (string) roomProperties["game_mode"], (string) roomProperties["game_map"], room.playerCount.ToString(), 8, (bool) roomProperties["is_playing"]);
		
			roomID++;
		}

		m_gameListScript.RepositionGrid();
	}

	private IEnumerator WaitSeconds(float seconds)
	{
		yield return new WaitForSeconds(seconds);
	}

	void OnClick()
	{
		RefreshLobby();
	}
}
