﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Player list loader is in charge of loading and displaying the players that are available in the lobby
/// </summary>
public class PlayerListLoader : Photon.MonoBehaviour {
	
	[SerializeField] GameObject m_playerGrid;
	[SerializeField] GameObject m_playerItemTemplate;

	public bool isInLobby = false;
	public bool isSearchingForRoom = false;

	// Use this for initialization
	void Start () 
	{
		if(PhotonNetwork.room != null && PhotonNetwork.room.name == TextUtil.ASHRAM_MAIN_CHANNEL)
		{
			Debug.Log ("Number of Players Online: " + PhotonNetwork.countOfPlayers);
			isInLobby = true;

			Debug.LogWarning("MY CURRENT ROOM IS MAIN CHANNEL");
		}
		else if(PhotonNetwork.room != null)
		{
			Debug.LogWarning("MY CURRENT ROOM IS: " +PhotonNetwork.room.name);
		}
	}


	/// <summary>
	/// Joins the main channel room.
	/// </summary>
	public void JoinMainChannelRoom()
	{
		if(PhotonNetwork.room == null)
		{
			PhotonNetwork.JoinRoom(TextUtil.ASHRAM_MAIN_CHANNEL);
		}
		// else if i am still in the lobby, do nothing
	}

	/// <summary>
	/// When a user joins the lobby, attempts to join the main channel
	/// </summary>
	void OnJoinedLobby()
	{
		if( isInLobby == false && isSearchingForRoom == false)
		{
			JoinMainChannelRoom();
		}
	}

	/// <summary>
	/// Raises the left room event.
	/// </summary>
	void OnLeftRoom()
	{
		isInLobby = false;
	}

	/// <summary>
	/// Raises the joined room event.
	/// </summary>
	void OnJoinedRoom()
	{
		if(PhotonNetwork.room.name == TextUtil.ASHRAM_MAIN_CHANNEL)
		{
			isInLobby = true;
		}
	}

	/// <summary> (No longer needed, logic is shifted to LoginHandler.cs)
	/// Raises the photon join room failed event. When room does not exist or room is full
	/// </summary>
	void OnPhotonJoinRoomFailed()
	{
		// LobbyRoom does not exist
		if(PhotonNetwork.room == null)
		{
			PhotonNetwork.CreateRoom(TextUtil.ASHRAM_MAIN_CHANNEL, true, true, 255, null, null);
		}
	}

	/// <summary>
	/// Removes the player from list.
	/// </summary>
	/// <param name="disconnectedPlayerName">Disconnected player name.</param>
	public void RemovePlayerFromList(string disconnectedPlayerName)
	{
		if(m_playerGrid.transform.Find(disconnectedPlayerName) == null)
		{
			Debug.Log ("PLAYER DOES NOT EXIST!! RemovePlayerList");
			return;
		}

		GameObject playerListGO = m_playerGrid.transform.Find(disconnectedPlayerName).gameObject;
		if(playerListGO != null)
		{
			playerListGO.transform.parent = null;
			NGUITools.DestroyImmediate(playerListGO);

			m_playerGrid.GetComponent<UIGrid>().Reposition();
		}
	}

	/// <summary>
	/// Adds the player to list.
	/// </summary>
	/// <param name="newUserName">New user name.</param>
	public void AddPlayerToList(string newUserName)
	{		
		// If child is already exist. then don't add to the grid
		if(Utility.IsChildExist(m_playerGrid, newUserName))
		   return;

		GameObject newPlayerListGO = Instantiate(m_playerItemTemplate, m_playerItemTemplate.transform.position, m_playerItemTemplate.transform.rotation) as GameObject;
		newPlayerListGO.SetActive(true);
		newPlayerListGO.name = newUserName;
		newPlayerListGO.transform.parent = m_playerGrid.transform;
		newPlayerListGO.transform.transform.localScale = Vector3.one;
		
		UILabel playerNameLabel = newPlayerListGO.GetComponentInChildren<UILabel>();
		
		if(playerNameLabel != null)
		{
			playerNameLabel.text = newUserName;
		}
		
		m_playerGrid.GetComponent<UIGrid>().Reposition();
	}
}
