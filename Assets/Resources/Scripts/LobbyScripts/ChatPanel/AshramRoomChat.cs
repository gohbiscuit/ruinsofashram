﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Ashram room chat that handles the chat functionality in Ahsram game room
/// </summary>
[RequireComponent(typeof(PhotonView))]
public class AshramRoomChat : Photon.MonoBehaviour {
	
	[SerializeField] UITextList m_chatTextList;
	[SerializeField] GameObject m_chatTextLabel;
	[SerializeField] RoomPlayerListLoader m_roomPlayerList;
	
	UIInput m_chatInput;
	
	bool showChat = false;
	
	string inputField = "";
	
	int ChatCounter = 0;
	Vector3 originalChatLabelPosition;
	
	string myName;
	
	ArrayList chatEntries = new ArrayList();
	class ChatEntry
	{
		public string name = "";
		public string text = "";
	}
	
	private PhotonView photonView;
	
	// Use this for initialization
	void Start () 
	{
		photonView = PhotonView.Get(this);

		if(string.IsNullOrEmpty(PhotonNetwork.playerName))
		{
			PhotonNetwork.playerName = "RandomName" + Random.Range(1,999);
		}
		
		myName = PhotonNetwork.playerName;
		
		m_chatInput = GetComponent<UIInput>();
		
		originalChatLabelPosition = m_chatTextLabel.transform.localPosition;

		// Init the chat window
		ShowChatWindow(true);

		m_roomPlayerList.LoadGameRoom();
	}
	
	/// <summary>
	/// Pressing 'enter' should immediately give focus to the input field.
	/// </summary>
	void Update()
	{	
		GrabInputFocus();
	}

	/// <summary>
	/// Grabs the input focus.
	/// </summary>
	void GrabInputFocus()
	{
		// If no selection
		if (UICamera.selectedObject == null)
		{
			//avoid showing the caret
			string previousInput = m_chatInput.text;
			m_chatInput.selected = true;
			m_chatInput.text = previousInput;
		}
		// If current selected GUI is not textfield. Grab focus
		else if(UICamera.selectedObject != m_chatInput.gameObject)
		{
			//avoid showing the caret
			string previousInput = m_chatInput.text;
			m_chatInput.selected = true;
			m_chatInput.text = previousInput;
		}
	}

	/// <summary>
	/// Raises the photon player disconnected event. When a player is disconnected or leaves the room
	/// </summary>
	/// <param name="disconnectedPlayer">Disconnected player.</param>
	void OnPhotonPlayerDisconnected(PhotonPlayer disconnectedPlayer)
	{
		m_roomPlayerList.DisconnectPlayer(disconnectedPlayer.name);
	}

	/// <summary>
	/// Raises the photon player connected event. When a new player joins the room
	/// </summary>
	/// <param name="newConnectedPlayer">New connected player.</param>
	void OnPhotonPlayerConnected(PhotonPlayer newConnectedPlayer)
	{ 
		AudioManager.Instance.PlayOnce ("Ashram/User Interface/(GameRoom Join Chat) FriendJoin", 1.0f);

		// When non-host joins the room
		m_roomPlayerList.AddPlayerToList(newConnectedPlayer.name);
	}

	/// <summary>
	/// Shows the chat window.
	/// </summary>
	/// <param name="isVisible">If set to <c>true</c> is visible.</param>
	void ShowChatWindow( bool isVisible )
	{
		showChat = isVisible;
		inputField = "";
	}

	/// <summary>
	/// Raises the chat submit event.
	/// </summary>
	public void OnChatSubmit()
	{
		if (m_chatTextList != null)
		{
			// It's a good idea to strip out all symbols as we don't want user input to alter colors, add new lines, etc
			inputField = NGUITools.StripSymbols(m_chatInput.text);
			
			if (!string.IsNullOrEmpty(inputField))
			{	
				SubmitEntry(inputField);
				m_chatInput.text = ""; //avoid showing the caret
				m_chatInput.selected = true;
			}
		}
	}

	/// <summary>
	/// Raises the GUI event.
	/// </summary>
	void OnGUI()
	{
		if(!showChat)
		{
			return;
		}
		
		if(ChatCounter < chatEntries.Count)
		{
			RefreshChat();
		}
	}

	/// <summary>
	/// Refreshs the chat.
	/// </summary>
	void RefreshChat()
	{
		// Keep track of a counter
		ChatCounter = chatEntries.Count;
		m_chatTextList.Clear();
		
		foreach(ChatEntry entry in chatEntries)
		{	
			string message = "";
			
			// Server-generated message
			if(entry.name == "")
			{
				message = entry.name + entry.text;
			}
			else
			{
				message = TextUtil.CHAT_PLAYER_NAME_COLOR_ + entry.name + ": " + 
					TextUtil.CHAT_MESSAGE_COLOR_ + entry.text;
			}
			
			m_chatTextList.Add(message);
		}
		
		// Refreshed the position of text label
		if(m_chatTextLabel.transform.localPosition.y > -361 && m_chatTextLabel.transform.localPosition.y < -243 && ChatCounter < 10)
		{
			float y_space_multiplier = 11.6875f;
			float new_y_pos = originalChatLabelPosition.y + (y_space_multiplier * ChatCounter);
			m_chatTextLabel.transform.localPosition = new Vector3(originalChatLabelPosition.x, new_y_pos, originalChatLabelPosition.z);
		}
		// If the content overflow
		else
		{
			UpdateScrollPosition();
		}
	}

	/// <summary>
	/// Updates the scroll position.
	/// </summary>
	void UpdateScrollPosition()
	{
		m_chatTextList.transform.parent.GetComponent<UIDraggablePanel>().MoveRelative(new Vector3(0.0f, 8.5f, 0.0f));
	}
	
	/// <summary>
	/// Submits the entry.
	/// </summary>
	/// <param name="msg">Message.</param>
	void SubmitEntry(string msg)
	{
		msg = msg.Replace("\n", "");
		photonView.RPC("SendChatEntry", PhotonTargets.All, myName, msg);
	}

	//void SendChatEntry(string name, bool broadCast, PhotonMessageInfo info)
	/// <summary>
	/// Sends the chat entry. [RPC] Remote Procedural call method
	/// </summary>
	/// <param name="name">Name.</param>
	/// <param name="msg">Message.</param>
	[RPC]
	void SendChatEntry(string name, string msg)
	{
		ChatEntry entry = new ChatEntry();
		//if(!systemMessage) 
		//	entry.name = info.sender.name;
		
		entry.name = name;
		entry.text = msg;
		
		chatEntries.Add (entry);
		
		if(chatEntries.Count > 100)
			chatEntries.RemoveAt(0);
		
		inputField = "";
	}
	
	/// <summary>
	/// Adds message, if broadcast = false means send to ourself only
	/// </summary>
	public void AddGlobalMessage(string str, bool broadCast = true)
	{
		// Send to ourself
		SendChatEntry("", str);
		
		// Send to others
		if(broadCast)
		{
			photonView.RPC("SendChatEntry", PhotonTargets.Others, "", str);
		}
	}
}
