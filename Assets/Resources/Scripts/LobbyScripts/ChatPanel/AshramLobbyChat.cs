﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Ashram lobby chat class handles the chat functionality in the game lobby
/// </summary>
[RequireComponent(typeof(PhotonView))]
public class AshramLobbyChat : Photon.MonoBehaviour {

	[SerializeField] UITextList m_chatTextList;
	[SerializeField] GameObject m_chatTextLabel;
	[SerializeField] PlayerListLoader m_playerListScript;
	[SerializeField] GameObject m_createGameTextfield;
	
	UIInput m_chatInput;

	bool showChat = false;
	
	string inputField = "";
	
	int ChatCounter = 0;
	Vector3 originalChatLabelPosition;
	
	string myName;
	
	ArrayList chatEntries = new ArrayList();
	class ChatEntry
	{
		public string name = "";
		public string text = "";
	}

	private PhotonView photonView;
	
	// Use this for initialization
	void Start () 
	{
		if(string.IsNullOrEmpty(PhotonNetwork.playerName))
		{
			PhotonNetwork.playerName = "RandomName" + Random.Range(1,999);
		}

		myName = PhotonNetwork.playerName;
		
		m_chatInput = GetComponent<UIInput>();
		
		originalChatLabelPosition = m_chatTextLabel.transform.localPosition;

		photonView = PhotonView.Get(this);

		if(PhotonNetwork.room != null && PhotonNetwork.room.name == TextUtil.ASHRAM_MAIN_CHANNEL)
		{
			InitPlayersPanel();
		}
	}
	
	/// <summary>
	/// Pressing 'enter' should immediately give focus to the input field.
	/// </summary>
	void Update()
	{	
		GrabInputFocus();
	}

	/// <summary>
	/// Grabs the input focus.
	/// </summary>
	void GrabInputFocus()
	{
		// If no selection
		if (UICamera.selectedObject == null)
		{
			//avoid showing the caret
			string previousInput = m_chatInput.text;
			m_chatInput.selected = true;
			m_chatInput.text = previousInput;
		}
		// If current selected GUI is not textfield. Grab focus
		else if(UICamera.selectedObject != m_chatInput.gameObject && UICamera.selectedObject != m_createGameTextfield)
		{
			//avoid showing the caret
			string previousInput = m_chatInput.text;
			m_chatInput.selected = true;
			m_chatInput.text = previousInput;
		}
	}

	/// <summary>
	/// Inits the list of players that are in the lobby
	/// </summary>
	void InitPlayersPanel()
	{
		ShowChatWindow( true );
		
		AddGlobalMessage(TextUtil.CHAT_MESSAGE_COLOR_ + "Welcome to Ashram [Main Channel].", false);
		AddGlobalMessage(TextUtil.CHAT_JOIN_CHAT_COLOR_ + myName + " joined the chat.");
		m_playerListScript.AddPlayerToList(myName);
		
		// Init Player List Panel
		InitOtherPlayersInList();
	}
	
	/// <summary>
	/// Raises the connected to server event. 
	/// Scenario: When I am connected to the server. TellServerOurName e.g. SubmitPlayerInfo
	/// </summary>
	void OnJoinedRoom()
	{
		InitPlayersPanel();
	}

	/// <summary>
	/// Inits other players in your player list
	/// </summary>
	void InitOtherPlayersInList()
	{
		foreach(PhotonPlayer photonPlayer in PhotonNetwork.otherPlayers) 
		{
			m_playerListScript.AddPlayerToList(photonPlayer.name);
			AddGlobalMessage(TextUtil.CHAT_JOIN_CHAT_COLOR_ + photonPlayer.name + " joined the chat.", false);
		}
	}

	/// <summary>
	/// Raises the photon player disconnected event.
	/// </summary>
	/// <param name="disconnectedPlayer">Disconnected player.</param>
	void OnPhotonPlayerDisconnected(PhotonPlayer disconnectedPlayer)
	{
		AddGlobalMessage(TextUtil.CHAT_LEAVE_CHAT_COLOR_ + disconnectedPlayer.name + " left the chat.", false);
		m_playerListScript.RemovePlayerFromList(disconnectedPlayer.name);
	}

	/// <summary>
	/// Raises the left room event.
	/// </summary>
	void OnLeftRoom()
	{
		ShowChatWindow(false);
		AddGlobalMessage(TextUtil.CHAT_LEAVE_CHAT_COLOR_ + myName + " left the chat.", false);
		m_playerListScript.RemovePlayerFromList(myName);
	}
	
	/// <summary>
	/// When a new player has join the game room, tell others that you have joined the room
	/// </summary>
	void OnPhotonPlayerConnected(PhotonPlayer newConnectedPlayer)
	{		
		m_playerListScript.AddPlayerToList(newConnectedPlayer.name);
	}

	/// <summary>
	/// Shows the chat window.
	/// </summary>
	/// <param name="isVisible">If set to <c>true</c> is visible.</param>
	void ShowChatWindow( bool isVisible )
	{
		showChat = isVisible;
		inputField = "";
		//chatEntries.Clear ();
		//m_chatTextList.Clear();
	}

	/// <summary>
	/// Raises the chat submit event.
	/// </summary>
	public void OnChatSubmit()
	{
		if (m_chatTextList != null)
		{
			// It's a good idea to strip out all symbols as we don't want user input to alter colors, add new lines, etc
			inputField = NGUITools.StripSymbols(m_chatInput.text);
			
			if (!string.IsNullOrEmpty(inputField))
			{	

				// If showchat is false e.g. not connected to lobby
				if(!showChat)
				{
					DisplayErrorChat();
				}
				else
				{
					SubmitEntry(inputField);
				}

				m_chatInput.text = ""; //avoid showing the caret
				m_chatInput.selected = true;
			}
		}
	}

	/// <summary>
	/// Raises the GUI event. Refreshes the chat every frame
	/// </summary>
	void OnGUI()
	{
		/*if(!showChat)
		{
			return;
		}*/
		
		if(ChatCounter < chatEntries.Count)
		{
			RefreshChat();
		}
	}

	/// <summary>
	/// Displays the error chat, message if message is not send
	/// </summary>
	void DisplayErrorChat()
	{
		AddGlobalMessage(TextUtil.CHAT_NOTSEND_CHAT_COLOR_ + "[System]: Your message was not send as you are not connected to the Main Channel.", false);
	}

	/// <summary>
	/// Refreshs the chat.
	/// </summary>
	void RefreshChat()
	{
		// Keep track of a counter
		ChatCounter = chatEntries.Count;
		m_chatTextList.Clear();
		
		foreach(ChatEntry entry in chatEntries)
		{	
			string message = "";
			
			// Server-generated message
			if(entry.name == "")
			{
				message = entry.name + entry.text;
			}
			else
			{
				message = TextUtil.CHAT_PLAYER_NAME_COLOR_ + entry.name + ": " + 
						  TextUtil.CHAT_MESSAGE_COLOR_ + entry.text;
			}
			
			m_chatTextList.Add(message);
		}
		
		// Refreshed the position of text label
		if(m_chatTextLabel.transform.localPosition.y > -361 && m_chatTextLabel.transform.localPosition.y < -243 && ChatCounter < 10)
		{
			float y_space_multiplier = 11.6875f;
			float new_y_pos = originalChatLabelPosition.y + (y_space_multiplier * ChatCounter);
			m_chatTextLabel.transform.localPosition = new Vector3(originalChatLabelPosition.x, new_y_pos, originalChatLabelPosition.z);
		}
		// If the content overflow
		else
		{
			UpdateScrollPosition();
		}
	}

	/// <summary>
	/// Updates the scroll position.
	/// </summary>
	void UpdateScrollPosition()
	{
		m_chatTextList.transform.parent.GetComponent<UIDraggablePanel>().MoveRelative(new Vector3(0.0f, 8.5f, 0.0f));
	}
	
	/// <summary>
	/// Submits the entry.
	/// </summary>
	/// <param name="msg">Message.</param>
	void SubmitEntry(string msg)
	{
		msg = msg.Replace("\n", "");
		photonView.RPC("SendChatEntry", PhotonTargets.All, myName, msg);
	}

	//void SendChatEntry(string name, bool broadCast, PhotonMessageInfo info)
	[RPC]
	/// <summary>
	/// [Remote Procedural Call] Network-based method that sends the chat entry.
	/// </summary>
	/// <param name="name">Name.</param>
	/// <param name="msg">Message.</param>
	void SendChatEntry(string name, string msg)
	{
		ChatEntry entry = new ChatEntry();
		//if(!systemMessage) 
		//	entry.name = info.sender.name;

		entry.name = name;
		entry.text = msg;
		
		chatEntries.Add (entry);
		
		if(chatEntries.Count > 100)
			chatEntries.RemoveAt(0);
		
		inputField = "";
	}

	/// <summary>
	/// Adds message, if broadcast = false means send to ourself only
	/// </summary>
	void AddGlobalMessage(string str, bool broadCast = true)
	{
		// Send to ourself
		SendChatEntry("", str);

		// Send to others
		if(broadCast)
		{
			photonView.RPC("SendChatEntry", PhotonTargets.Others, "", str);
		}
	}
}
