﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Send button handler that handles the event and submit the chat entry for AshramLobbyChat / AshramRoomChat
/// </summary>
public class SendButtonHandler : MonoBehaviour {

	[SerializeField] AshramLobbyChat m_lobbyChat;
	[SerializeField] AshramRoomChat m_roomChat;
	
	void OnClick()
	{
		if(m_lobbyChat != null)
		{
			m_lobbyChat.OnChatSubmit();
		}
		else if(m_roomChat != null)
		{
			m_roomChat.OnChatSubmit();
		}
	}
}
