﻿using UnityEngine;
using System.Collections;
using System;		// namespace for String

public class UserProfileLoader : MonoBehaviour {
	
	[SerializeField] UILabel m_username;
	[SerializeField] UILabel m_wins;
	[SerializeField] UILabel m_loses;
	[SerializeField] UILabel m_winRate;
	[SerializeField] UILabel m_level;
	[SerializeField] UILabel m_exp;
	[SerializeField] UILabel m_matchesPlayed;
	
	// Use this for initialization
	void Start () 
	{
		LoadUserProfile();
	}
	
	void LoadUserProfile()
	{		
		PhotonNetwork.playerName = Player.USERNAME;
		ExitGames.Client.Photon.Hashtable playerProperties = new ExitGames.Client.Photon.Hashtable();
		playerProperties.Add ("wins", Player.WINCOUNT.ToString());
		playerProperties.Add ("loses", Player.LOSECOUNT.ToString());
		playerProperties.Add ("exp", CalculateExp(Player.EXP, Player.LEVEL));
		playerProperties.Add ("level", Player.LEVEL.ToString());
		playerProperties.Add ("win_rate", CalculateWinRatio(Player.WINCOUNT, Player.LOSECOUNT));
		playerProperties.Add ("matches_played", (Player.WINCOUNT + Player.LOSECOUNT).ToString());
		playerProperties.Add("email", Player.EMAIL);

		// set a default team (Death-Match)
		playerProperties.Add("Team", Team.ON_YOUR_OWN);

		// ping (updated every frame in game)
		playerProperties.Add("ping", 0);

		// additional variables for tracking kills/deaths in match (added by hw)
		playerProperties.Add("match_isAlive", true);
		playerProperties.Add("match_respawn_timer", 0);
		playerProperties.Add("match_level", 0);
		playerProperties.Add("match_kills", 0);
		playerProperties.Add("match_deaths", 0);
		
		PhotonNetwork.SetPlayerCustomProperties(playerProperties);
		
		m_username.text = Player.USERNAME;
		m_wins.text = Player.WINCOUNT.ToString();
		m_loses.text = Player.LOSECOUNT.ToString();
		m_exp.text = CalculateExp(Player.EXP, Player.LEVEL);
		m_level.text = Player.LEVEL.ToString();
		m_winRate.text = CalculateWinRatio(Player.WINCOUNT, Player.LOSECOUNT);
		m_matchesPlayed.text = (Player.WINCOUNT + Player.LOSECOUNT).ToString();
	}
	
	/// <summary>
	/// Calculates the window ratio.
	/// Check http://www.csharp-examples.net/string-format-double/
	/// </summary>
	public static string CalculateWinRatio(double wins, double loses)
	{
		double total_matches_played = wins + loses;
		double win_ratio = 0.0;
		
		// Safe division e.g eliminates division by zero
		if(total_matches_played > 0)
			win_ratio = wins / total_matches_played * 100;
		
		return String.Format("{0:0.0}", win_ratio) + "%";
	}
	
	public static string CalculateExp(int exp, int level)
	{
		int nextExpToLevel = level * 1000;
		
		return exp + " / " + nextExpToLevel;
	}

}
