﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Game room sound loader in-charge of playing the sound when users enter the game room
/// </summary>
public class GameRoomSoundLoader : MonoBehaviour {

	/// <summary>
	/// Start this instance.
	/// </summary>
	void Start () 
	{
		AudioManager.Instance.PlayOnce("Ashram/User Interface/(Game Room OnEnter) iQuestActivate", 0.3f);
	}

}
