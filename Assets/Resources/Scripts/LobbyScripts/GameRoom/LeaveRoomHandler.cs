﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Leave room handler handles the leave room button functionaltiy when a user leaves a room
/// </summary>
public class LeaveRoomHandler : Photon.MonoBehaviour {

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		// means we are in a room
		if(PhotonNetwork.room != null)
		{
			PhotonNetwork.LeaveRoom();
		}
	}

	/// <summary>
	/// Raises the left room event. When a user leaves a room, load lobby scene
	/// </summary>
	void OnLeftRoom()
	{
		// go back to lobby
		PhotonNetwork.LoadLevel("Lobby");
	}
}
