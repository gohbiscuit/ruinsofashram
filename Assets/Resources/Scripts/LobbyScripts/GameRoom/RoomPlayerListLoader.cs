﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Room player list loader, loads all the player in the game room
/// </summary>
public class RoomPlayerListLoader : Photon.MonoBehaviour {

	[SerializeField] GameObject m_playerGrid;
	[SerializeField] GameObject m_playerItemTemplate;
	[SerializeField] AshramRoomChat m_roomChat;
	[SerializeField] JoinSlotHandler m_slot1;
	[SerializeField] JoinSlotHandler m_slot2;
	[SerializeField] JoinSlotHandler m_slot3;
	[SerializeField] JoinSlotHandler m_slot4;
	[SerializeField] JoinSlotHandler m_slot5;
	[SerializeField] JoinSlotHandler m_slot6;
	[SerializeField] JoinSlotHandler m_slot7;
	[SerializeField] JoinSlotHandler m_slot8;

	[SerializeField] UILabel m_map;
	[SerializeField] UILabel m_roomName;
	[SerializeField] UILabel m_hostName;
	[SerializeField] UILabel m_gameMode;
	[SerializeField] UILabel m_timeLimit;
	[SerializeField] UILabel m_noOfPlayers;

	[SerializeField] GameObject m_team1Logo;
	[SerializeField] GameObject m_team2Logo;

	private ExitGames.Client.Photon.Hashtable roomProperties = null;

	private string playerHost;
	private string player1;
	private string player2;
	private string player3;
	private string player4;
	private string player5;
	private string player6;
	private string player7;

	public string roomName;
	
	// Use this for initialization
	void Start () 
	{
		//LoadRoomProperties();
		//LoadSlotDisplay();
	}

	// Update is called once per frame
	void Update () 
	{
	}

	/// <summary>
	/// Once AshramRoomChat is successfully connected e.g. PhotonView is there, it invoke Loads the game room.
	/// </summary>
	public void LoadGameRoom()
	{
		LoadRoomProperties();
		LoadSlotDisplay();
	}

	/// <summary>
	/// Loads the slot display.
	/// </summary>
	void LoadSlotDisplay()
	{
		m_slot1.RefreshSlotDisplay();
		m_slot2.RefreshSlotDisplay();
		m_slot3.RefreshSlotDisplay();
		m_slot4.RefreshSlotDisplay();
		m_slot5.RefreshSlotDisplay();
		m_slot6.RefreshSlotDisplay();
		m_slot7.RefreshSlotDisplay();
		m_slot8.RefreshSlotDisplay();
	}

	/// <summary>
	/// Loads the room properties, and the existing players that are currently in the room
	/// </summary>
	void LoadRoomProperties()
	{
		if(PhotonNetwork.room == null)
		{
			Debug.LogError("PhotonNetwork room is NULL, you're not in a room yet!");
			return;
		}

		Debug.Log ("THE PROPERTIES IN ROOM: "+PhotonNetwork.room.ToString());


		// When scene is loaded. Load Room Properties
		roomProperties = PhotonNetwork.room.customProperties;
		m_map.text = (string) roomProperties["game_map"];

		roomName = (string) roomProperties["room_name"];

		m_roomName.text = (string) roomProperties["room_name"];
		m_hostName.text = (string) roomProperties["player_host"];
		m_gameMode.text = (string) roomProperties["game_mode"];
		m_timeLimit.text = (string) roomProperties["game_time"];
		m_noOfPlayers.text = PhotonNetwork.room.playerCount.ToString();		//(string) roomProperties["no_of_players"];		

		//m_chatTextList.Add (TextUtil.CHAT_MESSAGE_COLOR_ + "Welcome to " + roomName + ".");

		m_roomChat.AddGlobalMessage(TextUtil.CHAT_MESSAGE_COLOR_ + "Welcome to " + roomName + ".", false);

		// Death-match, hide the teams logo
		if( m_gameMode.text.Equals("Death-Match Classic"))
		{
			m_team1Logo.SetActive(false);
			m_team2Logo.SetActive(false);
		}


		// Add host (myself) to the player list
		AddPlayerToList( (string) roomProperties["player_host"] );

		if( string.IsNullOrEmpty( (string) roomProperties["player_1"]) == false)
		{
			AddPlayerToList( (string) roomProperties["player_1"] );
		}

		if(string.IsNullOrEmpty( (string) roomProperties["player_2"]) == false)
		{
			AddPlayerToList( (string) roomProperties["player_2"] );
		}

		if(string.IsNullOrEmpty( (string) roomProperties["player_3"]) == false)
		{
			AddPlayerToList( (string) roomProperties["player_3"] );
		}

		if(string.IsNullOrEmpty( (string) roomProperties["player_4"]) == false)
		{
			AddPlayerToList( (string) roomProperties["player_4"] );
		}

		if(string.IsNullOrEmpty( (string) roomProperties["player_5"]) == false)
		{
			AddPlayerToList( (string) roomProperties["player_5"] );
		}

		if(string.IsNullOrEmpty( (string) roomProperties["player_6"]) == false)
		{
			AddPlayerToList( (string) roomProperties["player_6"] );
		}

		if(string.IsNullOrEmpty( (string) roomProperties["player_7"]) == false)
		{
			AddPlayerToList( (string) roomProperties["player_7"] );
		}
	}
	

	/// <summary>
	/// Raises the joined room event. (When the host joins the room)
	/// </summary>
	void OnJoinedRoom()
	{
		// do nothing
	}

	/// <summary>
	/// Determines whether this instance can start game the specified playerName.
	/// </summary>
	/// <returns><c>true</c> if this instance can start game the specified playerName; otherwise, <c>false</c>.</returns>
	/// <param name="playerName">Player name.</param>
	public string CanStartGame(string playerName)
	{
		string message = "";
		roomProperties = PhotonNetwork.room.customProperties;

		int playerCount = PhotonNetwork.room.playerCount;
		int slotsTaken = (int) roomProperties["number_of_slots_taken"];
		playerHost = (string) roomProperties["player_host"];

		if(playerCount > slotsTaken)
		{
			message = "[System]: Not all players have taken up a slot.";
		}
		else if(playerCount <= slotsTaken)
		{
			// When game is started successfully, no error message
			message = "";
		}


		// If player clicks on start button but he is not the host
		if(playerHost.Equals(playerName) == false)
		{
			message = "[System]: Only host can start the game.";
		}

		return message;
	}

	/// <summary>
	/// Disconnects the player. and remove it from the list
	/// </summary>
	/// <param name="disconnectedPlayerName">Disconnected player name.</param>
	public void DisconnectPlayer(string disconnectedPlayerName)
	{
		RemovePlayerFromList(disconnectedPlayerName);
		
		// Update the room properties when a player leaves
		roomProperties = PhotonNetwork.room.customProperties;
		
		playerHost = (string) roomProperties["player_host"];
		player1 = (string) roomProperties["player_1"];
		player2 = (string) roomProperties["player_2"];
		player3 = (string) roomProperties["player_3"];
		player4 = (string) roomProperties["player_4"];
		player5 = (string) roomProperties["player_5"];
		player6 = (string) roomProperties["player_6"];
		player7 = (string) roomProperties["player_7"];
		
		// if host leaves the room. find and replace another player to be the host
		if( !string.IsNullOrEmpty(playerHost) && playerHost.Equals(disconnectedPlayerName) )
			FindAndReplaceHost(player1,player2,player3,player4,player5,player6,player7);
		else if( !string.IsNullOrEmpty(player1) && player1.Equals(disconnectedPlayerName) )
			roomProperties["player_1"] = "";
		else if( !string.IsNullOrEmpty(player2) && player2.Equals(disconnectedPlayerName) )
			roomProperties["player_2"] = "";
		else if( !string.IsNullOrEmpty(player3) && player3.Equals(disconnectedPlayerName) )
			roomProperties["player_3"] = "";
		else if( !string.IsNullOrEmpty(player4) && player4.Equals(disconnectedPlayerName) )
			roomProperties["player_4"] = "";
		else if( !string.IsNullOrEmpty(player5) && player5.Equals(disconnectedPlayerName) )
			roomProperties["player_5"] = "";
		else if( !string.IsNullOrEmpty(player6) && player6.Equals(disconnectedPlayerName) )
			roomProperties["player_6"] = "";
		else if( !string.IsNullOrEmpty(player7)  && player7.Equals(disconnectedPlayerName) )
			roomProperties["player_7"] = "";

		roomProperties["no_of_players"] = PhotonNetwork.room.playerCount.ToString();

		ClearSlot (disconnectedPlayerName);

		// Set the property when a player left the room
		PhotonNetwork.room.SetCustomProperties(roomProperties);
	}

	/// <summary>
	/// Find an existing player to replace the host 
	/// </summary>
	/// <param name="player1">Player1.</param>
	/// <param name="player2">Player2.</param>
	/// <param name="player3">Player3.</param>
	/// <param name="player4">Player4.</param>
	/// <param name="player5">Player5.</param>
	/// <param name="player6">Player6.</param>
	/// <param name="player7">Player7.</param>
	void FindAndReplaceHost(string player1, string player2, string player3, string player4, string player5, string player6, string player7)
	{
		// Set an existing player to be the new host
		if( !string.IsNullOrEmpty(player1) )
		{
			roomProperties["player_host"] = player1;
			roomProperties["player_1"] = "";

			// Set me to the host
			if(player1 == PhotonNetwork.playerName)
				PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
		else if(!string.IsNullOrEmpty(player2) )
		{
			roomProperties["player_host"] = player2;
			roomProperties["player_2"] = "";

			if(player2 == PhotonNetwork.playerName)
				PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
		else if(!string.IsNullOrEmpty(player3) )
		{
			roomProperties["player_host"] = player3;
			roomProperties["player_3"] = "";

			if(player3 == PhotonNetwork.playerName)
				PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
		else if(!string.IsNullOrEmpty(player4) )
		{
			roomProperties["player_host"] = player4;
			roomProperties["player_4"] = "";

			if(player4 == PhotonNetwork.playerName)
				PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
		else if(!string.IsNullOrEmpty(player5) )
		{
			roomProperties["player_host"] = player5;
			roomProperties["player_5"] = "";

			if(player5 == PhotonNetwork.playerName)
				PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
		else if(!string.IsNullOrEmpty(player6) )
		{
			roomProperties["player_host"] = player6;
			roomProperties["player_6"] = "";

			if(player6 == PhotonNetwork.playerName)
				PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
		else if(!string.IsNullOrEmpty(player7) )
		{
			roomProperties["player_host"] = player7;
			roomProperties["player_7"] = "";

			if(player7 == PhotonNetwork.playerName)
				PhotonNetwork.SetMasterClient(PhotonNetwork.player);
		}
	}

	/// <summary>
	/// Clear the slot that the leaver has taken
	/// </summary>
	/// <param name="userName">User name.</param>
	void ClearSlot(string userName)
	{
		string slot1 = (string) roomProperties["slot_1"];
		string slot2 = (string) roomProperties["slot_2"];
		string slot3 = (string) roomProperties["slot_3"];
		string slot4 = (string) roomProperties["slot_4"];
		string slot5 = (string) roomProperties["slot_5"];
		string slot6 = (string) roomProperties["slot_6"];
		string slot7 = (string) roomProperties["slot_7"];
		string slot8 = (string) roomProperties["slot_8"];
		
		if( !string.IsNullOrEmpty(slot1) && slot1.Equals (userName)  )
		{
			roomProperties["slot_1"] = "";
		}
		else if( !string.IsNullOrEmpty(slot2) && slot2.Equals (userName)  )
		{
			roomProperties["slot_2"] = "";
		}
		else if( !string.IsNullOrEmpty(slot3) && slot3.Equals (userName)  )
		{
			roomProperties["slot_3"] = "";
		}
		else if( !string.IsNullOrEmpty(slot4) && slot4.Equals (userName)  )
		{
			roomProperties["slot_4"] = "";
		}
		else if( !string.IsNullOrEmpty(slot5) && slot5.Equals (userName)  )
		{
			roomProperties["slot_5"] = "";
		}
		else if( !string.IsNullOrEmpty(slot6) && slot6.Equals (userName)  )
		{
			roomProperties["slot_6"] = "";
		}
		else if( !string.IsNullOrEmpty(slot7) && slot7.Equals (userName)  )
		{
			roomProperties["slot_7"] = "";
		}
		else if( !string.IsNullOrEmpty(slot8) && slot8.Equals (userName)  )
		{
			roomProperties["slot_8"] = "";
		}
	}

	/// <summary>
	/// Removes the player from game room list.
	/// </summary>
	/// <param name="disconnectedPlayerName">Disconnected player name.</param>
	public void RemovePlayerFromList(string disconnectedPlayerName)
	{
		if(Utility.IsChildExist(m_playerGrid, disconnectedPlayerName) == false)
			return;

		GameObject playerListGO = m_playerGrid.transform.Find(disconnectedPlayerName).gameObject;
		if(playerListGO != null)
		{
			playerListGO.transform.parent = null;
			NGUITools.DestroyImmediate(playerListGO);

			//m_chatTextList.Add (TextUtil.CHAT_LEAVE_ROOM_COLOR_ + disconnectedPlayerName + " left the room.");

			m_roomChat.AddGlobalMessage(TextUtil.CHAT_LEAVE_ROOM_COLOR_ + disconnectedPlayerName + " left the room.", false);

			m_playerGrid.GetComponent<UIGrid>().Reposition();
			//m_playerGrid.transform.parent.GetComponent<UIDraggablePanel>().ResetPosition();
		}
	}

	/// <summary>
	/// Adds the player to game room list.
	/// </summary>
	/// <param name="newUserName">New user name.</param>
	public void AddPlayerToList(string newUserName)
	{		
		// If child is already exist. then don't add to the grid
		if(Utility.IsChildExist(m_playerGrid, newUserName))
			return;

		//GameObject newPlayerListGO = Instantiate(m_playerItemTemplate, m_playerItemTemplate.transform.position, m_playerItemTemplate.transform.rotation) as GameObject;
		GameObject newPlayerListGO = Instantiate(m_playerItemTemplate) as GameObject;
		newPlayerListGO.SetActive(true);
		newPlayerListGO.name = newUserName;
		newPlayerListGO.transform.parent = m_playerGrid.transform;
		newPlayerListGO.transform.transform.localScale = Vector3.one;
		newPlayerListGO.transform.localPosition = Vector3.zero;
		
		UILabel playerNameLabel = newPlayerListGO.GetComponentInChildren<UILabel>();
		
		if(playerNameLabel != null)
		{
			playerNameLabel.text = newUserName;
		}
		
		m_playerGrid.GetComponent<UIGrid>().Reposition();

		m_roomChat.AddGlobalMessage(TextUtil.CHAT_JOIN_ROOM_COLOR_ + newUserName + " joined the room.", false);

		//m_chatTextList.Add (TextUtil.CHAT_JOIN_ROOM_COLOR_ + newUserName + " joined the room.");
	}
}
