﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Start game handler that handles the StartGame button functionality
/// </summary>
[RequireComponent(typeof(PhotonView))]
public class StartGameHandler : MonoBehaviour {
	
	[SerializeField] AshramRoomChat m_roomChat;
	[SerializeField] RoomPlayerListLoader m_roomListLoader;
	[SerializeField] UIImageButton m_leaveRoomButton;

	private ExitGames.Client.Photon.Hashtable roomProperties = null;
	
	private PhotonView photonView;

	public bool isGameStarting = false;		// been used by join slot handler, so when game countdown, player cannot switch slot
	
	// Use this for initialization
	void Start () {
		photonView = PhotonView.Get(this);
	}

	/// <summary>
	/// Starts the game.
	/// </summary>
	void StartGame()
	{
		string errorMsg = m_roomListLoader.CanStartGame(PhotonNetwork.playerName);
		
		// Start game
		if(string.IsNullOrEmpty(errorMsg))
		{
			//int secondsToStart = 5;
			PhotonNetwork.room.visible = false;	// Hide the room from get room list when room is started
			//StartCoroutine( CountDown("Game Starting in: {0}.", secondsToStart) );

			// Host click start-game
			DoStartGame();

			// Disable button once game is started
			gameObject.GetComponent<UIImageButton>().isEnabled = false;
		}
		else
		{
			m_roomChat.AddGlobalMessage(TextUtil.CHAT_NOTSEND_CHAT_COLOR_ + errorMsg, false);
		}
	}

	/// <summary>
	/// Counts down.
	/// </summary>
	/// <returns>The down.</returns>
	/// <param name="countDownMsg">Count down message.</param>
	/// <param name="time">Time.</param>
	IEnumerator CountDown(string countDownMsg, int time) 
	{
		for(int timer=time; timer>0; timer--)
		{
			m_roomChat.AddGlobalMessage(string.Format(TextUtil.CHAT_GAMESTARTING_COLOR + countDownMsg, timer), false);		// don't broadcast
			AudioManager.Instance.PlayOnce ("Ashram/User Interface/(Count Down) metallic accent tick", 0.4f);
			yield return new WaitForSeconds(1);
		}

		roomProperties = PhotonNetwork.room.customProperties;
		string gameMap = (string) roomProperties["game_map"];

		if(gameMap != null && gameMap.Equals ("Ruins of Ashram"))
		{
			GameNetworkManager.Instance.CurrentScene = GameNetworkManager.Instance.GameScene;
			PhotonNetwork.LoadLevel(GameNetworkManager.Instance.GameScene);
		}
		else
		{
			GameNetworkManager.Instance.CurrentScene = GameNetworkManager.Instance.GameScene2;
			PhotonNetwork.LoadLevel(GameNetworkManager.Instance.GameScene2);
		}



		GameNetworkManager.Instance.createLocalNetworkManager();
		yield return null;
	}

	/// <summary>
	/// Dos the start game.
	/// </summary>
	void DoStartGame()
	{
		// Pre-condition to start game (Teams should be already set when joining slot, on JoinSlotHandler.cs)
		// Death-Match and Team-DeathMatch game mode integrated for the game
		BroadcastStartGame();
	}

	/// <summary>
	/// Broadcasts the start game to everyone in the room and start the countdown
	/// </summary>
	void BroadcastStartGame()
	{
		photonView.RPC("StartCountDown", PhotonTargets.All);
	}
	
	/// <summary>
	/// Load the level, and since all players have already arrived in the room
	/// Create the input/output stream for network communication
	/// </summary>
	/// <param name="info">Info.</param>
	[RPC]
	void StartCountDown() 
	{
		int secondsToStart = 5;
		StartCoroutine( CountDown("Game Starting in: {0}.", secondsToStart) );

		isGameStarting = true;
		m_leaveRoomButton.isEnabled = false;
	}

	/// <summary>
	/// Raises the click event.
	/// </summary>
	void OnClick()
	{
		StartGame();
	}
	
	void OnHover()
	{
	}
}
