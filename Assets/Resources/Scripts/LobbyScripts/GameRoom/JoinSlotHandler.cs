﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Join slot handler that handles the functionality when the user clicks on a Slot in game room
/// </summary>
public class JoinSlotHandler : Photon.MonoBehaviour {

	[SerializeField] GameObject m_userName;
	[SerializeField] GameObject m_avatar;
	[SerializeField] GameObject m_joinSlot;
	[SerializeField] Team m_slotTeam;		// the team that this slot belongs to

	[SerializeField] StartGameHandler m_startGameHandler;

	private ExitGames.Client.Photon.Hashtable roomProperties = null;
	private string gameMode;
	private int gameTime;

	/// <summary>
	/// Raises the click event. E.g. user click on the slot in game room
	/// </summary>
	void OnClick()
	{
		if(IsEmptySlot() && m_startGameHandler.isGameStarting == false)
		{
			roomProperties = PhotonNetwork.room.customProperties;

			gameMode = (string) roomProperties["game_mode"];
			gameTime = int.Parse((string) roomProperties["game_time"]);

			SetGameTime();

			string slotName = gameObject.name;

			// Remove user from his current taken slot
			ClearSlot(PhotonNetwork.playerName);

			// Then take a new slot for him
			TakeNewSlot (slotName, PhotonNetwork.playerName);

			PhotonNetwork.room.SetCustomProperties(roomProperties);
		}
	}

	/// <summary>
	/// Slots the display name of the player.
	/// </summary>
	/// <param name="playerName">Player name.</param>
	void SlotDisplayPlayerName(string playerName)
	{
		string hostName = (string) roomProperties["player_host"];
		if( !string.IsNullOrEmpty(hostName) && playerName.Equals (hostName))
		{
			playerName += "*"; 
		}

		//Debug.Log ("AM I THE MASTER CLIENT: "+PhotonNetwork.isMasterClient);

		m_userName.GetComponent<UILabel>().text = playerName;
		
		m_avatar.gameObject.SetActive(true);
		m_userName.gameObject.SetActive(true);
		m_joinSlot.gameObject.SetActive(false);
	}

	/// <summary>
	/// Resets the slot display.
	/// </summary>
	void ResetSlotDisplay()
	{
		//m_userName.GetComponent<UILabel>().text = playerName;
		
		m_avatar.gameObject.SetActive(false);
		m_userName.gameObject.SetActive(false);
		m_joinSlot.gameObject.SetActive(true);
	}

	/// <summary>
	/// Increments the slot, and updates the room properties
	/// </summary>
	void IncrementSlot()
	{
		int numberOfSlotsTaken = (int) roomProperties["number_of_slots_taken"];
		roomProperties["number_of_slots_taken"] = numberOfSlotsTaken + 1;
	}

	/// <summary>
	/// Decrements the slot counter, and updates the room properties
	/// </summary>
	void DecrementSlot()
	{
		int numberOfSlotsTaken = (int) roomProperties["number_of_slots_taken"];
		roomProperties["number_of_slots_taken"] = numberOfSlotsTaken - 1;
	}

	/// <summary>
	/// Takes the new slot. Update the room properties accordingly
	/// </summary>
	/// <param name="slotName">Slot name.</param>
	/// <param name="playerName">Player name.</param>
	void TakeNewSlot(string slotName, string playerName)
	{
		if(slotName.ToLower ().Equals ("slot1"))
		{
			roomProperties["slot_1"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
		else if(slotName.ToLower ().Equals ("slot2"))
		{
			roomProperties["slot_2"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
		else if(slotName.ToLower ().Equals ("slot3"))
		{
			roomProperties["slot_3"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
		else if(slotName.ToLower ().Equals ("slot4"))
		{
			roomProperties["slot_4"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
		else if(slotName.ToLower ().Equals ("slot5"))
		{
			roomProperties["slot_5"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
		else if(slotName.ToLower ().Equals ("slot6"))
		{
			roomProperties["slot_6"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
		else if(slotName.ToLower ().Equals ("slot7"))
		{
			roomProperties["slot_7"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
		else if(slotName.ToLower ().Equals ("slot8"))
		{
			roomProperties["slot_8"] = playerName;
			SetMyTeam();
			IncrementSlot();
		}
	}

	/// <summary>
	/// Clears the slot. Update the room properties accordingly
	/// </summary>
	/// <param name="userName">User name.</param>
	void ClearSlot(string userName)
	{
		string slot1 = (string) roomProperties["slot_1"];
		string slot2 = (string) roomProperties["slot_2"];
		string slot3 = (string) roomProperties["slot_3"];
		string slot4 = (string) roomProperties["slot_4"];
		string slot5 = (string) roomProperties["slot_5"];
		string slot6 = (string) roomProperties["slot_6"];
		string slot7 = (string) roomProperties["slot_7"];
		string slot8 = (string) roomProperties["slot_8"];
		
		if( !string.IsNullOrEmpty(slot1) && slot1.Equals (userName)  )
		{
			roomProperties["slot_1"] = "";
			DecrementSlot();
		}
		else if( !string.IsNullOrEmpty(slot2) && slot2.Equals (userName)  )
		{
			roomProperties["slot_2"] = "";
			DecrementSlot();
		}
		else if( !string.IsNullOrEmpty(slot3) && slot3.Equals (userName)  )
		{
			roomProperties["slot_3"] = "";
			DecrementSlot();
		}
		else if( !string.IsNullOrEmpty(slot4) && slot4.Equals (userName)  )
		{
			roomProperties["slot_4"] = "";
			DecrementSlot();
		}
		else if( !string.IsNullOrEmpty(slot5) && slot5.Equals (userName)  )
		{
			roomProperties["slot_5"] = "";
			DecrementSlot();
		}
		else if( !string.IsNullOrEmpty(slot6) && slot6.Equals (userName)  )
		{
			roomProperties["slot_6"] = "";
			DecrementSlot();
		}
		else if( !string.IsNullOrEmpty(slot7) && slot7.Equals (userName)  )
		{
			roomProperties["slot_7"] = "";
			DecrementSlot();
		}
		else if( !string.IsNullOrEmpty(slot8) && slot8.Equals (userName)  )
		{
			roomProperties["slot_8"] = "";
			DecrementSlot();
		}
	}

	/// <summary>
	/// Refreshs the slot display. This will also be used when the client first joined the room in the RoomPlayerListLoader
	/// </summary>
	public void RefreshSlotDisplay()
	{
		roomProperties = PhotonNetwork.room.customProperties;
		
		string slot1 = (string) roomProperties["slot_1"];
		string slot2 = (string) roomProperties["slot_2"];
		string slot3 = (string) roomProperties["slot_3"];
		string slot4 = (string) roomProperties["slot_4"];
		string slot5 = (string) roomProperties["slot_5"];
		string slot6 = (string) roomProperties["slot_6"];
		string slot7 = (string) roomProperties["slot_7"];
		string slot8 = (string) roomProperties["slot_8"];
		
		string slotName = gameObject.name;
		
		if( string.IsNullOrEmpty(slot1) && slotName.ToLower().Equals("slot1"))
		{
			ResetSlotDisplay();
		}
		
		if( string.IsNullOrEmpty(slot2) && slotName.ToLower().Equals("slot2"))
		{
			ResetSlotDisplay();
		}
		
		if( string.IsNullOrEmpty(slot3) && slotName.ToLower().Equals("slot3"))
		{
			ResetSlotDisplay();
		}
		
		if( string.IsNullOrEmpty(slot4) && slotName.ToLower().Equals("slot4"))
		{
			ResetSlotDisplay();
		}
		
		if( string.IsNullOrEmpty(slot5) && slotName.ToLower().Equals("slot5"))
		{
			ResetSlotDisplay();
		}
		if( string.IsNullOrEmpty(slot6) && slotName.ToLower().Equals("slot6"))
		{
			ResetSlotDisplay();
		}
		if( string.IsNullOrEmpty(slot7) && slotName.ToLower().Equals("slot7"))
		{
			ResetSlotDisplay();
		}
		
		if( string.IsNullOrEmpty(slot8) && slotName.ToLower().Equals("slot8"))
		{
			ResetSlotDisplay();
		}
		
		// Display player name
		if( !string.IsNullOrEmpty(slot1) && slotName.ToLower().Equals("slot1"))
		{
			SlotDisplayPlayerName(slot1);
		}
		
		if( !string.IsNullOrEmpty(slot2) && slotName.ToLower().Equals("slot2"))
		{
			SlotDisplayPlayerName(slot2);
		}
		
		if( !string.IsNullOrEmpty(slot3) && slotName.ToLower().Equals("slot3"))
		{
			SlotDisplayPlayerName(slot3);
		}
		
		if( !string.IsNullOrEmpty(slot4) && slotName.ToLower().Equals("slot4"))
		{
			SlotDisplayPlayerName(slot4);
		}
		
		if( !string.IsNullOrEmpty(slot5) && slotName.ToLower().Equals("slot5"))
		{
			SlotDisplayPlayerName(slot5);
		}
		
		if( !string.IsNullOrEmpty(slot6) && slotName.ToLower().Equals("slot6"))
		{
			SlotDisplayPlayerName(slot6);
		}
		
		if( !string.IsNullOrEmpty(slot7) && slotName.ToLower().Equals("slot7"))
		{
			SlotDisplayPlayerName(slot7);
		}
		
		if( !string.IsNullOrEmpty(slot8) && slotName.ToLower().Equals("slot8"))
		{
			SlotDisplayPlayerName(slot8);
		}
	}

	/// <summary>
	/// Raises the photon custom room properties changed event. E.g. when slot become taken or empty, update the label
	/// </summary>
	void OnPhotonCustomRoomPropertiesChanged()
	{ 
		RefreshSlotDisplay();
	}
	
	/// <summary>
	/// Determines whether this instance is empty slot.
	/// </summary>
	/// <returns><c>true</c> if this instance is empty slot; otherwise, <c>false</c>.</returns>
	bool IsEmptySlot()
	{
		return m_joinSlot.activeSelf;
	}

	/// <summary>
	/// Sets the game time, when the user joins a slot
	/// </summary>
	void SetGameTime()
	{
		GameNetworkManager.Instance.setGameTime(gameTime);
	}

	/// <summary>
	/// Set Team functionality when user select a slot
	/// set local player to "Team.ON_YOUR_OWN" for Deathmatch
	/// set local player to either "Team.ONE" or "Team.TWO" for Team Deathmatch
	/// </summary>
	/// <param name="teamToSet">Team to set.</param>
	void SetTeam(Team teamToSet)
	{
		/*ExitGames.Client.Photon.Hashtable playerProperties = new ExitGames.Client.Photon.Hashtable();
		playerProperties.Add("Team", team);*/
		ExitGames.Client.Photon.Hashtable playerProperties = PhotonNetwork.player.customProperties;
		
		if(playerProperties == null)
		{
			Debug.LogError("Player custom properties is null, recreating!");
			playerProperties = new ExitGames.Client.Photon.Hashtable();
			playerProperties.Add ("wins", Player.WINCOUNT.ToString());
			playerProperties.Add ("loses", Player.LOSECOUNT.ToString());
			playerProperties.Add ("exp", UserProfileLoader.CalculateExp(Player.EXP, Player.LEVEL));
			playerProperties.Add ("level", Player.LEVEL.ToString());
			playerProperties.Add ("win_rate", UserProfileLoader.CalculateWinRatio(Player.WINCOUNT, Player.LOSECOUNT));
			playerProperties.Add ("matches_played", (Player.WINCOUNT + Player.LOSECOUNT).ToString());
			playerProperties.Add("Team", teamToSet);
		}
		else
		{
			if(playerProperties["Team"] == null)
				playerProperties.Add("Team", teamToSet);
			else
				playerProperties["Team"] = teamToSet;
		}
		
		PhotonNetwork.SetPlayerCustomProperties(playerProperties);
	}

	/// <summary>
	/// Sets my team.
	/// </summary>
	void SetMyTeam()
	{
		Team currentTeam;
		if(gameMode.Equals("Death-Match Classic"))
			currentTeam = Team.ON_YOUR_OWN;
		else
			currentTeam = m_slotTeam;

		SetTeam(currentTeam);
	}
}
