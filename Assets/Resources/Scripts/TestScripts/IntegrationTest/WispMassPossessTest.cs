﻿using UnityEngine;
using System.Collections;

public class WispMassPossessTest : TestCase {

	// test if the wisp can possess and release 4 creatures in order

	Creature wisp;
	Creature creature1;
	Creature creature2;
	Creature creature3;
	Creature creature4;

	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.WARRIOR, new Vector3(5,0,0), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.PRIEST, new Vector3(0,0,5), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.MAGE, new Vector3(5,0,5), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.ROGUE, new Vector3(5,0,0), Quaternion.identity);
	}

	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject creature1GO = Creator.Instance.getFirstAI(CreatureRace.OGRE, CreatureClass.WARRIOR);
		GameObject creature2GO = Creator.Instance.getFirstAI(CreatureRace.OGRE, CreatureClass.PRIEST);
		GameObject creature3GO = Creator.Instance.getFirstAI(CreatureRace.OGRE, CreatureClass.MAGE);
		GameObject creature4GO = Creator.Instance.getFirstAI(CreatureRace.OGRE, CreatureClass.ROGUE);

		wisp = wispGO.GetComponent<Creature>();
		creature1 = creature1GO.GetComponent<Creature>();
		creature2 = creature2GO.GetComponent<Creature>();
		creature3 = creature3GO.GetComponent<Creature>();
		creature4 = creature4GO.GetComponent<Creature>();
	}

	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(MassPossess());
	}

	IEnumerator MassPossess(){

		// possess
		wisp.UseSkillLater(wisp.getSkillList()[0], creature1);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		// release
		creature1.UseSkillLater(creature1.getSkillList()[0], creature1);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		// possess
		wisp = Creator.Instance.getFirstPlayerWisp().GetComponent<Creature>();
		wisp.UseSkillLater(wisp.getSkillList()[0], creature2);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		// release
		creature2.UseSkillLater(creature2.getSkillList()[0], creature2);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		// possess
		wisp = Creator.Instance.getFirstPlayerWisp().GetComponent<Creature>();
		wisp.UseSkillLater(wisp.getSkillList()[0], creature3);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		// release
		creature3.UseSkillLater(creature3.getSkillList()[0], creature3);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		// possess
		wisp = Creator.Instance.getFirstPlayerWisp().GetComponent<Creature>();
		wisp.UseSkillLater(wisp.getSkillList()[0], creature4);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// release
		creature4.UseSkillLater(creature4.getSkillList()[0], creature4);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		wisp = Creator.Instance.getFirstPlayerWisp().GetComponent<Creature>();
	}

	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {

		if(creature4 == null && wisp != null && PlayerCommand.Instance.isControlling(wisp)){
			IntegrationTest.Pass(this.gameObject);
		}


	}
}
