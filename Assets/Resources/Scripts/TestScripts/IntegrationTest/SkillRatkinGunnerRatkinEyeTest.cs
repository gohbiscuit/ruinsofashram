﻿using UnityEngine;
using System.Collections;

public class SkillRatkinGunnerRatkinEyeTest : TestCase {

	Creature wisp;
	Creature puppet;
	Creature target;

	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.GUNNER, new Vector3(-2,0,5), Quaternion.identity);
	}

	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject puppetGO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.GUNNER);

		wisp = wispGO.GetComponent<Creature>();
		puppet = puppetGO.GetComponent<Creature>();
	}

	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(PossessUseSkill());
	}

	IEnumerator PossessUseSkill(){
		
		// possess
		wisp.UseSkillLater(wisp.getSkillList()[0], puppet);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// use skill
		puppet.getSkillList()[4].Cooldown(9999);
		puppet.UseSkillLater(puppet.getSkillList()[4], puppet);
	}

	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(PlayerCommand.Instance.isControlling(puppet) && puppet.hasStatusEffectNow(StatusEffect.RATKIN_EYE))
			IntegrationTest.Pass(gameObject);
	}
}
