﻿using UnityEngine;
using System.Collections;

public class SkillWispTeleportTest: TestCase {

	Creature wisp;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		wisp = wispGO.GetComponent<Creature>();
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		wisp.getSkillList()[1].Cooldown(9999);
		wisp.UseSkillLater(wisp.getSkillList()[1], new Vector3(5,0,0));
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		
		if(Vector3.Distance(wisp.transform.position, new Vector3(5,0,0)) <= 1.0f)
			IntegrationTest.Pass(this.gameObject);
	}
}
