﻿using UnityEngine;
using System.Collections;

public class SkillRatkinShamanChainLightningTest : TestCase {

	Creature wisp;
	Creature puppet;
	Creature target1;
	Creature target2;
	Creature target3;

	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.SHAMAN, new Vector3(-2,0,5), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.MAGE, new Vector3(5,0,0), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.MAGE, new Vector3(7,0,5), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.MAGE, new Vector3(3,0,-5), Quaternion.identity);
	}

	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject puppetGO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.SHAMAN);
		GameObject targetGO1 = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.MAGE, 0);
		GameObject targetGO2 = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.MAGE, 1);
		GameObject targetGO3 = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.MAGE, 2);

		wisp = wispGO.GetComponent<Creature>();
		puppet = puppetGO.GetComponent<Creature>();
		target1 = targetGO1.GetComponent<Creature>();
		target2 = targetGO2.GetComponent<Creature>();
		target3 = targetGO3.GetComponent<Creature>();
	}

	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(PossessUseSkill());
	}

	IEnumerator PossessUseSkill(){
		
		// possess
		wisp.UseSkillLater(wisp.getSkillList()[0], puppet);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// use skill
		puppet.getSkillList()[3].Cooldown(9999);
		puppet.UseSkillLater(puppet.getSkillList()[3], target1);
	}

	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(PlayerCommand.Instance.isControlling(puppet) && 
		   target1.HP != target1.MaxHP &&
		   target2.HP != target2.MaxHP &&
		   target3.HP != target3.MaxHP)
			IntegrationTest.Pass(gameObject);
	}
}
