﻿using UnityEngine;
using System.Collections;

public class WispMoveStopTest : TestCase {
	
	// test if the wisp can move
	
	Creature wisp;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {
		
		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		wisp = wispGO.GetComponent<Creature>();
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(MoveThenStop());
	}
	
	IEnumerator MoveThenStop(){
		
		// possess
		wisp.MoveLater(new Vector3(8,0,8));
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// use skill
		wisp.StopLater();
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		IntegrationTest.Pass(this.gameObject);
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(Vector3.Distance(wisp.transform.position, new Vector3(8,0,8)) <= 1.0f)
			IntegrationTest.Fail(this.gameObject);
	}
}