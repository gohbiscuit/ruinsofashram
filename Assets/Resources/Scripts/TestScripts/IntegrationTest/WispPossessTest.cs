﻿using UnityEngine;
using System.Collections;

public class WispPossessTest : TestCase {

	// test if the wisp can possess an AI

	Creature wisp;
	Creature ogre1;

	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.WARRIOR, new Vector3(5,0,0), Quaternion.identity);
	}

	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject ogre1GO = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.WARRIOR, 0);

		wisp = wispGO.GetComponent<Creature>();
		ogre1 = ogre1GO.GetComponent<Creature>();
	}

	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		// possess creature
		wisp.UseSkillLater(wisp.getSkillList()[0], ogre1);
	}

	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {

		// now i'll test if the wisp manage to possess the ogre on every update
		if(wisp == null && PlayerCommand.Instance.isControlling(ogre1)){	// meaning wisp is gone
																			// and player now takes command of the ogre creature!
			IntegrationTest.Pass(this.gameObject);
		}


	}
}
