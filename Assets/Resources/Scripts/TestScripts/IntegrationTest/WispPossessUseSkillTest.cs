﻿using UnityEngine;
using System.Collections;

public class WispPossessUseSkillTest : TestCase {
	
	// test if the wisp can possess and release 4 creatures in order
	
	Creature wisp;
	Creature creature1;
	Creature creature2;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.WARRIOR, new Vector3(5,0,0), Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.GUNNER, new Vector3(0,0,5), Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject creature1GO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.WARRIOR);
		GameObject creature2GO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.GUNNER);
		
		wisp = wispGO.GetComponent<Creature>();
		creature1 = creature1GO.GetComponent<Creature>();
		creature2 = creature2GO.GetComponent<Creature>();
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(PossessUseSkill());
	}
	
	IEnumerator PossessUseSkill(){
		
		// possess
		wisp.UseSkillLater(wisp.getSkillList()[0], creature2);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// use skill
		creature2.UseSkillLater(creature2.getSkillList()[2], creature1);
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		
		if(creature1.getStats().Curr_HP != creature1.getStats().HP){	// took damage
			IntegrationTest.Pass(this.gameObject);
		}
		
		
	}
}
