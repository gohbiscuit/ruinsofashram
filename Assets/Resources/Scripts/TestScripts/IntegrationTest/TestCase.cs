﻿using UnityEngine;
using System.Collections;

public abstract class TestCase : MonoBehaviour {

	CreatureAISpawn dummySpawn;

	// destroy all existing creatures from previous test cases
	void Awake () {

		PlayerCommand.Instance.getPlayerAttributes().ResetAttributes();

		GameObject[] oldCreaturesGO = GameObject.FindGameObjectsWithTag("Creature");
		for(int i=0; i<oldCreaturesGO.Length; i++){
			Creature creatureToDestroy = oldCreaturesGO[i].GetComponent<Creature>();
			if(creatureToDestroy != null)
				oldCreaturesGO[i].GetComponent<Creature>().Die(null);
			else
				Debug.Log("Unable to make " + creatureToDestroy.gameObject.name + " die");
		}

		GameObject[] oldCreaturesGODest = GameObject.FindGameObjectsWithTag("CreatureDest");
		for(int i=0; i<oldCreaturesGODest.Length; i++){
			Destroy(oldCreaturesGODest[i]);
		}

		dummySpawn = GameObject.FindGameObjectWithTag("SpawnPoint").GetComponent<CreatureAISpawn>();

		StartCoroutine("WaitToReady");
	}

	bool canCreateCreature;
	bool readyToUpdate;
	IEnumerator WaitToReady(){
		yield return new WaitForSeconds(0.1f);
		canCreateCreature = true;

		// destroy all corpses
		GameObject[] oldCorpseGO = GameObject.FindGameObjectsWithTag("Corpse");
		for(int i=0; i<oldCorpseGO.Length; i++){
			Destroy(oldCorpseGO[i]);
		}

		// create the creatures for the new test case
		BeginCreatureCreation();

		// give it time for creatures to get created
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.1f);

		// run actual test case
		TestAwake();
		TestStart();
		readyToUpdate = true;
	}

	protected void CreateWisp(Vector3 position, Quaternion rotation){
		if(!canCreateCreature)
			Debug.LogError("Cannot create creature!");
		else
			Creator.Instance.createMyWispLater(dummySpawn, position, rotation);
	}

	protected void CreateAI(CreatureRace cRace, CreatureClass cClass, Vector3 position, Quaternion rotation){
		if(!canCreateCreature)
			Debug.LogError("Cannot create creature!");
		else
			Creator.Instance.createAILater(dummySpawn, cRace, cClass, position, rotation);
	}
	
	void Update(){
		if(readyToUpdate)
			TestUpdate();
	}
	
	protected virtual void BeginCreatureCreation(){}
	protected virtual void TestAwake(){}
	protected virtual void TestStart(){}
	protected virtual void TestUpdate(){}
}