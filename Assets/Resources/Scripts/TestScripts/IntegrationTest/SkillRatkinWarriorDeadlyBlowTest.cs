﻿using UnityEngine;
using System.Collections;

public class SkillRatkinWarriorDeadlyBlowTest : TestCase {

	Creature wisp;
	Creature puppet;
	Creature target;

	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.WARRIOR, new Vector3(-2,0,5), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.MAGE, new Vector3(5,0,0), Quaternion.identity);
	}

	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject puppetGO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.WARRIOR);
		GameObject targetGO = Creator.Instance.getFirstAI(CreatureRace.OGRE, CreatureClass.MAGE);

		wisp = wispGO.GetComponent<Creature>();
		puppet = puppetGO.GetComponent<Creature>();
		target = targetGO.GetComponent<Creature>();
	}

	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(PossessUseSkill());
	}

	IEnumerator PossessUseSkill(){
		
		// possess
		wisp.UseSkillLater(wisp.getSkillList()[0], puppet);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// use skill
		puppet.getSkillList()[3].Cooldown(9999);
		puppet.UseSkillLater(puppet.getSkillList()[3], target);
	}

	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(PlayerCommand.Instance.isControlling(puppet) && target.HP != target.MaxHP
		   && target.hasStatusEffectNow(StatusEffect.SLOW))
			IntegrationTest.Pass(gameObject);
	}
}
