﻿using UnityEngine;
using System.Collections;

public class WispUseSkillStopTest : TestCase {
	
	// test if the wisp can move
	
	Creature wisp;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		wisp = wispGO.GetComponent<Creature>();
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(TeleportThenStop());
	}
	
	IEnumerator TeleportThenStop(){
		
		// possess
		wisp.UseSkillLater(wisp.getSkillList()[1], new Vector3(8,0,8));
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + wisp.getSkillList()[1].getMaxChargeTime()/2);
		
		// use skill
		wisp.StopLater();
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);

		IntegrationTest.Pass(this.gameObject);
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(Vector3.Distance(wisp.transform.position, new Vector3(8,0,8)) <= 1.0f)
			IntegrationTest.Fail(this.gameObject);
	}
}