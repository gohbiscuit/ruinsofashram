﻿using UnityEngine;
using System.Collections;

public class SkillOgrePriestOgreAuraTest : TestCase {
	
	Creature wisp;
	Creature puppet;
	Creature target;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.PRIEST, new Vector3(-2,0,5), Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.SHAMAN, new Vector3(5,0,0), Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {
		
		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject puppetGO = Creator.Instance.getFirstAI(CreatureRace.OGRE, CreatureClass.PRIEST);
		GameObject targetGO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.SHAMAN);
		
		wisp = wispGO.GetComponent<Creature>();
		wisp.setTeam(Team.ONE);
		
		puppet = puppetGO.GetComponent<Creature>();
		target = targetGO.GetComponent<Creature>();
		target.setTeam(Team.ONE);
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(PossessUseSkill());
	}
	
	IEnumerator PossessUseSkill(){
		
		// possess
		wisp.UseSkillLater(wisp.getSkillList()[0], puppet);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// use skill
		puppet.MoveLater(target);
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(PlayerCommand.Instance.isControlling(puppet) && target.hasStatusEffectNow(StatusEffect.ORGE_AURA))
			IntegrationTest.Pass(gameObject);
	}
}
