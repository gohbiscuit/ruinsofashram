﻿using UnityEngine;
using System.Collections;

public class DieTest : TestCase {
	
	// test if the wisp can possess and release 4 creatures in order
	
	Creature wisp;
	Creature creature1;
	Creature creature2;
	Creature creature3;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.GUNNER, new Vector3(5,0,0), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.MAGE, new Vector3(0,0,5), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.MAGE, new Vector3(5,0,5), Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject creature1GO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.GUNNER);
		GameObject creature2GO = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.MAGE, 0);
		GameObject creature3GO = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.MAGE, 1);
		
		wisp = wispGO.GetComponent<Creature>();
		creature1 = creature1GO.GetComponent<Creature>();
		creature2 = creature2GO.GetComponent<Creature>();
		creature3 = creature3GO.GetComponent<Creature>();
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(PossessUseSkill());
	}
	
	IEnumerator PossessUseSkill(){
		
		// possess
		wisp.UseSkillLater(wisp.getSkillList()[0], creature1);
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
		
		// use skill
		creature1.UseSkillLater(creature1.getSkillList()[2], creature2);
		while(creature2 != null)
			yield return new WaitForSeconds(0.1f);

		// use skill
		creature1.UseSkillLater(creature1.getSkillList()[3], creature3);
		while(creature3 != null)
			yield return new WaitForSeconds(0.1f);
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {

		// for purpose of test, make the player always full HP and MP
		Creature playerCreature = PlayerCommand.Instance.getControlledCreature();
		if(playerCreature != null){
			playerCreature.getStats().Curr_HP = playerCreature.getStats().HP;
			playerCreature.getStats().Curr_MP = playerCreature.getStats().MP;
		}
		
		if(creature2 == null && creature3 == null){	// both creatures die
			IntegrationTest.Pass(this.gameObject);
		}
	}
}
