﻿using UnityEngine;
using System.Collections;

public class AIReturnTest : TestCase {
	
	// test for state change - engage to return
	
	Creature wisp;
    GameObject wispGO;
    Creature AI;
    GameObject AIGO;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
        CreateAI(CreatureRace.RATKIN, CreatureClass.GUNNER, new Vector3(-2, 0, 5), Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

        wispGO = Creator.Instance.getFirstPlayerWisp();
        AIGO = Creator.Instance.getFirstAI(CreatureRace.RATKIN, CreatureClass.GUNNER);

        wisp = wispGO.GetComponent<Creature>();
        AI = AIGO.GetComponent<Creature>();
        AI.StopLocallyNow();
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		StartCoroutine(MoveThenStop());
	}
	
	IEnumerator MoveThenStop(){
		
		// possess
		wisp.MoveLater(new Vector3(-23,0,-23));
		yield return new WaitForSeconds(GameNetworkManager.Instance.getTotalLag() + 0.5f);
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if((AI.gameObject.transform.position - wisp.gameObject.transform.position).magnitude > 14)
        {
            IntegrationTest.Pass(gameObject);
        }
	}
}