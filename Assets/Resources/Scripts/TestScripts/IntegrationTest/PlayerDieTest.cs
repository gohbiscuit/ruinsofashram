﻿using UnityEngine;
using System.Collections;

public class PlayerDieTest : TestCase {
	
	// test if the wisp can possess and release 4 creatures in order
	
	Creature wisp;
	Creature creature1;
	Creature creature2;
	
	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.WARRIOR, new Vector3(5,0,0), Quaternion.identity);
		CreateAI(CreatureRace.RATKIN, CreatureClass.WARRIOR, new Vector3(0,0,5), Quaternion.identity);
	}
	
	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject creature1GO = Creator.Instance.getAI(CreatureRace.RATKIN, CreatureClass.WARRIOR, 0);
		GameObject creature2GO = Creator.Instance.getAI(CreatureRace.RATKIN, CreatureClass.WARRIOR, 1);
		
		wisp = wispGO.GetComponent<Creature>();
		wisp.getStats().Curr_HP = 100;	// reduce for testing's sake
		creature1 = creature1GO.GetComponent<Creature>();
		creature2 = creature2GO.GetComponent<Creature>();
	}
	
	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
	}
	
	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(wisp == null){	// wisp dies
			IntegrationTest.Pass(this.gameObject);
		}
	}
}
