﻿using UnityEngine;
using System.Collections;

public class TestCaseExample : TestCase {

	// a simple example where
	// 1 wisp, 2 ogres are spawned
	// wisp moves up
	// pass if ogres follow and damage wisp

	Creature wisp;
	Creature ogre1;
	Creature ogre2;

	/// <summary>
	/// Use this for creature creation at the beginning of the test case
	/// </summary>
	protected override void BeginCreatureCreation () {
		CreateWisp(Vector3.zero, Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.WARRIOR, new Vector3(5,0,0), Quaternion.identity);
		CreateAI(CreatureRace.OGRE, CreatureClass.WARRIOR, new Vector3(-5,0,0), Quaternion.identity);
	}

	/// <summary>
	/// Use this for initialisation (executed before TestStart). 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestAwake () {

		// one of many ways to get the creatures created in BeginCreatureCreation()
		// see Creator.Instance.getPlayer... & Creator.Instance.getAI... for more details
		GameObject wispGO = Creator.Instance.getFirstPlayerWisp();
		GameObject ogre1GO = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.WARRIOR, 0);
		GameObject ogre2GO = Creator.Instance.getAI(CreatureRace.OGRE, CreatureClass.WARRIOR, 1);

		wisp = wispGO.GetComponent<Creature>();
		ogre1 = ogre1GO.GetComponent<Creature>();
		ogre2 = ogre2GO.GetComponent<Creature>();
	}

	/// <summary>
	/// Use this for initialisation. 
	/// All creatures should have been spawned at this stage.
	/// </summary>
	protected override void TestStart () {
		// move wisp up for fun
		wisp.MoveLater(new Vector3(0, 0, 5));
	}

	/// <summary>
	/// Same as Update(), except now all creatures are spawned.
	/// </summary>
	protected override void TestUpdate () {
		if(wisp.HP != wisp.MaxHP)
			IntegrationTest.Pass(gameObject);	// passes if wisp takes damage
												// there's also Integration.Fail(gameObject)
												// will fail automatically when test expires
												// (see timeout under the test case GO)
	}
}
