﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class SkillFactoryTest {

	CreatureStatusEffects status = null;

	// this initialisation function will be automatically 
	// executed per [Test] functions by NUnit.
	[SetUp]
	public void Init(){
		SkillFactory skillFactory = new SkillFactory();
		skillFactory.Awake();
	}

	[Test]
	public void GetWispSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.WISP, CreatureClass.WISP);
		Assert.That(skills[0].SkillName.Equals("Possess"));
		Assert.That(skills[1].SkillName.Equals("Teleport"));
	}

	[Test]
	public void GetRatkinWarriorSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.RATKIN, CreatureClass.WARRIOR);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Thunder Clap"));
		Assert.That(skills[3].SkillName.Equals("Deadly Blow"));
		Assert.That(skills[4].SkillName.Equals("Shield Barrier"));
	}

	[Test]
	public void GetRatkinGunnerSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.RATKIN, CreatureClass.GUNNER);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Rapid Fire"));
		Assert.That(skills[3].SkillName.Equals("Gunner's Assassination"));
		Assert.That(skills[4].SkillName.Equals("Ratkin's Eye"));
	}

	[Test]
	public void GetRatkinMageSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.RATKIN, CreatureClass.MAGE);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Arcane Blast"));
		Assert.That(skills[3].SkillName.Equals("Impact Wave"));
		Assert.That(skills[4].SkillName.Equals("Freezing Shield"));
	}

	[Test]
	public void GetRatkinShamanSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.RATKIN, CreatureClass.SHAMAN);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Divine Heal"));
		Assert.That(skills[3].SkillName.Equals("Chain Lightning"));
		Assert.That(skills[4].SkillName.Equals("Earth Shield"));
	}

	[Test]
	public void GetOgreWarriorSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.OGRE, CreatureClass.WARRIOR);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Earth Quake"));
		Assert.That(skills[3].SkillName.Equals("Skull Crusher"));
		Assert.That(skills[4].SkillName.Equals("Battle Roar"));
	}

	[Test]
	public void GetOgreRogueSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.OGRE, CreatureClass.ROGUE);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Assassin's Lock"));
		Assert.That(skills[3].SkillName.Equals("Assassin's Strike"));
		Assert.That(skills[4].SkillName.Equals("Backstab"));
	}

	[Test]
	public void GetOgreMageSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.OGRE, CreatureClass.MAGE);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Fireball"));
		Assert.That(skills[3].SkillName.Equals("Infernal Scorch"));
		Assert.That(skills[4].SkillName.Equals("Blink"));
	}

	[Test]
	public void GetOgrePriestSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.OGRE, CreatureClass.PRIEST);
		Assert.That(skills[0].SkillName.Equals("Release"));
		Assert.That(skills[1].SkillName.Equals("Basic Attack"));
		Assert.That(skills[2].SkillName.Equals("Holy Light"));
		Assert.That(skills[3].SkillName.Equals("Mana Shield"));
		Assert.That(skills[4].SkillName.Equals("Ogre's Aura"));
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "Invalid creature or no skill for the creature")]
	public void GetInvalidSkills(){
		List<Skill> skills = SkillFactory.Instance.getCreatureSkills(CreatureRace.DEMON, CreatureClass.ARCHER);
	}

}