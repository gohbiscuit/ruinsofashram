﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class AudioManagerTest : MonoBehaviour {

	// this initialisation function will be automatically 
	// executed per [Test] functions by NUnit.
	[SetUp]
	public void Init(){
        AudioManager.Instance.Add("Test");
        AudioManager.Instance.Add("Ashram/User Interface/Background Music/Approaching the WestLands");
	}

    [Test]
    public void TestPlay(){
        Assert.That(AudioManager.Instance.Play("Ashram/User Interface/Background Music/Approaching the WestLands"));
    }

    [Test]
    public void TestPlayOnce()
    {
        Assert.That(AudioManager.Instance.PlayOnce("Test", 0));
    }

    [Test]
    public void TestLoop()
    {
        Assert.That(AudioManager.Instance.Loop("Test"));
    }

    [Test]
    public void TestStop()
    {
        Assert.That(!AudioManager.Instance.Stop("No such name"));
    }

    [Test]
    public void TestStop2()
    {
        Assert.That(AudioManager.Instance.Stop("Test"));
    }

    [Test]
    public void TestIsPlaying()
    {
        AudioManager.Instance.Play("Ashram/User Interface/Background Music/Approaching the WestLands");
        Assert.That(AudioManager.Instance.IsPlaying("Ashram/User Interface/Background Music/Approaching the WestLands"));
    }
}