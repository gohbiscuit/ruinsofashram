﻿using System;
using UnityEngine;
using NUnit.Framework;

public class CreatureStatsCalculatorTest {

	// this initialisation function will be automatically 
	// executed per [Test] functions by NUnit.
	[SetUp]
	public void Init(){
		CreatureStatsCalculator calc = new CreatureStatsCalculator();
		calc.Awake();
	}

	// TEST computeUnpossessedStats

	[Test]
	public void ComputeUnpossessedCreature(){
		CreatureStatsCalculator.Instance.computeUnpossessedStats(CreatureRace.OGRE,CreatureClass.WARRIOR);
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "invalid creature type")]
	public void ComputeUnpossessedCreatureOfInvalidClass(){
		CreatureStatsCalculator.Instance.computeUnpossessedStats(CreatureRace.OGRE,CreatureClass.GUNNER);
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "invalid creature type")]
	public void ComputeUnpossessedCreatureOfInvalidRace(){
		CreatureStatsCalculator.Instance.computeUnpossessedStats(CreatureRace.DEMON,CreatureClass.ARCHER);
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "invalid creature type")]
	public void ComputeUnpossessedCreatureOfInvalidRaceAndClass(){
		CreatureStatsCalculator.Instance.computeUnpossessedStats(CreatureRace.SKELETON,CreatureClass.WARLOCK);
	}

	// TEST loadStats

	[Test]
	public void CanLoadData(){
		CreatureStatsCalculator.Instance.LoadStats();
	}

	// TEST getCreatureStats

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "invalid length")]
	public void GetStatsFromInvalidStringArray(){
		CreatureStatsCalculator.Instance.getCreatureStats(new string[2]);
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "wrong string array format")]
	public void GetStatsFromEmptyStringArray(){
		CreatureStatsCalculator.Instance.getCreatureStats(new string[14]);
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "wrong string array format")]
	public void GetStatsFromNonNumericStringArray(){
		string[] statsString = new string[14];
		for(int i=0; i<14; i++)
			statsString[i] = "KK";
		CreatureStatsCalculator.Instance.getCreatureStats(statsString);
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "wrong string array format")]
	public void GetStatsFromIncorrectStringArrayFormat(){
		string[] statsString = new string[14];
		for(int i=0; i<14; i++)
			statsString[i] = "123.123";
		CreatureStatsCalculator.Instance.getCreatureStats(statsString);
	}

	[Test]
	public void GetStatsFromStringArray(){
		string[] statsString = new string[14];
		for(int i=0; i<14; i++)
			statsString[i] = "10";
		CreatureStatsCalculator.Instance.getCreatureStats(statsString);
	}

	// TEST getSerializedCreatureStats

	[Test]
	public void GetSerializedCreatureStats(){
		CreatureStats stats = new CreatureStats();
		CreatureStatsCalculator.Instance.getSerizalizedCreatureStats(stats);
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "null stats object")]
	public void GetNullSerializedCreatureStats(){
		CreatureStatsCalculator.Instance.getSerizalizedCreatureStats(null);
	}
}