﻿using System;
using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;

public class SkillTest {

	List<Skill> allSkills = null;

	// this initialisation function will be automatically 
	// executed per [Test] functions by NUnit.
	[SetUp]
	public void Init(){
		SkillFactory skillFactory = new SkillFactory();
		skillFactory.Awake();
		allSkills = new List<Skill>();

		// add all possible skills from all possible creatures
		addCreatureSkills(allSkills, CreatureRace.WISP, CreatureClass.WISP);
		addCreatureSkills(allSkills, CreatureRace.OGRE, CreatureClass.WARRIOR);
		addCreatureSkills(allSkills, CreatureRace.OGRE, CreatureClass.ROGUE);
		addCreatureSkills(allSkills, CreatureRace.OGRE, CreatureClass.MAGE);
		addCreatureSkills(allSkills, CreatureRace.OGRE, CreatureClass.PRIEST);
		addCreatureSkills(allSkills, CreatureRace.RATKIN, CreatureClass.WARRIOR);
		addCreatureSkills(allSkills, CreatureRace.RATKIN, CreatureClass.MAGE);
		addCreatureSkills(allSkills, CreatureRace.RATKIN, CreatureClass.SHAMAN);
		addCreatureSkills(allSkills, CreatureRace.RATKIN, CreatureClass.GUNNER);
	}

	List<Skill> addCreatureSkills(List<Skill> skills, CreatureRace CRace, CreatureClass CClass){
		List<Skill> creatureSkills = SkillFactory.Instance.getCreatureSkills(CRace, CClass);
		foreach(Skill creatureSkill in creatureSkills)
			skills.Add(creatureSkill);
		return skills;
	}


	[Test]
	public void HighCooldown(){
		foreach(Skill skill in allSkills){
			skill.Cooldown(9999);
			Assert.That(skill.isCooled());
		}
	}

	[Test]
	public void NormalCooldown(){
		foreach(Skill skill in allSkills){
			skill.Cooldown(1);
		}
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "Negative Cooldown")]
	public void InvalidCooldown(){
		foreach(Skill skill in allSkills){
			skill.Cooldown(-1);
		}
	}

	[Test]
	public void StartCharge(){
		foreach(Skill skill in allSkills){
			skill.StartCharging();
			Assert.That(skill.getCurrentChargeTime() >= 0);
		}
	}

	[Test]
	public void HighCharge(){
		foreach(Skill skill in allSkills){
			skill.StartCharging();
			skill.Charge(9999);
			Assert.That(skill.isCharged());
		}
	}

	[Test]
	[ExpectedException (typeof (ArgumentException), ExpectedMessage = "Negative Charge")]
	public void InvalidCharge(){
		foreach(Skill skill in allSkills){
			skill.StartCharging();
			skill.Charge(-1);
		}
	}

	[Test]
	public void NormalCharge(){
		foreach(Skill skill in allSkills){
			skill.StartCharging();
			skill.Charge(1);
		}
	}

	[Test]
	public void PositiveChargeTimeReduction(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getChargeTimeReduction() >= 0);
		}
	}

	[Test]
	public void PositiveCooldown(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getCooldown() >= 0);
		}
	}

	[Test]
	public void PositiveCooldownReduction(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getCooldownReduction() >= 0);
		}
	}

	[Test]
	public void PositiveChargeTime(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getCurrentChargeTime() >= 0);
		}
	}

	[Test]
	public void PositiveMaxChargeTime(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getMaxChargeTime() >= 0);
		}
	}

	[Test]
	public void PositiveMaxCooldownTime(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getMaxCooldown() >= 0);
		}
	}

	[Test]
	public void PositiveMPCost(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getMPCost() >= 0);
		}
	}

	[Test]
	public void PositiveRange(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getRange() >= 0);
		}
	}

	[Test]
	public void PositiveRangeIncrease(){
		foreach(Skill skill in allSkills){
			Assert.That(skill.getRangeIncrease() >= 0);
		}
	}
}