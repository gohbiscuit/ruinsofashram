﻿using System;
using UnityEngine;
using NUnit.Framework;

public class CreatureStatusEffectsTest {

	CreatureStatusEffects status = null;

	// this initialisation function will be automatically 
	// executed per [Test] functions by NUnit.
	[SetUp]
	public void Init(){
		status = new CreatureStatusEffects(null);
	}
	
	[Test]
	public void AssassinLockAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.ASSASSIN_LOCK, 10, 10);
		Assert.That(status.isAssassinLocked());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isAssassinLocked());
	}

	[Test]
	public void SlowAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.SLOW, 10, 10);
		Assert.That(status.isSlowed());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isSlowed());
	}

	[Test]
	public void ManaShieldAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.MANA_SHIELD, 10, 10);
		Assert.That(status.isManaShielded());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isManaShielded());
	}

	[Test]
	public void FreezingShieldAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.FREEZING_SHIELD, 10, 10);
		Assert.That(status.isFreezingShielded());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isFreezingShielded());
	}

	[Test]
	public void EarthShieldAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.EARTH_SHIELD, 10, 10);
		Assert.That(status.isEarthShielded());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isEarthShielded());
	}

	[Test]
	public void BattleRoarAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.BATTLE_ROAR, 10, 10);
		Assert.That(status.isBattleRoared());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isBattleRoared());
	}

	[Test]
	public void ShieldBarrierAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.SHIELD_BARRIER, 10, 10);
		Assert.That(status.isShieldBarriered());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isShieldBarriered());
	}

	[Test]
	public void RatkinEyeAfflictionAndRecovery(){
		status.AfflictStatusEffectsLocallyNow(StatusEffect.RATKIN_EYE, 10, 10);
		Assert.That(status.isRatkinEyed());
		status.UpdateStatusEffects(10);
		Assert.That(!status.isRatkinEyed());
	}
}