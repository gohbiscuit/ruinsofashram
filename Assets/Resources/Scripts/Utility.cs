﻿
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public class Utility : MonoBehaviour{

	const string StatsFile = "Stat";
	const string SkillsFile = "Skill";

	public static string CalculateMD5Hash(string input)
	{
    	// step 1, calculate MD5 hash from input
    	MD5 md5 = System.Security.Cryptography.MD5.Create();
    	byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
    	byte[] hash = md5.ComputeHash(inputBytes);
 
    	// step 2, convert byte array to hex string
    	StringBuilder sb = new StringBuilder();
    	for (int i = 0; i < hash.Length; i++)
    	{
        	sb.Append(hash[i].ToString("X2"));
    	}
    	return sb.ToString();
	}

	public static Dictionary<string, object[]> ReadStatCSV()
	{
		// Read files and return a dictionary from CSV
		TextAsset csv = (TextAsset)(Resources.Load("CSV/" + StatsFile));
		string[,] stats = CSVReader.SplitCsvGrid(csv.text);
		Dictionary<string, object[]> dictionary = new Dictionary<string, object[]>();

		// skip first headers
		int i = 1;
		while(true)
		{
			// finish reading
			if(stats[0,i] == null)
				break;

			string creatureName = stats[0,i] + "_" + stats[1,i];	// race_class

			dictionary.Add (creatureName, new object[]
			{
				int.Parse(stats[2,i]), 				// exp_given
				int.Parse(stats[3,i]), 				// hp
				int.Parse(stats[4,i]), 				// mp
				double.Parse(stats[5,i]), 			// hp_regen
				double.Parse(stats[6,i]), 			// mp_regen
				int.Parse(stats[7,i]), 				// phy_atk
				int.Parse(stats[8,i]), 				// mag_atk
				int.Parse (stats[9,i]), 			// phy_def
				int.Parse(stats[10,i]), 			// mag_def
				int.Parse(stats[11,i]), 			// evasion
				int.Parse(stats[12,i]), 			// movement_speed

				double.Parse(stats[13,i]), 			// strength multiplier	
				double.Parse(stats[14,i]), 			// agility multiplier	
				double.Parse(stats[15,i]), 			// int multiplier	
			});

            i++;
		}

		return dictionary;
	}

	public static void ReadSkillCSV(Dictionary<string, List<object[]>> skillParametersList, 
	                                Dictionary<int, string> skillDamageFormulaList)
	{
		// Read files and return a dictionary from CSV
		TextAsset csv = (TextAsset)(Resources.Load("CSV/" + SkillsFile));
		string[,] skills = CSVReader.SplitCsvGrid(csv.text);
		
		// skip first headers
		int i = 1;
		while(true)
		{
			if(skills[0,i] == null)
				break;

			int id = int.Parse(skills[0,i]);						// skill id
			string creatureName = skills[1,i] + "_" + skills[2,i];	// race_class

			// ordered by creature name, which has a list of skills
			if(!skillParametersList.ContainsKey(creatureName))
				skillParametersList.Add(creatureName, new List<object[]>());

			skillParametersList[creatureName].Add (new object[] {
				id, 								// skill id
				skills[3,i], 						// skill name
				int.Parse(skills[4,i]), 			// MP cost
				skills[5,i],						// Description 1
				skills[6,i],						// Description 2
				int.Parse(skills[8,i]) == 1, 		// is Persistent
				float.Parse(skills[9,i]), 			// range
				float.Parse(skills[10,i]), 			// range increase per SKI
				float.Parse(skills[11,i]), 			// cooldown
				float.Parse (skills[12,i]),			// cooldown reduction per SKI
				float.Parse(skills[13,i]), 			// charge duration
				float.Parse(skills[14,i]), 			// charge reduction per SKI
				int.Parse(skills[15,i]) == 1, 		// can select point
				int.Parse(skills[16,i]) == 1, 		// can select enemy
				int.Parse(skills[17,i]) == 1, 		// can select ally
				int.Parse(skills[18,i]) == 1, 		// is Execution on release
				int.Parse(skills[19,i]) == 1, 		// is Passive
			});

			string damageFormula = (string)(skills[7, i]).Replace(" ", string.Empty);
			skillDamageFormulaList.Add (id, damageFormula);	// damage formula
			
			i++;
		}

		// add 'release' skill to all creatures
		object[] releaseParameters = skillParametersList["ALL_ALL"][0];
		foreach(List<object[]> creatureSkills in skillParametersList.Values)
			creatureSkills.Insert(0, releaseParameters);
		skillParametersList["WISP_WISP"].RemoveAt(0);	// except for wisp
		skillParametersList.Remove("ALL_ALL");			// no longer required

		// test output
		/*foreach(string creatureName in skillParametersList.Keys){
			string skillNames = "";
			foreach(object[] para in skillParametersList[creatureName])
				skillNames += para[1].ToString() + ", ";
			Debug.Log(creatureName + " - " + skillNames);
		}*/
	}

	public static bool IsNumeric(string stringToTest)
	{
		int result;
		return int.TryParse(stringToTest, out result);
	}

	public static GameObject FindChild(GameObject parentGO, string childName)
	{
		return parentGO.transform.Find(childName).gameObject;
	}

	public static bool IsChildExist(GameObject parentGO, string childName)
	{
		return parentGO.transform.Find(childName) != null;
	}

	// Delete all active childs only
	public static void DeleteAllNGUIChilds(GameObject parentObj, bool deleteActiveOnly = false)
	{
		foreach (Transform childTransform in parentObj.transform) 
		{
			if(childTransform.gameObject.activeSelf)
			{
				childTransform.parent = null;		// to ensure immediate update in the GUI
				NGUITools.DestroyImmediate(childTransform.gameObject);
			}
			else if(deleteActiveOnly == false)
			{
				childTransform.parent = null;		// to ensure immediate update in GUI
				NGUITools.DestroyImmediate(childTransform.gameObject);
			}
		}
	}
	
}
