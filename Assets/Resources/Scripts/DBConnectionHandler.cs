﻿using UnityEngine;
using System;
using System.Collections;
 
public class DBConnectionHandler : MonoBehaviour {

    private string secretKey = "mySecretKey"; // Edit this value and make sure it's the same as the one stored on the server
    
	//be sure to add a ? to your url for GET method
	private readonly string DB_LOGIN_URL = "http://ashram.net78.net/ashram/do-login.php"; 

    void Start()
    {
        //StartCoroutine( Login("ahleow200", "qwerty") );
		//StartCoroutine( Login("gohbiscuit", "gohbiscuit") );
    }
 
    /* remember to use StartCoroutine when calling this function!
    IEnumerator LoginUser(string username, string password)
    {
        //This connects to a server side php script that will add the name and score to a MySQL DB.
        // Supply it with a string representing the players name and the players score.
        //string hash = MD5Test.Md5Sum(username + password);
 
        //string post_url = addScoreURL + "username=" + WWW.EscapeURL(username) + "&password=" + password + "&hash=" + hash;
 
		string post_url = DB_LOGIN_URL + "username=" + WWW.EscapeURL(username) + "&password=" + password;
		
        // Post the URL to the site and create a download object to get the result.
        WWW hs_post = new WWW(post_url);
        yield return hs_post; // Wait until the download is done
 
        if (hs_post.error != null)
        {
            Debug.Log("There was an error posting the high score: " + hs_post.error);
        }
		else
		{
			Debug.Log("Successfully login to the system!");
		}
    }
 
    // Get the scores from the MySQL DB to display in a GUIText.
    // remember to use StartCoroutine when calling this function!
    IEnumerator GetScores()
    {
       // gameObject.guiText.text = "Loading Scores";
        WWW hs_get = new WWW(highscoreURL);
        yield return hs_get;
 
        if (hs_get.error != null)
        {
            Debug.Log("There was an error getting the high score: " + hs_get.error);
        }
        else
        {
            //gameObject.guiText.text = hs_get.text; // this is a GUIText that will display the scores in game.
			Debug.Log ("SCORE IS: "+ hs_get.text);
        }
    }
	
	IEnumerator Login(string username, string password) 
	{
		string hash = "";
		string hashed_password = Utility.CalculateMD5Hash(password);
		
    	WWWForm form = new WWWForm(); //here you create a new form connection
    	//form.AddField( "myform_hash", hash ); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
    	form.AddField( "username", username );
    	form.AddField( "password", hashed_password.ToLower() );

		WWW loginResult = new WWW(DB_LOGIN_URL, form); //here we create a var called 'loginResult' and we sync with our URL and the form
		yield return loginResult; //we wait for the form to check the PHP file, so our game dont just hang
		
    	if ( !string.IsNullOrEmpty(loginResult.error) )
		{
			
        	Debug.Log("There is an error with the login, Error: "+loginResult.error); //if there is an error, tell us
    	}
		else  // Either successfully login or user pw does not match
		{	
        	string formText = loginResult.data; //here we return the data our PHP told us
        	loginResult.Dispose(); //clear our form in game
			Debug.Log(formText);
    	}
	}*/
}