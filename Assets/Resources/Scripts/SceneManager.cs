﻿using UnityEngine;
using System.Collections;

public enum SceneType {Start, Lobby, GameRoom, Game}

public class SceneManager : MonoBehaviour{
	
	public static SceneType CurrentScene;
	
	private static SceneManager instance;
	
	// Singleton Design Pattern
   	public static SceneManager Instance 
	{
       	get {
			if(instance == null) {
            	var go = new GameObject("SceneManager");
            	instance = go.AddComponent<SceneManager>();
			}
         	return instance;
		}
	}
	
	// There are 2 types of loading
	// Async / Non-Async
	// For async visit: 	// Visit: http://docs.unity3d.com/Documentation/ScriptReference/Application.LoadLevelAsync.html for more info
	// Co-routine requires instance methods calling thus Singleton is used
	public void LoadLevel(SceneType type)
	{
		switch(type)
		{
			case SceneType.Start:
			StartCoroutine( LoadStartScene() );	
			break;
			
			case SceneType.Lobby:
			StartCoroutine( LoadLobbyScene() );
			break;
			
			case SceneType.GameRoom:
			StartCoroutine( LoadGameRoomScene() );
			break;
			
			case SceneType.Game:
			StartCoroutine( LoadGameScene() );
			break;
		}
	}
	
	// Login Scene
	private IEnumerator LoadStartScene(bool asynchronous = false)
	{
		CurrentScene = SceneType.Start;
		if(asynchronous)
		{
			AsyncOperation async = Application.LoadLevelAsync("Start");
			yield return async;
			Debug.Log ("Loading complete");
		}
		else
		{
			Application.LoadLevel("Start");
		}
	}
	
	// Lobby Scene
	private IEnumerator LoadLobbyScene(bool asynchronous = false)
	{
		CurrentScene = SceneType.Lobby;
		
		if(asynchronous)
		{
			AsyncOperation async = Application.LoadLevelAsync("Lobby");
			yield return async;
			Debug.Log ("Loading complete");
		}
		else
		{
			Application.LoadLevel("Lobby");
		}
	}

	// Game Room Scene
	private IEnumerator LoadGameRoomScene(bool asynchronous = false)
	{
		CurrentScene = SceneType.GameRoom;
		if(asynchronous)
		{
			AsyncOperation async = Application.LoadLevelAsync("GameRoom");
			yield return async;
			Debug.Log ("Loading complete");
		}
		else
		{
			Application.LoadLevel("GameRoom");
		}
	}
	
	// Actual Game Scene
	private IEnumerator LoadGameScene(bool asynchronous = false)
	{
		CurrentScene = SceneType.Game;
		if(asynchronous)
		{
			AsyncOperation async = Application.LoadLevelAsync("Game");
			yield return async;
			Debug.Log ("Loading complete");
		}
		else
		{
			Application.LoadLevel("Game");
		}
	}
}
