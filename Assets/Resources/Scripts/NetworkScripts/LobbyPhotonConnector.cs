﻿using UnityEngine;
using System.Collections;
using ExitGames.Client.Photon;

public class LobbyPhotonConnector : Photon.MonoBehaviour, IPhotonPeerListener {
	
	NetworkingPeer networkingPeer;
	
	void OnEnable () {

	}

	void ConnectToNetworkingLobbyPeer()
	{
		networkingPeer = new NetworkingPeer(this, "matchmakingPeer", ConnectionProtocol.Tcp);
		networkingPeer.mAppVersion = PhotonNetwork.networkingPeer.mAppVersion;
		
		Debug.Log ("mAppVersion: "+networkingPeer.mAppVersion);
		
		networkingPeer.MasterServerAddress = PhotonNetwork.networkingPeer.MasterServerAddress;
		Debug.Log ("MasterServerAddress: "+networkingPeer.MasterServerAddress);
		
		networkingPeer.Connect(PhotonNetwork.ServerAddress, PhotonNetworkManager.APPID);
		
		Debug.Log ("Networking Peer is connected to LOBBY!!");


		//roomName, true, true, 0, null, null);

		networkingPeer.OpCreateGame("ANOTHER ROOM", true, true, 8, PhotonNetwork.autoCleanUpPlayerObjects, null, null);

		Debug.Log ("I JOINED ANOTHER ROOM!! TRY TRY!");
		//networkingPeer.Connect(PhotonNetwork.ServerAddress, PhotonNetwork.Photon.PhotonId());
	}

	void OnPhotonJoinRoomFailed()
	{
		Debug.Log("JOIN ROOMED FAILED!!");
	}

	void OnJoinedLobby()
	{
		ConnectToNetworkingLobbyPeer();
	}
	
	void OnGUI(){
		if(networkingPeer != null)
			GUILayout.Box(networkingPeer.State.ToString());
	}
	
	#region Implementation of IPhotonPeerListener
	public void DebugReturn(DebugLevel level, string message)
	{
		if (level == DebugLevel.ERROR)
		{
			Debug.LogError(message);
		}
		else if (level == DebugLevel.WARNING)
		{
			Debug.LogWarning(message);
		}
		else if (level == DebugLevel.INFO && PhotonNetwork.logLevel >= PhotonLogLevel.Informational)
		{
			Debug.Log(message);
		}
		else if (level == DebugLevel.ALL && PhotonNetwork.logLevel == PhotonLogLevel.Full)
		{
			Debug.Log(message);
		}
	}
	
	public void OnOperationResponse(OperationResponse operationResponse)
	{
		Debug.Log(operationResponse.DebugMessage);
	}
	
	public void OnStatusChanged(StatusCode statusCode)
	{
		Debug.Log(statusCode.ToString());
	}
	
	public void OnEvent(EventData photonEvent)
	{
		Debug.Log(photonEvent.ToStringFull());
	}
	#endregion
}