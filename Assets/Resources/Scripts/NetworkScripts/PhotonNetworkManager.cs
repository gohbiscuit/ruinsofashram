﻿using UnityEngine;
using System.Collections;

public class PhotonNetworkManager : MonoBehaviour {

	public static readonly string APPID = "fb92be07-3585-4566-aa29-1d5ef23f4744";

	// Use this for initialization
	void Start () {
		PhotonNetwork.ConnectToBestCloudServer("v1.0");
		Debug.Log ("Connected to PhotonNetwork in PhotonNetworkManager.cs");
		Debug.Log ("PhotonServer URL: "+PhotonNetwork.ServerAddress);
		StartCoroutine("PrintPing");
	}

	// print ping after 3s (given time to connect)
	IEnumerator PrintPing(){
		yield return new WaitForSeconds(3);
		Debug.Log ("Ping: (" + PhotonNetwork.GetPing() +")" );
	}

	void Awake()
	{
		DontDestroyOnLoad(this);
	}
}
