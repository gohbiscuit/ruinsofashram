﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// All data sent/received over network are in the form of Instructions
/// </summary>
public class Instruction {

	InstructionType m_type;	// type of instruction
	int m_srcViewID;		// photon view id of creature sending the instruction
	object[] m_parameters;	// parameters needed to process the instruction

	string cachedString = "";	// if instruction was converted to string before,
								// then no need recomputation

	/// <summary>
	/// Initializes a new instance of the <see cref="Instruction"/> class.
	/// </summary>
	/// <param name="type">Type of instruction</param>
	/// <param name="srcViewID">photon view id of creature</param>
	/// <param name="parameters">parameters needed to process the instruction</param>
	public Instruction(InstructionType type, int srcViewID, params object[] parameters){
		this.m_type = type;
		this.m_srcViewID = srcViewID;
		this.m_parameters = parameters;
	}

	public Instruction(string str){
		char[] delimiters = {','};
		string[] paraStrs = str.Split(delimiters);

		m_type = (InstructionType)(int.Parse(paraStrs[0]));
		m_srcViewID = int.Parse(paraStrs[1]);

		m_parameters = new object[paraStrs.Length-3];
		for(int i=2; i<paraStrs.Length-1; i++)
			m_parameters[i-2] = stringToObject(paraStrs[i]);
	}
	
	/// <summary>
	/// Gets the type of instruction
	/// </summary>
	/// <returns>The instruction type</returns>
	public InstructionType getType(){
		return m_type;
	}

	/// <summary>
	/// Gets the photon view id of creature sending the instruction
	/// </summary>
	/// <returns>The photon view id</returns>
	public int getSrcViewID(){
		return m_srcViewID;
	}

	/// <summary>
	/// Gets the parameters of the instruction.
	/// </summary>
	/// <returns>instruction parameters</returns>
	public object[] getParameters(){
		return m_parameters;
	}

	/// <summary>
	/// Gets the number of parameters in the instruction
	/// </summary>
	/// <returns>The number of parameters</returns>
	public int getNumParameters(){
		return m_parameters.GetLength(0);
	}

	enum InstructionParameterType { INT32, STRING, FLOAT, STATUS_EFFECT, VECTOR3, QUATERNION, STRING_ARRAY };	// use ON_YOUR_OWN for Deathmatch

	object stringToObject(string str){
		InstructionParameterType type = (InstructionParameterType)(int.Parse(str.Substring(0, 1)));
		char[] delimiters = {'&'};
		string[] paraStrs;
		switch(type){
		case InstructionParameterType.INT32:
			return int.Parse(str.Substring(1));
		case InstructionParameterType.STRING:
			return str.Substring(1);
		case InstructionParameterType.FLOAT:
			return float.Parse(str.Substring(1));
		case InstructionParameterType.STATUS_EFFECT:
			return (StatusEffect)(int.Parse(str.Substring(1)));
		case InstructionParameterType.VECTOR3:
			paraStrs = str.Substring(1).Split(delimiters);
			return new Vector3(float.Parse(paraStrs[0]), float.Parse(paraStrs[1]), float.Parse(paraStrs[2]));
		case InstructionParameterType.QUATERNION:
			paraStrs = str.Substring(1).Split(delimiters);
			Vector3 quatEuler = new Vector3(float.Parse(paraStrs[0]), float.Parse(paraStrs[1]), float.Parse(paraStrs[2]));
			return Quaternion.Euler(quatEuler);
		case InstructionParameterType.STRING_ARRAY:
			return str.Substring(1).Split(delimiters);
		}

		throw new ArgumentException("Invalid parameter type");
		return null;
	}

	public string ToString(){

		// cached before
		if(cachedString != "")
			return cachedString;

		string str = "|" + (int)m_type + "," + m_srcViewID + ",";

		foreach(object parameter in m_parameters){

			if(parameter is Int32)
				str += (int)InstructionParameterType.INT32 + "" + parameter;
			else if(parameter is string)
				str += (int)InstructionParameterType.STRING + "" + parameter;
			else if(parameter is float)
				str += (int)InstructionParameterType.FLOAT + "" + parameter;
			else if(parameter is StatusEffect)
				str += (int)InstructionParameterType.STATUS_EFFECT + "" + (int)(parameter);
			else if(parameter is Vector3){
				Vector3 paraVec3 = (Vector3)parameter;
				str += (int)InstructionParameterType.VECTOR3;
				str += paraVec3.x + "&" + paraVec3.y + "&" + paraVec3.z;
			} else if(parameter is Quaternion){
				Vector3 paraVec3 = ((Quaternion)(parameter)).eulerAngles;
				str += (int)InstructionParameterType.QUATERNION;
				str += paraVec3.x + "&" + paraVec3.y + "&" + paraVec3.z;
			} else if(parameter is string[]){
				str += (int)InstructionParameterType.STRING_ARRAY;
				string[] paraStrArr = (string[])(parameter);
				foreach(string para in paraStrArr)
					str += para + "&";
				str = str.Remove(str.Length - 1);
			} else {
				throw new ArgumentException("unidentified " + parameter.GetType().ToString());
			}

			str += ",";
		}

		cachedString = str;
		return str;
	}
}

