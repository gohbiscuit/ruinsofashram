﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This script govers the communication between the client and other clients
/// InputStream - Instructions from other clients will travel to this client
/// OutputStream - Instruction from this client will travel to other clients
/// </summary>
public class NetworkStream : Photon.MonoBehaviour {

	int m_currRound;
	int m_count = 0;
	System.DateTime m_prev;

	// INPUT variables
	Dictionary<int, List<Instruction>> m_inputinstructionBuffer;

	// OUTPUT variables
	List<Instruction> m_sendingInstructionBuffer;	// stores instruction to be send in this round
	List<Instruction> m_nextInstructionBuffer;		// stores instruction to be sent next round
	bool m_toSend;									// if checked, send the buffer asap


	void Awake () {

		// don't destroy when scene changes
		DontDestroyOnLoad(gameObject);

		if(photonView.isMine){
			// output stream initialisation
			m_sendingInstructionBuffer = new List<Instruction>();
			m_nextInstructionBuffer = new List<Instruction>();
		}

		// input stream initialisation
		m_inputinstructionBuffer = new Dictionary<int, List<Instruction>>();
	}
	
	/// <summary>
	/// store instruction (but don't send over network yet).
	/// </summary>
	/// <param name="inst">instruction to queue</param>
	public void Store(Instruction inst){
		if(inst == null)
			throw new ArgumentException("Invalid instruction");
		m_nextInstructionBuffer.Add(inst);
	}
	
	/// <summary>
	/// send instruction, but doesn't take place until OnPhotonSerializeView is called
	/// will also add to input buffer to process own instruction
	/// </summary>
	/// <param name="roundNum">Round number to send</param>
	public void Send(int roundNum){

		if(!m_inputinstructionBuffer.ContainsKey(roundNum)){
			// INPUT: add to input buffer to process after lag
			m_inputinstructionBuffer.Add(roundNum, new List<Instruction>());
			foreach(Instruction inst in m_nextInstructionBuffer)
				m_inputinstructionBuffer[roundNum].Add(inst);

			// OUTPUT: add to output buffer to send as soon as possible
			foreach(Instruction inst in m_nextInstructionBuffer)
				m_sendingInstructionBuffer.Add(inst);
			m_nextInstructionBuffer.Clear();
			m_currRound = roundNum;
			m_toSend = true;
		}
	}

	/// <summary>
	/// get instruction list from inputstream (cannot get again after this, except for round 0 which is reserved for init)
	/// </summary>
	/// <returns>instruction for the round</returns>
	/// <param name="roundNum">Round number</param>
	public List<Instruction> GrabRoundInstructions(int roundNum){
		if(m_inputinstructionBuffer.ContainsKey(roundNum)){
			List<Instruction> instructions = m_inputinstructionBuffer[roundNum];
			m_inputinstructionBuffer.Remove(roundNum);	// removed after retrieving to save memory space
			return instructions;
		} else 
			return null;
	}

	/// <summary>
	/// Called by Photon Networking, for receiving and sending of instructions
	/// </summary>
	/// <param name="stream">photon stream</param>
	/// <param name="info">info regarding the communication</param>
	void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
	{
		// OUTPUT
		if (stream.isWriting)
		{
			if(m_toSend){

				if(m_sendingInstructionBuffer.Count > 0){
					string bundledInstructionString = "RND" + m_currRound + "-";
					foreach(Instruction inst in m_sendingInstructionBuffer)
						bundledInstructionString += inst.ToString();
					stream.SendNext(bundledInstructionString);
				}

				m_sendingInstructionBuffer.Clear();
				m_toSend = false;
			}
		}

		// INPUT
		else
		{
			string bundledInstructionString = (string)(stream.ReceiveNext());

			string tempStr = bundledInstructionString.Substring(3);
			int endsIdx = tempStr.IndexOf("-");
			int roundNum = int.Parse(tempStr.Substring(0, endsIdx));
			tempStr = tempStr.Substring(tempStr.IndexOf("|") + 1);

			m_inputinstructionBuffer.Add(roundNum, new List<Instruction>());

			char[] delimiters = {'|'};
			string[] instructions = tempStr.Split(delimiters);

			foreach(string instruction in instructions)
				m_inputinstructionBuffer[roundNum].Add(new Instruction(instruction));

			// late message handling, simply force it to process immediately
			GameNetworkManager networkManager = GameNetworkManager.Instance;
			if(networkManager.ProcessedRounds >= roundNum){	// already past it, message is late!
				//Debug.Log("late message!");
				networkManager.ProcessBucket(roundNum);		// force to process late round
			}
		}
	}

	/// <summary>
	/// Informs the MasterClient that it is ready to play.
	/// </summary>
	public void informReady(){
		photonView.RPC("informReadyToMaster", PhotonTargets.MasterClient);
	}

	/// <summary>
	/// Called once per message received from other clients
	/// indicating that they are ready
	/// </summary>
	[RPC]
	void informReadyToMaster() {
		if(PhotonNetwork.isMasterClient)
			GameNetworkManager.Instance.readyOnePlayer();
	}

	/// <summary>
	/// Synchronise the clock with other clients
	/// </summary>
	public void syncClock(){
		photonView.RPC("SyncTime", PhotonTargets.All);
	}

	/// <summary>
	/// Called to synchronise the clock by other clients
	/// </summary>
	/// <param name="info">Info</param>
	[RPC]
	void SyncTime(PhotonMessageInfo info) {
		GameNetworkManager.Instance.beginRepeat(info.timestamp + 10);
		//GameNetworkManager.Instance.beginRepeat(info.timestamp + 15);
	}


}
