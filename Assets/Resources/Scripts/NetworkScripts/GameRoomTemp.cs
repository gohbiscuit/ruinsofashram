﻿using UnityEngine;
using System.Collections;

public class GameRoomTemp : Photon.MonoBehaviour {
	/*
	void Awake(){
		// default: deathmatch
		SetTeam(Team.ON_YOUR_OWN);
	}

	// set local player to "Team.ON_YOUR_OWN" for Deathmatch
	// set local player to either "Team.ONE" or "Team.TWO" for Team Deathmatch
	void SetTeam(Team team){
		ExitGames.Client.Photon.Hashtable playerProperties = new ExitGames.Client.Photon.Hashtable();
		playerProperties.Add("Team", team);
		PhotonNetwork.player.SetCustomProperties(playerProperties);
	}
	
	void OnGUI(){

		// Game Mode/Team Selection
		if (GUI.Button(new Rect(10,220,200,30),"Deathmatch (Default)"))
			SetTeam(Team.ON_YOUR_OWN);
		if (GUI.Button(new Rect(10,280,200,30),"Team Deathmatch - Team One"))
			SetTeam(Team.ONE);
		if (GUI.Button(new Rect(10,310,200,30),"Team Deathmatch - Team Two"))
			SetTeam(Team.TWO);

		// Print out current network status (eg: Not connected, JoinedLobby, etc)
		GUILayout.Label("Network Status :" +  PhotonNetwork.connectionStateDetailed.ToString());

		GUILayout.Label("You are now in game room \"" +  PhotonNetwork.room.name + "\"");
		GUILayout.Label("Number of Players : " + PhotonNetwork.room.playerCount);

		if(PhotonNetwork.isMasterClient){
			GUILayout.Label("You are the host of this game. When all players have arrived, press 'Start Game'");
			if (GUI.Button(new Rect(10,120,150,30),"Start Game"))
				broadcastStartGame();
		} else {
			GUILayout.Label("Please wait for host to start the game");
		}

		GUILayout.Label("Synchronised Time :" + PhotonNetwork.time.ToString());

	}

	void broadcastStartGame(){
		photonView.RPC("StartGame", PhotonTargets.All);
		// not sure how to block new players from this point on but your $60 manual may help
	}

	// load the level, and since all players have already arrived in the room
	// create the input/output stream for network communication
	[RPC]
	void StartGame(PhotonMessageInfo info) {
		GameNetworkManager.Instance.createLocalNetworkManager();
		Application.LoadLevel(GameNetworkManager.Instance.GameScene);
	}*/
}
