﻿using UnityEngine;
using System.Collections;

// look for hongwei if something goes wrong in this script
public class NewPhotonLobby : MonoBehaviour {
	
	void Start () {
		// connect to photon cloud to the lobby (similar to masterserver)
		// only need to be done once
		PhotonNetwork.ConnectUsingSettings("1.0");
	}

	void Update(){
		// go to game room once joined a room
		if(PhotonNetwork.room != null)
			Application.LoadLevel("GameRoom_Temp");
	}
	
	void OnGUI(){

		// Creating a room and join immediately (once joined, roomlist won't update)
		// note that no 2 rooms can have the same name
		if (GUI.Button(new Rect(10,70,150,30),"Create Room"))
			PhotonNetwork.CreateRoom("a UNIQUE Name that you can define");

		// Join a room with given name (once joined, roomlist won't update)
		if (GUI.Button(new Rect(10,120,150,30),"Join Room (if any)"))
			PhotonNetwork.JoinRoom("a UNIQUE Name that you can define");

		// Poll room list from cloud on every frame
		foreach (RoomInfo room in PhotonNetwork.GetRoomList())
			GUILayout.Label("Room : " + room.name + " " + room.playerCount + "/" + room.maxPlayers);

		// Print out current network status (eg: Not connected, JoinedLobby, etc)
		GUILayout.Label("Network Status :" +  PhotonNetwork.connectionStateDetailed.ToString());

		GUILayout.Label("Synchronised Time :" + PhotonNetwork.time.ToString());
	}
}
