﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum Team { NEUTRAL, ONE, TWO, ON_YOUR_OWN };	// use ON_YOUR_OWN for Deathmatch
public enum GameMode { DEATHMATCH, TEAM_DEATHMATCH };
public enum InstructionType { STOP, MOVE_TO_POSITION, MOVE_TO_CREATURE, USE_SKILL_AT_CREATURE, USE_SKILL_AT_POSITION, 
	TAKE_DAMAGE, DIE, GAIN_HEALTH, POSSESS, RELEASE, CREATE_PLAYER_WISP, CREATE_AI, RECOMPUTE_STATS, AFFLICT, 
	AFFLICT_RECOVER, CORRUPTED };

/// <summary>
/// Manages the networking component of the game
/// </summary>
public class GameNetworkManager : Photon.MonoBehaviour {

	const int MAX_SEND_RATE = 20;

	NetworkStream outputStream;
	static GameNetworkManager instance;

	// scene to load upon start
	[SerializeField] string m_gameScene;
	[SerializeField] string m_gameScene_2;

	bool m_playingGame;		// game is ready to be played
	bool m_informedReady;	// nonMasterClient has informed MasterClient that it is ready to play game

	// time synchronisation
	int m_currRound = 0;
	int m_processedRound = -99;
	double m_startingTime;
	int m_gameTime;
	[SerializeField] float m_roundLength;
	[SerializeField] int m_lagRounds;

	private string m_currentScene;

	// number of players that are ready
	int m_numReady = 0;

	public static GameNetworkManager Instance {
		get {
			return instance;
		}
	}

	void Awake () {

		// don't destroy when moving to next scene
		DontDestroyOnLoad(gameObject);

		// max sending rate over network
		PhotonNetwork.sendRate = MAX_SEND_RATE;
		PhotonNetwork.sendRateOnSerialize = MAX_SEND_RATE;

		// singleton instantiation
		instance = this;
	}

	void Update(){

		if(m_playingGame){

			// process all buckets that have yet to be processed
			double timePassed = PhotonNetwork.time - m_startingTime;
			int realCurrRound = (int)(timePassed/m_roundLength);
			while(m_currRound < realCurrRound){
				SendBucket(m_currRound);
				ProcessBucket(m_currRound - m_lagRounds);
				m_currRound++;
			}

		} else {

			// For Debugging Only
			if(PhotonNetwork.offlineMode) {
				if(Creator.Instance != null){
					this.m_gameTime = 15 * 60;	// set 15 minutes of round time
					//this.m_gameTime = 5;		// 5s round (for testing)

					createLocalNetworkManager();
					StartCoroutine(Creator.Instance.SpawnAll());
					m_playingGame = true;
				}
			}

			// inform MasterClient that client has loaded the game
			// and is ready to start
			else if(!m_informedReady && (Application.loadedLevelName.Equals(m_gameScene) || Application.loadedLevelName.Equals(m_gameScene_2)) ){
				if(PhotonNetwork.isNonMasterClientInRoom)
					outputStream.informReady();	// ready masterClient
				else
					readyOnePlayer();			// ready ownself
				m_informedReady = true;
			}
		}
	}

	// uncomment to show rounds
	/*void OnGUI(){
		if(Application.loadedLevelName.Equals(m_gameScene)){
			if(!PhotonNetwork.offlineMode){
				// Print out current network status (eg: Not connected, JoinedLobby, etc)
				GUILayout.Label(PhotonNetwork.connectionStateDetailed.ToString());
				GUILayout.Label("Round " + m_currRound);
				GUILayout.Label("Round in seconds " + m_currRound * m_roundLength);
			}
		}
	}*/

	// ---------------------------------------------------------------------------

	#region Instruction Storage
	// for execution later when message is received

	bool displayOutgoingTraffic;

	/// <summary>
	/// Stores stop instruction.
	/// </summary>
	/// <param name="photonView">Photon view of the creature to stop</param>
	public void StoreStop(PhotonView photonView){
		if(m_playingGame)
			outputStream.Store(new Instruction(InstructionType.STOP, photonView.viewID));
		if(displayOutgoingTraffic) Debug.Log("send stop");
	}

	/// <summary>
	/// Stores move instruction (to position)
	/// </summary>
	/// <param name="photonView">Photon view of the creature to move</param>
	/// <param name="targetPosition">Position to move to</param>
	public void StoreMoveToPosition(PhotonView photonView, Vector3 targetPosition){
		if(m_playingGame){
			if(!(targetPosition is Vector3))
				Debug.LogError("Position is not Vector 3?");
			outputStream.Store(new Instruction(InstructionType.MOVE_TO_POSITION, photonView.viewID, targetPosition));
		}
		if(displayOutgoingTraffic) Debug.Log("send move");
	}

	/// <summary>
	/// Stores move instruction (to creature)
	/// </summary>
	/// <param name="photonView">Photon view of the creature to move</param>
	/// <param name="targetView">Photon view of target creature to chase after</param>
	public void StoreMoveToCreature(PhotonView photonView, PhotonView targetView){
		if(m_playingGame)
			outputStream.Store(new Instruction(InstructionType.MOVE_TO_CREATURE, photonView.viewID, targetView.viewID));
		if(displayOutgoingTraffic) Debug.Log("send move");
	}

	/// <summary>
	/// Stores use skill instruction (on creature)
	/// </summary>
	/// <param name="photonView">Photon view of the creature using the skill</param>
	/// <param name="skillTypeID">ID of skill to use</param>
	/// <param name="targetView">Photon view of the creature to use the skill on</param>
	public void StoreUseSkill(PhotonView photonView, int skillTypeID, PhotonView targetView){
		if(m_playingGame)
			outputStream.Store(new Instruction(InstructionType.USE_SKILL_AT_CREATURE, photonView.viewID, targetView.viewID, skillTypeID));
		if(displayOutgoingTraffic) Debug.Log("send skill");
	}

	/// <summary>
	/// Stores use skill instruction (on position)
	/// </summary>
	/// <param name="photonView">Photon view of the creature using the skill</param>
	/// <param name="skillTypeID">ID of skill to use</param>
	/// <param name="targetPosition">Position to use skill on</param>
	public void StoreUseSkill(PhotonView photonView, int skillTypeID, Vector3 targetPosition){
		if(m_playingGame)
			outputStream.Store(new Instruction(InstructionType.USE_SKILL_AT_POSITION, photonView.viewID, targetPosition, skillTypeID));
		if(displayOutgoingTraffic) Debug.Log("send skill");
	}

	/// <summary>
	/// Stores take damage instruction
	/// </summary>
	/// <param name="photonView">Photon view of the creature to take damage</param>
	/// <param name="attackerView">Photon view of the attacker</param>
	/// <param name="damage">amount of damage</param>
	public void StoreTakeDamage(PhotonView photonView, PhotonView attackerView, int damage){
		if(m_playingGame)
			outputStream.Store(new Instruction(InstructionType.TAKE_DAMAGE, photonView.viewID, attackerView.viewID, damage));
		if(displayOutgoingTraffic) Debug.Log("send damage");
	}

	/// <summary>
	/// Stores die instruction
	/// </summary>
	/// <param name="photonView">Photon view of the creature to die</param>
	/// <param name="attackerView">Photon view of the attacker</param>
	/// <param name="damage">amount of damage</param>
	public void StoreDie(PhotonView photonView, PhotonView attackerView, int damage){
		if(m_playingGame)
			outputStream.Store(new Instruction(InstructionType.DIE, photonView.viewID, attackerView.viewID, damage));
		if(displayOutgoingTraffic) Debug.Log("send die");
	}

	/// <summary>
	/// Stores gain health instruction
	/// </summary>
	/// <param name="photonView">Photon view of the creature to gain health</param>
	/// <param name="gain">amount of health gained</param>
	public void StoreGainHealth(PhotonView photonView, int gain){
		if(m_playingGame)
			outputStream.Store(new Instruction(InstructionType.GAIN_HEALTH, photonView.viewID, gain));
		if(displayOutgoingTraffic) Debug.Log("send gain");
	}

	/// <summary>
	/// Stores possess instruction
	/// </summary>
	/// <param name="photonView">Photon view of the original creature</param>
	/// <param name="targetView">Photon view of the creature to possess</param>
	/// <param name="targetStats">New stats of the creature to possess</param>
	public void StorePossess(PhotonView photonView, PhotonView targetView, CreatureStats targetStats){
		string[] targetStatsSerialized = CreatureStatsCalculator.Instance.getSerizalizedCreatureStats(targetStats);
		outputStream.Store(new Instruction(InstructionType.POSSESS, photonView.viewID, targetView.viewID, targetStatsSerialized));
		if(displayOutgoingTraffic) Debug.Log("send possess");
	}

	/// <summary>
	/// Stores release instruction
	/// </summary>
	/// <param name="photonView">Photon view of the original creature</param>
	/// <param name="wispStats">Stats of the wisp that is to be created</param>
	public void StoreRelease(PhotonView photonView, CreatureStats wispStats){
		string[] wispStatsSerialized = CreatureStatsCalculator.Instance.getSerizalizedCreatureStats(wispStats);
		outputStream.Store(new Instruction(InstructionType.RELEASE, photonView.viewID, PhotonNetwork.AllocateViewID(), wispStatsSerialized));
		if(displayOutgoingTraffic) Debug.Log("send release");
	}

	/// <summary>
	/// Stores creating own wisp instruction
	/// </summary>
	/// <param name="spawn">Spawn Point that produces this wisp</param>
	/// <param name="position">Position to produce the wisp on</param>
	/// <param name="rotation">Rotation the wisp will face</param>
	/// <param name="wispStats">Stats of the wisp</param>
	public void StoreCreateOwnPlayerWisp(CreatureSpawn spawn, Vector3 position, Quaternion rotation, CreatureStats wispStats){
		string[] wispStatsSerialized = CreatureStatsCalculator.Instance.getSerizalizedCreatureStats(wispStats);
		int spawnID = -1;
		if(spawn != null)
			spawnID = spawn.gameObject.GetComponent<PhotonView>().viewID;
		outputStream.Store(new Instruction(InstructionType.CREATE_PLAYER_WISP, PhotonNetwork.player.ID, PhotonNetwork.AllocateViewID(),
		                                   spawnID, position, rotation, wispStatsSerialized));
		if(displayOutgoingTraffic) Debug.Log("send create wisp");
	}

	/// <summary>
	/// Stores creating AI instruction
	/// </summary>
	/// <param name="spawn">Spawn Point that produces this AI</param>
	/// <param name="creatureName">Standardised name of the AI to produce</param>
	/// <param name="position">Position to produce the AI on</param>
	/// <param name="rotation">Rotation the AI will face</param>
	public void StoreCreateAI(CreatureSpawn spawn, string creatureName, Vector3 position, Quaternion rotation){
		int spawnID = -1;
		if(spawn != null)
			spawnID = spawn.gameObject.GetComponent<PhotonView>().viewID;
		outputStream.Store(new Instruction(InstructionType.CREATE_AI, -1, PhotonNetwork.AllocateViewID(),
		                                   spawnID, creatureName, position, rotation));
		if(displayOutgoingTraffic) Debug.Log("send create AI");
	}

	/// <summary>
	/// Stores recompute stats instruction
	/// </summary>
	/// <param name="photonView">Photon view of the creature</param>
	/// <param name="newStats">New stats for the creature</param>
	public void StoreRecomputePossessedStats(PhotonView photonView, CreatureStats newStats){
		string[] newStatsSerialized = CreatureStatsCalculator.Instance.getSerizalizedCreatureStats(newStats);
		outputStream.Store(new Instruction(InstructionType.RECOMPUTE_STATS, photonView.viewID, newStatsSerialized));
		if(displayOutgoingTraffic) Debug.Log("send recompute stats");
	}

	/// <summary>
	/// Stores status effect affliction instruction
	/// </summary>
	/// <param name="photonView">Photon view of the creature</param>
	/// <param name="type">Type of status effect to inflict</param>
	/// <param name="duration">Duration of status effect</param>
	/// <param name="intensity">Intensity of status effect (if any)</param>
	public void StoreAffliction(PhotonView photonView, StatusEffect type, float duration, float intensity){
		outputStream.Store(new Instruction(InstructionType.AFFLICT, photonView.viewID, type, duration, intensity));
		if(displayOutgoingTraffic) Debug.Log("send afflict");
	}

	/// <summary>
	/// Stores affliction recovery instruction
	/// </summary>
	/// <param name="photonView">Photon view of the creature</param>
	/// <param name="type">Type of status effect to recover</param>
	/// <param name="intensityToRecover">Intensity of status effect to recover (if any)</param>
	public void StoreAfflictionRecovery(PhotonView photonView, StatusEffect type, float intensityToRecover){
		outputStream.Store(new Instruction(InstructionType.AFFLICT_RECOVER, photonView.viewID, type, intensityToRecover));
		if(displayOutgoingTraffic) Debug.Log("send afflicion recovery");
	}

	#endregion

	// ---------------------------------------------------------------------------

	#region Instruction Process
	// for execution upon receipt of message

	/// <summary>
	/// Processes the stop instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessStopInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null)
			return;
		srcView.GetComponent<Creature>().StopLocallyNow();
	}

	/// <summary>
	/// Processes the move to position instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessMoveToPositionInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null)
			return;

		if(inst.getParameters()[0] is Vector3)
			srcView.GetComponent<Creature>().MoveLocallyNow((Vector3)(inst.getParameters()[0]));
		else{
			//Debug.LogError(inst.getType() + " " + inst.getSrcViewID() + " " + inst.getNumParameters() + " " + inst.getParameters()[0]+ inst.getParameters()[1]+ inst.getParameters()[2]+ inst.getParameters()[3]+ inst.getParameters()[4]+ inst.getParameters()[5]);
		}
	}

	/// <summary>
	/// Processes the move to creature instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessMoveToCreatureInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		PhotonView targetView = PhotonView.Find((int)(inst.getParameters()[0]));

		if(targetView == null || srcView == null)	// src/target is removed from game
			return;

		Creature srcCreature = srcView.GetComponent<Creature>();
		Creature targetCreature = targetView.GetComponent<Creature>();

		srcCreature.MoveLocallyNow(targetCreature);
	}

	/// <summary>
	/// Processes the use skill at creature instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessUseSkillAtCreatureInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		PhotonView targetView = PhotonView.Find((int)(inst.getParameters()[0]));

		if(targetView == null || srcView == null)	// src/target was removed from game
			return;

		Creature srcCreature = srcView.GetComponent<Creature>();
		Creature targetCreature = targetView.GetComponent<Creature>();

		Skill skill = srcCreature.getCreatureSkill((int)(inst.getParameters()[1]));
		srcCreature.UseSkillLocallyNow(skill, targetCreature);
	}

	/// <summary>
	/// Processes the use skill at position instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessUseSkillAtPositionInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null)	// src was removed from game
			return;
		Creature srcCreature = srcView.GetComponent<Creature>();

		Vector3 position = (Vector3)(inst.getParameters()[0]);
		Skill skill = srcCreature.getCreatureSkill((int)(inst.getParameters()[1]));
		srcCreature.UseSkillLocallyNow(skill, position);
	}

	/// <summary>
	/// Processes the take damage instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessTakeDamageInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null) return;
		Creature srcCreature = srcView.GetComponent<Creature>();
		if(srcCreature == null) return;

		PhotonView atkView = PhotonView.Find((int)(inst.getParameters()[0])); if(atkView == null) return;
		Creature attacker = atkView.GetComponent<Creature>(); if(attacker == null) return;

		int damage = (int)(inst.getParameters()[1]);
		srcCreature.TakeDamageLocallyNow(attacker, damage);
	}

	/// <summary>
	/// Processes the die instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessDieInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID()); if(srcView == null) return;
		Creature srcCreature = srcView.GetComponent<Creature>(); if(srcCreature == null) return;

		PhotonView atkView = PhotonView.Find((int)(inst.getParameters()[0])); if(atkView == null) return;
		Creature attacker = atkView.GetComponent<Creature>(); if(attacker == null) return;

		int damage = (int)(inst.getParameters()[1]);
		srcCreature.DieLocallyNow(attacker, damage);
	}

	/// <summary>
	/// Processes the gain health instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessGainHealthInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null) return;

		Creature srcCreature = srcView.GetComponent<Creature>();
		if(srcCreature == null) return;

		int gain = (int)(inst.getParameters()[0]);
		srcCreature.GainHealthLocallyNow(gain);
	}

	/// <summary>
	/// Processes the possess instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessPossessInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		PhotonView targetView = PhotonView.Find((int)(inst.getParameters()[0]));

		if(srcView == null || targetView == null)	// target was removed from game
			return;

		GameObject srcGO = srcView.gameObject;
		GameObject targetGO = targetView.gameObject;

		CreatureStats stats = CreatureStatsCalculator.Instance.getCreatureStats((string[])(inst.getParameters()[1]));
		Creator.Instance.possessCreatureLocallyNow(srcGO, targetGO, stats);
	}

	/// <summary>
	/// Processes the release instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessReleaseInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		int newViewID = (int)(inst.getParameters()[0]);

		if(srcView == null)	// target was removed from game
			return;

		CreatureStats stats = CreatureStatsCalculator.Instance.getCreatureStats((string[])(inst.getParameters()[1]));
		Creator.Instance.releaseCreatureLocallyNow(srcView.gameObject, newViewID, stats);
	}

	/// <summary>
	/// Processes the create player wisp instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessCreatePlayerWispInstruction(Instruction inst){
		int ownerID = inst.getSrcViewID();
		int newViewID = (int)(inst.getParameters()[0]);

		CreatureSpawn spawnPoint = null;
		if((int)(inst.getParameters()[1]) != -1){
			PhotonView spawnPointView = PhotonView.Find((int)(inst.getParameters()[1]));
			spawnPoint = spawnPointView.gameObject.GetComponent<CreatureSpawn>();
		}

		Vector3 pos = (Vector3)(inst.getParameters()[2]);
		Quaternion rot = (Quaternion)(inst.getParameters()[3]);
		CreatureStats stats = CreatureStatsCalculator.Instance.getCreatureStats((string[])(inst.getParameters()[4]));

		Creator.Instance.createPlayerWispLocallyNow(newViewID, spawnPoint, pos, rot, ownerID, stats);
	}

	/// <summary>
	/// Processes the create AI instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessCreateAIInstruction(Instruction inst){
		int newViewID = (int)(inst.getParameters()[0]);

		CreatureSpawn spawnPoint = null;
		if((int)(inst.getParameters()[1]) != -1){
			PhotonView spawnPointView = PhotonView.Find((int)(inst.getParameters()[1]));
			spawnPoint = spawnPointView.gameObject.GetComponent<CreatureSpawn>();
		}

		string creatureName = (string)(inst.getParameters()[2]);
		Vector3 pos = (Vector3)(inst.getParameters()[3]);
		Quaternion rot = (Quaternion)(inst.getParameters()[4]);

		Creator.Instance.createAILocallyNow(newViewID, spawnPoint, creatureName, pos, rot);
	}

	/// <summary>
	/// Processes the recompute possessed stats instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessRecomputePossessedStatsInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null)	// target was removed from game
			return;

		string[] para = new string[inst.getNumParameters()];
		for(int i=0; i<para.Length; i++)
			para[i] = (string)((inst.getParameters()[i]));

		CreatureStats stats = CreatureStatsCalculator.Instance.getCreatureStats(para);
		Creature creature = srcView.GetComponent<Creature>();
		creature.initStats(stats);
	}

	/// <summary>
	/// Processes the afflict status instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessAfflictInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null) return;
		Creature srcCreature = srcView.GetComponent<Creature>();
		if(srcCreature == null) return;

		StatusEffect type = (StatusEffect)(inst.getParameters()[0]);
		float duration = (float)(inst.getParameters()[1]);
		float intensity = (float)(inst.getParameters()[2]);

		srcCreature.AfflictStatusEffectsLocallyNow(type, duration, intensity);
	}

	/// <summary>
	/// Processes the affliction recovery instruction.
	/// </summary>
	/// <param name="inst">Instruction</param>
	void ProcessAfflictRecoverInstruction(Instruction inst){
		PhotonView srcView = PhotonView.Find(inst.getSrcViewID());
		if(srcView == null) return;
		Creature srcCreature = srcView.GetComponent<Creature>();
		if(srcCreature == null) return;

		StatusEffect type = (StatusEffect)(inst.getParameters()[0]);
		float intensity = (float)(inst.getParameters()[1]);

		srcCreature.RemoveStatusEffectsLocallyNow(type, intensity);
	}

	#endregion

	// ---------------------------------------------------------------------------

	/*void BucketUpdate(){
		m_playingGame = true;
		SendBucket(m_currRound);
		ProcessBucket(m_currRound - m_lagRounds + 1);
		m_currRound++;
	}*/

	/// <summary>
	/// send a 'bucket' of instuctions at the end of every round
	/// </summary>
	/// <param name="roundNum">Bucket to send</param>
	void SendBucket(int roundNum){
		outputStream.Send(roundNum);
	}

	/// <summary>
	/// receive and processes all buckets of instructions from its InputStream
	/// </summary>
	/// <param name="roundNum">Bucket round number</param>
	public void ProcessBucket(int roundNum){

		// game has yet to begin
		if(roundNum < 0)
			return;

		GameObject[] inputStreamGOs = GameObject.FindGameObjectsWithTag("InputStream");
		foreach(GameObject inputStreamGO in inputStreamGOs){
			NetworkStream inputStream = inputStreamGO.GetComponent<NetworkStream>();

			// retrieve instructions from the bucket in the stream
			List<Instruction> instList = inputStream.GrabRoundInstructions(roundNum);

			// call the function to execute all instructions in the bucket
			if(instList != null){
				foreach(Instruction inst in instList){
					switch(inst.getType())
					{
					case InstructionType.STOP:
						ProcessStopInstruction(inst);
						break;
					case InstructionType.MOVE_TO_POSITION:
						ProcessMoveToPositionInstruction(inst);
						break;
					case InstructionType.MOVE_TO_CREATURE:
						ProcessMoveToCreatureInstruction(inst);
						break;
					case InstructionType.USE_SKILL_AT_CREATURE:
						ProcessUseSkillAtCreatureInstruction(inst);
						break;
					case InstructionType.USE_SKILL_AT_POSITION:
						ProcessUseSkillAtPositionInstruction(inst);
						break;
					case InstructionType.TAKE_DAMAGE:
						ProcessTakeDamageInstruction(inst);
						break;
					case InstructionType.DIE:
						ProcessDieInstruction(inst);
						break;
					case InstructionType.GAIN_HEALTH:
						ProcessGainHealthInstruction(inst);
						break;
					case InstructionType.POSSESS:
						ProcessPossessInstruction(inst);
						break;
					case InstructionType.RELEASE:
						ProcessReleaseInstruction(inst);
						break;
					case InstructionType.CREATE_PLAYER_WISP:
						ProcessCreatePlayerWispInstruction(inst);
						break;
					case InstructionType.CREATE_AI:
						ProcessCreateAIInstruction(inst);
						break;
					case InstructionType.RECOMPUTE_STATS:
						ProcessRecomputePossessedStatsInstruction(inst);
						break;
					case InstructionType.AFFLICT:
						ProcessAfflictInstruction(inst);
						break;
					case InstructionType.AFFLICT_RECOVER:
						ProcessAfflictRecoverInstruction(inst);
						break;
					}
				}
			}
		}

		if(roundNum > m_processedRound)
			m_processedRound = roundNum;
	}

	/// <summary>
	/// Starts the debug server for debugging purpose.
	/// </summary>
	public void startDebugServer(){

		// set offline mode
		PhotonNetwork.offlineMode = true;

		// create a test room to play in
		PhotonNetwork.CreateRoom("TestRoom");

		// place the player into a team
		ExitGames.Client.Photon.Hashtable table = new ExitGames.Client.Photon.Hashtable();
		table.Add("Team", Team.ON_YOUR_OWN);
		PhotonNetwork.player.SetCustomProperties(table);
	}

	/// <summary>
	/// Creates a stream for all players to send data to them.
	/// </summary>
	public void createLocalNetworkManager(){
		GameObject localNetworkManager = PhotonNetwork.Instantiate("NetworkInputStream", Vector3.zero, Quaternion.identity, 0);
		outputStream = localNetworkManager.GetComponent<NetworkStream>();
		localNetworkManager.name = "NetworkOutputStream";
	}
	
	/// <summary>
	/// a non-masterClient has informed masterClient that it is ready
	/// if all players are ready, masterClient will sync the clock and start the game
	/// </summary>
	public void readyOnePlayer(){
		m_numReady++;
		if(m_numReady == PhotonNetwork.room.playerCount)
			syncClock();
	}

	/// <summary>
	/// synchronises the clock for all players.
	/// must only be done when all player's streams are already in
	/// </summary>
	private void syncClock(){
		outputStream.syncClock();
	}

	/// <summary>
	/// called after receiving time to start
	/// </summary>
	/// <param name="startingTime">Starting time</param>
	public void beginRepeat(double startingTime){
		m_playingGame = true;
		this.m_startingTime = startingTime;
		//InvokeRepeating("BucketUpdate", (float)waitTime, m_roundLength);	// time to process round 0 and move on!
	}

	/// <summary>
	/// Calculate the total induced lag for every instruction to send
	/// </summary>
	/// <returns>The total lag</returns>
	public float getTotalLag(){
		return m_roundLength*m_lagRounds;
	}
	
	/// <summary>
	/// Store the game max round time, is being invoked in JoinSlotHandler.cs
	/// </summary>
	/// <param name="minutes">minutes</param>
	public void setGameTime(int minutes){
		this.m_gameTime = minutes * 60;
	}
	
	/// <summary>
	/// Gets the game round time left in seconds.
	/// </summary>
	/// <returns>The round time</returns>
	public int getRoundTime()
	{
		int synchronisedTimeInSeconds = (int) (m_currRound * m_roundLength);
		int timeLeftInSeconds = this.m_gameTime - synchronisedTimeInSeconds;

		return timeLeftInSeconds;
	}

	/// <summary>
	/// Gets the scene where the actual game begins
	/// </summary>
	/// <value>The game scene</value>
	public string GameScene {
		get {return m_gameScene;}
	}

	/// <summary>
	/// Gets the scene where the actual game begins
	/// </summary>
	/// <value>The game scene 2</value>
	public string GameScene2 {
		get {return m_gameScene_2;}
	}

	/// <summary>
	/// Gets the current scene of the game
	/// </summary>
	/// <value>The current game scene</value>
	public string CurrentScene {
		set {m_currentScene = value ;}
		get {return m_currentScene;}
	}

	/// <summary>
	/// Gets the latest processed round
	/// </summary>
	/// <value>The processed rounds</value>
	public int ProcessedRounds {
		get {return m_processedRound;}
	}

	/// <summary>
	/// Gets the current round
	/// </summary>
	/// <value>The current round</value>
	public int CurrentRound {
		get {return m_currRound;}
	}

	/// <summary>
	/// Gets the length of a round
	/// </summary>
	/// <value>The round length</value>
	public float RoundLength{
		get {return m_roundLength;}
	}

}
