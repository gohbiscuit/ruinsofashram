﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using MiniJSON;


/// <summary>
/// Text util class handle the manipulations of all Text / String data
/// It also include changing of color for text, handline JSONs translations etc
/// </summary>
public class TextUtil 
{
	
	public static readonly string CHAT_PLAYER_NAME_COLOR_  = "[D2B772]"; 
	public static readonly string CHAT_MESSAGE_COLOR_  = "[FFFFFF]"; 
	public static readonly string CHAT_JOIN_CHAT_COLOR_  = "[52D858]"; 
	public static readonly string CHAT_LEAVE_CHAT_COLOR_  = "[D85252]"; 
	public static readonly string CHAT_JOIN_ROOM_COLOR_  = "[C7FF14]"; 
	public static readonly string CHAT_LEAVE_ROOM_COLOR_  = "[14FCFF]";		// old color #FF1414
	public static readonly string CHAT_NOTSEND_CHAT_COLOR_  = "[14FCFF]";	// old color #585858
	public static readonly string CHAT_GAMESTARTING_COLOR  = "[FF1414]";

	public static readonly string STATS_TOOLTIP_TITLE_COLOR_ = "[FFFFFF]";	
	public static readonly string STATS_TOOLTIP_DESC_COLOR_ = "[F4EEA6]";	//F0F2C7

	public static readonly string ASHRAM_MAIN_CHANNEL = "ASHRAM_MAIN_CHANNEL_LOBBY";
	
	/// <summary>
	/// Uppercases the word. E.g. FRUIT JUMBLE = Fruit jumble
	/// </summary>
	/// <returns>
	/// The word.
	/// </returns>
	/// <param name='str'>
	/// String.
	/// </param>
	public static string UppercaseFirst(string str)
    {
		// Check for empty string.
		if (string.IsNullOrEmpty(str))
		    return string.Empty;

		// remove leading, ending spaces
		str = str.Trim();

		// Return char and concat substring.
		return char.ToUpper(str[0]) + str.Substring(1).ToLower();

    }

	/// <summary>
	/// Recursive method that Uppercases the word. 
	/// E.g. FRUIT JUMBLE = Fruit Jumble
	/// RUINS OF ASHRAM = Ruins of Ashram
	/// </summary>
	/// <returns>
	/// The word.
	/// </returns>
	/// <param name='str'>
	/// String.
	/// </param>
	public static string UppercaseWord(string str)
    {		
		// Check for empty string.
		if (string.IsNullOrEmpty(str))
		{
		    return string.Empty;
		}

		// remove leading, ending spaces
		str = str.Trim();

		if(	str.Contains(" ") )
		{
			// uppercase it, if the string is not one of this
			if( !str.StartsWith("a") && !str.StartsWith("of") && !str.StartsWith("are") && 
				!str.StartsWith("is") && !str.StartsWith("was") && !str.StartsWith("were") )
			{
				str = char.ToUpper(str[0]) + str.Substring(1).ToLower();
			}

			string truncateStr = str.Substring(0, str.IndexOf(" "));
			string nextStr = str.Substring( str.IndexOf(" ") + 1 );
			return truncateStr + " " + UppercaseWord( nextStr );
		}
		
		return char.ToUpper(str[0]) + str.Substring(1).ToLower();
    }
	
	/// <summary>
	/// Decodes the JSON string and returns a dictionary object
	/// </summary>
	public static Dictionary<string,object> DecodeJSON(string jsonStr)
	{
		return Json.Deserialize(jsonStr) as Dictionary<string,object>;
	}
	
	/// <summary>
	/// Gets the string value of a JSON particular field
	/// </summary>
	public static string GetJSONValueForKey(Dictionary<string,object> dict, string key)
	{
		if(dict == null || dict.ContainsKey(key) == false)
		{
			Debug.LogError("JSON is NULL or has no value for KEY: "+key);
			return "<JSON_ERROR>";
		}
		
		return (string) dict[key];
	}
	
	/// <summary>
	/// Extract contents from the specified tags start and end delimiter
	/// Example <json>{username: abc}</json> returns {username: abc}
	/// </summary>
	public static string SeperateJsonString(string originalStr, string startTag, string endTag)
	{
		// it will be something like <json> and </json>
		// example if we uses <p> and </p> it will be something like
		// String pattern = @"(?<=<p.*>).*(?=</p>)";
		string pattern = @"(?<="+startTag+").*(?="+endTag+")";
		var matches = Regex.Matches(originalStr, pattern);
		StringBuilder result = new StringBuilder();
		
		if(matches.Count == 0)
		{
			Debug.LogError("There is no matches in the JSON!");
		}
		else if(matches.Count > 1)
		{
			Debug.LogError("THERE IS MORE THAN 1 JSON Matches for Seperate JSON String, remember to handle that!");
		}
		
		//result.Append("<p>");
		foreach (Match match in matches)
		{
			Debug.Log ("Extracted JSON string is: "+match.Value);
		    result.Append(match.Value);
		}
		
		return result.ToString();
	}
	
	//[52D858]RandomName549 joined the chat.
	public static string ExtractContentsFromTag(string message, string tagOne, string tagTwo)
	{
		if(string.IsNullOrEmpty(message))
		{
			Debug.LogError ("Error <string is null or empty> from ExtractContentsFromTag");
			return "";
		}
		
		int firstIndex = message.IndexOf(tagOne);
		int secondIndex = message.IndexOf(tagTwo) - 1;
		
		if(firstIndex == -1 && secondIndex == -1)
		{
			Debug.LogError ("Error <missing index> from ExtractContentsFromTag");
			return "";
		}
		
		string result = message.Substring(firstIndex + tagOne.Length, secondIndex - tagTwo.Length);
		return result;
	}
}
