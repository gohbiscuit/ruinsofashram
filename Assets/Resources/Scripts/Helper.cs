﻿using UnityEngine;
using System.Collections;

public class Helper {
	
	// assume up dir is y axis
	public static void SmoothLookAt(Transform transform, Vector3 target, float smooth)
	{
	    Vector3 dir = target - transform.position;
	    Quaternion targetRotation = Quaternion.LookRotation(dir);
	    transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * smooth);
		
		var r = transform.eulerAngles;
   		transform.rotation = Quaternion.Euler(0, r.y, 0);
	}
	
	// for displaying debug message (put in onGUI)
	public static void DrawDebugText(string msg){
		GUI.Label(new Rect(20.00f,40.40f,1000,100), msg);
	}
}
