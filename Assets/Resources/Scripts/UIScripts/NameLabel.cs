﻿using UnityEngine;
using System.Collections;

public class NameLabel : MonoBehaviour {
	
	public Creature linkedCreature;
	
	// Update is called once per frame
	void Update () {
		if(linkedCreature == null)
			Destroy(gameObject);
	}
	
	public void setCreature(Creature creature){
		linkedCreature = creature;
	}
}
