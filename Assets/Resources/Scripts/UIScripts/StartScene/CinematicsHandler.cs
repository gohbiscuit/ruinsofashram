﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Class that is in-charge of event handling when the Cinematics button is being pressed
/// </summary>
public class CinematicsHandler : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick()
	{
		Debug.Log("Cinematics do nothing for now!");
	}
	
	void OnHover(bool isOver)
	{
	}
}
