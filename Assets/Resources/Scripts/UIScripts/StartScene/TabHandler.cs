﻿using UnityEngine;
using System.Collections;

public class TabHandler : MonoBehaviour {

	[SerializeField] GameObject m_usernameGO;
	[SerializeField] GameObject m_passwordGO;
	[SerializeField] GameObject m_loginButton;
	[SerializeField] GameObject m_forgetPassword;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyDown(KeyCode.Tab))
		{
			GrabFocus();
		}

		if(Input.GetKeyDown(KeyCode.Return))
		{
			DoLogin();
		}
	}

	void DoLogin()
	{
		if (UICamera.selectedObject == m_usernameGO || UICamera.selectedObject == m_usernameGO || UICamera.selectedObject == m_loginButton)
		{
			m_loginButton.GetComponent<LoginHandler>().DoLogin();
		}
	}

	void GrabFocus()
	{
		// If no selection
		if (UICamera.selectedObject == m_usernameGO)
		{
			UIInput passwordField = m_passwordGO.GetComponent<UIInput>();
			
			//avoid showing the caret
			string previousInput = passwordField.text;
			passwordField.defaultText = "";
			passwordField.selected = true;
			passwordField.text = "";

		}
		else if(UICamera.selectedObject == m_passwordGO)
		{
			UICamera.selectedObject = m_loginButton;
		}
		else if(UICamera.selectedObject == m_loginButton)
		{
			UICamera.selectedObject = m_forgetPassword;
		}
		else if(UICamera.selectedObject == m_forgetPassword)
		{
			UIInput usernameField = m_usernameGO.GetComponent<UIInput>();
			
			//avoid showing the caret
			string previousInput = usernameField.text;
			usernameField.selected = true;
			usernameField.text = previousInput;
		}
	}
}
