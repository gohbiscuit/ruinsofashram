﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LoginHandler : Photon.MonoBehaviour {
	
	// Visit NGUI link for more events overriding
	// http://www.tasharen.com/?page_id=160 for more information
	
	private readonly string DB_LOGIN_URL = "http://ashram.net78.net/ashram/register/do-login.php"; 
	
	// TextFields
	[SerializeField] UILabel m_username;
	[SerializeField] UILabel m_password;
	
	// Message Labels
	[SerializeField] GameObject m_overlay1;
	[SerializeField] GameObject m_overlay2;
	[SerializeField] GameObject m_message;
	
	// Use this for initialization
	void Start ()
	{
		// hide message
		ToggleMessageDisplay(false);
	}
	
	void OnClick()
	{
		DoLogin();
	}

	
	void ToggleMessageDisplay(bool isVisible)
	{
		m_overlay1.SetActive(isVisible);
		m_overlay2.SetActive(isVisible);
		m_message.SetActive (isVisible);
	}

	public void DoLogin()
	{
		StartCoroutine( DoLogin(m_username.text, m_password.text) );
	}

	void OnJoinedLobby()
	{
		Debug.Log ("Connected to Lobby (OnJoinedLobby)");
	}
	
	IEnumerator DoLogin(string username, string password) 
	{
		string hash = "";
		string hashed_password = Utility.CalculateMD5Hash(password);
		
		ToggleMessageDisplay(true);
		m_message.GetComponent<UILabel>().color = Color.green;
		m_message.GetComponent<UILabel>().text = "AUTHENTICATING TO NETWORK...";
		
    	WWWForm form = new WWWForm(); //here you create a new form connection
    	//form.AddField( "myform_hash", hash ); //add your hash code to the field myform_hash, check that this variable name is the same as in PHP file
    	form.AddField( "username", username );
    	form.AddField( "password", hashed_password.ToLower() );

		WWW loginResult = new WWW(DB_LOGIN_URL, form); //here we create a var called 'loginResult' and we sync with our URL and the form
		yield return loginResult; //we wait for the form to check the PHP file, so our game dont just hang
		
		string resultMessage = "";
		
    	if ( !string.IsNullOrEmpty(loginResult.error) )
		{
			m_message.GetComponent<UILabel>().color = Color.red;
			resultMessage = "NETWORK ERROR OCCURED.\nCHECK YOUR INTERNET CONNECTION.";
        	Debug.LogError("Network Error: "+loginResult.error); //if there is an error, tell us
    	}
		else  // Either successfully login or user pw does not match
		{	
        	string php_result_text = loginResult.data; //here we return the data our PHP told us
        	loginResult.Dispose(); 				//clear our form in game
			
			Debug.Log ("Result Text from PHP: "+php_result_text);
			
			//string message = "success<br><json>{\"username\":\"gohbiscuit\",\"email\":\"edwin_goh88@hotmail.com\",\"win\":\"0\",\"lose\":\"0\",\"win_rate\":\"0\",\"exp\":\"0\"}</json>";
			/*Debug.Log ("username: "+TextUtil.GetJSONValueForKey(user_profile_dict, "username"));
			Debug.Log ("email: "+TextUtil.GetJSONValueForKey(user_profile_dict, "email"));
			Debug.Log ("win: "+TextUtil.GetJSONValueForKey(user_profile_dict, "win"));
			Debug.Log ("lose: "+TextUtil.GetJSONValueForKey(user_profile_dict, "lose"));
			Debug.Log ("win_rate: "+TextUtil.GetJSONValueForKey(user_profile_dict, "win_rate"));
			Debug.Log ("exp: "+TextUtil.GetJSONValueForKey(user_profile_dict, "exp"));*/
			
			if(php_result_text.Contains ("success"))
			{
				m_message.GetComponent<UILabel>().color = Color.green;
				resultMessage = "LOGIN SUCCESS." + "\n" +"CONNECTING TO SERVER.";
				
				// Extract the content in between <json> and </json> tags
				string json_result = TextUtil.SeperateJsonString(php_result_text, "<json.*>", "</json>");
				Dictionary<string,object> user_profile_dict = TextUtil.DecodeJSON(json_result);
				
				Player.UpdateUserProfile
				(
					TextUtil.GetJSONValueForKey(user_profile_dict, "username"),
					TextUtil.GetJSONValueForKey(user_profile_dict, "email"),
					TextUtil.GetJSONValueForKey(user_profile_dict, "win"),
					TextUtil.GetJSONValueForKey(user_profile_dict, "lose"),
					TextUtil.GetJSONValueForKey(user_profile_dict, "win_rate"),
					TextUtil.GetJSONValueForKey(user_profile_dict, "exp")
				);

				//SceneManager.Instance.LoadLevel(SceneType.Lobby);
				JoinMainChannelRoom();
			}
			else
			{
				m_message.GetComponent<UILabel>().color = Color.red;
				resultMessage = "LOGIN FAILED." + "\n" + "INCORRECT CREDENTIALS.";
			}
    	}
		
		m_message.GetComponent<UILabel>().text = resultMessage;
	}

	void OnJoinedRoom()
	{
		if(PhotonNetwork.room.name == TextUtil.ASHRAM_MAIN_CHANNEL)
		{
			//If successfully join main channel
			PhotonNetwork.LoadLevel("Lobby");
		}
	}

	/// <summary>
	/// Raises the photon join room failed event. When room does not exist or room is full
	/// </summary>
	void OnPhotonJoinRoomFailed()
	{
		// LobbyRoom does not exist
		if(PhotonNetwork.room == null)
		{
			PhotonNetwork.CreateRoom(TextUtil.ASHRAM_MAIN_CHANNEL, true, true, 255, null, null);
		}
	}

	public void JoinMainChannelRoom()
	{
		if(PhotonNetwork.room == null)
		{
			PhotonNetwork.JoinRoom(TextUtil.ASHRAM_MAIN_CHANNEL);
		}
	}
}
