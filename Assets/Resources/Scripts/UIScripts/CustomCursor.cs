﻿//----------------------------------------------
// Custom Cursor: Customize cursor based on NGUI objects
// Writted by: Joreldraw
//----------------------------------------------


// Use:  Add to your main or simple attach to a Empty child.
using UnityEngine;
using System.Collections;

public enum CURSOR_STATE {TARGET_ENEMY, DEFAULT, USE_SKILL_POSSESSRELEASE, USE_SKILL_1, USE_SKILL_2, USE_SKILL_3, TARGET_ALLY, CANNOT_ATTACK, HOVER_ENEMY};

public class CustomCursor : MonoBehaviour {
	private Texture2D cursorImage;
	[SerializeField] Texture2D DefaultCursor;
	[SerializeField] Texture2D HoverEnemyCursor;	
	[SerializeField] Texture2D UseSkillCursor;		// When user uses a skill on enemy (target)
	[SerializeField] Texture2D TargetCursor;		// When user press A / basic attack

	private GameObject CursorSelection;
	private bool Dragging = false;

	public CURSOR_STATE CurrentCursor;
	
	void Awake() 
	{
		Screen.showCursor = false;
		cursorImage = DefaultCursor;
		CurrentCursor = CURSOR_STATE.DEFAULT;
	}
	
	void OnGUI() 
	{
		if(cursorImage != null)
		{
			switch(CurrentCursor)
			{
				case CURSOR_STATE.DEFAULT:
					cursorImage = DefaultCursor;
					break;
					
				case CURSOR_STATE.TARGET_ALLY:
					break;
					
				case CURSOR_STATE.TARGET_ENEMY:
					cursorImage = TargetCursor;
					break;
					
				case CURSOR_STATE.HOVER_ENEMY:
					cursorImage = HoverEnemyCursor;
					break;
					
				case CURSOR_STATE.USE_SKILL_POSSESSRELEASE:
					cursorImage = UseSkillCursor;
					break;
					
				case CURSOR_STATE.USE_SKILL_1:
					cursorImage = UseSkillCursor;
					break;
					
				case CURSOR_STATE.USE_SKILL_2:
					cursorImage = UseSkillCursor;
					break;
					
				case CURSOR_STATE.USE_SKILL_3:
					cursorImage = UseSkillCursor;
					break;
			}

			Vector3 mousePos = Input.mousePosition;
			Rect pos = new Rect (mousePos.x,Screen.height - mousePos.y,cursorImage.width,cursorImage.height);
			GUI.Label(pos, cursorImage);
		}
	}

	public void SetToDefault()
	{
		CurrentCursor = CURSOR_STATE.DEFAULT;
	}

	public void SetToAttack()
	{
		CurrentCursor = CURSOR_STATE.TARGET_ENEMY;
	}

	public void SetToHoverEnemy()
	{
		CurrentCursor = CURSOR_STATE.HOVER_ENEMY;
	}

	public void SetToUseSkill_1()
	{
		CurrentCursor = CURSOR_STATE.USE_SKILL_1;
	}

	public void SetToUseSkill_2()
	{
		CurrentCursor = CURSOR_STATE.USE_SKILL_2;
	}

	public void SetToUseSkill_3()
	{
		CurrentCursor = CURSOR_STATE.USE_SKILL_3;
	}

	public void SetToUseSkill_PossessRelease()
	{
		CurrentCursor = CURSOR_STATE.USE_SKILL_POSSESSRELEASE;
	}

	public void SetToCannotAttack()
	{
		CurrentCursor = CURSOR_STATE.CANNOT_ATTACK;
	}
	
}