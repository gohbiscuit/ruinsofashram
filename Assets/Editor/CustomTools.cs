﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;

static public class CustomTools
{
	[MenuItem("CustomTools/Convert .csv to .txt")]
	static public void ChangeCSVToTXT ()
	{
		ChangeAllFileExtensions(false);
	}

	[MenuItem("CustomTools/Convert .txt to .csv")]
	static public void ChangeSeletedTXTToCSV ()
	{
		ChangeAllFileExtensions(true);
	}

	
	[MenuItem("CustomTools/Convert selected .csv to .txt")]
	static public void ChangeSelectedCSVToTXT ()
	{
		ChangeSelectFileExtension(false);
	}

	[MenuItem("CustomTools/Convert selected .txt to .csv")]
	static public void ChangeTXTToCSV ()
	{
		ChangeSelectFileExtension(true);
	}

	static void ChangeSelectFileExtension(bool fromTxtToCsv)
	{
		foreach(Object obj in Selection.GetFiltered(typeof(Object),SelectionMode.Assets))
		{
			string path = AssetDatabase.GetAssetPath(obj);
			var fullPath = path.Replace("Assets", Application.dataPath);
			ChangeFileExtension(fullPath, fromTxtToCsv);
		}
	}

	static void ChangeFileExtension(string file, bool fromTxtToCsv)
	{
		
		string sourceExtension = fromTxtToCsv ? ".txt" : ".csv";
		string targetExtension = fromTxtToCsv ? ".csv" : ".txt";

		if(file.Contains(sourceExtension))
		{
			string content 	= File.ReadAllText(file);
			var newFile 	= file.Replace(sourceExtension, targetExtension);
			File.WriteAllText(newFile, content);

			var relativeFile = file.Replace(Application.dataPath, "Assets");

			if(AssetDatabase.DeleteAsset(relativeFile))
			{
				Debug.LogWarning("Converted: "+file);
			}

			AssetDatabase.Refresh();
		}
	}

	static void ChangeAllFileExtensions(bool fromTxtToCsv)
	{
		var files = Directory.GetFiles(Application.dataPath + "/" + "Resources");
		foreach(var file in files)
		{
			if(file.Contains(".meta"))
			{
				continue;
			}
			ChangeFileExtension(file, fromTxtToCsv);
		}  
	}

}
